#!/usr/bin/env bash

echo "Installing pre-requisites, assuming Ubuntu 24.04 LTS..."
wget https://github.com/Kitware/CMake/releases/download/v3.31.6/cmake-3.31.6-linux-x86_64.sh
chmod +x cmake-3.31.6-linux-x86_64.sh
./cmake-3.31.6-linux-x86_64.sh
export PATH=$PATH:~/cmake-3.31.6-linux-x86_64/bin
sudo apt update -y
sudo apt install ninja-build clang libstdc++-10-dev clang-tools -y
mkdir build