#/usr/bin/env bash

APPDIR=../build/Debug/

(cd $APPDIR && ./pmoserver &)
echo "Server PID:" `pidof pmoserver`
sleep 1
(cd $APPDIR && ./pmoclient -l client1.log &)
sleep 1
(cd $APPDIR && ./pmoclient -l client2.log &)

sleep 4
killall -9 pmoclient
kill -9 `pidof pmoserver`