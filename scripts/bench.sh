#/usr/bin/env bash

APPDIR=../build/Release/

(cd $APPDIR && ./pmoserver &)
(cd $APPDIR && ./loadclient -c 50 -l client.log &)

echo "Server PID:" `pidof pmoserver`
perf record -F 99 -p `pidof pmoserver` -g -- sleep 30

kill -9 `pidof loadclient`
kill -9 `pidof pmoserver`