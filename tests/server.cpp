#ifdef IS_SERVER
#include <catch2/catch_all.hpp>
#include <algorithm>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "src/schemas/messages.h"
#include "src/serialization/simple.h"
#include "src/net/server.h"
#include "src/net/client.h"
#include "src/net/socket.h"
#include "src/crypto/crypto.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

using namespace std::chrono_literals;

TEST_CASE( "Start and Stop server thread", "[server]" ) 
{
    auto InQueue = std::make_shared<net::PacketQueue>();
    auto OutQueue = std::make_shared<net::PacketQueue>();
    logging::SpdLogger Logger("server_start", TEST_ASSET_DIR "logs/server_start_stop.log");
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);

    auto Socket = std::make_shared<net::Socket>("127.0.0.1", 5999, Logger);
    net::Server UDPServer(InQueue, OutQueue, Crypt, Socket, Logger);
    UDPServer.Start();
    // let thread start
    std::this_thread::sleep_for(100ms);
    UDPServer.Stop();
}


TEST_CASE( "Client Server Communications", "[server]" ) 
{
    auto Port = 6999;
    auto ServerInQueue = std::make_shared<net::PacketQueue>();
    auto ServerOutQueue = std::make_shared<net::PacketQueue>();
    
    logging::SpdLogger Logger("server_client_communications", TEST_ASSET_DIR "logs/server_client_communications.log");
    auto ServerCrypto = std::make_shared<crypto::Cryptor>(Logger);
    auto ServerSocket = std::make_shared<net::Socket>("127.0.0.1", Port, Logger);
    net::Server UDPServer(ServerInQueue, ServerOutQueue, ServerCrypto, ServerSocket, Logger);
    UDPServer.Start();
    // let server start
    std::this_thread::sleep_for(100ms);

    auto ClientInQueue = std::make_shared<net::PacketQueue>();
    auto ClientOutQueue = std::make_shared<net::PacketQueue>();
    auto ClientCrypto = std::make_shared<crypto::Cryptor>(Logger);
    auto ClientSocket = std::make_shared<net::Socket>("127.0.0.1", Port, Logger);
    auto Client = std::make_shared<net::Client>(ClientInQueue, ClientOutQueue, ClientCrypto, ClientSocket, Logger);

    Client->SetUserId(0xd34db33f);
    Client->Start();
    // let client start
    std::this_thread::sleep_for(100ms);

    Client->Authenticate();
    std::this_thread::sleep_for(100ms);

    auto ServerUser = UDPServer.GetEndpointChannel(Client->GetUserID(), Reliability::Unreliable);
    REQUIRE( ServerUser != nullptr );
    auto ServerKey = ServerUser->UserKey();
    auto ResponsePacket = ServerInQueue->Pop();
    REQUIRE( ResponsePacket != std::nullopt );
    REQUIRE( ResponsePacket->get()->InternalType == Game::Message::NewUser);
    std::this_thread::sleep_for(100ms);
    auto ClientKey = Client->GetUserKey();

    REQUIRE( std::ranges::equal(*ServerKey, ClientKey) );

    Game::Message::ReleaseGameMessage(std::move(ResponsePacket.value()));
    Client->Stop();
    UDPServer.Stop();
}

TEST_CASE( "Test FakeAuthServer", "[server]")
{
    uint32_t UserId = 0xd34db33f;
    logging::SpdLogger Logger("server_fakeauth", TEST_ASSET_DIR "logs/server_fakeauth.log");
    auto Crypt = crypto::Cryptor(Logger);
    
    crypto::KeyPair KeyPair;
    auto Ret = Crypt.GenerateKeyEncryptingKey(KeyPair);
    REQUIRE(Ret == true);

    unsigned long long OutputLen{0};

    auto Result = crypto::FakeAuthServer(UserId, KeyPair.PublicKey.data(), OutputLen);
    REQUIRE( Result != nullptr);
  
    auto SignedMessageLen = sizeof(uint8_t)+sizeof(uint32_t)+sizeof(std::time_t)+crypto_sign_ed25519_PUBLICKEYBYTES+crypto_sign_BYTES;
    REQUIRE(SignedMessageLen == OutputLen);
    
    REQUIRE(Crypt.VerifyUser(Result->data(), OutputLen) == true);
}
#endif
