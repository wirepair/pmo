#include <catch2/catch_all.hpp>

#include "ecs/network/network.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "Client State Logic", "[client_state]" )
{
    logging::SpdLogger Logger("client_state", TEST_ASSET_DIR "logs/client_state.log");
    network::ServerStateBuffer Buff{};
    
    for (uint8_t i = 0; i < 48; i++)
    {
        for (uint8_t j = 0; j < 48; j++)
        {
            Buff.CalculateSequenceDistance(j % 24);
            Buff.State.SequenceId = i % 24;
            Logger.Info("Serv Seq: {} Client: {}, Dist: {}", Buff.State.SequenceId, j%24, Buff.SequenceDistance);
        }
    }
}