#ifndef IS_SERVER
#include <catch2/catch_all.hpp>

#include "ecs/init.h"

#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "Test Flecs Stuff", "[flecs]")
{
    logging::SpdLogger Logger("flecs", TEST_ASSET_DIR "logs/flecstuff.log");
    flecs::world GameWorld;
    flecs::entity MapChunk1 = GameWorld.entity();
    flecs::entity MapChunk2 = GameWorld.entity();
    flecs::entity Player = GameWorld.entity();

    Player.child_of(MapChunk1);
    Player.child_of(MapChunk2);
    auto Dat = GameWorld.to_json();
    Logger.Info("{}", Dat.c_str());
}

struct SomeComp {
    int X;
};

TEST_CASE( "Test Flecs By Id", "[flecs]")
{
    //logging::SpdLogger Logger("flecs", TEST_ASSET_DIR "logs/flecsbyid.log");
    flecs::world GameWorld;
    GameWorld.set_entity_range(5000, 0);

    auto Player = GameWorld.make_alive(5001);
    Player.set<SomeComp>({5});
    REQUIRE(Player.is_alive());
    
    auto SamePlayer = GameWorld.entity(5001);
    REQUIRE(SamePlayer.is_alive());
    auto Val = SamePlayer.get<SomeComp>();
    REQUIRE(Val->X == 5);

    auto OtherPlayer = GameWorld.entity(5002);
    REQUIRE(OtherPlayer.is_valid() == false);
    //auto Val2 = OtherPlayer.get<SomeComp>();
    // REQUIRE(Val2 == nullptr);
}

#endif