#include <catch2/catch_all.hpp>
#include "../src/serialization/simple.h"

TEST_CASE( "Check uint64 to char, char to uint64", "[serialization]" ) 
{
    std::vector<uint64_t> UserIds{0xd34db33fc4f3b4b3, 0x0, 0xd34db33f, 0xFFFFFFFFFFFFFFFF, 0xf10432f02345a};
    for (const auto UserId : UserIds)
    {
        unsigned char Result[sizeof(uint64_t)];
        serialization::UInt64tToUChar(UserId, Result);
        
        uint64_t ResultantUserId = serialization::UCharToUInt64t(Result);
        REQUIRE( UserId == ResultantUserId );
    }
}

TEST_CASE( "Check uint32 to char, char to uint32", "[serialization]" ) 
{
    std::vector<uint32_t> UserIds{0xd34db33f, 0x0, 0xd34db33f, 0xFFFFFFFF, 0x432f023};
    for (const auto UserId : UserIds)
    {
        unsigned char Result[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, Result);
        uint64_t ResultantUserId = serialization::UCharToUint32t(Result);
        REQUIRE( UserId == ResultantUserId );
    }
}
