#include <catch2/catch_all.hpp>
#include <algorithm>
#include <cstring>
#include <span>

#include "../src/schemas/messages.h"
#include "../src/containers/historybuffer.h"
#include "../src/input/input.h"
#include "../src/crypto/crypto.h"
#include "../src/serialization/simple.h"
#include "ecs/network/network.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

using namespace Game::Message;

void f(std::span<unsigned char> &BuffSlice)
{
    
   REQUIRE( std::memcmp(BuffSlice.data(), "op", 2) == 0);
   //REQUIRE( BuffSlice.at(1) == 'p');
}

TEST_CASE( "Test Span", "[schemas]")
{
    std::array<unsigned char, 64> Buffer{0};
    std::memcpy(Buffer.data(), "boop", 5);
    REQUIRE( Buffer.at(0) == 'b');
    auto BufferOffset = Buffer.data()+2;
    std::span<unsigned char> BuffSlice{BufferOffset, 2};
    
    f(BuffSlice);
}

TEST_CASE("FlatBuffers", "[flatbuffers]")
{
    flatbuffers::FlatBufferBuilder FBBuilder(1024);
    flatbuffers::FlatBufferBuilder PKBuilder(256);

    std::vector<unsigned char> Bytez{1,2,3,4};
    std::vector<uint8_t> Nonce{4,5,6,7};
    uint32_t UserId = 0xd34db33f;
    
    auto PubKey = Game::Message::CreateSignKeyRequest(FBBuilder, UserId, 0, 32, FBBuilder.CreateVector(Bytez));
    

    // Build the Reliable Message Header and apply our encrypted message type
    auto NonceVector = FBBuilder.CreateVector(Nonce);
    Game::Message::MessageBuilder GameMessage(FBBuilder);
    GameMessage.add_user_id(UserId);
    GameMessage.add_channel_id(0);
    GameMessage.add_nonce(NonceVector);
    GameMessage.add_message_data(PubKey.Union());
    GameMessage.add_message_data_type(Game::Message::MessageData_SignKeyRequest);

    auto FinishedGame = GameMessage.Finish();
    FBBuilder.Finish(FinishedGame);

    auto PubKeyBytez = PKBuilder.CreateVector(Bytez);
    Game::Message::SignKeyRequestBuilder PK(PKBuilder);
    PK.add_user_id(UserId);
    PK.add_time_stamp(0);
    PK.add_pubkey(PubKeyBytez);
    auto Offset = PK.Finish();
    PKBuilder.Finish(Offset);

    int PKSize = PKBuilder.GetSize();
    REQUIRE ( PKSize == 36 );

    
    uint8_t *Buf = FBBuilder.GetBufferPointer();
    int Size = FBBuilder.GetSize();
    REQUIRE( Size == 96 );

    auto Deserialized = Game::Message::GetMessage(Buf);
    REQUIRE(Deserialized->user_id() == UserId);
    auto Ret = std::equal(Nonce.begin(), Nonce.end(), Deserialized->nonce()->begin(), Deserialized->nonce()->end());
    REQUIRE( Ret == true );

}

TEST_CASE("Create, Finish, Serialize, Deserialize", "[flatbuffers]")
{
    flatbuffers::FlatBufferBuilder Builder(512);
    std::array<unsigned char, 32> PublicKey{1,2,3,4};
    std::array<unsigned char, 32> Signature{4,5,6,7};

    // Create nested first
    auto PubKeyValue = Builder.CreateVector(PublicKey.data(), 32);
    auto SignatureValue = Builder.CreateVector(Signature.data(), 32);
    auto GenKey = Game::Message::CreateGenKeyRequest(Builder, serialization::TimeNow(), 32, 32, PubKeyValue, SignatureValue);

    const uint32_t ExpectedUserId = 0xd34db33f;
    // Create Message
    Game::Message::MessageBuilder GameMessage(Builder);
    GameMessage.add_user_id(ExpectedUserId);
    GameMessage.add_channel_id(0);
    GameMessage.add_message_data_type(Game::Message::MessageData_GenKeyRequest);
    GameMessage.add_message_data(GenKey.Union());
    Builder.Finish(GameMessage.Finish());

    // Copy buffer
    auto Buffer = Builder.GetBufferPointer();
    const int OutputLen = Builder.GetSize();
    auto Output = std::make_unique<std::vector<unsigned char>>(OutputLen);
    std::memcpy(Output->data(), Buffer, OutputLen);

    // Release it
    Builder.Release();


    // Deserialize
    auto Deserialized = Game::Message::GetMessage(Output->data());
    auto UserId = Deserialized->user_id();
    REQUIRE (UserId == ExpectedUserId);

    REQUIRE(Deserialized->message_data_type() == Game::Message::MessageData_GenKeyRequest);
    auto PubKeyRequest = Deserialized->message_data_as_GenKeyRequest();
    REQUIRE (PubKeyRequest != nullptr );
    auto PK = PubKeyRequest->pubkey();
    
    REQUIRE ( std::equal(PK->begin(), PK->end(), PublicKey.begin()));
}


TEST_CASE("Test Encrypt/Decrypt", "[flatbuffers]")
{
    const uint32_t ExpectedUserId = 0xd34db33f;
    logging::SpdLogger Logger("schemas_encrypt_decrypt", TEST_ASSET_DIR "logs/schemas_encrypt_decrypt.log");
    auto Crypt = crypto::Cryptor(Logger);
    std::array<unsigned char, PMO_KEY_SIZE> Key1;
    Crypt.GenerateKey(Key1);

 
    input::Input KeyPress{};
    KeyPress.Press(input::Forward);
    auto Input = network::Inputs{KeyPress, 0, 0, 0};

    flatbuffers::FlatBufferBuilder OutBuilder(512);
    auto Inputs = CreateInputs(OutBuilder, Input.KeyPresses.KeyPresses, Input.PitchAngle, Input.YawAngle);
    ClientCommandBuilder CmdBuilder(OutBuilder);
    CmdBuilder.add_inputs(Inputs.o);
    CmdBuilder.add_opcode(ClientOpCode_Input);
    OutBuilder.Finish(CmdBuilder.Finish());

    std::vector<uint8_t> Output;
    Output.resize(1024);
    auto OutputLen = OutBuilder.GetSize();
    Output.resize(OutputLen);

    std::memcpy(Output.data(), OutBuilder.GetBufferPointer(), OutBuilder.GetSize());
    OutBuilder.Release();


    std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
    auto Encrypted = std::make_unique<std::vector<unsigned char>>();
    Encrypted->resize(OutputLen+Crypt.AuthByteSize());

    auto Success = Crypt.Encrypt(ExpectedUserId, Key1, Nonce, Output.data(), *Encrypted.get(), OutputLen);
    REQUIRE( Success == true );

    flatbuffers::FlatBufferBuilder Builder(1024);
    auto EncryptedBuffer = Builder.CreateVector(Encrypted->data(), Encrypted->size());
    auto NonceValue = Builder.CreateVector(Nonce.data(), PMO_NONCE_BYTES);
    auto EncryptedData = CreateEncClientCommand(Builder, EncryptedBuffer);

    // Build the Message Header and apply our encrypted message type
    Game::Message::MessageBuilder GameMessage(Builder);
    GameMessage.add_user_id(ExpectedUserId);
    GameMessage.add_channel_id(0);
    GameMessage.add_nonce(NonceValue);
    GameMessage.add_message_data(EncryptedData.Union());
    GameMessage.add_message_data_type(Game::Message::MessageData_EncClientCommand);
    
    Builder.Finish(GameMessage.Finish());

    // Test deserialization data matches expected
    auto Deserialized = GetMessage(Builder.GetBufferPointer());
    auto UserId = Deserialized->user_id();
    REQUIRE(UserId == ExpectedUserId);

    auto DeserializedData = Deserialized->message_data_as_EncClientCommand();
    REQUIRE (DeserializedData != nullptr);

    REQUIRE(DeserializedData->encrypted() != nullptr);
    REQUIRE(DeserializedData->encrypted()->size() == 60);
    
    // Test decryption
    unsigned long long DecryptOutputLen{0};
    std::vector<unsigned char> Decrypted(1024);
 
    Success = Crypt.Decrypt(ExpectedUserId, Key1, Deserialized->nonce()->data(), 
        DeserializedData->encrypted()->data(), DeserializedData->encrypted()->size(), 
        Decrypted, DecryptOutputLen);
    REQUIRE(Success == true);

}
