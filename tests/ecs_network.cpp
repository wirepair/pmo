#include <catch2/catch_all.hpp>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "ecs/init.h"
#include "ecs/state.h"
#include "ecs/level.h"
#include "config/config.h"
#include "../src/schemas/messages.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"
#include "ecs/network/network.h"
#ifdef IS_SERVER
#include "ecs/network/network_server.h"


TEST_CASE( "Test Server Network Heartbeat", "[ecs_network]")
{

    config::Config Config{TEST_ASSET_DIR "logs/ecs_network_heartbeat.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4247, nullptr};
    logging::SpdLogger Logger("network_heartbeat", Config.GetLogFile());
    flecs::world GameWorld;
    GameWorld.import<network::Network>();
    GameWorld.import<network::NetworkServer>();

    GameWorld.import<physics::Physics>();
    auto Physics = GameWorld.get_mut<physics::Physics>();
    physics::Landscape Landscape{Physics->System, Config, Logger};
    GameWorld.get_mut<physics::Physics>()->InitializeWorld(&Logger, Config, GameWorld, Landscape);
    GameWorld.import<level::Level>();
    GameWorld.get_mut<level::Level>()->Init(&Logger, &Config, GameWorld, Physics, PMOCallerType::Server);
    GameWorld.set_target_fps(30);

    
    auto ServerInQueue = std::make_shared<net::PacketQueue>();
    auto ServerOutQueue = std::make_shared<net::PacketQueue>();
    auto ServerCrypto = std::make_shared<crypto::Cryptor>(Logger);
    auto ServerSocket = std::make_shared<net::Socket>("127.0.0.1", Config.GetServerPort(), Logger);
 
    auto Network = GameWorld.get_mut<network::NetworkServer>();
    Network->Init(&Logger, nullptr, ServerInQueue, ServerOutQueue);

    //REQUIRE(InitPMOServer(GameWorld, Logger, Config));
   // auto Network = GameWorld.get_mut<network::NetworkServer>();
    auto Heartbeat = std::chrono::milliseconds(100);
    auto Timeout = std::chrono::milliseconds(400);
    
    Network->SetHeartbeatTimeout(Heartbeat);
    Network->SetDisconnectedTimeout(Timeout);

    auto Start = std::chrono::system_clock::now();
    
    flecs::entity Player = GameWorld.entity()
        .set<network::User>({1})
        .set<network::PMOServer>({Network->Thread, Network->OutQueue})
        .set<network::Connected>({Start});

    REQUIRE( Player.is_alive() );

    // auto TimedOut = false;
    // GameWorld.observer<network::HeartbeatTimedOut>()
    //     .event(flecs::OnAdd)
    //     .each([&TimedOut](flecs::iter& It, size_t Index, network::HeartbeatTimedOut) {
    //         TimedOut = true;
    //     }
    // );

    
    // while(GameWorld.progress())
    // {
    //     auto Now = std::chrono::system_clock::now();
    //     auto Runtime = std::chrono::duration_cast<std::chrono::milliseconds>(Now-Start);
    //     if (Runtime > Heartbeat) 
    //     { 
    //         break;
    //     }
    // }

    // // We should have timed out, but not disconnected yet
    // REQUIRE( TimedOut == true );
    // REQUIRE( Player.is_alive() == true );

    // // Update our connected with a new 'packet' to remove our HeartbeatTimedOut component
    // Player.set<network::Connected>({std::chrono::system_clock::now()});
    // Logger.Info("Updated network::Connected");
    // GameWorld.progress();

    // // HeartbeatTimedOut should no longer be applied to the entity
    // REQUIRE( !Player.has<network::HeartbeatTimedOut>() );
    
    // // reset start time
    // Start = std::chrono::system_clock::now();
    // while(GameWorld.progress())
    // {
    //     auto Now = std::chrono::system_clock::now();
    //     auto Runtime = std::chrono::duration_cast<std::chrono::milliseconds>(Now-Start);
    //     if (Runtime > Timeout) 
    //     { 
    //         break;
    //     }
    // }
    // // player should be destructed
    // REQUIRE( Player.is_alive() == false );
    // bool RemoveUserCalled = false;
    // auto Size = Network->OutQueue->Size();
    // REQUIRE(Size > 0);
    // for (auto GameData = Network->OutQueue->Pop(); GameData != std::nullopt; GameData = Network->OutQueue->Pop())
    // {
    //     auto MsgType = GameData->get()->InternalType;
    //     switch (MsgType)
    //     {
    //         case Game::Message::InternalMessage::RemoveUser:
    //         {
    //             RemoveUserCalled = true;
    //             break;
    //         }
    //         default:
    //             break;
    //     }
    //     Game::Message::ReleaseGameMessage(std::move(GameData.value()));
    // }
    // REQUIRE(Network->OutQueue->Size() == 0);
    // REQUIRE(Network->InQueue->Size() == 0);
    // REQUIRE(RemoveUserCalled == true);
    Physics->ShutdownWorld();
    //DeinitPMO(GameWorld);
}

#endif