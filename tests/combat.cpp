#ifdef IS_SERVER
#include <catch2/catch_all.hpp>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "ecs/init_server.h"
#include "ecs/network/network_server.h"

#include "ecs/network/network.h"
#include "ecs/state.h"
#include "ecs/level.h"
#include "ecs/combat/combat.h"
#include "ecs/combat/damageclass.h"
#include "ecs/character/components.h"
#include "ecs/character/controllable.h"
#include "ecs/character/pmocharacter.h"
#include "input/input.h"
#include "../src/schemas/messages.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "Test Combat ApplyDamage", "[ecs_combat]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/combat_applydmg.log", TEST_ASSET_DIR "/maps", "Level1", "0.0.0.0", 4243,  nullptr);
    logging::SpdLogger Logger("ecs_applydamage", Config.GetLogFile());

    flecs::world GameWorld{};

    GameWorld.import<combat::Combat>();
    GameWorld.get_mut<combat::Combat>()->Init(&Logger, GameWorld, nullptr);

    auto Instigator = GameWorld.entity()
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({});
    
    auto Target = GameWorld.entity()
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({});

    GameWorld.entity().set<combat::ApplyDamage>({
        .Instigator=Instigator, 
        .TargetHit=Target, 
        .HitPosition=JPH::Vec3::sZero(), 
        .BaseDamage=100});

    GameWorld.progress();
    REQUIRE( Target.get<units::Attributes>()->Health == 400.f);

    Target.destruct();

    // Attack a target with a shield and verify it depletes
    auto Target2 = GameWorld.entity()
        .set<units::Attributes>({.Health=500.f, .Shield=50.f})
        .set<units::TraitAttributes>({});

    GameWorld.entity().set<combat::ApplyDamage>({
        .Instigator=Instigator, 
        .TargetHit=Target2,
        .HitPosition=JPH::Vec3::sZero(), 
        .BaseDamage=100, 
        .Type=combat::DamageClass::SLASH
    });

    GameWorld.progress();
    REQUIRE( Target2.get<units::Attributes>()->Health == 450.f);
    REQUIRE( Target2.get<units::Attributes>()->Shield == 0.f);

    Target2.destruct();

     // Attack a target with a shield and verify it doesn't deplete it fully and doesn't take health dmg
    auto Target3 = GameWorld.entity()
        .set<units::Attributes>({.Health=500.f, .Shield=50.f})
        .set<units::TraitAttributes>({});

    GameWorld.entity().set<combat::ApplyDamage>({
        .Instigator=Instigator, 
        .TargetHit=Target3, 
        .HitPosition=JPH::Vec3::sZero(), 
        .BaseDamage=25, 
        .Type=combat::DamageClass::SLASH
    });
    
    GameWorld.progress();
    REQUIRE( Target3.get<units::Attributes>()->Shield == 25.f);
    REQUIRE( Target3.get<units::Attributes>()->Health == 500.f);
    Target3.destruct();

    GameWorld.reset();
}

TEST_CASE( "Test Combat", "[ecs_combat]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/combat.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4242,  nullptr);
    logging::SpdLogger Logger("ecs_combat", Config.GetLogFile());
    
    flecs::world GameWorld{};

    REQUIRE( InitPMOServer(GameWorld, Logger, Config));
    auto Physics = GameWorld.get_mut<physics::Physics>();
    auto Pos1 = units::Vector3{0.f,0.f,0.f};
    flecs::entity Player1 = GameWorld.entity()
        .set<network::User>({0xd34db33f})
        .set<network::Connected>({})
        .set<units::Velocity>({})
        .set<units::Quat>({})
        .set<units::Vector3>(Pos1)
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({})
        .add(character::ActionType::NOOP)
        .add<network::Inputs>()
        .set<character::Visibility>({})
        .set<state::PlayerWorldStates>({});

    Player1.set<character::PMOCharacter>({Player1, std::make_unique<character::ControllableCharacter>(&Logger, Physics, Pos1)})
        .child_of<level::MapLevel>();
    
    auto Pos2 = units::Vector3{0.f,0.f,1.f};
    auto Player2 = GameWorld.entity()
        .set<network::User>({0xd34db333})
        .set<network::Connected>({})
        .set<units::Vector3>(Pos2)
        .set<units::Velocity>({})
        .set<units::Quat>({})
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({})
        .add(character::ActionType::NOOP)
        .add<network::Inputs>()
        .set<character::Visibility>({})
        .set<state::PlayerWorldStates>({});

    Player2.set<character::PMOCharacter>({Player2, std::make_unique<character::ControllableCharacter>(&Logger, Physics, Pos2)})
        .child_of<level::MapLevel>();

    const int MaxIterations = 11;
    int CurrentIteration = 0;
    while(GameWorld.progress() && CurrentIteration < MaxIterations) 
    {
        CurrentIteration++;

        if (CurrentIteration == 10)
        {
            auto AttackEntity = GameWorld.entity().set<combat::AttackDetails>({
                .Instigator=Player1, 
                .BaseDamage=100.f, 
                .RadiusOrLength=2.f, 
                .DelayStartTime=.0f, 
                .RunTime=.99f, 
                .NumTargets=1, 
                .AttackShape=combat::AttackContactShape::Sphere,
                .Type=combat::DamageClass::SLASH,
            });

            Logger.Info("AttackEntity: {} created", AttackEntity.id());
        }
    }

    DeinitPMO(GameWorld);
}

TEST_CASE( "Test Respawn", "[ecs_combat]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/combat_respawn.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4244,  nullptr);
    logging::SpdLogger Logger("ecs_combat_respawn", Config.GetLogFile());
    
    flecs::world GameWorld{};

    REQUIRE( InitPMOServer(GameWorld, Logger, Config));
    auto Physics = GameWorld.get_mut<physics::Physics>();
    auto Pos1 = units::Vector3{0.f,0.f,0.f};
    flecs::entity Player1 = GameWorld.entity()
        .set<network::User>({0xd34db33f})
        .set<network::Connected>({})
        .set<units::Velocity>({})
        .set<units::Quat>({})
        .set<units::Vector3>(Pos1)
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({})
        .add(character::ActionType::NOOP)
        .add<network::Inputs>()
        .set<character::Visibility>({})
        .set<state::PlayerWorldStates>({});

    Player1.set<character::PMOCharacter>({Player1, std::make_unique<character::ControllableCharacter>(&Logger, Physics, Pos1)})
        .child_of<level::MapLevel>();
    
    auto Pos2 = units::Vector3{0.f,0.f,1.f};
    auto Player2 = GameWorld.entity()
        .set<network::User>({0xd34db333})
        .set<network::Connected>({})
        .set<units::Vector3>(Pos2)
        .set<units::Velocity>({})
        .set<units::Quat>({})
        .set<units::Attributes>({})
        .set<units::TraitAttributes>({})
        .add(character::ActionType::NOOP)
        .add<network::Inputs>()
        .set<character::Visibility>({})
        .set<state::PlayerWorldStates>({});

    Player2.set<character::PMOCharacter>({Player2, std::make_unique<character::ControllableCharacter>(&Logger, Physics, Pos2)})
        .child_of<level::MapLevel>();

    const int MaxIterations = 25;
    int CurrentIteration = 0;
    auto Died = false;
    auto RespawnLocation = units::Vector3{10.f, 0.f, 10.f};
    while(GameWorld.progress() && CurrentIteration < MaxIterations) 
    {
        CurrentIteration++;

        if (CurrentIteration == 10)
        {
            auto AttackEntity = GameWorld.entity().set<combat::AttackDetails>({
                .Instigator=Player1, 
                .BaseDamage=1000.f, 
                .RadiusOrLength=2.f, 
                .DelayStartTime=.0f, 
                .RunTime=.99f, 
                .NumTargets=1, 
                .AttackShape=combat::AttackContactShape::Sphere,
                .Type=combat::DamageClass::SLASH,
            });

            Logger.Info("AttackEntity: {} created", AttackEntity.id());
        }
        // Mock sending a "RequestRespawn" from the client (handled in network_server.cpp)
        if (Player2.has<character::IsDead>())
        {
            Died = true;
            Player2.set<character::BindLocation>({RespawnLocation});
            Player2.add<character::Respawn>();
        }
    }
    auto RespawnedLocation = Player2.get<units::Vector3>();
    REQUIRE(Died == true);
    Logger.Info("Respawned: {} {} {}", RespawnedLocation->vec3[0], RespawnedLocation->vec3[1], RespawnedLocation->vec3[2]);

    // Skip Y axis check
    REQUIRE(RespawnedLocation->vec3[0] == RespawnLocation.vec3[0]);
    REQUIRE(RespawnedLocation->vec3[2] == RespawnLocation.vec3[2]);

    DeinitPMO(GameWorld);
}
#endif