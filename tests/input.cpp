#include <catch2/catch_all.hpp>
#include <algorithm>
#include <cstring>
#include <span>

#include "input/input.h"

#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE("Test Input KeyPresses", "[input]")
{
    logging::SpdLogger Logger("input_keypress",TEST_ASSET_DIR "logs/input_keypress.log");

    input::Input TestInputs;

    auto Ret = TestInputs.ProcessInputs(std::vector<input::InputType>{input::InputType::Forward, input::InputType::StrafeLeft});
    REQUIRE(Ret == false);

    Ret = TestInputs.IsPressed(input::InputType::Forward);
    REQUIRE(Ret == true);

    Ret = TestInputs.IsPressed(input::InputType::StrafeLeft);
    REQUIRE(Ret == true);

    Ret = TestInputs.IsPressed(input::InputType::StrafeRight);
    REQUIRE(Ret == false);

    // should match previous inputs
    Ret = TestInputs.ProcessInputs(std::vector<input::InputType>{input::InputType::Forward, input::InputType::StrafeLeft});
    REQUIRE(Ret == true);

    // no longer matches previous inputs
    Ret = TestInputs.ProcessInputs(std::vector<input::InputType>{input::InputType::Left});
    REQUIRE(Ret == false);

    Ret = TestInputs.IsPressed(input::InputType::StrafeLeft);
    REQUIRE(Ret == false);


    Ret = TestInputs.IsPressed(input::InputType::Left);
    REQUIRE(Ret == true);

    TestInputs.Clear();

    Ret = TestInputs.IsPressed(input::InputType::Left);
    REQUIRE(Ret == false);

    TestInputs.Clear();
    TestInputs.Press(input::InputType::XAxisMovement);
    
    Ret = TestInputs.IsPressed(input::InputType::XAxisMovement);
    REQUIRE(Ret == true);

   
    Ret = TestInputs.IsPressed(input::InputType::Left);
    REQUIRE(Ret == true);

    TestInputs.Unpress(input::InputType::XAxisMovement);

    Ret = TestInputs.IsPressed(input::InputType::Left);
    REQUIRE(Ret == false);

    Ret = TestInputs.IsPressed(input::InputType::XAxisMovement);
    REQUIRE(Ret == false);

    TestInputs.Clear();

    TestInputs.Unpress(input::InputType::XAxisMovement);
    Ret = TestInputs.IsPressed(input::InputType::Left);
    REQUIRE(Ret == false);

    Ret = TestInputs.IsPressed(input::InputType::XAxisMovement);
    REQUIRE(Ret == false);

}
