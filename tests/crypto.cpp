#include <catch2/catch_all.hpp>
#include <cstring>
#include <iostream>

#include "../src/crypto/cryptor.h"
#include "../src/serialization/simple.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "Tests Symmetrical Key generation", "[crypto]" ) 
{
    logging::SpdLogger Logger("crypto_symmetrical", TEST_ASSET_DIR "logs/crypto_symmetrical.log");
    auto Crypt = crypto::Cryptor(Logger);
    std::array<unsigned char, PMO_KEY_SIZE> Key1;
    Crypt.GenerateKey(Key1);

    std::array<unsigned char, PMO_KEY_SIZE> Key2;
    Crypt.GenerateKey(Key2);

    REQUIRE( std::memcmp(Key1.data(), Key2.data(), Crypt.KeySize()) != 0);
}

TEST_CASE( "Tests KeyPair Creation", "[crypto]" ) 
{
    logging::SpdLogger Logger("KeyPair", TEST_ASSET_DIR "logs/KeyPair.log");
    auto Crypto = crypto::Cryptor(Logger);
    crypto::KeyPair KeyEncryptingKeys;
    // ensure they are both full of 0
    Crypto.GenerateKeyEncryptingKey(KeyEncryptingKeys);
    REQUIRE( std::memcmp(KeyEncryptingKeys.PublicKey.data(), KeyEncryptingKeys.PrivateKey.data(), 32) != 0 );
}

TEST_CASE( "Tests Asymmetrical Key generation generates unique keys", "[crypto]" ) 
{
    logging::SpdLogger Logger("crypto_asymmetrical", TEST_ASSET_DIR "logs/crypto_asymmetrical.log");
    auto Crypt = crypto::Cryptor(Logger);
    
    crypto::KeyPair Key1;
    auto Ret = Crypt.GenerateKeyEncryptingKey(Key1);
    REQUIRE( Ret == true );

    crypto::KeyPair Key2;
    Ret = Crypt.GenerateKeyEncryptingKey(Key2);
    REQUIRE( Ret == true );
    REQUIRE( std::memcmp(Key1.PrivateKey.data(), Key2.PrivateKey.data(), Crypt.KeySize()) != 0);
}

TEST_CASE( "Tests EncryptKey / DecryptKey is valid", "[crypto]" ) 
{
    logging::SpdLogger Logger("crypto_encrypt_decrypt", TEST_ASSET_DIR "logs/crypto_encrypt_decrypt.log");
    auto Crypt = crypto::Cryptor(Logger);

    std::array<unsigned char, PMO_KEY_SIZE> GeneratedKey;
    Crypt.GenerateKey(GeneratedKey);

    crypto::KeyPair UserKey;
    auto Ret = Crypt.GenerateKeyEncryptingKey(UserKey);
    REQUIRE (Ret == true);
    
    // Encrypt key with users public key and servers private Key
    std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
    std::vector<unsigned char> CipherText;
    CipherText.resize(Crypt.EncryptKeySize());
    
    auto Success = Crypt.EncryptKey(Nonce, UserKey.PublicKey.data(), GeneratedKey, CipherText);
    REQUIRE( Success == true );

    // // Decrypt with users private key and servers public key
    std::array<unsigned char, PMO_KEY_SIZE> DecryptedKey;
    Crypt.DecryptKey(Nonce.data(), CipherText.data(), Crypt.TestGameServerPubKey.data(), UserKey.PrivateKey.data(), DecryptedKey);
    REQUIRE( std::memcmp(DecryptedKey.data(), GeneratedKey.data(), 32) == 0);
}

TEST_CASE( "Tests Crypto Encrypt Has AAD Visible", "[crypto]" ) 
{
    logging::SpdLogger Logger("crypto_aad", TEST_ASSET_DIR "logs/crypto_aad.log");
    auto Crypt = crypto::Cryptor(Logger);

    std::array<unsigned char, PMO_KEY_SIZE> UserKey;
    Crypt.GenerateKey(UserKey);
    uint32_t UserId = 0xd34db33f;

    auto MessageLen = 4;
    std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
    std::vector<unsigned char> CipherText(MessageLen + Crypt.AuthByteSize());
    auto Success = Crypt.Encrypt(UserId, UserKey, Nonce, (unsigned char*)"1234", CipherText, MessageLen);
    REQUIRE( Success == true );

    unsigned long long DecryptedLen{0};
    std::vector<unsigned char> PlainText(1024);
    Success = Crypt.Decrypt(UserId, UserKey, Nonce.data(), CipherText.data(), CipherText.size(), PlainText, DecryptedLen);

    REQUIRE( Success == true );
    REQUIRE( std::memcmp(PlainText.data(), "1234", 4) == 0);
}

TEST_CASE( "Tests Crypto Invalid AD (UserId) causes decryption failure", "[crypto]" ) 
{
    logging::SpdLogger Logger("crypto_invalid_ad", TEST_ASSET_DIR "logs/crypto_invalid_ad.log");
    auto Crypt = crypto::Cryptor(Logger);
    std::array<unsigned char, PMO_KEY_SIZE> UserKey;
    Crypt.GenerateKey(UserKey);
    uint32_t ValidUser = 0xd34db33f;

    // Create a malicous user to test that the UserId is actually validated in the AD check of decrypt
    std::array<unsigned char, PMO_KEY_SIZE> MaliciousUserKey;
    Crypt.GenerateKey(MaliciousUserKey);
    uint32_t MalicousUserId = 0xc4f3b4b3;

    auto MessageLen = 4;
    std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
    std::vector<unsigned char>CipherText(MessageLen+Crypt.AuthByteSize());
    auto Success = Crypt.Encrypt(ValidUser, UserKey, Nonce, (unsigned char*)"1234", CipherText, MessageLen);
    REQUIRE( Success == true );

    // Overwrite the original ciphertext AD userid with the malicious user one
    unsigned char MaliciousUserIdBuffer[sizeof(uint32_t)];
    serialization::UInt32tToUChar(MalicousUserId, MaliciousUserIdBuffer);
    std::memmove(CipherText.data(), MaliciousUserIdBuffer, sizeof(MaliciousUserIdBuffer));

    uint32_t AuthenticatedUserId{0};
    unsigned long long DecryptedLen{0};
    std::vector<unsigned char> PlainText(1024);
    Success = Crypt.Decrypt(AuthenticatedUserId, MaliciousUserKey, Nonce.data(), CipherText.data(), CipherText.size(), PlainText, DecryptedLen);
    REQUIRE( Success == false );
}