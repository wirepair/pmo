//#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_all.hpp>

TEST_CASE( "Test sizes", "[main]" ) 
{
    REQUIRE( sizeof(std::time_t) == 8);
    REQUIRE( sizeof(uint32_t) == 4);
    REQUIRE( sizeof(uint8_t) == 1);
}