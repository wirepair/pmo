#include <catch2/catch_all.hpp>
#include <Jolt/Jolt.h>
#include <flecs.h>
#include <chrono>
#include "ecs/init.h"
#include "ecs/animations/animation.h"
#include "ecs/animations/animdata.h"
#include "config/config.h"
#include "math/curves/curve.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"
#include "parsers/csv.hpp"

using namespace std::chrono_literals;
TEST_CASE( "Test Loading Animations", "[animations]")
{
    flecs::world GameWorld;
    GameWorld.set_target_fps(30);

    auto Config = config::Config(TEST_ASSET_DIR "logs/animations.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4242,  nullptr);
    logging::SpdLogger Logger("animations", Config.GetLogFile());
    GameWorld.import<animations::Animation>();
    auto Anim = GameWorld.get_mut<animations::Animation>();

    REQUIRE(Anim->Init(&Logger, Config));
    // create a query where we can search our prefab of anims
    auto AnimQuery = GameWorld.query_builder<animations::AnimData>().with(flecs::Prefab).build();
    auto ExpectedCount = 6;
    int AnimCount = 0;
    // make sure all prefabs are loaded
    AnimQuery.each([&](flecs::entity e, animations::AnimData &Anim) 
    {
        REQUIRE(Anim.Path != "");
        AnimCount++;
    });
    REQUIRE(ExpectedCount == AnimCount);
    // get a ref to attack seq 3
    auto AttackSeq3 = AnimQuery.find([&](animations::AnimData &Anim)
    {
        return Anim.Path == "SSH/UE5_SSH_Attack_03_Seq.csv";
    });
    REQUIRE(AttackSeq3.is_valid());

    // create an instance of it
    flecs::entity Player = GameWorld.entity();
    Player.is_a(AttackSeq3).add<animations::RunTime>();
    REQUIRE(Player.has<animations::AnimData>());

    // make sure the player has the anim data applied
    bool PlayerHasAnimData = false;
    GameWorld.each([&](flecs::entity Entity, animations::AnimData &Anim)
    {
        Logger.Info("{} has anim data seq: {}", Entity.id(), Anim.Path);
        REQUIRE(Anim.Path == "SSH/UE5_SSH_Attack_03_Seq.csv");
        PlayerHasAnimData = true;
    });
    REQUIRE(PlayerHasAnimData == true);

    auto AnimRunTime = Player.get_mut<animations::RunTime>();
    REQUIRE(AnimRunTime != nullptr);
    auto PlayerAnim = Player.get<animations::AnimData>();
    REQUIRE(PlayerAnim != nullptr);
    while(GameWorld.progress())
    {
        auto DeltaTime = GameWorld.delta_time();
        auto PreviousDelta = JPH::Clamp<float>(AnimRunTime->Time-DeltaTime, 0, 999);
        if (!Player.has<animations::AnimData>())
        {
            REQUIRE(!Player.has<animations::RunTime>());
            GameWorld.quit();
            break;
        }
        auto Value = PlayerAnim->AnimCurve.Eval(AnimRunTime->Time);
        auto PreviousValue = PlayerAnim->AnimCurve.Eval(PreviousDelta);
        auto Distance = Value.Length() - PreviousValue.Length();
        auto Velocity = Distance / DeltaTime;
        Logger.Info("AnimTotalRunTime: {} Velocity (cm/s): {}, Velocity (m/s): {} Distance: {}, Value: {}, PreviousValue: {}, DeltaTime: {}, RunTime: {}",
            PlayerAnim->AnimCurve.RunTime(), Velocity, Velocity/100.f, Distance, Value.Length(), PreviousValue.Length(), DeltaTime, AnimRunTime->Time);
        AnimRunTime->Time += DeltaTime;
        if (AnimRunTime->Time >= PlayerAnim->AnimCurve.RunTime())
        {
            auto Prefab = GameWorld.lookup(PlayerAnim->Path.c_str());
            Player
                .remove(flecs::IsA, Prefab)
                .remove<animations::MovementRotationDirection>()
                .remove<animations::RunTime>();
        }
    }
}
