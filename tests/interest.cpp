#include <catch2/catch_all.hpp>

#include "ecs/init.h"
#include "ecs/level.h"
#include "ecs/character/components.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "Interest Management Logic", "[AoI]" )
{
    logging::SpdLogger Logger("AoI", TEST_ASSET_DIR "logs/aoi.log");
    flecs::world GameWorld;
    GameWorld.import<level::Level>();
    GameWorld.get_mut<level::Level>()->Init(&Logger, nullptr, GameWorld, nullptr, PMOCallerType::Server);
    for (int i = 0; i < 3; i++)
    {
        JPH::BodyID X(i);
        // Create map chunk
        GameWorld.entity()
            .set<level::MapChunk>({X})
            .set<level::LocalSensor>({X})
            .set<level::MultiSensor>({X});
    }

    // make 11 players
    std::vector<flecs::entity> Players{};
    Players.reserve(6);
    for (int i = 0; i < 6; i++)
    {
        auto Player = GameWorld.entity().set<character::Visibility>({});
        Logger.Info("Player {} created", Player.id());
        Players.emplace_back(Player);
    }

    auto q = GameWorld.query<level::LocalSensor, level::MultiSensor>();
    q.each([&](flecs::iter& It, size_t Index, level::LocalSensor &Local, level::MultiSensor &Multi)
    {
        Logger.Info("Inserting players into sensors {}", Index);
        switch(Index)
        {
            case 0:
                Local.Characters.insert(Players.at(0));
                Local.Characters.insert(Players.at(1));

                Multi.Characters.insert(Players.at(0));
                Multi.Characters.insert(Players.at(1));
                Multi.Characters.insert(Players.at(2));

            break;
            case 1:
                Local.Characters.insert(Players.at(3));

                Multi.Characters.insert(Players.at(3));
                Multi.Characters.insert(Players.at(4));
                Multi.Characters.insert(Players.at(5));
            break;
            case 2:
                Local.Characters.insert(Players.at(2));
                Local.Characters.insert(Players.at(4));
                Local.Characters.insert(Players.at(5));

                Multi.Characters.insert(Players.at(0));
                Multi.Characters.insert(Players.at(1));
                Multi.Characters.insert(Players.at(2));
                Multi.Characters.insert(Players.at(4));
            break;
        }
    });
    // SetupPlayerVisibility flecs system should have setup our characters visibility sets
    GameWorld.progress();


    auto Player0Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        // Case 0 Local (& Multi):
        Players.at(0), Players.at(1), 
        // Case 0 Multi
        Players.at(2), 
        // Case 2 Multi
        Players.at(4)
    };

    REQUIRE(Player0Expected == Players.at(0).get<character::Visibility>()->Neighbors);

    auto Player1Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        // Case 0 Local (& Multi):
        Players.at(0), Players.at(1),
        // Case 2 Multi:
        Players.at(2), Players.at(4)
    };

    REQUIRE(Player1Expected == Players.at(1).get<character::Visibility>()->Neighbors);

    auto Player2Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        // Case 2 Local
        Players.at(2), Players.at(4), Players.at(5),
        // Case 0 Multi
        Players.at(0), Players.at(1)
    };
    REQUIRE(Player2Expected == Players.at(2).get<character::Visibility>()->Neighbors);

    auto Player3Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        // Case 1 Multi
        Players.at(3), Players.at(4), Players.at(5),
    };
    REQUIRE(Player3Expected == Players.at(3).get<character::Visibility>()->Neighbors);

    auto Player4Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        // Case 2 Local (Case 1 Multi):
        Players.at(2), Players.at(4), Players.at(5),
        // Case 2 Multi:
        Players.at(0), Players.at(1)
    };
    REQUIRE(Player4Expected == Players.at(4).get<character::Visibility>()->Neighbors);

    auto Player5Expected = std::unordered_set<flecs::entity, units::FlecsEntityHash>{
        Players.at(2), Players.at(4), Players.at(5)
    };
    REQUIRE(Player5Expected == Players.at(5).get<character::Visibility>()->Neighbors);
}