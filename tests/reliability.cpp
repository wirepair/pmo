#include <catch2/catch_all.hpp>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "net/socket.h"
#include "net/listener.h"
#include "net/reliable_peer.h"
#include "net/reliable_endpoint.h"
#include "ecs/network/network.h"
#include "logger/spdlogger.h"
#include "input/input.h"
#include "containers/historybuffer.h"

using namespace Game::Message;

const uint32_t ValidUserId = 0xd34db33f;

struct MockSock : SocketSender {
    virtual size_t Send(const void *Buffer, const int Len) const noexcept override 
    {
        if (SendFn)
        {
            return SendFn(Buffer, Len);
        }
        return 0;
    };

    virtual size_t SendTo(struct sockaddr_in &ToAddr, const void *Buffer, const int Len) const noexcept override 
    {
        if (SendFn)
        {
            return SendFn(Buffer, Len);
        }
        return 0;
    }

    virtual ~MockSock() = default;

    std::function<size_t(const void *Buffer, const int Len)> SendFn = nullptr;
};

void BuildCommandOutput(const int BuilderSize, const network::Inputs &Inputs, std::vector<uint8_t> &Output, uint64_t &OutputLen)
{
    flatbuffers::FlatBufferBuilder OutBuilder(BuilderSize);
  
    auto FBInputs = CreateInputs(OutBuilder, Inputs.KeyPresses.KeyPresses, Inputs.PitchAngle, Inputs.YawAngle);
    ClientCommandBuilder CmdBuilder(OutBuilder);
    CmdBuilder.add_inputs(FBInputs.o);
    CmdBuilder.add_opcode(ClientOpCode_Input);
    OutBuilder.Finish(CmdBuilder.Finish());

    OutputLen = OutBuilder.GetSize();
    Output.resize(OutputLen);

    std::memcpy(Output.data(), OutBuilder.GetBufferPointer(), OutBuilder.GetSize());
    OutBuilder.Release();
    return;
}

TEST_CASE("Test Reliable Endpoint", "[reliability]")
{
    
    logging::SpdLogger Logger("reliable_endpoint", TEST_ASSET_DIR "logs/reliability.log");
    auto SenderSock = std::make_shared<MockSock>();
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);
    struct sockaddr_in Addr;
    auto Config = net::ReliableEndpointConfig();

    auto Endpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);

    net::ReliableMessage Message{};
    Message.Sequence = 10000;
    
    uint16_t WriteAck = 100;
    uint32_t WriteAckBits = 0x3;
    
    flatbuffers::FlatBufferBuilder Builder(1024);
    Game::Message::MessageBuilder GameMessage(Builder);
    GameMessage.add_user_id(0xd34db33f);
    GameMessage.add_channel_id(0);
    GameMessage.add_ack(WriteAck);
    GameMessage.add_ack_bits(WriteAckBits);
    Builder.Finish(GameMessage.Finish());
    auto Buffer = Builder.GetBufferPointer();
    const int OutputLen = Builder.GetSize();
    auto Output = std::make_unique<std::vector<unsigned char>>(OutputLen);
    std::memcpy(Output->data(), Buffer, OutputLen);

    // Release it
    Builder.Release();
    // Deserialize
    auto Deserialized = Game::Message::GetMessage(Output->data());

    uint16_t ReadAck = 0;
    uint32_t ReadAckBits = 0;
    Endpoint.ReadAck(*Deserialized, ReadAck, ReadAckBits);
    REQUIRE( ReadAck == WriteAck );
    REQUIRE( ReadAckBits == WriteAckBits );
}

TEST_CASE("Test Reliable Acks", "reliability")
{
    
    const int NumAckIterations = 256;
    logging::SpdLogger Logger("reliable_endpoint_acks", TEST_ASSET_DIR "logs/acks_reliability.log");

    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);

    auto Config = net::ReliableEndpointConfig();
    struct sockaddr_in Addr;

    auto SenderSock = std::make_shared<MockSock>();
    auto ReceiverSock = std::make_shared<MockSock>();
    
    auto SenderEndpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);

    ReceiverSock->SendFn = [&SenderEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        std::vector<unsigned char> Reassembled;
        SenderEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        return Len;
    };


    auto ReceiverEndpoint = net::ReliableEndpoint(0, ValidUserId, ReceiverSock, Addr, UserKey, Config, 0);

    SenderSock->SendFn = [&ReceiverEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        std::vector<unsigned char> Reassembled;
        ReceiverEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        return Len;
    };

    double Time = 100.0f;
    double DeltaTime = 0.01f;
    for (int i = 0; i < NumAckIterations; ++i)
    {
        auto Output = std::make_unique<std::vector<uint8_t>>();
        Output->resize(1024);
        uint64_t OutputLen{0};
        input::Input KeyPress{};
        KeyPress.Press(input::Forward);
        auto Input = network::Inputs{KeyPress, 0, 0, 0};
        BuildCommandOutput(net::DefaultConfig.FragmentSize, Input, *Output, OutputLen);
        Game::Message::GameMessage Msg(ValidUserId, std::move(Output), Game::Message::MessageData_EncClientCommand);

        SenderEndpoint.SendPacket(Logger, *Crypt, Msg);
        ReceiverEndpoint.SendPacket(Logger, *Crypt, Msg);

        SenderEndpoint.Update(Time);
        ReceiverEndpoint.Update(Time);
        Time += DeltaTime;
    }

    uint8_t SenderAckedPackets[256] = {0};
    int SenderNumAcks = 0;
    auto SenderAcks = SenderEndpoint.GetAcks(SenderNumAcks);
    for (auto SenderAck : SenderAcks)
    {
        SenderAckedPackets[SenderAck] = 1; 
    }

    for (int i = 0; i < NumAckIterations / 2; ++i)
    {
        REQUIRE(SenderAckedPackets[i] == 1);
    }

    uint8_t ReceiverAckedPackets[256] = {0};
    int ReceiverNumAcks = 0;
    auto ReceiverAcks = ReceiverEndpoint.GetAcks(ReceiverNumAcks);
    for (auto ReceiverAck : ReceiverAcks)
    {
        ReceiverAckedPackets[ReceiverAck] = 1;
    }

    for (int i = 0; i < NumAckIterations / 2; ++i)
    {
        REQUIRE(ReceiverAckedPackets[i] == 1);
    }

}

TEST_CASE( "Test Reliable Acks Packet Loss", "[reliability]")
{
    
    const int NumAckIterations = 256;
    logging::SpdLogger Logger("reliable_endpoint_acks_packet_loss", TEST_ASSET_DIR "logs/reliability_packet_loss.log");
    
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);

    auto Config = net::ReliableEndpointConfig();
    struct sockaddr_in Addr;

    auto SenderSock = std::make_shared<MockSock>();
    auto ReceiverSock = std::make_shared<MockSock>();

    auto SenderEndpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);


    bool bShouldDrop = false;
    ReceiverSock->SendFn = [&bShouldDrop, &SenderEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        if (bShouldDrop) 
        {
            return Len; 
        }

        std::vector<unsigned char> Reassembled;
        SenderEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        return Len;
    };

    auto ReceiverEndpoint = net::ReliableEndpoint(0, ValidUserId, ReceiverSock, Addr, UserKey, Config, 0);

    SenderSock->SendFn = [&bShouldDrop, &ReceiverEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);
        if (bShouldDrop) 
        {
            return Len; 
        }

        std::vector<unsigned char> Reassembled;
        ReceiverEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        return Len;
    };

    double Time = 100.0f;
    double DeltaTime = 0.01f;
    for (int i = 0; i < NumAckIterations; ++i)
    {
        std::vector<uint8_t> Output;
        Output.resize(1024);
        uint64_t OutputLen{0};
        input::Input KeyPress{};
        KeyPress.Press(input::Forward);
        auto Input = network::Inputs{KeyPress, 0, 0, 0};
        BuildCommandOutput(net::DefaultConfig.FragmentSize, Input, Output, OutputLen);

        bShouldDrop = i % 2;
        Game::Message::GameMessage Msg(ValidUserId, std::make_unique<std::vector<uint8_t>>(Output), Game::Message::MessageData_EncClientCommand);
        SenderEndpoint.SendPacket(Logger, *Crypt, Msg);
        ReceiverEndpoint.SendPacket(Logger, *Crypt, Msg);

        SenderEndpoint.Update(Time);
        ReceiverEndpoint.Update(Time);
        Time += DeltaTime;
    }

    uint8_t SenderAckedPackets[256] = {0};
    int TotalSenderAcks = 0;
    auto SenderAcks = SenderEndpoint.GetAcks(TotalSenderAcks);
    for (auto SenderAck : SenderAcks)
    {
        SenderAckedPackets[SenderAck] = 1; 
    }
    

    for (int i = 0; i < NumAckIterations / 2; ++i)
    {
        REQUIRE(SenderAckedPackets[i] == (i+1) % 2);
    }

    uint8_t ReceiverAckedPackets[256] = {0};
    int TotalRecvAcks = 0;
    auto ReceiverAcks = ReceiverEndpoint.GetAcks(TotalRecvAcks);
    for (auto ReceiverAck : ReceiverAcks)
    {
        ReceiverAckedPackets[ReceiverAck] = 1;
    }

    for (int i = 0; i < NumAckIterations / 2; ++i)
    {
        REQUIRE(ReceiverAckedPackets[i] == (i+1) % 2);
    }
}

TEST_CASE( "Test Reliable Single Packets", "[reliability]")
{
    
    logging::SpdLogger Logger("reliable_endpoint_packets", TEST_ASSET_DIR "logs/single_reliability.log");
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);

    auto Config = net::ReliableEndpointConfig();
    struct sockaddr_in Addr;

    auto SenderSock = std::make_shared<MockSock>();
    auto ReceiverSock = std::make_shared<MockSock>();

    auto SenderEndpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);

    ReceiverSock->SendFn = [&SenderEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
        auto HavePacket = SenderEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        REQUIRE(HavePacket == true);
        auto Message = Game::Message::GetClientCommand(Reassembled.data());
        REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);
        auto Inputs = Message->inputs()->inputs();
        REQUIRE( static_cast<input::Input>(Inputs).IsPressed(input::Forward) == true);
        return Len;
    };

    auto ReceiverEndpoint = net::ReliableEndpoint(0, ValidUserId, ReceiverSock, Addr, UserKey, Config, 0);
    SenderSock->SendFn = [&ReceiverEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized);
        REQUIRE(Deserialized->user_id() == ValidUserId);
       
        std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
        auto HavePacket = ReceiverEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        // should always return a reassembled packet
        REQUIRE(HavePacket == true);
        auto Message = Game::Message::GetClientCommand(Reassembled.data());
        auto Input = static_cast<input::Input>(Message->inputs()->inputs());
        REQUIRE( Input.IsPressed(input::Forward) == true);
        REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);
        return Len;
    };

    double Time = 100.0f;
    double DeltaTime = 0.01f;
    for (int i = 0; i < 16; ++i)
    {
        auto Output = std::make_unique<std::vector<uint8_t>>();
        Output->reserve(net::DefaultConfig.FragmentSize);
        uint64_t OutputLen{0};
        input::Input KeyPress{};
        KeyPress.Press(input::Forward);
        auto Input = network::Inputs{KeyPress, 0, 0, 0};
        BuildCommandOutput(net::DefaultConfig.FragmentSize, Input, *Output, OutputLen);
        REQUIRE( OutputLen < static_cast<uint64_t>(net::DefaultConfig.FragmentAbove));
        Output->resize(OutputLen);

        Game::Message::GameMessage Msg(ValidUserId, std::move(Output), Game::Message::MessageData_EncClientCommand);
        SenderEndpoint.SendPacket(Logger, *Crypt, Msg);
        ReceiverEndpoint.SendPacket(Logger, *Crypt, Msg);

        SenderEndpoint.Update(Time);
        ReceiverEndpoint.Update(Time);
        Time += DeltaTime;
    }

}

// TODO: Re-enable with a packet type that actually gets big now.
// TEST_CASE( "Test Reliable Fragmented Packets", "[reliability]")
// {
    
//     logging::SpdLogger Logger("reliable_endpoint_fragmented_packets", TEST_ASSET_DIR "logs/frag_reliability.log");
//     auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
//     auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
//     Crypt->GenerateKey(*UserKey);

//     auto Config = net::ReliableEndpointConfig();
//     struct sockaddr_in Addr;

//     auto SenderSock = std::make_shared<MockSock>();
//     auto ReceiverSock = std::make_shared<MockSock>();
    
//     auto SenderEndpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);
//     int RecvPacketCount = 0;
//     ReceiverSock->SendFn = [&RecvPacketCount, &SenderEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
//     {
//         auto Deserialized = Game::Message::GetMessage(Buffer);
//         REQUIRE(Deserialized);
//         REQUIRE(Deserialized->user_id() == ValidUserId);

//         std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
//         auto HavePacket = SenderEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
//         if (HavePacket)
//         {
//             RecvPacketCount++;
//             auto Message = Game::Message::GetClientCommand(Reassembled.data());
//             REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);
//             auto Input = static_cast<input::Input>(Message->inputs()->inputs());
//             REQUIRE( Input.IsPressed(input::Forward) == true);
//         }
//         return Len;
//     };


//     auto ReceiverEndpoint = net::ReliableEndpoint(0, ValidUserId, ReceiverSock, Addr, UserKey, Config, 0);
//     int SentPacketCount = 0;
//     SenderSock->SendFn = [&SentPacketCount, &ReceiverEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
//     {
//         auto Deserialized = Game::Message::GetMessage(Buffer);
//         REQUIRE(Deserialized);
//         REQUIRE(Deserialized->user_id() == ValidUserId);
       
//         std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
//         auto HavePacket = ReceiverEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
//         if (HavePacket)
//         {
//             SentPacketCount++;
//             auto Size = Reassembled.size();
//             REQUIRE( Size > static_cast<size_t>(Len));
//             auto Message = Game::Message::GetClientCommand(Reassembled.data());
//             REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);

//             auto Input = static_cast<input::Input>(Message->inputs()->inputs());
//             REQUIRE( Input.IsPressed(input::Forward) == true);
//         }
//         return Len;
//     };

//     double Time = 100.0f;
//     double DeltaTime = 0.01f;
//     int NumPackets = 16;
//     for (int i = 0; i < NumPackets; ++i)
//     {
//         auto Output = std::make_unique<std::vector<uint8_t>>();
//         Output->reserve(net::DefaultConfig.MaxPacketSize);
//         uint64_t OutputLen{0};
//         input::Input KeyPress{};
//         KeyPress.Press(input::Forward);
//         auto Input = network::Inputs{KeyPress, 0, 0, 0};
//         BuildCommandOutput(net::DefaultConfig.MaxPacketSize, Input, *Output, OutputLen);

//         REQUIRE( OutputLen > static_cast<uint64_t>(net::DefaultConfig.FragmentAbove));

//         Game::Message::GameMessage Msg(ValidUserId, std::move(Output), Game::Message::MessageData_EncClientCommand);
//         SenderEndpoint.SendPacket(Logger, *Crypt, Msg);
//         ReceiverEndpoint.SendPacket(Logger, *Crypt, Msg);

//         SenderEndpoint.Update(Time);
//         ReceiverEndpoint.Update(Time);
//         Time += DeltaTime;
//     }

//     REQUIRE(SentPacketCount == NumPackets);
//     REQUIRE(RecvPacketCount == NumPackets);
// }

TEST_CASE( "Test Reliable Packet Rollover", "[reliability]")
{
    
    //SKIP("disable this script if using valgrind");
    
    logging::SpdLogger Logger("reliable_endpoint_packet_rollover", TEST_ASSET_DIR "logs/rollover_reliability.log");
    
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);

    auto Config = net::ReliableEndpointConfig();
    struct sockaddr_in Addr;

    auto SenderSock = std::make_shared<MockSock>();
    auto ReceiverSock = std::make_shared<MockSock>();
    
    auto SenderEndpoint = net::ReliableEndpoint(0, ValidUserId, SenderSock, Addr, UserKey, Config, 0);
    int RecvPacketCount = 0;
    ReceiverSock->SendFn = [&RecvPacketCount, &SenderEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
        auto HavePacket = SenderEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        if (HavePacket)
        {
            RecvPacketCount++;
            auto Message = Game::Message::GetClientCommand(Reassembled.data());
            REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);
            auto Input = static_cast<input::Input>(Message->inputs()->inputs());
            REQUIRE( Input.IsPressed(input::Forward) == true);
        }
        return Len;
    };

    auto ReceiverEndpoint = net::ReliableEndpoint(0, ValidUserId, ReceiverSock, Addr, UserKey, Config, 0);

    int SentPacketCount = 0;
    SenderSock->SendFn = [&SentPacketCount, &ReceiverEndpoint, &Logger, &Crypt](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized);
        REQUIRE(Deserialized->user_id() == ValidUserId);
       
        std::vector<unsigned char> Reassembled(net::DefaultConfig.FragmentAbove);
        auto HavePacket = ReceiverEndpoint.ReceivePacket(Logger, *Crypt, *Deserialized, Reassembled);
        if (HavePacket)
        {
            SentPacketCount++;
            auto Size = Reassembled.size();
            REQUIRE( Size > static_cast<size_t>(Len));
            auto Message = Game::Message::GetClientCommand(Reassembled.data());
            REQUIRE(Message->opcode() == Game::Message::ClientOpCode_Input);
            auto Input = static_cast<input::Input>(Message->inputs()->inputs());
            REQUIRE( Input.IsPressed(input::Forward) == true);
        }
        return Len;
    };

    double Time = 100.0f;
    double DeltaTime = 0.01f;
    int NumPackets = 65537;
    for (int i = 0; i < NumPackets; ++i)
    {
        auto Output = std::make_unique<std::vector<uint8_t>>();
        Output->resize(net::DefaultConfig.MaxPacketSize);
        uint64_t OutputLen{0};
        input::Input KeyPress{};
        KeyPress.Press(input::Forward);
        auto Input = network::Inputs{KeyPress, 0, 0, 0};
        BuildCommandOutput(net::DefaultConfig.MaxPacketSize, Input, *Output, OutputLen);

        Game::Message::GameMessage Msg(ValidUserId, std::move(Output), Game::Message::MessageData_EncClientCommand);
        SenderEndpoint.SendPacket(Logger, *Crypt, Msg);
        ReceiverEndpoint.SendPacket(Logger, *Crypt, Msg);

        SenderEndpoint.Update(Time);
        ReceiverEndpoint.Update(Time);
        Time += DeltaTime;
    }

    REQUIRE(SentPacketCount == NumPackets);
    REQUIRE(RecvPacketCount == NumPackets);
    auto SenderCounters = SenderEndpoint.GetCounters();
    auto NextSequence = SenderEndpoint.NextPacketSequence();
    REQUIRE( NextSequence == 1);
    REQUIRE(SenderCounters[net::EndpointNumPacketsReceived] == static_cast<uint64_t>(NumPackets));
    auto RecvCounters = ReceiverEndpoint.GetCounters();
    REQUIRE(RecvCounters[net::EndpointNumPacketsReceived] == static_cast<uint64_t>(NumPackets));
}

TEST_CASE( "Test Send Bad Packets", "[reliability]")
{
    
    logging::SpdLogger Logger("reliable_endpoint_send_bad_packet", TEST_ASSET_DIR "logs/bad_reliability.log");
    auto Crypt = std::make_shared<crypto::Cryptor>(Logger);
    auto UserKey = std::make_shared<std::array<unsigned char, PMO_KEY_SIZE>>();
    Crypt->GenerateKey(*UserKey);

    auto Config = net::ReliableEndpointConfig();
    struct sockaddr_in Addr;

    auto Sock = std::make_shared<MockSock>();

    auto Endpoint = net::ReliableEndpoint(0, ValidUserId, Sock, Addr, UserKey, Config, 0);
    net::ReliableMessage Message{};
    Message.Sequence = 10000;
    
    auto Output = std::make_unique<std::vector<unsigned char>>();
    Output->reserve(net::DefaultConfig.MaxPacketSize+1);
    Output->resize(net::DefaultConfig.MaxPacketSize+1);
    std::memset(Output->data(), 0x41, net::DefaultConfig.MaxPacketSize+1);

    // Send too big of data
    Game::Message::GameMessage Msg(ValidUserId, std::move(Output), Game::Message::MessageData_EncClientCommand);
    Endpoint.SendPacket(Logger, *Crypt, Msg);
        
    auto Counters = Endpoint.GetCounters();
    REQUIRE(Counters[net::EndpointNumPacketsTooLargeToSend] == 1);
}

// TODO: Re-enable with bigger packet type
// TEST_CASE( "Test Reliable Peer", "[reliability]")
// {

//     logging::SpdLogger Logger("reliable_peer_resend", TEST_ASSET_DIR "logs/peer_reliability.log");
//     auto Crypto = crypto::Cryptor(Logger);
//     struct sockaddr_in ClientAddr;

//     auto SenderSock = std::make_shared<MockSock>();
//     auto ReceiverSock = std::make_shared<MockSock>();
//     const auto UserId = 0xd34db33f;
//     auto UserKey = std::make_shared<std::array<uint8_t, PMO_KEY_SIZE>>();
//     Crypto.GenerateKey(*UserKey.get());
//     auto SenderPeer = net::ReliablePeer(UserId, SenderSock, ClientAddr, UserKey);
//     auto ReceiverPeer = net::ReliablePeer(UserId, ReceiverSock, ClientAddr, UserKey);

//     std::vector<uint8_t> Output(net::DefaultConfig.MaxPacketSize);
//     uint64_t OutputLen{0};
//     input::Input KeyPress{};
//     KeyPress.Press(input::Forward);
//     auto Input = network::Inputs{KeyPress, 0, 0, 0};

//     BuildCommandOutput(net::DefaultConfig.MaxPacketSize, Input, Output, OutputLen);

//     REQUIRE( OutputLen > static_cast<uint64_t>(net::DefaultConfig.FragmentAbove));
//     Game::Message::GameMessage Msg(ValidUserId, std::make_unique<std::vector<uint8_t>>(Output), Game::Message::MessageData_EncClientCommand);

//     SenderPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);
//     ReceiverPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);

//     // too large for stack, alloc on heap so we don't get valgrind errors
//     auto OutQueue = std::make_shared<net::PacketQueue>();
//     // Should not resend packet as it has 'expired'
//     double Time = 4000.1f;
//     auto Ret = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time);
//     REQUIRE(Ret == 0);
//     auto Val = OutQueue->Pop();
//     REQUIRE(Val.has_value() == true);
//     // Should resend 1 packet
//     SenderPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);
    
//     // 1800 < 2000 threshold
//     Time = 5800.f;
//     Ret = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time);
//     REQUIRE(Ret == 1);
//     Val = OutQueue->Pop();
//     REQUIRE(Val.has_value() == false);
//     // Expired again
//     Time = 6001.f;
//     Ret = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time);
//     REQUIRE(Ret == 0);
//     Val = OutQueue->Pop();
//     REQUIRE(Val.has_value() == true);

// }

TEST_CASE( "Test Reliable Peer With Acks", "[reliability]")
{

    logging::SpdLogger Logger("reliable_peer_resend_acks", TEST_ASSET_DIR "logs/resend_acks_reliability.log");
    auto Crypto = crypto::Cryptor(Logger);
    struct sockaddr_in ClientAddr;

    auto SenderSock = std::make_shared<MockSock>();
    auto ReceiverSock = std::make_shared<MockSock>();
    const auto UserId = 0xd34db33f;
    auto UserKey = std::make_shared<std::array<uint8_t, PMO_KEY_SIZE>>();
    Crypto.GenerateKey(*UserKey.get());
    auto SenderPeer = net::ReliablePeer(UserId, SenderSock, ClientAddr, UserKey);
    bool bShouldDrop = false;
    ReceiverSock->SendFn = [&bShouldDrop, &SenderPeer, &Logger, &Crypto](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);

        if (bShouldDrop) 
        {
            return Len; 
        }

        std::vector<unsigned char> Reassembled;
        SenderPeer.ReceivePacket(Reliability::Reliable, Logger, Crypto, *Deserialized, Reassembled);
        return Len;
    };


    auto ReceiverPeer = net::ReliablePeer(UserId, ReceiverSock, ClientAddr, UserKey);
    SenderSock->SendFn = [&bShouldDrop, &ReceiverPeer, &Logger, &Crypto](const void *Buffer, const int Len) -> size_t 
    {
        auto Deserialized = Game::Message::GetMessage(Buffer);
        REQUIRE(Deserialized->user_id() == ValidUserId);
        if (bShouldDrop) 
        {
            return Len; 
        }

        std::vector<unsigned char> Reassembled;
        ReceiverPeer.ReceivePacket(Reliability::Reliable, Logger, Crypto, *Deserialized, Reassembled);
        return Len;
    };

    double Time = 100.0f;
    double DeltaTime = 10.f;
    auto NumAckIterations = 24;
    for (int i = 0; i < NumAckIterations; ++i)
    {
        std::vector<uint8_t> Output;
        Output.resize(1024);
        uint64_t OutputLen{0};
        input::Input KeyPress{};
        KeyPress.Press(input::Forward);
        auto Input = network::Inputs{KeyPress, 0, 0, 0};
        BuildCommandOutput(net::DefaultConfig.FragmentSize, Input, Output, OutputLen);

        bShouldDrop = i % 2;
        Game::Message::GameMessage Msg(UserId, std::make_unique<std::vector<uint8_t>>(Output), Game::Message::MessageData_EncClientCommand);
        SenderPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);
        ReceiverPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);
        SenderPeer.Update(Time);
        ReceiverPeer.Update(Time);
        Time += DeltaTime;
    }
    auto OutQueue = std::make_shared<net::PacketQueue>();

    // should have dropped half packets which need to resend.
    bShouldDrop = true;
    auto Resent = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time+50.f);
    REQUIRE(Resent == 12);

    // should have to resend because they were dropped during resend (e.g. we never ack'd them).
    bShouldDrop = false;
    Resent = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time+50.f);
    REQUIRE(Resent == 12);

    std::vector<uint8_t> Output;
    Output.resize(1024);
    uint64_t OutputLen{0};
    input::Input KeyPress{};
    KeyPress.Press(input::Forward);
    auto Input = network::Inputs{KeyPress, 0, 0, 0};
    BuildCommandOutput(net::DefaultConfig.FragmentSize, Input, Output, OutputLen);
    
    // Send a packet that will also acknowledge
    Game::Message::GameMessage Msg(UserId, std::make_unique<std::vector<uint8_t>>(Output), Game::Message::MessageData_EncClientCommand);
    ReceiverPeer.SendPacket(Reliability::Reliable, Logger, Crypto, Msg);

    // should now have none left to resend.
    Resent = SenderPeer.ResendReliablePackets(Logger, Crypto, *OutQueue, Time+50.f);
    REQUIRE(Resent == 0);
}