#include <catch2/catch_all.hpp>

#include "ecs/init.h"
#include "ecs/items/items.h"

#include "logger/nulllogger.h"
#include "logger/spdlogger.h"
#include "config/config.h"

TEST_CASE( "Test Equipment", "[items]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/items.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4251,  nullptr);
    flecs::world GameWorld;
    logging::SpdLogger Logger("items", Config.GetLogFile());

    REQUIRE( items::LoadEquipment(GameWorld, Config, Logger) == true );
    int ExpectedWepCount = 9;
    int WepCount = 0;
    
    auto WeaponQuery = GameWorld.query_builder<items::Weapon>().with(flecs::Prefab).build();
    WeaponQuery.each([&](flecs::entity e, items::Weapon &Weapon) {
        REQUIRE(Weapon.Id != 0);
        REQUIRE(Weapon.Name != "");
        REQUIRE(Weapon.BaseDamage != 0);
        Logger.Info("Id: {}, Name: {}, BaseDamage: {}", Weapon.Id, Weapon.Name, Weapon.BaseDamage);
        WepCount++;
    });

    REQUIRE( ExpectedWepCount == WepCount );

    int ExpectedShieldCount = 2;
    int ShieldCount = 0;
    auto ShieldQuery = GameWorld.query_builder<items::Shield>().with(flecs::Prefab).build();
    ShieldQuery.each([&](flecs::entity e, items::Shield &Shield) {
        REQUIRE(Shield.Id != 0);
        REQUIRE(Shield.Name != "");
        REQUIRE(Shield.BaseArmor != 0);
        Logger.Info("Id: {}, Name: {}, BaseDamage: {}", Shield.Id, Shield.Name, Shield.BaseArmor);
        ShieldCount++;
    });

    REQUIRE( ExpectedShieldCount == ShieldCount );
}