#include <catch2/catch_all.hpp>

#include "ecs/init.h"
#include "ecs/items/inventory.h"
#include "ecs/items/items.h"

#include "logger/nulllogger.h"
#include "logger/spdlogger.h"
#include "config/config.h"

TEST_CASE( "Test Inventory", "[inventory]")
{

    auto Config = config::Config(TEST_ASSET_DIR "logs/inventory.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4248, nullptr);
    flecs::world GameWorld;
    logging::SpdLogger Logger("items_inventory", Config.GetLogFile());

    items::LoadItems(GameWorld, Config, Logger);

    REQUIRE( items::LoadEquipment(GameWorld, Config, Logger) == true);

    auto Player = GameWorld.entity("Player").is_a<items::Body>();

    // Find and get a reference to our weapon (prefab)
    auto WeaponQuery = GameWorld.query_builder<items::Weapon>().with(flecs::Prefab).build();
    auto AxeWeapon = WeaponQuery.find([&](items::Weapon &Weapon)
    {
        Logger.Info("Searching {}", Weapon.Name);
        return Weapon.Name == "Small Axe";
    });
    
    REQUIRE(AxeWeapon.is_valid());

    auto ShortSwordWeapon = WeaponQuery.find([&](items::Weapon &Weapon)
    {
        Logger.Info("Searching {}", Weapon.Name);
        return Weapon.Name == "Short Sword";
    });

    REQUIRE(ShortSwordWeapon.is_valid());

    // Get player's left hand
    auto LeftHand = Player.target<items::Body::LeftHand>();

    // Add/Equip (e.g. spawn) a Small axe and put it in the "container"
    LeftHand.with<items::ContainedBy>([&]{
        GameWorld.entity().is_a(AxeWeapon);
    });

    auto FoundWeapon = items::FindItemWithKind(LeftHand, GameWorld.entity<items::Weapon>());

    //id() won't match because one is a prefab and one is an instance, so just compare the internal Ids.
    REQUIRE(FoundWeapon.get<items::Weapon>()->Id == AxeWeapon.get<items::Weapon>()->Id);

    FoundWeapon.destruct();
    
    // Another method of creating an item and equipping it
    auto ShortSwordInst = GameWorld.entity().is_a(ShortSwordWeapon);
    ShortSwordInst.add<items::ContainedBy>(LeftHand);

    // Search the left hand again 
    FoundWeapon = items::FindItemWithKind(LeftHand, GameWorld.entity<items::Weapon>());
    
    REQUIRE(FoundWeapon.is_valid());

    // make sure the found weapon now points to our short sword and not the axe
    REQUIRE(FoundWeapon.get<items::Weapon>()->Id == ShortSwordWeapon.get<items::Weapon>()->Id);

}


TEST_CASE( "Test Equip Weapon", "[inventory]")
{

    auto Config = config::Config(TEST_ASSET_DIR "logs/equip_weapon.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4249, nullptr);
    flecs::world GameWorld;
    logging::SpdLogger Logger("items_equip", Config.GetLogFile());

    items::LoadItems(GameWorld, Config, Logger);

    REQUIRE( items::LoadEquipment(GameWorld, Config, Logger) == true);

    auto Player = GameWorld.entity("Player").is_a<items::Body>();
    Player.add<items::Inventory>(
        GameWorld.entity().add<items::Container>()
    );

    auto WeaponQuery = GameWorld.query_builder<items::Weapon>().with(flecs::Prefab).build();
    auto ShortSwordWeapon = WeaponQuery.find([&](items::Weapon &Weapon)
    {
        Logger.Info("Searching {}", Weapon.Name);
        return Weapon.Name == "Short Sword";
    });

    auto ShortSwordInst = GameWorld.entity().is_a(ShortSwordWeapon);

    REQUIRE(items::EquipItem(Player, ShortSwordInst, items::EquipmentSlot::RIGHT_HAND, items::GetContainer(Player)) == true);

    // Get player's hand
    auto RightHand = Player.target<items::Body::RightHand>();
    auto FoundWeapon = items::FindItemWithKind(RightHand, GameWorld.entity<items::Weapon>());

    // Ensure the item was equipped
    REQUIRE(FoundWeapon.get<items::Weapon>()->Id == ShortSwordInst.get<items::Weapon>()->Id);

    // Get ref to an axe
    auto AxeWeapon = WeaponQuery.find([&](items::Weapon &Weapon)
    {
        Logger.Info("Searching {}", Weapon.Name);
        return Weapon.Name == "Small Axe";
    });

    // create an instance of an axe and equip it
    auto AxeInst = GameWorld.entity().is_a(AxeWeapon);
    REQUIRE(items::EquipItem(Player, AxeInst, items::EquipmentSlot::RIGHT_HAND, items::GetContainer(Player)) == true);

    // Get the weapon that's in the right hand
    FoundWeapon = items::FindItemWithKind(RightHand, GameWorld.entity<items::Weapon>());

    // Ensure our axe is now equipped
    REQUIRE(FoundWeapon.get<items::Weapon>()->Id == AxeInst.get<items::Weapon>()->Id);

    // Ensure the sword is still in inventory and valid
    auto SwordInInventory = items::FindItemWithKind(Player, GameWorld.entity<items::Weapon>());
    REQUIRE(SwordInInventory.is_valid());
    REQUIRE(SwordInInventory.get<items::Weapon>()->Id == ShortSwordInst.get<items::Weapon>()->Id);

    // Re-equip Sword and insure it's in the right hand again
    REQUIRE(items::EquipItem(Player, ShortSwordInst, items::EquipmentSlot::RIGHT_HAND, items::GetContainer(Player)) == true);
    FoundWeapon = items::FindItemWithKind(RightHand, GameWorld.entity<items::Weapon>());
    REQUIRE(FoundWeapon.get<items::Weapon>()->Id == ShortSwordInst.get<items::Weapon>()->Id);
    
    // Ensure the axe was moved to inventory and is still valid
    auto AxeInInventory = items::FindItemWithKind(Player, GameWorld.entity<items::Weapon>());
    REQUIRE(AxeInInventory.is_valid());
    REQUIRE(AxeInInventory.get<items::Weapon>()->Id == AxeInst.get<items::Weapon>()->Id);
}


TEST_CASE( "Test Equip Weapon By Id", "[inventory]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/equip_weapon_by_id.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4250, nullptr);
    flecs::world GameWorld;
    logging::SpdLogger Logger("items_by_id", Config.GetLogFile());

    items::LoadItems(GameWorld, Config, Logger);

    REQUIRE( items::LoadEquipment(GameWorld, Config, Logger) == true);

    auto Player = GameWorld.entity("Player").is_a<items::Body>();
    Player.add<items::Inventory>(
        GameWorld.entity().add<items::Container>()
    );

    flecs::entity Inventory = items::GetContainer(Player);
        
    REQUIRE(Inventory.is_valid());

    // Get ref to an axe
    auto WeaponQuery = GameWorld.query_builder<items::Weapon>().with(flecs::Prefab).build();
    auto AxeWeapon = WeaponQuery.find([&](items::Weapon &Weapon)
    {
        Logger.Info("Searching {}", Weapon.Name);
        return Weapon.Name == "Small Axe";
    });

    // create an instance of an axe and put it in the inventory
    auto AxeInst = GameWorld.make_alive(1999).is_a(AxeWeapon);
    items::TransferItem(Inventory, AxeInst);

    // mimic serialization between id and uint64
    uint64_t FlecsId = AxeInst.id();
    flecs::id ItemId = GameWorld.id(FlecsId);

    // Make sure we can find it in our inventory by Id
    flecs::entity ItemToEquip = items::FindItemById(Inventory, ItemId);
    REQUIRE(ItemToEquip.is_valid());

    auto Slot = 10; // items::EquipmentSlot::RIGHT_HAND;
    
    REQUIRE(items::EquipItem(Player, ItemToEquip, static_cast<items::EquipmentSlot>(Slot), Inventory));
}