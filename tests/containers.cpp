#include <catch2/catch_all.hpp>
#include <cstring>
#include <iostream>

#include "../src/input/input.h"
#include "../src/containers/historybuffer.h"
#include "../src/containers/sequencebuffer.h"

TEST_CASE( "Test HistoryBuffer", "[containers]" )
{
    HistoryBuffer<uint32_t, 24> History;
    std::fill(History.begin(), History.end(), input::NoOp);
    input::Input NewInput{};
    NewInput.ProcessInputs(std::vector<input::InputType>{input::InputType::Forward});

    History.Push(NewInput.KeyPresses);

    std::array<uint32_t, 24> FirstOut;
    History.Get(FirstOut);
    REQUIRE(FirstOut.at(0) == input::InputType::Forward);



    History.Push(NewInput.KeyPresses);
    History.Push(NewInput.KeyPresses);
    History.Push(NewInput.KeyPresses);
    NewInput.ProcessInputs(std::vector<input::InputType>{input::InputType::Right});
    REQUIRE( History.Index() == 4 );
    for (auto i = 0; i < 19; i++)
    {
        History.Push(NewInput.KeyPresses);
    }
    REQUIRE( History.Index() == 23 );

    NewInput.ProcessInputs(std::vector<input::InputType>{input::InputType::Forward});
    History.Push(NewInput.KeyPresses);
    REQUIRE(History.Index() == 0);

    NewInput.ProcessInputs(std::vector<input::InputType>{input::InputType::Backward});
    History.Push(NewInput.KeyPresses);
    REQUIRE(History.Index() == 1);

    std::vector<uint32_t> CompressedOut;
    uint32_t CompressionKey;
    History.CompressBuffer(CompressedOut, CompressionKey);
    /*
    In memory:
    | 2 | 1 | 1 | 1 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 1 |

    In Expected Order:
    | 2 | 1 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 1 | 1 | 1 |

    In Expected Order Reversed (changes in values should match compression key bit indexes):
    | 1 | 1 | 1 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 8 | 1 | 2 |

    compression key:
    | 0 | 0 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 | 1 | 1 |
    */
    REQUIRE(CompressionKey == 0b001000000000000000000111);
    
    std::vector<uint32_t> Expected{2, 1, 8, 1};
    REQUIRE(std::ranges::equal(CompressedOut, Expected));

    HistoryBuffer<uint32_t, 24> NewHistory{};
    NewHistory.LoadCompressed(CompressedOut.begin(), CompressedOut.end(), CompressionKey);
    REQUIRE(NewHistory.Index() == 23);

    std::array<uint32_t, 24> OriginalOut;
    History.Get(OriginalOut);

    std::array<uint32_t, 24> NewOut;
    NewHistory.Get(NewOut);

    REQUIRE(std::ranges::equal(OriginalOut, NewOut));
    
    // Now test all different variations of Idx
    for ( auto i = 0; i < 25; i++)
    {
        NewInput.ProcessInputs(std::vector<input::InputType>{input::InputType::Left});
        History.Push(NewInput.KeyPresses);

        std::vector<uint32_t> Out2;
        uint32_t CompressionKey2;
        History.CompressBuffer(Out2, CompressionKey2);
        NewHistory.LoadCompressed(Out2.begin(), Out2.end(), CompressionKey2);
        REQUIRE(NewHistory.Index() == 23);

        std::array<uint32_t, 24> OriginalOut2;
        History.Get(OriginalOut2);

        std::array<uint32_t, 24> NewOut2;
        NewHistory.Get(NewOut2);

        REQUIRE(std::ranges::equal(OriginalOut2, NewOut2));

    }
}

TEST_CASE( "Tests SequenceBuffer", "[containers]" ) 
{
    auto BufferSize = 256;
    struct TestPacket
    {
        uint16_t SequenceId;
    };

    SequenceBuffer<TestPacket> Buff{BufferSize};

    for (int i = 0; i < BufferSize; ++i )
    {
        REQUIRE( Buff.Find(i) == nullptr);
    }

    for (uint16_t i = 0; i <= BufferSize*4; ++i )
    {
        auto Entry = Buff.Insert(i);
        Entry->SequenceId = i;
        REQUIRE( Buff.GetSequence() == i+1);
    }

    for (uint16_t i = 0; i <= BufferSize; ++i )
    {
        auto Entry = Buff.Insert(i);
        REQUIRE( Entry == nullptr );
    }

    int Index = BufferSize * 4;
    for (uint16_t i = 0; i < BufferSize; ++i )
    {
        auto Entry = Buff.Find((uint16_t)Index);
        REQUIRE( Entry != nullptr );
        REQUIRE( Entry->SequenceId == (uint32_t)Index);
        Index--;
    }

    Buff.Reset();
    REQUIRE( Buff.GetSequence() == 0 );

    for (int i = 0; i < BufferSize; ++i )
    {
        auto Entry = Buff.Find((uint16_t)i); 
        REQUIRE(Entry == nullptr);
    }

    Buff.Reset();
}


TEST_CASE( "Tests SequenceBuffer Ack Bits", "[containers]" ) 
{
    auto BufferSize = 256;
    struct TestPacket
    {
        uint16_t SequenceId;
    };

    SequenceBuffer<TestPacket> Buff{BufferSize};
    uint16_t Ack = 0;
    uint32_t AckBits = 0xFFFFFFFF;
    Buff.GenerateAckBits(Ack, AckBits);
    REQUIRE( Ack == 0xFFFF );
    
    for (uint16_t i = 0; i <= BufferSize; ++i)
    {
       Buff.Insert(i);
    }
    Buff.GenerateAckBits(Ack, AckBits);

    REQUIRE( Ack == (uint16_t)BufferSize );
    REQUIRE( AckBits == 0xFFFFFFFF );

    Buff.Reset();

    uint16_t InputAcks[] = { 1, 5, 9, 11 };
    int NumInputAcks = sizeof( InputAcks ) / sizeof( uint16_t );
    for ( uint16_t i = 0; i < NumInputAcks; ++i )
    {
        Buff.Insert(InputAcks[i]);
    }

    Buff.GenerateAckBits(Ack, AckBits);
    REQUIRE( Ack == 11 );
    REQUIRE( AckBits == ( 1 | (1<<(11-9)) | (1<<(11-5)) | (1<<(11-1)) ) );

    Buff.Reset();
}
