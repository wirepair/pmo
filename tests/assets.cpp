#include <catch2/catch_all.hpp>

#include "ecs/physics/landscape.h"
#include "ecs/physics/broadphase.h"
#include "config/config.h"
#include "logger/spdlogger.h"

#include <filesystem>
namespace fs = std::filesystem;

// Jolt includes
#include <Jolt/Jolt.h>
#include <Jolt/Core/Memory.h>
#include <Jolt/RegisterTypes.h>
#include <Jolt/Core/Factory.h>
#include <Jolt/Core/TempAllocator.h>
#include <Jolt/Core/JobSystemThreadPool.h>
#include <Jolt/Physics/PhysicsSettings.h>
#include <Jolt/Physics/PhysicsSystem.h>
#include <Jolt/Physics/Collision/Shape/BoxShape.h>
#include <Jolt/Physics/Collision/Shape/SphereShape.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>
#include <Jolt/Physics/Body/BodyActivationListener.h>


TEST_CASE( "Test Config Load", "[assets]" )
{
    auto Parsed = toml::parse_file(TEST_ASSET_DIR "server_config.toml");
    auto Config = config::Config(Parsed);
    REQUIRE( std::string(Config.GetLogFile()) == "server.log");

    auto Config2 = config::Config("hardcoded.log", "./maps/", "Level1", "0.0.0.0", 4040, nullptr);

    REQUIRE( std::string(Config2.GetLogFile()) == "hardcoded.log");
}

TEST_CASE( "Test Import Map", "[assets]" )
{
    // Calling skip in MSVC throws exceptions and terminates :shrugs:
 
    //SKIP("Valgrind");
    //// Woof, we have to enable the entire physics system since we are going to test creation of the entire landscape body
    //JPH::RegisterDefaultAllocator();
    //JPH::TempAllocator *Allocator = new JPH::TempAllocatorImpl(10*1024*1024);
    //auto JobThreadPool = new JPH::JobSystemThreadPool(JPH::cMaxPhysicsJobs, JPH::cMaxPhysicsBarriers, JPH::thread::hardware_concurrency() - 1);
    //auto System = new JPH::PhysicsSystem();
    //const uint cMaxBodies = 1024;
    //const uint cNumBodyMutexes = 0;
    //const uint cMaxBodyPairs = 1024;
    //const uint cMaxContactConstraints = 1024;
    //auto BroadPhaseLayer = std::make_unique<physics::BPLayerInterfaceImpl>();
    //auto ObjectVsBroadPhaseLayerFilter = std::make_unique<physics::ObjectVsBroadPhaseLayerFilterImpl>();
    //auto ObjectLayerPairFilter = std::make_unique<physics::ObjectLayerPairFilterImpl>();
    //System->Init(cMaxBodies, cNumBodyMutexes, cMaxBodyPairs, cMaxContactConstraints, *BroadPhaseLayer.get(), *ObjectVsBroadPhaseLayerFilter.get(), *ObjectLayerPairFilter.get());
    //JPH::Factory::sInstance = new JPH::Factory();

    //JPH::RegisterTypes();

    //auto Config = config::Config(TEST_ASSET_DIR "logs/import_map.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4241,  nullptr);
    //logging::SpdLogger Logger("assets", Config.GetLogFile());
    //
    //auto Landscape = physics::Landscape{System, Config, Logger};

    //Ref<JPH::Shape> BodyShape;
    //
    //const auto ChunkCount = 16;
    //int Chunks = 0;
    //while(Landscape.NextLandscapeChunk(BodyShape))
    //{
    //    JPH::BodyInterface &BodyInterface = System->GetBodyInterface();
    //    JPH::BodyCreationSettings LandSettings(BodyShape, RVec3::sZero(), JPH::Quat::sIdentity(), EMotionType::Static, physics::Layers::NON_MOVING);
    //    auto LandscapeBody = BodyInterface.CreateBody(LandSettings);
    //    BodyInterface.AddBody(LandscapeBody->GetID(), JPH::EActivation::DontActivate);
    //    Chunks++;
    //}
   
    //REQUIRE( Chunks == ChunkCount );
    //JPH::UnregisterTypes();
    //
    //delete Allocator;
    //delete JobThreadPool;
    //delete System;
    //// Destroy the factory
    //delete JPH::Factory::sInstance;
    //JPH::Factory::sInstance = nullptr;

}
