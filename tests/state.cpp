#ifdef IS_SERVER
#include <catch2/catch_all.hpp>

#include "ecs/init_server.h"
#include "ecs/state.h"
#include "schemas/server_generated.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

TEST_CASE( "State Diff Logic", "[state]" )
{
    logging::SpdLogger Logger("state", TEST_ASSET_DIR "logs/state.log");
    flecs::world GameWorld;
    state::PlayerWorldStates States{};

    REQUIRE(States.SequenceId == 0);
    flatbuffers::FlatBufferBuilder Builder(4096);
    

    States.Delta(&Logger, Builder, 0);
    REQUIRE(States.SequenceId == 1);
    Logger.Info("Builder 1 size: {}", Builder.GetSize());

    Builder.Release();

    
    std::vector<flecs::entity> Players{};
    Players.reserve(6);
    auto BasePos = units::Vector3{1.f, 1.f, 1.f};
    auto BaseVel = units::Velocity{0.f, 0.f, 0.f};
    auto BaseRot = units::Quat{1.f, 1.f, 1.f, 0.f};
    auto BaseAttr = units::Attributes{100.0f};
    auto BaseTrait = units::TraitAttributes{1,1,1,1,1};

    for (int i = 0; i < 6; i++)
    {
        auto Player = GameWorld.entity()
            .set<character::Visibility>({})
            .set<character::ActionType>(character::ActionType::NOOP)
            .set<units::Vector3>({BasePos})
            .set<units::Velocity>({BaseVel})
            .set<units::Quat>({BaseRot})
            .set<units::Attributes>({BaseAttr})
            .set<units::TraitAttributes>({BaseTrait});
        Logger.Info("Player {} created", Player.id());
        Players.emplace_back(Player);
    }
    auto Player1Vis = Players.at(0).get_mut<character::Visibility>();
    Player1Vis->Neighbors.insert(Players.at(1));
    Player1Vis->Neighbors.insert(Players.at(2));
    Player1Vis->Neighbors.insert(Players.at(3));

    auto Pos = units::Vector3{2.f, 2.f, 2.f};
    auto Rot = units::Quat{0.5f, 0.f, 0.2f, 0.f};
    auto Vel = units::Velocity{0.f, 0.f, 0.f};

    States.UpdateState(Logger, *Player1Vis, BaseAttr, BaseTrait, Pos, Vel, Rot);
    Builder = flatbuffers::FlatBufferBuilder(4096);
    States.Delta(&Logger, Builder, 0);
   
    Logger.Info("Builder 2 size: {}", Builder.GetSize());
    // Deserialize
    auto Deserialized = Game::Message::GetServerCommand(Builder.GetBufferPointer());

    REQUIRE(States.SequenceId == 2);
    // Ack it so we have something to diff for the next iteration
    States.AckId(States.SequenceId);

    REQUIRE(Deserialized->state()->in_entities()->size() == 3);
    REQUIRE(Deserialized->state()->out_entities() == nullptr);
    REQUIRE(Deserialized->state()->updated_entities() == nullptr);

    // Release it
    Builder.Release();


    // Now update the character's visibility and pos/rot slightly to see if the diff works
    Player1Vis->Neighbors.erase(Players.at(2));
    Pos = units::Vector3{3.f, 3.f, 2.f};
    Rot = units::Quat{0.5f, 0.f, 0.2f, 0.f};
    Vel = units::Velocity{0.f, 0.f, 0.f};

    States.UpdateState(Logger, *Player1Vis, BaseAttr, BaseTrait, Pos, Vel, Rot);
    
    Builder = flatbuffers::FlatBufferBuilder(4096);
    States.Delta(&Logger, Builder, 0);
    Logger.Info("Builder 3 size: {}", Builder.GetSize());
    
    // Deserialize
    Deserialized = Game::Message::GetServerCommand(Builder.GetBufferPointer());
    
    REQUIRE(Deserialized->state()->in_entities() == nullptr);
    REQUIRE(Deserialized->state()->out_entities()->size() == 1);
    REQUIRE(Deserialized->state()->updated_entities()->size() == 2);

    // should be new
    REQUIRE(Deserialized->state()->pos()->x() == Pos.vec3[0]);
    REQUIRE(Deserialized->state()->pos()->y() == Pos.vec3[1]);
    REQUIRE(Deserialized->state()->pos()->z() == Pos.vec3[2]);

    // wasn't updated
    REQUIRE(Deserialized->state()->rot() == nullptr);
    Builder.Release();
}

TEST_CASE( "Combat State Diff Logic", "[state]" )
{
    logging::SpdLogger Logger("combat_state", TEST_ASSET_DIR "logs/state.log");
    flecs::world GameWorld;
    state::PlayerWorldStates States{};
    character::InitCharacter(GameWorld);

    REQUIRE(States.SequenceId == 0);
    flatbuffers::FlatBufferBuilder Builder(4096);
    std::vector<flecs::entity> Players{};
    Players.reserve(3);
    auto BasePos = units::Vector3{1.f, 1.f, 1.f};
    auto BaseVel = units::Velocity{0.f, 0.f, 0.f};
    auto BaseRot = units::Quat{1.f, 1.f, 1.f, 0.f};
    auto BaseAttr = units::Attributes{100.0f};
    auto BaseTrait = units::TraitAttributes{1,1,1,1,1};
    
    for (int i = 0; i < 3; i++)
    {
        auto Player = GameWorld.entity()
            .set<character::Visibility>({})
            .add(character::ActionType::NOOP)
            .set<units::Vector3>({BasePos})
            .set<units::Velocity>({BaseVel})
            .set<units::Quat>({BaseRot})
            .set<units::TraitAttributes>({BaseTrait})
            .set<units::Attributes>({BaseAttr});
        Logger.Info("Player {} created", Player.id());
        
        Players.emplace_back(Player);
    }
    auto Player1 = Players.at(0);
    auto Player2 = Players.at(1);
    auto Player3 = Players.at(2);
    auto Player1Vis = Player1.get_mut<character::Visibility>();
    auto Player2Vis = Player2.get_mut<character::Visibility>();
    //auto Player3Vis = Player3.get_mut<character::Visibility>();

    Player1Vis->Neighbors.insert(Player2);
    auto DmgEvent = GameWorld.entity().set<combat::ApplyDamage>({.Instigator=Player1, .TargetHit=Player2, .BaseDamage=100.f});
    Player1Vis->DamageEvents.insert(DmgEvent);

    REQUIRE(DmgEvent.get<combat::ApplyDamage>()->Instigator == Player1);
    
    auto Pos = units::Vector3{2.f, 2.f, 2.f};
    auto Rot = units::Quat{0.5f, 0.f, 0.2f, 0.f};
    auto Vel = units::Velocity{0.f, 0.f, 0.f};

    auto DmgEvent2 = GameWorld.entity().set<combat::ApplyDamage>({.Instigator=Player2, .TargetHit=Player1, .BaseDamage=150.f});
    REQUIRE(DmgEvent2.get<combat::ApplyDamage>()->Instigator == Player2);
    

    Player2Vis->DamageEvents.insert(DmgEvent2);
    Player2Vis->Neighbors.insert(Player1);

    States.UpdateState(Logger, *Player1Vis, BaseAttr, BaseTrait, Pos, Vel, Rot);
    Builder = flatbuffers::FlatBufferBuilder(4096);
    States.Delta(&Logger, Builder, 0);
   
    Logger.Info("Builder 2 size: {}", Builder.GetSize());
    // Deserialize
    auto Deserialized = Game::Message::GetServerCommand(Builder.GetBufferPointer());

    REQUIRE(States.SequenceId == 1);
    // Ack it so we have something to diff for the next iteration
    States.AckId(States.SequenceId);


    REQUIRE(Deserialized->state()->in_entities()->size() == 1);
    bool HasDmgEvent = false;
    for (auto It = Deserialized->state()->in_entities()->begin(); It != Deserialized->state()->in_entities()->end(); ++It)
    {
        auto Entity = *It;
        for (auto DmgIt = Entity->player()->damage_events()->begin(); DmgIt != Entity->player()->damage_events()->end(); ++DmgIt)
        {
            auto Event = *DmgIt;
            REQUIRE(Event->amount() == 150.f);
            HasDmgEvent = true;
        }
    }
    
    REQUIRE(HasDmgEvent == true);

    REQUIRE(Deserialized->state()->damage_events()->size() == 1);
    for (auto It = Deserialized->state()->damage_events()->begin(); It != Deserialized->state()->damage_events()->end(); ++It)
    {
        auto Event = *It;
        REQUIRE(Event->amount() == 100.f);
    }
    REQUIRE(Deserialized->state()->out_entities() == nullptr);
    REQUIRE(Deserialized->state()->updated_entities() == nullptr);

    // Release it
    Builder.Release();
    
    Player1Vis->Clear();
    Player2Vis->Clear();

    // Test an UPDATED player (not new) attacking a NEW player

    // Update again! Make player 2 attack 3, but Player1 sees it //
    auto DmgEvent3 = GameWorld.entity().set<combat::ApplyDamage>({.Instigator=Player2, .TargetHit=Player3, .BaseDamage=175.f});
    REQUIRE(DmgEvent3.get<combat::ApplyDamage>()->Instigator == Player2);

    Player2Vis->DamageEvents.insert(DmgEvent3);
    Player2Vis->Neighbors.insert(Player3);
    // make sure player1 can see all of this
    Player1Vis->Neighbors.insert(Player2);
    Player1Vis->Neighbors.insert(Player3);

    States.UpdateState(Logger, *Player1Vis, BaseAttr, BaseTrait, Pos, Vel, Rot);
    Builder = flatbuffers::FlatBufferBuilder(4096);
    States.Delta(&Logger, Builder, 0);

    REQUIRE(States.SequenceId == 2);
    // Ack it so we have something to diff for the next iteration
    States.AckId(States.SequenceId);

    // Deserialize again
    Deserialized = Game::Message::GetServerCommand(Builder.GetBufferPointer());


    REQUIRE(Deserialized->state()->in_entities()->size() == 1);
    HasDmgEvent = false;
    // This is for NEW players, the attack should be coming from UPDATED instead
    for (auto It = Deserialized->state()->in_entities()->begin(); It != Deserialized->state()->in_entities()->end(); ++It)
    {
        auto Entity = *It;
        if (!Entity->player()->damage_events())
        {
            continue;
        }

        for (auto DmgIt = Entity->player()->damage_events()->begin(); DmgIt != Entity->player()->damage_events()->end(); ++DmgIt)
        {
            HasDmgEvent = true;
        }
    }
    REQUIRE(HasDmgEvent == false);

    // Iterate over updated players, player 2 attacked player 3
    for (auto It = Deserialized->state()->updated_entities()->begin(); It != Deserialized->state()->updated_entities()->end(); ++It)
    {
        auto Entity = *It;
        for (auto DmgIt = Entity->player()->damage_events()->begin(); DmgIt != Entity->player()->damage_events()->end(); ++DmgIt)
        {
            auto Event = *DmgIt;
            REQUIRE(Event->target() == Player3.id());
            REQUIRE(Event->amount() == 175.f);
            HasDmgEvent = true;
        }
    }  
    REQUIRE(HasDmgEvent == true);

    // our character did not attack
    REQUIRE(Deserialized->state()->damage_events() == nullptr);
    REQUIRE(Deserialized->state()->out_entities() == nullptr);

    // Release it
    Builder.Release();
}
#endif