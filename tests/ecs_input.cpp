#include <catch2/catch_all.hpp>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "ecs/init.h"
#include "ecs/level.h"
#include "ecs/network/network.h"
#include "ecs/character/components.h"
#include "ecs/character/controllable.h"
#include "ecs/character/pmocharacter.h"
#include "math/math.h"
#include "input/input.h"
#include "../src/schemas/messages.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"

#ifndef IS_SERVER
TEST_CASE( "Test ECS Input/Movement", "[ecs_input]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/ecs_input_movement.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4245,  nullptr);
    logging::SpdLogger Logger("ecs_input", Config.GetLogFile());
    
    flecs::world GameWorld{};

    REQUIRE( InitPMOClient(GameWorld, Logger, Config, 0xd34db33f));
    auto Physics = GameWorld.get_mut<physics::Physics>();
    auto Pos = units::Vector3{0.f,0.f,0.f};
    flecs::entity Player = GameWorld.entity()
        .set<network::User>({0xd34db33f})
        .set<network::Connected>({})
        .add<network::ClientStateBuffer>()
        .set<units::Velocity>({0.f,0.f,0.f})
        .add<network::Inputs>();
    
    Player.set<character::PMOCharacter>({Player, std::make_unique<character::ControllableCharacter>(&Logger, Physics, Pos)})
        .child_of<level::MapLevel>();

    REQUIRE( Player.is_alive() );
    input::Input UserKeyPresses{};
    float CurrentYaw{};
    float CurrentPitch{};

    // Move forward
    UserKeyPresses.Press(input::InputType::Forward);
    Player.set<network::Inputs>({ UserKeyPresses, CurrentYaw, CurrentPitch, 0 });
    GameWorld.progress();
    auto Char = Player.get<character::PMOCharacter>();
    auto Velocity = Char->GetVelocity();
    Logger.Info("Velocity: {} {} {}", Velocity.vec3[0], Velocity.vec3[1], Velocity.vec3[2]);
    REQUIRE(!math::IsNearlyZero(Velocity.vec3[0])); // confirm we only moved forward
    //REQUIRE(math::IsNearlyZero(Velocity.vec3[1])); <-- technically we are falling to start
    REQUIRE(math::IsNearlyZero(Velocity.vec3[2]));

    // Now move forward and left
    UserKeyPresses.Press(input::InputType::Forward);
    UserKeyPresses.Press(input::InputType::Left);
    Player.set<network::Inputs>({ UserKeyPresses, CurrentYaw, CurrentPitch, 0 });
    GameWorld.progress();
    Velocity = Char->GetVelocity();
    Logger.Info("Velocity: {} {} {}", Velocity.vec3[0], Velocity.vec3[1], Velocity.vec3[2]);
    REQUIRE(!math::IsNearlyZero(Velocity.vec3[0])); 
    //REQUIRE(!math::IsNearlyZero(Velocity.vec3[1])); 
    REQUIRE(!math::IsNearlyZero(Velocity.vec3[2])); // confirm we also moved left
    DeinitPMO(GameWorld);
}
#endif