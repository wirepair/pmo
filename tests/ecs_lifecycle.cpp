#ifndef IS_SERVER
#include <catch2/catch_all.hpp>
#include <thread>
#include <chrono>
#include <memory>
#include <cstring>

#include "ecs/init.h"
#include "ecs/units.h"
#include "ecs/character/components.h"
#include "ecs/combat/combat.h"
#include "input/input.h"
#include "../src/schemas/messages.h"
#include "logger/nulllogger.h"
#include "logger/spdlogger.h"


TEST_CASE( "Test ECS Lifecycle", "[ecs_lifecycle]")
{
    auto Config = config::Config(TEST_ASSET_DIR "logs/ecs_lifecycle.log", TEST_ASSET_DIR, "Level1", "0.0.0.0", 4246,  nullptr);
    logging::SpdLogger Logger("ecs_lifecycle", Config.GetLogFile());
    
    flecs::world GameWorld{};

    REQUIRE( InitPMOClient(GameWorld, Logger, Config, 0xd34db33f));
    GameWorld.progress();
    auto Player = GameWorld.entity();
    Player.set<units::Attributes>({.Health=100.f});
    GameWorld.entity().set<combat::NetDamage>({Player, Player, combat::DamageClass::BLOOD, 100.f});
    //Player.set<character::BindLocation>({0.f, 0.f, 0.f});
    GameWorld.progress();
    GameWorld.system<combat::NetDamage>()
        .write<character::IsDead>()
        .each([&](flecs::iter& It, size_t Index, combat::NetDamage &Dmg)
    {
        auto CurrPlayer = Dmg.TargetHit;
        REQUIRE(CurrPlayer.has<units::Attributes>());
        Logger.Info("Health: {}", CurrPlayer.get<units::Attributes>()->Health);
        CurrPlayer.set<character::IsDead>({});
    });
    GameWorld.progress();

    REQUIRE( Player.has<character::IsDead>() == true);
    DeinitPMO(GameWorld);
}
#endif