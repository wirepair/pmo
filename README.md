## PMO

A PvP MMORPG server written for research & learning, do not use.

### Building

`/app` binaries will be built into the `./<build>/<debug|release>/<binary>` path.

### Linux

This systems built primarly on Ubuntu 24.04.

Build libsodium first (TODO Automate this)

Make sure cmake is around 3.28+

- Install ninja: `sudo apt install ninja-build`
- Install clang-10 at least c`sudo apt install clang clang-tools`
- Install `sudo apt install libstdc++-10-dev` if you are getting messages about failed `span` includes.

#### Debug:

```
mkdir build && cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
ninja
```

#### Release:
```
mkdir build && cd build
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release ..
ninja
```

### Windows

Build libsodium first (TODO Automate this) 

Make your life easier and just use VSCode and have it auto-configure the project.

or:

```bash
mkdir build ; cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
```

#### Memory leak testing

```bash
valgrind --leak-check=full \
         --show-leak-kinds=all \
         --track-origins=yes \
         --verbose \
         --log-file=valgrind-out.txt \
         ./testlib "<Test Name>"
```

### Directory Structure

```bash
.
├── CMakeLists.txt
├── README.md
├── apps - Individual executable apps
├── assets - Any resources (maps, collision meshes, configs etc)
├── cmake - For finding modules (Sodium only right now)
├── docs - TBD
├── logger - Logging interface to be used by pmo_library and callers of library
├── scripts - Helper scripts
├── services - External non-game services to help manage the MMORPG
├── src - The primary server/client pmo_library source
├── tests - Test suite
└── third_party - Vendored non-cmake based projects we depend on (libsodium)
```