package main

import (
	"crypto/ed25519"
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/wirepair/pmo/services/handler"
)

var (
	cmd string
)

func init() {
	flag.StringVar(&cmd, "cmd", "run", "run or generate")
}

func main() {
	flag.Parse()
	if cmd == "generate" {
		generate()
		return
	}

	app := gin.New()

	app.GET("/", func(c *gin.Context) {
		c.Data(http.StatusOK, "text/plain", []byte("OK"))
	})

	app.POST("/login", handler.Login)

	if err := app.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}

func generate() {
	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		log.Fatal("failed to generate key")
	}
	fmt.Printf("PublicKey:\n")
	for i := 0; i < len(pub); i++ {
		if i%16 == 0 {
			fmt.Printf("\n")
		}
		fmt.Printf("0x%02x, ", pub[i])
	}
	fmt.Printf("\n---\nPrivateKey:\n")
	for i := 0; i < len(priv); i++ {
		if i%16 == 0 {
			fmt.Printf("\n")
		}
		fmt.Printf("0x%02x, ", priv[i])
	}
	fmt.Printf("\n---\n")
}
