module gitlab.com/wirepair/pmo/services

go 1.13

require (
	github.com/gin-gonic/gin v1.9.1 // indirect
	github.com/gofiber/fiber/v2 v2.47.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
)
