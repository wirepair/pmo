package handler_test

import (
	"crypto/ed25519"
	"encoding/hex"
	"fmt"
	"io"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/require"
	"gitlab.com/wirepair/pmo/services"
	"gitlab.com/wirepair/pmo/services/handler"
)

func TestLogin(t *testing.T) {

	// https://libsodium.gitbook.io/doc/advanced/ed25519-curve25519 NOTE THIS IS JUST FOR TESTING
	// We can't generate x25519 keys in Go easily :(
	userPubKey, _, err := ed25519.GenerateKey(nil)
	require.NoError(t, err)

	buf := strings.NewReader("user=user&password=pass&pubkey=" + hex.EncodeToString(userPubKey))
	req := httptest.NewRequest("POST", "/login", buf)
	req.Header.Set("Content-Type", " application/x-www-form-urlencoded")
	app := gin.New()

	w := httptest.NewRecorder()
	app.POST("/login", handler.Login)
	app.ServeHTTP(w, req)
	require.Equal(t, 200, w.Result().StatusCode)
	require.NotNil(t, w, "resp was empty")
	resp, err := io.ReadAll(w.Result().Body)
	require.NoError(t, err)

	respBuf := services.NewBufferFromBytes(resp)

	// extract message part (msgtype,uid,ts,pubkey)
	message, err := respBuf.GetBytesFromPos(services.SizeUint8 + services.SizeUint32 + services.SizeInt64 + ed25519.PublicKeySize)
	require.NoError(t, err)

	// reset position so we can easily re-read the userid/timestamp
	respBuf.Reset()
	// get message type
	messageType, err := respBuf.GetUint8()
	require.NoError(t, err)
	require.Equal(t, handler.PubKeyMessage, messageType)

	// get user id
	userID, err := respBuf.GetUint32()
	require.NoError(t, err)
	require.Equal(t, uint32(0xd34db33f), userID)

	// read the time stamp to move our buffer pos
	_, err = respBuf.GetInt64()
	require.NoError(t, err)

	// Get the signed message pubkey
	responsePubKey, err := respBuf.GetBytesFromPos(ed25519.PublicKeySize)
	require.NoError(t, err)
	require.Equal(t, []byte(userPubKey), responsePubKey)

	// signature is everything remaining, verify the message matches our sig
	signature := respBuf.GetRemainingBytes()
	require.True(t, ed25519.Verify(handler.TestPublicKey, message, signature))

	// corrupt/modify the message
	message[0] = 0xfe
	require.False(t, ed25519.Verify(handler.TestPublicKey, message, signature))

	// verify our user id is correct

}

func TestGenerateLoginPubKeyPacket(t *testing.T) {
	Skip()
	// https://libsodium.gitbook.io/doc/advanced/ed25519-curve25519 NOTE THIS IS JUST FOR TESTING
	// We can't generate x25519 keys in Go easily :(
	userPubKey, _, err := ed25519.GenerateKey(nil)
	require.NoError(t, err)

	buf := strings.NewReader("user=user&password=pass&pubkey=" + hex.EncodeToString(userPubKey))
	req := httptest.NewRequest("POST", "/login", buf)
	req.Header.Set("Content-Type", " application/x-www-form-urlencoded")
	app := gin.New()

	w := httptest.NewRecorder()
	app.POST("/login", handler.Login)
	app.ServeHTTP(w, req)
	require.Equal(t, 200, w.Result().StatusCode)
	require.NotNil(t, w, "resp was empty")
	resp, err := io.ReadAll(w.Result().Body)
	require.NoError(t, err)
	for i := 0; i < len(resp); i++ {
		if i%16 == 0 {
			fmt.Printf("\n")
		}
		fmt.Printf("0x%02x, ", resp[i])
	}
	fmt.Printf("\n")
	t.Fail()
}
