package handler

import (
	"crypto/ed25519"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"time"
	"unsafe"

	"github.com/gin-gonic/gin"
	"gitlab.com/wirepair/pmo/services"
)

const (
	PubKeyMessage uint8 = 0
	GameMessage   uint8 = 1
)

const Uint64Size = unsafe.Sizeof(uint64(0))
const Int64Size = unsafe.Sizeof(int64(0))

// Obviously never do this in production, should be pulled from a KMS :>
var TestPrivateKey = ed25519.PrivateKey{
	0x4d, 0x11, 0xfa, 0x7c, 0x12, 0x1b, 0x89, 0xc2, 0x5b, 0x15, 0xb8, 0xe7, 0x72, 0xa9, 0xde, 0xc3,
	0x63, 0x5c, 0xe6, 0xed, 0x31, 0xaf, 0xef, 0x63, 0x70, 0x2d, 0xb3, 0x31, 0x48, 0x95, 0xb5, 0x7d,
	0x32, 0x0d, 0x38, 0x28, 0xee, 0x6b, 0x27, 0x11, 0x24, 0xa5, 0xbf, 0xed, 0x1e, 0xc6, 0x5f, 0x7c,
	0xfc, 0x47, 0xae, 0x54, 0x2a, 0x4c, 0x68, 0x0b, 0xcd, 0xf3, 0x8d, 0xd6, 0x0f, 0x30, 0x9d, 0x08,
}

var TestPublicKey = ed25519.PublicKey{
	0x32, 0x0d, 0x38, 0x28, 0xee, 0x6b, 0x27, 0x11, 0x24, 0xa5, 0xbf, 0xed, 0x1e, 0xc6, 0x5f, 0x7c,
	0xfc, 0x47, 0xae, 0x54, 0x2a, 0x4c, 0x68, 0x0b, 0xcd, 0xf3, 0x8d, 0xd6, 0x0f, 0x30, 0x9d, 0x08,
}

// TODO: move to a proper data store
func getUserId(username, password string) (uint32, error) {
	if username != "user" || password != "pass" {
		return 0, fmt.Errorf("user not found")
	}

	return uint32(0xd34db33f), nil
}

// Login get user and password
func Login(c *gin.Context) {

	userID, err := getUserId(c.PostForm("user"), c.PostForm("password"))
	if err != nil {
		c.Status(http.StatusUnauthorized)
		return
	}

	userPublicKey, err := hex.DecodeString(c.PostForm("pubkey"))
	if err != nil {
		log.Printf("failed to decode users public key")
		c.Status(http.StatusBadRequest)
		return
	}

	if len(userPublicKey) != ed25519.PublicKeySize {
		log.Printf("invalid public key length")
		c.Status(http.StatusBadRequest)
		return
	}

	// Create a new buffer to store our message type, userid, a timestamp, users temp pubkey and signature
	bufferSize := services.SizeUint8 + services.SizeUint32 + services.SizeInt64 + ed25519.PublicKeySize + ed25519.SignatureSize
	buf := services.NewBuffer(bufferSize)
	buf.WriteUint8(PubKeyMessage)
	buf.WriteUint32(userID)
	buf.WriteInt64(time.Now().Add(time.Minute).Unix())
	buf.WriteBytes(userPublicKey)

	message := buf.GetWrittenBytes()

	signature := ed25519.Sign(TestPrivateKey, message)

	buf.WriteBytes(signature)

	message = buf.GetWrittenBytes()

	log.Printf("Sending response: %s\n", hex.EncodeToString(message))
	c.Data(http.StatusOK, "application/octet-stream", message)
}
