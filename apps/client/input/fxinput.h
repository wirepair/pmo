#pragma once

#include <memory>

#include "stringify.h"

#include "src/input/input.h"
#include "ftxui/component/event.hpp"
#include "ftxui/component/screen_interactive.hpp"

#include "containers/lockfreequeue.h"

using InputEventQueue = std::shared_ptr<LockFreeQueue<input::InputType, 100>>;

using namespace ftxui;

namespace clientinput 
{
    class Input 
    {

    public:
        explicit Input(InputEventQueue InQueue) : EventQueue(InQueue), Screen(ScreenInteractive::TerminalOutput()) {};

        void Start();

        void ListenEvents();

        void Stop();

        input::InputType Translate(Event event);

    private:
        InputEventQueue EventQueue;
        std::thread Thread;

        ScreenInteractive Screen;
    };
}