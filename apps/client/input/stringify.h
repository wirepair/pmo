#pragma once

#include "schemas/client_generated.h"

#if defined(_WIN32)
#else
#include <ftxui/component/event.hpp>           // for Event
#include <ftxui/component/captured_mouse.hpp>  // for ftxui
#include <ftxui/component/mouse.hpp>  // for Mouse, Mouse::Left, Mouse::Middle, Mouse::None, Mouse::Pressed, Mouse::Released, Mouse::Right, Mouse::WheelDown, Mouse::WheelUp
using namespace ftxui;
#endif



namespace clientinput 
{
    std::string Stringify(Event event);
}