#include "fxinput.h"

#include "ftxui/component/component.hpp"       // for CatchEvent, Renderer
#include "ftxui/dom/elements.hpp"  // for text, vbox, window, Element, Elements

namespace clientinput 
{
    void Input::Start()
    {
        Thread = std::thread(&Input::ListenEvents, this);
    }

    void Input::ListenEvents()
    {
        std::vector<Event> keys;
    
        auto component = Renderer([&] {
            Elements children;
            for (size_t i = std::max(0, (int)keys.size() - 20); i < keys.size(); ++i)
            children.emplace_back(text(Stringify(keys[i])));
            return window(text("keys"), vbox(std::move(children)));
        });
        
        component |= CatchEvent([&](Event event) {
            keys.emplace_back(event);
            auto Evt = Translate(event);
            //auto Evt = std::make_unique<Event>(event);
            EventQueue->PushByValue(Evt);
            return true;
        });
        Screen.Loop(component);
    }

    input::InputType Input::Translate(Event event)
    {
        if (event.is_character())
        {
            if (event.input() == "w")
            {
                return input::Forward;
            } 
            else if (event.input() == "a")
            {
                return input::Left;
            }
            else if (event.input() == "s")
            {
                return input::Backward;

            }
            else if (event.input() == "d")
            {
                return input::Right;
            }
        }
        return input::NoOp;
    }

    void Input::Stop()
    {
        Screen.Exit();
        if (Thread.joinable())
        {
            Thread.join();
        }
    }
}
