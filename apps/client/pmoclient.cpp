#include <flecs.h>
#include <thread>
#include <atomic>
#include <iostream>
#include <random>
#include <vector>
#include <tuple>

#include <cxxopts.hpp>
#include "apps/shared/ctrlc/ctrlc.h"

#ifndef _WIN32
#include "input/fxinput.h"
#endif

#include "logger/spdlogger.h"

#include "crypto/crypto.h"
#include "net/client.h"
#include "ecs/character/network.h"
#include "containers/lockfreequeue.h"
#include "schemas/messages.h"
#include "config/config.h"

#include "ecs/init.h"
#include "ecs/physics/physics.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/world.h"

#ifndef _WIN32
#include "ftxui/component/event.hpp"
#endif

int main(int argc, char *argv[]) 
{
    cxxopts::Options options("pmoclient", "Runs the PMO headless client for testing");

    options.add_options()
        ("l,log", "Logfile name", cxxopts::value<std::string>()->default_value("client.log"))
        ("v,verbosity", "Set log verbosity level (debug, info, warn, error)", cxxopts::value<std::string>()->default_value("debug"))
        ("a,assets", "Path to assets", cxxopts::value<std::string>()->default_value("../../assets"))
        ("m,map", "Map to load for this server", cxxopts::value<std::string>()->default_value("Level1"))
        ("i,ip",  "Server IP Address", cxxopts::value<std::string>()->default_value("0.0.0.0"))
        ("p,port", "Server Port", cxxopts::value<int>()->default_value("4242"))
        ("y,yaw", "Yaw", cxxopts::value<float>()->default_value("1.0"))
        #ifndef _WIN32
        ("u,ui", "Run with capturing ui", cxxopts::value<bool>()->default_value("false"))
        #endif
        ("h,help", "Print usage");

    auto Args = options.parse(argc, argv);

    if (Args.count("help"))
    {
        std::cout << options.help() << std::endl;
        exit(0);
    }


    bool bIsRunning = true;
    
    CtrlCLibrary::SetCtrlCHandler([&bIsRunning](enum CtrlCLibrary::CtrlSignal event) -> bool 
    {
        switch (event) 
        {
            case CtrlCLibrary::kCtrlCSignal:
                std::cout << "Caught Ctrl+C" << std::endl;
                bIsRunning = false;
                return true;
        }
        return false;
    });
    
    auto Config = config::Config(Args["log"].as<std::string>().c_str(), 
        Args["assets"].as<std::string>().c_str(), 
        Args["map"].as<std::string>().c_str(), 
        Args["ip"].as<std::string>().c_str(),
        Args["port"].as<int>(),
        nullptr
    );

    auto Log = logging::SpdLogger("test", Config.GetLogFile());
    
    #ifndef _WIN32
    auto bUseUi = Args["ui"].count();
    auto KeyQueue = std::make_shared<LockFreeQueue<input::InputType, 100>>();
    auto InputSystem = clientinput::Input(KeyQueue);

    if (bUseUi)
    {
        InputSystem.Start();
    }
    #endif

    auto UserKeyPress = input::Input{};

    flecs::world GameWorld;

    std::random_device Rand;
    std::mt19937 Gen(Rand());
    std::uniform_int_distribution<uint32_t> Distrib(100000, 9999999);
    uint32_t UserId = Distrib(Gen);
    auto NewPlayerT = InitPMOClient(GameWorld, Log, Config, UserId);
    if (NewPlayerT == 0)
    {
        Log.Error("Failed to initialize PMO System, exiting");
        return -1;
    }
    
    auto NewPlayer = GameWorld.entity(NewPlayerT);

    Log.Info("Starting client loop...");
    int Rot = 0;
    while(GameWorld.progress() && bIsRunning)
    {
        // send input after we are no longer falling
        if (NewPlayer.has<character::PMOCharacter>() && NewPlayer.get<character::PMOCharacter>()->GetGroundState() == character::GroundState::OnGround)
        {
            #ifndef _WIN32
            if (bUseUi)
            {
                std::vector<input::InputType> InputEvents;
                for (auto InputEvent = KeyQueue->Pop(); InputEvent != std::nullopt; InputEvent = KeyQueue->Pop())
                {
                    InputEvents.emplace_back(*InputEvent);
                }
                UserKeyPress.ProcessInputs(InputEvents);
            }
            else
            {
                //UserKeyPress.Press(input::Forward);
            }
            #else
            UserKeyPress.Press(input::Forward);
            #endif
        }
        Rot++;
        float Yaw = static_cast<int>(Rot) % 360; // Args["yaw"].as<float>(); //static_cast<int>(Rot) % 2;
        // if (Rot > 200)
        // {
        //     Yaw = 30.f;
        // }
        NewPlayer.set<network::Inputs>({UserKeyPress, Yaw, 0, 0});

        units::Vector3 NetPos{};
        units::Vector3 LocalPos{};

        if (NewPlayer.has<network::ServerPosition>())
        {
            NetPos = NewPlayer.get<network::ServerPosition>()->Position;
        }
        if (NewPlayer.has<character::PMOCharacter>())
        {
            LocalPos = NewPlayer.get<character::PMOCharacter>()->GetPosition();
        }
        uint8_t SeqId = 0;
        if (NewPlayer.has<network::ClientStateBuffer>())
        {
            SeqId = NewPlayer.get<network::ClientStateBuffer>()->State.LastSequenceId();
        }
        
        Log.Info("-------------- Client Seq {} Local: {} {} {} Net: {} {} {} ----------------\n\n", SeqId, 
            LocalPos.vec3[0], LocalPos.vec3[1], LocalPos.vec3[2],
            NetPos.vec3[0], NetPos.vec3[1], NetPos.vec3[2]
        );
    }
    #ifndef _WIN32
    if (bUseUi)
    {
        InputSystem.Stop();
    }
    #endif

    DeinitPMO(GameWorld);
}
