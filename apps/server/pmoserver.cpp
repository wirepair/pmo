#include <flecs.h>
#include <memory>
#include <chrono>
#include <thread>
#include <iostream>
#include <vector>
#include <tuple>

#include <cxxopts.hpp>
#include "apps/shared/ctrlc/ctrlc.h"
#include "logger/spdlogger.h"
#include "spdlog/stopwatch.h"

#include "ecs/init_server.h"
#include "ecs/network/network_server.h"

int main(int argc, char *argv[]) 
{
    using std::chrono::high_resolution_clock;
    using std::chrono::milliseconds;
    using std::chrono::duration;

    cxxopts::Options options("pmoserver", "Runs the PMO Server");

    options.add_options()
        ("l,log", "Logfile name", cxxopts::value<std::string>()->default_value("server.log"))
        ("v,verbosity", "Set log verbosity level (debug, info, warn, error)", cxxopts::value<std::string>()->default_value("debug"))
        ("a,assets", "Path to assets", cxxopts::value<std::string>()->default_value("../../assets"))
        ("m,map", "Map to load for this server", cxxopts::value<std::string>()->default_value("Level1"))
        ("i,ip",  "Server IP address to listen on", cxxopts::value<std::string>()->default_value("0.0.0.0"))
        ("p,port", "Server port to listen on", cxxopts::value<int>()->default_value("4242"))
        ("h,help", "Print usage");

    auto Args = options.parse(argc, argv);

    if (Args.count("help"))
    {
        std::cout << options.help() << std::endl;
        exit(0);
    }

    bool bIsRunning = true;
    CtrlCLibrary::SetCtrlCHandler([&bIsRunning](enum CtrlCLibrary::CtrlSignal event) -> bool 
    {
        switch (event) 
        {
            case CtrlCLibrary::kCtrlCSignal:
                std::cout << "Caught Ctrl+C" << std::endl;
                bIsRunning = false;
                return true;
        }
        return false;
    });

    auto Config = config::Config(
        Args["log"].as<std::string>().c_str(), 
        Args["assets"].as<std::string>().c_str(), 
        Args["map"].as<std::string>().c_str(), 
        Args["ip"].as<std::string>().c_str(),
        Args["port"].as<int>(),
        nullptr
    );
    
    flecs::world GameWorld;
    logging::SpdLogger Log("server", Config.GetLogFile());

    if (!InitPMOServer(GameWorld, Log, Config))
    {
        Log.Error("Failed to initialize PMO System, exiting");
        return -1;
    }

    Log.Info("Starting server loop...");
    
    int i = 0;
    auto Start = high_resolution_clock::now();
    while(GameWorld.progress() && bIsRunning)
    {
        auto End = high_resolution_clock::now();
        
        // Diff how long GameWorld.progress takes
        duration<double, std::milli> Diff = Start - End;
        
        Diff = End - Start;
        Log.Debug("------------------------Tick: {} {}ms----------------------------\n\n", i++%24, Diff.count());
        Start = high_resolution_clock::now();
    }


    DeinitPMO(GameWorld);
}
