#include <flecs.h>
#include <thread>
#include <atomic>
#include <iostream>
#include <random>
#include <vector>
#include <tuple>

#include <cxxopts.hpp>
#include "apps/shared/ctrlc/ctrlc.h"

#include "logger/spdlogger.h"

#include "crypto/crypto.h"
#include "loadclient.h"
#include "containers/lockfreequeue.h"
#include "schemas/messages.h"
#include "config/config.h"

#include "ecs/init.h"
#include "ecs/physics/physics.h"
#include "ecs/world.h"


int main(int argc, char *argv[]) 
{
    cxxopts::Options options("pmoclient", "Runs the PMO headless client for testing");

    options.add_options()
        ("l,log", "Logfile name", cxxopts::value<std::string>()->default_value("client.log"))
        ("v,verbosity", "Set log verbosity level (debug, info, warn, error)", cxxopts::value<std::string>()->default_value("debug"))
        ("a,assets", "Path to assets", cxxopts::value<std::string>()->default_value("../../assets"))
        ("m,map", "Map to load for this server", cxxopts::value<std::string>()->default_value("Level1"))
        ("c,clients", "Number of mock clients", cxxopts::value<int>()->default_value("25"))
        ("i,ip",  "Server IP Address", cxxopts::value<std::string>()->default_value("0.0.0.0"))
        ("p,port", "Server Port", cxxopts::value<int>()->default_value("4242"))
        ("u,ui", "Run with capturing ui", cxxopts::value<bool>()->default_value("false"))
        ("h,help", "Print usage");

    auto Args = options.parse(argc, argv);

    if (Args.count("help"))
    {
        std::cout << options.help() << std::endl;
        exit(0);
    }

    bool bIsRunning = true;
    CtrlCLibrary::SetCtrlCHandler([&bIsRunning](enum CtrlCLibrary::CtrlSignal event) -> bool 
    {
        switch (event) 
        {
        case CtrlCLibrary::kCtrlCSignal:
            std::cout << "Caught Ctrl+C" << std::endl;
            bIsRunning = false;
            return true;
        }
        return false;
    });
    
    auto Config = config::Config(Args["log"].as<std::string>().c_str(), 
        Args["assets"].as<std::string>().c_str(), 
        Args["map"].as<std::string>().c_str(), 
        Args["ip"].as<std::string>().c_str(),
        Args["port"].as<int>(),
        nullptr
    );

    logging::SpdLogger Log("load", Config.GetLogFile());

    flecs::world GameWorld;

    auto NumClients = Args["clients"].as<int>();
    auto Crypto = std::make_shared<crypto::Cryptor>(Log);

    std::vector<std::unique_ptr<load::LoadClient>>ClientThreads;
    auto ServerSocket = std::make_shared<net::Socket>(Config.GetServerAddr(), Config.GetServerPort(), Log);

    for (int i = 0; i < NumClients; ++i)
    {
        // Generate random user id for now
        std::random_device Rand;
        std::mt19937 Gen(Rand());
        std::uniform_int_distribution<uint32_t> Distrib(100000, 9999999);
        uint32_t UserId = Distrib(Gen);
        Log.Info("Created UserID {}", UserId);

        // Build our clients
        auto InQueue = std::make_shared<net::PacketQueue>();
        auto OutQueue = std::make_shared<net::PacketQueue>();
        
        auto ClientThread = std::make_unique<net::Client>(InQueue, OutQueue, Crypto, ServerSocket, Log);

        Log.Info("Client thread starting...");
        ClientThread->SetUserId(UserId);
        ClientThread->Start();
        ClientThreads.emplace_back(std::make_unique<load::LoadClient>(UserId, InQueue, OutQueue, std::move(ClientThread)));

    }    
    
    // Use flecs just to setup a timer :>

    GameWorld.set_target_fps(30.f);
    Log.Info("Starting client loop...");

    while(GameWorld.progress() && bIsRunning)
    {
        for (auto& Client : ClientThreads)
        {
            if (!Client->Connected())
            {
                Client->Auth();
            }
            else
            {
                // Process reliable packets if any
                auto Process = std::make_unique<GameMessage>(Client->UserId, InternalMessage::ProcessReliableSocket);
                Process->Time = GameWorld.delta_time();
                Client->Out()->Push(std::move(Process));
                
                // then call move
                Client->SendInputs();
            }

            for (auto GameData = Client->In()->Pop(); GameData != std::nullopt; GameData = Client->In()->Pop())
            {
                if (GameData->get()->InternalType != Game::Message::InternalMessage::Socket)
                {
                    switch(GameData->get()->InternalType)
                    {
                        case Game::Message::InternalMessage::NewUser:
                        {
                            if (!Client->Connected())
                            {
                                Client->SetConnected(true);
                                Log.Info("We are now connected.");
                            }

                            break;
                        }
                        default:
                        {
                            Log.Warn("Unknown message type returned");
                            break;
                        }
                    }
                }
                switch(GameData->get()->MessageType)
                {
                    case Game::Message::MessageData_EncServerCommand:
                    {
                        auto Command = Game::Message::GetServerCommand(GameData->get()->Data->data());
                        if (!Command)
                        {
                            Log.Warn("Failed to decode command!");
                            break;
                        }
                        Client->SetSeqId(Command->sequence_id());
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                Game::Message::ReleaseGameMessage(std::move(GameData.value()));
            }
        }
    }

    for (auto& Client : ClientThreads)
    {
        Client->Stop();
    }

}
