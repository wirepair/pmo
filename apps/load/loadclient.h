#pragma once

#include "net/client.h"
#include "crypto/keypair.h"
#include "net/reliable_peer.h"
#include "net/reliable_endpoint.h"
#include "ecs/network/network.h"
#include "input/input.h"

namespace load
{
    class LoadClient
    {
    public:
        
        LoadClient(const uint32_t UserId, net::PacketQueueSharedPtr InQueue, net::PacketQueueSharedPtr OutQueue, std::unique_ptr<net::Client> Client) : UserId(UserId), InQueue(InQueue), OutQueue(OutQueue), Client(std::move(Client))
        {};

        net::PacketQueueSharedPtr In() { return InQueue; } 
        net::PacketQueueSharedPtr Out() { return OutQueue; }

        void SetConnected(bool bIsConnected)
        {
            bConnected = bIsConnected;
        }

        void SetSeqId(uint64_t SeqId)
        {
            ServerSequenceId = SeqId;
        }

        void Auth() { Client->Authenticate(); }

        bool Connected() { return bConnected; }

        void Stop() { Client->Stop(); }

        void SendInputs()
        {            
            input::Input KeyPress{};
            KeyPress.Press(input::Forward);
            auto Input = network::Inputs{KeyPress, 0, 0, 0};

            flatbuffers::FlatBufferBuilder Builder(512);
            auto SendInputBuffer = Game::Message::CreateInputs(Builder, Input.KeyPresses.KeyPresses, Input.PitchAngle, Input.YawAngle);
            
            ClientCommandBuilder CmdBuilder(Builder);
            CmdBuilder.add_server_seq_id_ack(ServerSequenceId);
            CmdBuilder.add_inputs(SendInputBuffer.o);
            CmdBuilder.add_opcode(ClientOpCode_Input);
            Builder.Finish(CmdBuilder.Finish());

            auto OutputLen = Builder.GetSize();
            auto ClientCommands = std::make_unique<std::vector<uint8_t>>(OutputLen);

            std::memcpy(ClientCommands->data(), Builder.GetBufferPointer(), Builder.GetSize());
            Builder.Release();
            
            auto CommandMessage = std::make_unique<Game::Message::GameMessage>(UserId, std::move(ClientCommands), MessageData_EncClientCommand);
            OutQueue->Push(std::move(CommandMessage));
        }

        uint8_t ServerSequenceId = 0;
        uint32_t UserId = 0;
        net::PacketQueueSharedPtr InQueue; 
        net::PacketQueueSharedPtr OutQueue;
        std::unique_ptr<net::Client> Client;
        bool bConnected = false;
    };
}