## PMO Network Movement


### Client

```mermaid
sequenceDiagram
    KeyInput->>InputsComponent: Press Key + Magnitude
    InputsComponent-->OnNewInputs: OnSet Event
    OnNewInputs->>InputsHistoryComponent: Push InputsComponent
    OnNewInputs-->UpdateVelocity: Read/Apply Inputs
```


### Server


