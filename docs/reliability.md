### PMO Reliability


```mermaid
sequenceDiagram
    Client->>ClientECS: Authenticate
    ClientECS->>ClientSocket: GenKeyRequest
    ClientSocket->>ServerSocket: GenKeyRequest Msg
    ServerSocket->>ServerSocket: GenKeyResponse, Create Endpoint
    ServerSocket->>Server: Create Entity
    Server->>ServerECS: Send GenKeyResponse
    ServerECS->>ServerSocket: Send GenKeyResponse
    ServerSocket->>ClientSocket: GenKeyResponse
    ClientSocket->>ClientSocket: Add Key To Endpoint
    ClientSocket->>Client: User Logged In
    Client->>ClientECS: remove AuthComponent, add ConnectComponent
    ClientECS->>ClientSocket: Send Inputs
    ClientSocket->>ServerSocket: Process EncClientCommand
    ServerSocket->>Server: Process ClientCommand
    Server->>ServerECS: remove AuthComponent, add ConnectComponent
```

