#pragma once

#include <vector>
#include "curvekey.h"

namespace curves
{
    class Curve
    {
    public:
        /**
         * @brief Construct a new Curve object, for now only supports linear interpolation
         * 
         */
        Curve() {};
        
        /**
         * @brief Returns the total curve animation time
         * 
         * @return float 
         */
        float RunTime() const;

        /**
         * @brief Assumes the Key.Time is > last Key inserted.
         * 
         * @param Key 
         */
        void InsertOrderedKey(CurveKey Key);

        /**
         * @brief Given an input time, return the lerp'd value by searching
         * through our vector of Keys.
         * 
         * @param Time 
         * @return units::vector3 
         */
        JPH::Vec3 Eval(float Time) const;

    private:
        std::vector<CurveKey> Keys{};
        float TotalCurveTime{};
    };
}