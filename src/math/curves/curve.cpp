#include "curve.h"
#include <cstdint>
#include "math/math.h"
#include "Jolt/Jolt.h"

namespace curves
{
    void Curve::InsertOrderedKey(CurveKey Key)
    {
        Keys.emplace_back(Key);
        TotalCurveTime = Key.Time; 
    }

    float Curve::RunTime() const
    {
        return TotalCurveTime;
    }

    // Adapted/Simplified from UE5 RichCurve.cpp
    JPH::Vec3 Curve::Eval(float InTime) const 
    {
        const uint32_t NumKeys = Keys.size();
        JPH::Vec3 Result{};
        if (NumKeys == 0)
        {
            return JPH::Vec3::sZero();
        }

        else if (NumKeys < 2 || InTime < Keys[0].Time)
        {
            float DT = Keys[1].Time - Keys[0].Time;
            if (math::IsNearlyZero(DT))
            {
                Result = Keys[0].Value;
            }
            else
            {
                JPH::Vec3 DV = Keys[1].Value - Keys[0].Value;
                auto Slope = DV / DT;
                Result = Slope * (InTime - Keys[0].Time) + Keys[0].Value;
            }
        }
        else if (InTime <= Keys[NumKeys - 1].Time)
        {
            uint32_t First = 1;
            const uint32_t Last = NumKeys - 1;
            uint32_t Count = Last - First;

            while(Count > 0)
            {
                const uint32_t Step = Count / 2;
                const uint32_t Middle = First + Step;
                if (InTime >= Keys[Middle].Time)
                {
                    First = Middle + 1;
                    Count -= Step + 1;
                }
                else
                {
                    Count = Step;
                }
            }
            // Do Lerp
            JPH::Vec3 TimeVec{InTime, InTime, InTime};
            JPH::Vec3 From = Keys[First - 1].Value;
            JPH::Vec3 To = Keys[First].Value;
            Result = To - From;
            Result = TimeVec * Result;
            Result += From;
        }
        else
        {
            float DT = Keys[NumKeys - 2].Time - Keys[NumKeys - 1].Time;
            if (math::IsNearlyZero(DT))
            {
                Result = Keys[NumKeys - 1].Value;
            }
            else
            {
                JPH::Vec3 DV = Keys[NumKeys - 2].Value - Keys[NumKeys - 1].Value;
                JPH::Vec3 Slope = DV / DT;

                Result = Slope * (InTime - Keys[NumKeys - 1].Time) + Keys[NumKeys - 1].Value;
            }
        }
        return Result;
    }
}