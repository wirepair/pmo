#pragma once 
#include <Jolt/Jolt.h>

namespace curves
{   
    /**
     * @brief Represents a key in a curve, for now default to linear interpolation between key
     * 
     */
    struct CurveKey
    {
        CurveKey(float Time, JPH::Vec3 Value) : Time(Time), Value(Value) {};
        
        float Time{};
        JPH::Vec3 Value{};

        bool operator==(const CurveKey& Other) const { return Other.Time == Time && Other.Value == Value; };
        bool operator!=(const CurveKey& Other) const { return !(*this == Other); };
    };
}