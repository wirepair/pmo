#pragma once

#include <cmath>

namespace math
{
    const double PMO_SMALL_NUMBER (1.e-8); // taken from UE engine for IsNearlyZero check
    inline bool IsNearlyZero(float Value)
    {
        return std::abs(Value) <= PMO_SMALL_NUMBER;
    };
    
    inline bool IsNearlyEqual(float A, float B)
    {
        return std::fabs(A - B) <= ( (fabs(A) < fabs(B) ? fabs(B) : fabs(A)) * PMO_SMALL_NUMBER);
    }
}