#include "world.h"

#include "ecs/character/components.h"
#include "ecs/combat/combat.h"
#include "ecs/network/network.h"
#include "ecs/character/network.h"
#include "ecs/animations/animation.h"

namespace world
{


    void PMOWorld::Init(const char *MapName, flecs::entity ClientPlayer, logging::Logger *Logger) 
    {
        Map = World.lookup(MapName);
        PhysicsSystem = World.get_mut<physics::Physics>()->System;
        Player = ClientPlayer;
        Log = Logger;
    }

    void PMOWorld::ProcessRespawnFailed(const Game::Message::ClientOpCode OpCode)
    {
        if (Player.has<combat::RequestRespawn>())
        {
            Log->Error("failed to respawn player");
        }
        else
        {
            Log->Error("got failed request respawn message but player doesn't have combat::RequestRespawn");
        }
    }

    void PMOWorld::ProcessServerUpdate(const Game::Message::ServerCommand *Command)
    {

        //Log->Debug("ProcessServerCommand ServerSeqId: {}", Command->sequence_id());

        if (Command->state() == nullptr)
        {
            Log->Error("error state was null");
            return;
        }

        Player.world().defer_begin();
        
        // Update Local player
        UpdateCurrentPlayer(Command->state(), Command->sequence_id(), Command->server_time());

        // Add Events
        if (Command->state()->in_entities() != nullptr)
        {
            Log->Info("PreFrame: Adding {} new entities for SeqId: {}",Command->state()->in_entities()->size(), Command->sequence_id());
            for (auto It = Command->state()->in_entities()->begin(); It != Command->state()->in_entities()->end(); ++It)
            {
                auto Entity = *It;
                switch(Entity->type())
                {
                    case Game::Message::EntityType_Player:
                        AddNetworkPlayer(Entity);
                        break;
                    case Game::Message::EntityType_NPC:
                        break;
                    case Game::Message::EntityType_Structure:
                        break;
                    case Game::Message::EntityType_Item:
                        break;
                    default:
                        break;
                }
            }
        } 

        // Update Events
        if (Command->state()->updated_entities() != nullptr)
        {
            Log->Info("PreFrame: Updating {} new entities for SeqId: {}",Command->state()->updated_entities()->size(), Command->sequence_id());
            for (auto It = Command->state()->updated_entities()->begin(); It != Command->state()->updated_entities()->end(); ++It)
            {
                auto Entity = *It;
                switch(Entity->type())
                {
                    case Game::Message::EntityType_Player:
                        UpdateNetworkPlayer(Entity);
                        break;
                    case Game::Message::EntityType_NPC:
                        break;
                    case Game::Message::EntityType_Structure:
                        break;
                    case Game::Message::EntityType_Item:
                        break;
                    default:
                        break;
                }
            }
        }

        // Delete Events
        if (Command->state()->out_entities() != nullptr)
        {
            for (auto It = Command->state()->out_entities()->begin(); It != Command->state()->out_entities()->end(); ++It)
            {
                auto Entity = *It;
                Log->Info("PreFrame: Deleting player {} from users' world.", Entity);
                World.delete_with(static_cast<flecs::id_t>(Entity));
            }
        }
        Player.world().defer_end();
    }

    void PMOWorld::UpdateCurrentPlayer(const Game::Message::State *State, const uint8_t SequenceId, const uint64_t ServerTime)
    {
        // Update our last known server state ack id, to be sent in next input packet
        Player.set<network::ServerStateSequenceId>({SequenceId});
        auto ServerState = Player.get_mut<network::ServerStateBuffer>();
        auto ClientState = Player.get<network::ClientStateBuffer>();

        ServerState->State.SequenceId = SequenceId;

        ServerState->CalculateSequenceDistance(ClientState->State.SequenceId);

        // Update our character where the server thinks we are so we can validate, only if it's changed
        if (State->pos() != nullptr)
        {
            units::Vector3 Position{State->pos()->x(), State->pos()->y(), State->pos()->z()};
            Player.set<network::ServerPosition>({Position});
            ServerState->State.AddPosition(Position);
            Log->Debug("PreFrame: Server Sees client at: {} {} {} for SeqId: {}", Position.vec3[0], Position.vec3[1], Position.vec3[2], SequenceId);
        }
        else
        {
            auto LastPosition = ServerState->State.PositionAt(ServerState->State.LastSequenceId());
            Log->Debug("PreFrame: Last Server Sees client at: {} {} {} for SeqId: {}", LastPosition.vec3[0], LastPosition.vec3[1], LastPosition.vec3[2], SequenceId);
            ServerState->State.AddLastPosition();
        }

        // Update our character where the server thinks we are so we can validate, only if it's changed
        if (State->rot() != nullptr)
        {
            units::Quat Rotation{State->rot()->x(), State->rot()->y(), State->rot()->z(), State->rot()->w()};
            Player.set<network::ServerRotation>({Rotation});
            ServerState->State.AddRotation(Rotation);
            //Log->Debug("Server Sees client rot at: {} {} {} {} for SeqId: {}", Rotation.vec4[0], Rotation.vec4[1], Rotation.vec4[2], Rotation.vec4[3], Command->sequence_id());
        }
        else
        {
            ServerState->State.AddLastRotation();
        }

        if (State->vel() != nullptr)
        {
            units::Velocity Velocity{State->vel()->x(), State->vel()->y(), State->vel()->z()};
            Player.set<network::ServerVelocity>({Velocity});
            ServerState->State.AddVelocity(Velocity);
        }
        else
        {
            ServerState->State.AddLastVelocity();
        }

        // determine if server and client mismatched
        uint8_t DesynchedAt = (ClientState->State.SequenceId + 24 - ServerState->SequenceDistance) % 24;
        auto PastPos = ClientState->State.PositionAt(DesynchedAt);
        auto PastRot = ClientState->State.RotationAt(DesynchedAt);
        auto PastVel = ClientState->State.VelocityAt(DesynchedAt);

        auto ServerPos = ServerState->State.PositionAt(SequenceId);
        auto ServerRot = ServerState->State.RotationAt(SequenceId);
        auto ServerVel = ServerState->State.VelocityAt(SequenceId);
        if (ServerState->SequenceDistance > 6)
        {
            Log->Warn("PreFrame: DISTANCE > 6");
        }
        
        if (PastPos != ServerPos || PastRot != ServerRot || PastVel != ServerVel)
        {
            Log->Info("============ server =============");
            ServerState->State.Print(*Log);
            Log->Info("============ client =============");
            ClientState->State.Print(*Log);
            
            Log->Warn("PreFrame: Error server/client don't match (distance {}) (Serv: {} time {}, Client: {}) Client desynched at Seq {}", ServerState->SequenceDistance, ServerState->State.SequenceId, ServerTime, ClientState->State.SequenceId, DesynchedAt);
            Log->Warn("PreFrame: Desynched Client Pos: {} {} {}",
                PastPos.vec3[0], PastPos.vec3[1], PastPos.vec3[2]);
            Log->Warn("PreFrame: Desynched Server Pos: {} {} {}",
                ServerPos.vec3[0], ServerPos.vec3[1], ServerPos.vec3[2]);
            Player.set<network::PhysicsDesynced>({DesynchedAt});
        }  
        else
        {
            Log->Info("PreFrame: Server Pos and Past client Pos match (distance {}) (Serv: {} time {}, Client: {})!", ServerState->SequenceDistance, ServerState->State.SequenceId, ServerTime, ClientState->State.SequenceId);
        }

        const auto Equipment = State->equipment();
        if (Equipment != nullptr)
        {
            // size_t Idx = 0;
            // make a vector, add all equipment to it
            // then create a new items:SetNetEquipment component where UE5 can have an OnSet event
            // waiting for any equipment changes.
            // for (auto It = Equipment->begin(); It != Equipment->end(); ++It)
            // {
            //     auto Event = *It;
            //     Player.set<items::Equipment>
            // }
        }

        const auto Inventory = State->inventory();
        if (Inventory != nullptr)
        {
            // size_t Idx = 0;
            // make a vector, add all equipment to it
            // then create a new items:SetNetEquipment component where UE5 can have an OnSet event
            // waiting for any equipment changes.
            // for (auto It = Equipment->begin(); It != Equipment->end(); ++It)
            // {
            //     auto Event = *It;
            //     Player.set<items::Equipment>
            // }
        }
    

        const auto NetAttributes = State->attributes();
        if (NetAttributes != nullptr)
        {
            Player.set<units::Attributes>({
                .Health=NetAttributes->health(), 
                .Shield=NetAttributes->shield(),
                .Stamina=NetAttributes->stamina(),
                .Mana=NetAttributes->mana(),
            });
        }

        const auto NetTraits = State->traits();
        if (NetTraits != nullptr)
        {
            Player.set<units::TraitAttributes>({
                .Intelligence=NetTraits->intelligence(), 
                .Wisdom=NetTraits->wisdom(),
                .Dexterity=NetTraits->dexterity(),
                .Strength=NetTraits->strength(),
                .Constitution=NetTraits->constitution(),
            });
        }

        auto DamageEvents = State->damage_events();
        if (DamageEvents != nullptr)
        {
            for (auto It = DamageEvents->begin(); It != DamageEvents->end(); ++It)
            {
                auto Event = *It;
                Log->Info("PreFrame: (Player) DamageEvent: Target: {} Instigator: {} Amount: {}", Event->target(), Player.id(), Event->amount());
                World.entity().set<combat::NetDamage>({
                    .Instigator=Player,
                    .TargetHit=World.entity(Event->target()),
                    .Type=static_cast<combat::DamageClass>(Event->damage_class()),
                    .Amount=Event->amount(),
                });
            }
        }
    }

    void PMOWorld::AddNetworkPlayer(const Game::Message::Entity *Entity)
    {
        auto Pos = units::Vector3{Entity->player()->pos()->x(), Entity->player()->pos()->y(), Entity->player()->pos()->z()};
        auto Rot = units::Quat{Entity->player()->rot()->x(), Entity->player()->rot()->y(), Entity->player()->rot()->z(), Entity->player()->rot()->w()};
        auto Vel = units::Velocity{Entity->player()->vel()->x(), Entity->player()->vel()->y(), Entity->player()->vel()->z()};
        auto Attributes = units::Attributes{
            .Health=Entity->player()->attributes()->health(), 
            .Shield=Entity->player()->attributes()->shield(),
            .Stamina=Entity->player()->attributes()->stamina(),
            .Mana=Entity->player()->attributes()->mana(),
        };

        auto Traits = units::TraitAttributes{
            .Intelligence=Entity->player()->traits()->intelligence(), 
            .Wisdom=Entity->player()->traits()->wisdom(),
            .Dexterity=Entity->player()->traits()->dexterity(),
            .Strength=Entity->player()->traits()->strength(),
            .Constitution=Entity->player()->traits()->constitution(),
        };

        // use the servers flecs entity id so its easier to correlate across
        auto NewNetworkPlayer = World.make_alive(Entity->id());

        Log->Info("PreFrame: Player {}: Adding new player {} (NNP: {}, is_alive {}), client at: {} {} {}", Player.id(), Entity->id(), NewNetworkPlayer.id(), NewNetworkPlayer.is_alive(), Pos.vec3[0], Pos.vec3[1], Pos.vec3[2]);
        NewNetworkPlayer
            .set<units::Attributes>({Attributes})
            .set<units::TraitAttributes>({Traits})
            .set<units::Velocity>({Vel})
            .set<units::Vector3>({Pos})
            .set<units::Quat>({Rot})
            .set<character::NetworkCharacter>({PhysicsSystem, Pos, Rot, Entity->id()});
        
        NewNetworkPlayer.add(flecs::ChildOf, Map);
        NewNetworkPlayer.add(static_cast<character::ActionType>(Entity->player()->action()));
        NewNetworkPlayer.add(animations::AnimSet::SWORDSH); // todo get from equipped item
    }

    void PMOWorld::UpdateNetworkPlayer(const Game::Message::Entity *Entity)
    {
        flecs::entity LocalEntity = World.entity(Entity->id());

        if (!LocalEntity.is_valid() || !LocalEntity.is_alive())
        {
            Log->Warn("PreFrame: Player {}: PlayerEntity {} is not alive on client!", Player.id(), Entity->id());
            return;
        }

        if (Entity->player()->pos() != nullptr)
        {
            auto Pos = units::Vector3{Entity->player()->pos()->x(), Entity->player()->pos()->y(), Entity->player()->pos()->z()};
            LocalEntity.set<units::Vector3>({Pos});
            Log->Debug("PreFrame: Player {}: Updating player {}, client at: {} {} {}", Player.id(), Entity->id(), Pos.vec3[0], Pos.vec3[1], Pos.vec3[2]);
        }

        if (Entity->player()->rot() != nullptr)
        {
            auto Rot = units::Quat{Entity->player()->rot()->x(), Entity->player()->rot()->y(), Entity->player()->rot()->z(), Entity->player()->rot()->w()};
            LocalEntity.set<units::Quat>({Rot});
            Log->Debug("PreFrame: Updating player {}, client rot at: {} {} {}", Entity->id(), Rot.vec4[0], Rot.vec4[1], Rot.vec4[2], Rot.vec4[3]);
        }

        if (Entity->player()->vel() != nullptr)
        {
            auto Vel = units::Velocity{Entity->player()->vel()->x(), Entity->player()->vel()->y(), Entity->player()->vel()->z()};
            LocalEntity.set<units::Velocity>({Vel});
            Log->Debug("PreFrame: Updating player {}, client vel at: {} {} {}", Entity->id(), Vel.vec3[0], Vel.vec3[1], Vel.vec3[2]);
        }

        LocalEntity.add(static_cast<character::ActionType>(Entity->player()->action()));

        auto UpdateAttributes = Entity->player()->attributes();
        if (UpdateAttributes != nullptr)
        {
            auto NewAttributes = units::Attributes{
                .Health=UpdateAttributes->health(),
                .Shield=UpdateAttributes->shield(),
                .Stamina=UpdateAttributes->stamina(),
                .Mana=UpdateAttributes->mana(),
            };
            
            LocalEntity.set<units::Attributes>({NewAttributes});
        }

        auto UpdateTraits = Entity->player()->traits();
        if (UpdateTraits != nullptr)
        {
            auto NewTraits = units::TraitAttributes{
                .Intelligence=UpdateTraits->intelligence(),
                .Wisdom=UpdateTraits->wisdom(),
                .Dexterity=UpdateTraits->dexterity(),
                .Strength=UpdateTraits->strength(),
                .Constitution=UpdateTraits->constitution(),
            };

            LocalEntity.set<units::TraitAttributes>({NewTraits});
        }

        auto DamageEvents = Entity->player()->damage_events();
        if (DamageEvents != nullptr)
        {
            for (auto It = DamageEvents->begin(); It != DamageEvents->end(); ++It)
            {
                auto Event = *It;
                Log->Info("PreFrame: (Other) DamageEvent: Target: {} Instigator: {} Amount: {}", Event->target(), LocalEntity.id(), Event->amount());
                World.entity().set<combat::NetDamage>({
                    .Instigator=LocalEntity,
                    .TargetHit=World.entity(Event->target()),
                    .Type=static_cast<combat::DamageClass>(Event->damage_class()),
                    .Amount=Event->amount(),
                });
            }
        }
    }
}