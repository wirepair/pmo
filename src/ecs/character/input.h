#pragma once

#include <flecs.h>
#include "ecs/character/pmocharacter.h"
#include "ecs/network/network.h"
#include "logger/logger.h"
#include "config/config.h"

namespace character
{
    
    /**
     * @brief Processes input for both server and clients since both use PMOCharactersa
     * 
     * @param GameWOrld 
     * @param Log 
     * @param Config 
     */
    void RegisterInputSystem(flecs::world &GameWorld, logging::Logger *Log, config::Config *Config);

    void ProcessUserInput(logging::Logger *Log, character::PMOCharacter &Character, network::Inputs &Input);
}