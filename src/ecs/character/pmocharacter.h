#pragma once

#include <Jolt/Jolt.h>

#include "constants.h"
#include "components.h"
#include "controllable.h"
#include "ecs/units.h"
#include "ecs/animations/animdata.h"
#include "logger/logger.h"

namespace character
{
    struct PMOCharacter
    {
        /**
         * @brief HandleMovementInput takes user input and updates how the character should move in the physics engine
         * 
         * @param DeltaTime 
         * @param InMovementDirection 
         * @param InRotation 
         * @param bIsJumping 
         */
        void HandleMovementInput(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, JPH::Quat InRotation, bool bIsMovingBackwards, bool bIsJumping) const;

        /**
         * @brief Like Movement, but handles action based inputs (attacks/abilities etc)
         * 
         * @param DeltaTime 
         * @param Log 
         * @param Inputs 
         */
        void HandleActionInput(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, const JPH::Quat InRotation, input::Input &Inputs);

        /**
         * @brief Gets the anim data for applying root motion, and checks to see if we can transition to other states
         * 
         * @param Log 
         * @param DeltaTime 
         * @param DirectionVector 
         * @param InRotation 
         * @param AttackType 
         * @return true 
         * @return false 
         */
        bool HandleActionAnimations(logging::Logger *Log, JPH::Vec3Arg &DirectionVector, const JPH::Quat &InRotation, character::ActionType &AttackType, flecs::entity &CurrentAnimPrefab) const;

        /**
         * @brief Handles Dodges, sets flecs components on the character so we can track movement direction and which animations root motion to apply
         * 
         * @param Log 
         * @param DeltaTime 
         * @param InMovementDirection 
         * @param InRotation 
         * @param Inputs 
         * @param AttackType 
         * @param CurrentAnimPrefab 
         */
        void HandleDodge(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, const JPH::Quat &InRotation, input::Input &Inputs,  character::ActionType &AttackType, flecs::entity &CurrentAnimPrefab) const;

        /**
         * @brief Returns true if attacking
         * 
         * @return true 
         * @return false 
         */
        inline bool IsAttacking(character::ActionType Action) const;

        /**
         * @brief Returns true if attacking
         * 
         * @return true 
         * @return false 
         */
        inline bool IsDodging(character::ActionType Action) const;
        
        /**
         * @brief Does some checks (is valid and ActionType::NOOP) to see if they can execute some action based off input
         * 
         * @param bIsInterruptable can what they are doing be interrupted (e.g. an attack they can transition out)
         * @return true 
         * @return false 
         */
        bool IsAbleToDoAction(const bool bIsInterruptable) const;

        /**
         * @brief Removes any active animation if AnimPrefab is valid.
         * 
         * @param CurrentAnimPrefab 
         */
        void RemoveActiveAnimation(logging::Logger *Log, flecs::entity &CurrentAnimPrefab) const;

        /**
         * @brief Equips an item
         * 
         * @param Slot 
         * @param ItemId 
         * @param ContainerId 
         * @return true 
         * @return false 
         */
        bool EquipItem(uint8_t Slot, flecs::id ItemId, uint8_t ContainerId);

        float GetCapsuleRadius() const;

        /**
         * @brief Set the Location of our character
         * 
         * @param NewLocation 
         */
        void SetLocation(const units::Vector3 &NewLocation);

        /**
         * @brief Set the Location of our character
         * 
         * @param NewLocation 
         */
        void SetRotation(const units::Quat &NewRotation);

        /**
         * @brief Set the Velocity of our character
         * 
         * @param NewLocation 
         */
        void SetVelocity(const units::Velocity &NewVelocity);

        /**
         * @brief Get the Position in units
         * 
         * @return units::Vector3 
         */
        units::Vector3 GetPosition() const;

        /**
         * @brief Get the Rotation in units
         * 
         * @return units::Quat 
         */
        units::Quat GetRotation() const;

        /**
         * @brief Get the Forward Vector in units
         * 
         * @return units::Vector3 
         */
        units::Vector3 GetForwardVector() const;

        /**
         * @brief Get the Forward & Right Vector in units
         * 
         * @return units::Vector3 
         */
        units::Vector3 GetForwardRightVector() const;

        /**
         * @brief Get the Velocity in units
         * 
         * @return units::Velocity 
         */
        units::Velocity GetVelocity() const;

        /**
         * @brief Get the Ground Velocity in units
         * 
         * @return units::Velocity 
         */
        units::Velocity GetGroundVelocity() const;

        /**
         * @brief Get the Jolt Forward Vector
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetJoltForwardVector() const;

        /**
         * @brief Get the Jolt Right Vector
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetJoltRightVector() const;

        /**
         * @brief Get the Jolt Forward & Right Vector
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetJoltForwardRightVector() const;

        /**
         * @brief Get the Cos Max Slope Angle
         * 
         * @return float 
         */
        float GetCosMaxSlopeAngle() const;
        
        /**
         * @brief Get the Ground State of the character (InAir, OnGround etc)
         * 
         * @return GroundState 
         */
        GroundState GetGroundState() const;

        /**
         * @brief Runs pre physics updates (e.g, called before Physics->Update())
         * 
         * @param Alloc 
         */
        void PrePhysicsUpdate(JPH::TempAllocatorImpl *Alloc) const;

        /**
         * @brief Used to debug draw the capsule
         * 
         * @param DebugRenderer 
         */
        void DebugDraw(debug::JoltDebugRenderer *DebugRenderer) const;
        
        /**
         * @brief Checks if the passed in Body matches our CharacterCapsule Jolt Body
         * 
         * @param Body 
         * @return true 
         * @return false 
         */
        bool IsBody(const JPH::BodyID &Body) const;

        flecs::entity CharacterEntity;
        std::unique_ptr<ControllableCharacter> CharacterController = nullptr;
        flecs::query<animations::AnimData> AnimQuery;
        
    };
}