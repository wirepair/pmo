#include "input.h"
#include <chrono>

#include <Jolt/Jolt.h>
#include "math/math.h"
#include "pmocharacter.h"
#include "ecs/units.h"
#include "ecs/network/network.h"
#include "ecs/state.h"

namespace character
{
    void RegisterInputSystem(flecs::world &GameWorld, logging::Logger *Log, config::Config *Config)
    {
        // Handle player input on both clients & servers
        GameWorld.system<network::Inputs, character::PMOCharacter>("ProcessInput")
            .kind(flecs::OnLoad)
            .immediate(true)
            .write<character::ActionType>()
            .each([Log](flecs::iter& It, size_t Index, network::Inputs &Input, character::PMOCharacter &Character)
        {
            Log->Debug("PreUpdate: ProcessInput: Processing {}", Input.KeyPresses.KeyPresses);
            ProcessUserInput(Log, Character, Input);
            Input.Clear();
        });
    }

    void ProcessUserInput(logging::Logger *Log, character::PMOCharacter &Character, network::Inputs &Input)
    {
        #ifndef IS_SERVER
        auto ClientState = Character.CharacterEntity.get_mut<network::ClientStateBuffer>();
        ClientState->AddInput(Input);
        ClientState->AddDesiredVelocity(Character.CharacterController->DesiredVelocity);
        #endif

        auto ControlInput = JPH::Vec3::sZero();

        auto MovementYaw = JPH::DegreesToRadians(Input.YawAngle); 
        auto FacingYaw = 0.f;

        auto Fourty5Degrees = JPH::DegreesToRadians(45.f);
        auto NinetyDegrees = JPH::DegreesToRadians(90.f);

        if (Input.KeyPresses.IsPressed(input::InputType::Left))
        {
            ControlInput.SetZ(-1.0f);
            FacingYaw = JPH::DegreesToRadians(-90.f);
        }
        if (Input.KeyPresses.IsPressed(input::InputType::Right))
        {
            ControlInput.SetZ(1.0f);
            FacingYaw = JPH::DegreesToRadians(90.f);
        }

        if (Input.KeyPresses.IsPressed(input::InputType::Forward))
        {
            ControlInput.SetX(1.0f);
            
            if (math::IsNearlyEqual(FacingYaw, NinetyDegrees)) { FacingYaw = Fourty5Degrees;}
            if (math::IsNearlyEqual(FacingYaw, -NinetyDegrees)) { FacingYaw = -Fourty5Degrees;}
        }

        auto bIsMovingBackwards = Input.KeyPresses.IsPressed(input::InputType::Backward);
        auto bIsJumping = Input.KeyPresses.IsPressed(input::InputType::Jump);

        if (bIsMovingBackwards)
        {
            ControlInput.SetX(-1.0f);
            if (math::IsNearlyEqual(FacingYaw, NinetyDegrees)) { FacingYaw = Fourty5Degrees;}
            if (math::IsNearlyEqual(FacingYaw, -NinetyDegrees)) { FacingYaw = -Fourty5Degrees;}

            FacingYaw = -FacingYaw; // invert
        }

        if (ControlInput != JPH::Vec3::sZero())
        {
            ControlInput = ControlInput.Normalized();
            //Log->Info("UpdateVelocity: Move Normalized");
        }

        // Camera controls to multiply rotatation by direction
        auto CameraForward = JPH::Vec3::sZero();
        CameraForward.SetX(JPH::Cos(MovementYaw));
        CameraForward.SetY(0.f);
        CameraForward.SetZ(JPH::Sin(MovementYaw));
        CameraForward = CameraForward.NormalizedOr(JPH::Vec3::sAxisX());
        JPH::Quat CameraRot = Quat::sFromTo(JPH::Vec3::sAxisX(), CameraForward);
        
        ControlInput = CameraRot * ControlInput;
        auto CharFacingRot = JPH::Quat::sRotation(JPH::Vec3::sAxisY(), MovementYaw+FacingYaw);
        
        Log->Info("PreUpdate: ProcessInput: ControlInput x:{} y:{} z:{} *= (CameraRot: x:{} y:{} z:{} w:{} (Euler: x:{} y:{} z:{} ))",
            ControlInput.GetX(), ControlInput.GetY(), ControlInput.GetZ(),
            CameraRot.GetX(), CameraRot.GetY(), CameraRot.GetZ(), CameraRot.GetW(),
            CameraRot.GetEulerAngles().GetX(), CameraRot.GetEulerAngles().GetY(), CameraRot.GetEulerAngles().GetZ()
        );
        // DEBUG
        uint8_t SequenceId = 0;
        #ifdef IS_SERVER
        if (Character.CharacterEntity.has<state::PlayerWorldStates>())
        {
            SequenceId = Character.CharacterEntity.get<state::PlayerWorldStates>()->SequenceId;
        }
        #else
        if (Character.CharacterEntity.has<network::ServerStateSequenceId>())
        {
            SequenceId = Character.CharacterEntity.get<network::ServerStateSequenceId>()->SequenceId;
        }
        #endif
        auto EulerFacing = CharFacingRot.GetEulerAngles();
        Log->Info("PreUpdate: ProcessInput: SeqId: {} Yaw: {} Pitch: {} KeyPress: {} Calling HandleInput Input: Input: {} {} {} Rot: {} {} {} {} (Euler: x:{} y:{} z:{})", 
            SequenceId,
            Input.YawAngle,
            Input.PitchAngle,
            Input.KeyPresses.KeyPresses,
            ControlInput.GetX(), ControlInput.GetY(), ControlInput.GetZ(),
            CharFacingRot.GetX(), CharFacingRot.GetY(), CharFacingRot.GetZ(), CharFacingRot.GetW(),
            EulerFacing.GetX(), EulerFacing.GetY(), EulerFacing.GetZ()
        );
        Character.HandleMovementInput(Log, ControlInput, CharFacingRot, bIsMovingBackwards, bIsJumping);
        Character.HandleActionInput(Log, ControlInput, CharFacingRot, Input.KeyPresses);
    }
}