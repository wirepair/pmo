#include "pmocharacter.h"
#include "components.h"
#include "ecs/items/items.h"
#include "ecs/items/inventory.h"
#include "ecs/animations/animation.h"
#include "ecs/combat/combat.h"

namespace character
{
    void PMOCharacter::HandleMovementInput(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, JPH::Quat InRotation, bool bIsMovingBackwards, bool bIsJumping) const
    {
        auto ActType = character::ActionType::NOOP;

        if (bIsJumping && IsAbleToDoAction(false))
        {
            CharacterEntity.add(character::JUMPING);
        }

        if (CharacterEntity.has<character::ActionType>())
        {
            ActType = *CharacterEntity.get<character::ActionType>();
        }
        
        bool bShouldDampen = false;
        switch (ActType)
        {
            // disable movement for all cases
            case character::ActionType::KNOCKED_DOWN:
            case character::ActionType::PARRYING:
            case character::ActionType::DEAD:
            case character::ActionType::STUNNED:
            {
                CharacterController->PhysicsCharacter->SetLinearVelocity(JPH::Vec3::sZero());
                return;
            }
            // dampen movements for these
            case character::ActionType::BLOCKING:
            case character::ActionType::BOW_ATTACK:
            {
                bShouldDampen = true;
                break;
            }
            // otherwise do nothing
            default:
            break;
        }
        // Don't handle movement inputs if we are attacking / dodging
        if (IsDodging(ActType) || IsAttacking(ActType))
        {
            //CharacterController->PhysicsCharacter->SetLinearVelocity(JPH::Vec3::sZero());
            Log->Info("PreUpdate: HandleMovementInput: in attack/dodge not allowing movement or rotation");
            return;
        }
        
        // TODO we are going to need to set a tag/timer for hardcoding a velocity for certain melee attacks
        // lots of attack animations move the character forward, we want to stop velocity and then set a timer to have
        // the character move forward for the attack duration.
        MovementFlags Flags{false, bIsMovingBackwards, bIsJumping, bShouldDampen};
        CharacterController->HandleMovementInput(CharacterEntity, InMovementDirection, InRotation, Flags);
    }

    void PMOCharacter::HandleActionInput(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, JPH::Quat InRotation, input::Input &Inputs)
    {
        // Always allow clearing blocking
        if (!Inputs.IsPressed(input::MouseRight))
        {
            if (CharacterEntity.is_valid() && CharacterEntity.is_alive() && CharacterEntity.has<character::ActionType>())
            {   
                auto ActType = CharacterEntity.get<character::ActionType>();
                if (*ActType == character::ActionType::BLOCKING)
                {
                    CharacterEntity.add(character::ActionType::NOOP);
                }   
            }
        }
       
        flecs::entity CurrentAnimPrefab;
        auto DirectionVector = GetJoltForwardRightVector();
        auto ActionType = character::ActionType::SWORDSH_ATTACK_1; // TODO get this from equipped item(s)

        bool bCanTransitionNextAnimation = HandleActionAnimations(Log, DirectionVector, InRotation, ActionType, CurrentAnimPrefab);
        //Log->Info("PreUpdate: HandleActionInput: CanTransition: {} ActionType: {}", bCanTransitionNextAnimation, static_cast<uint8_t>(ActionType)); 

        // make sure they are not doing some action that would prevent them from doing another action
        if (!IsAbleToDoAction(bCanTransitionNextAnimation))
        {
            return;
        }

        // Base Attack
        if (Inputs.IsPressed(input::MouseLeft))
        {
            Log->Info("PreUpdate: HandleActionInput: attack pressed");
            // Now look up the new one
            auto World = CharacterEntity.world();
            auto AnimPrefab = AnimQuery.find([&](animations::AnimData &Anim)
            {
                return Anim.ActionType == ActionType;
            });

            if (!AnimPrefab)
            {
                Log->Warn("PreUpdate: HandleActionInput: failed to find animation prefab data");
                return;
            }

            // remove the old one as we are transitioning to a new attack
            RemoveActiveAnimation(Log, CurrentAnimPrefab);

            auto AnimData = AnimPrefab.get<animations::AnimData>();
            
            World.entity().set<combat::AttackDetails>({
                .Instigator=CharacterEntity, 
                .BaseDamage=100.f, 
                .RadiusOrLength=1.f, 
                .DelayStartTime=AnimData->AttackAtSeconds,
                .RunTime=AnimData->AnimCurve.RunTime(),
                .CanTransitionTime=AnimData->TransitionAtSeconds,
                .NumTargets=1,
                .AttackShape=combat::AttackContactShape::Sphere,
                .Type=combat::DamageClass::SLASH,
            });

            // Create an instance of the prefab to attach to our player
            float NewRunTime = 0.f;
            CharacterEntity.is_a(AnimPrefab)
                .set<animations::RunTime>({NewRunTime})
                //.set<animations::MovementRotationDirection>({DirectionVector, InRotation}) // lock our direction/rotation during animation
                .add(AnimData->ActionType);

            Log->Info("PreUpdate: HandleActionInput: Created combat attack! Ent: {}, Anim Attack: {} Has Anim: {}", CharacterEntity.id(), AnimData->Path, CharacterEntity.has<animations::AnimData>());
            
            CharacterController->ApplyRootMotion(CharacterEntity, DirectionVector, InRotation, AnimData->AnimCurve, NewRunTime);
        } 
        else if (Inputs.IsPressed(input::MouseRight))
        {
            RemoveActiveAnimation(Log, CurrentAnimPrefab);
            CharacterEntity.add(character::BLOCKING);
        }
        else if (Inputs.IsPressed(input::Dodge))
        {
            HandleDodge(Log, InMovementDirection, InRotation, Inputs, ActionType, CurrentAnimPrefab);
        }
    }

    bool PMOCharacter::IsAttacking(const character::ActionType Action) const
    {
        return (Action > character::ActionType::START_ATTACK && Action < character::ActionType::END_ATTACK);
    };

    bool PMOCharacter::IsDodging(const character::ActionType Action) const
    {
        return (Action > character::ActionType::START_DODGE && Action < character::ActionType::END_ATTACK);
    }

    void PMOCharacter::HandleDodge(logging::Logger *Log, JPH::Vec3Arg InMovementDirection, const JPH::Quat &InRotation, input::Input &Inputs,  character::ActionType &AttackType, flecs::entity &CurrentAnimPrefab) const
    {
        RemoveActiveAnimation(Log, CurrentAnimPrefab);
        JPH::Vec3 MovementDirection = InMovementDirection;

        bool bIsBackward = Inputs.IsPressed(input::Backward);
        bool bIsForward = Inputs.IsPressed(input::Forward);
        bool bIsRight = Inputs.IsPressed(input::Left);
        bool bIsLeft = Inputs.IsPressed(input::Right);

        Log->Info("PreUpdate: HandleDodge: Dodge Direction: {} {} {} back: {} fwd: {} right: {} left: {}", MovementDirection.GetX(), MovementDirection.GetY(), MovementDirection.GetZ(), 
            bIsBackward, bIsForward, bIsRight, bIsLeft);
        
        // If dodge was pressed but no direction was set, assume they want to dodge backwards
        if (!bIsBackward && !bIsForward && !bIsRight && !bIsLeft)
        {
            bIsBackward = true;
        }
        auto AnimSet = CharacterEntity.get<animations::AnimSet>();

        std::string DodgeLookup = "";
        character::ActionType DodgeAction = character::ActionType::DODGE_FORWARD;

        // TODO: Figure out how to do this without strings
        switch (*AnimSet)
        {
            case animations::AnimSet::SWORDSH:
            {
                DodgeLookup = "SSH/UE5_SSH_Dodge_01_";
                break;
            }
            default:
            {
                DodgeLookup = "SSH/UE5_SSH_Dodge_01_";
                break;
            }
        }

        // TODO Check if DirectionVector is perfectly forward, perfectly backward, perfectly left or perfectly right
        // Otherwise just apply backward/forward animation root motion across the direction vector
        if (bIsForward)
        {
            DodgeLookup += "FR_Seq.csv";
            DodgeAction = character::ActionType::DODGE_FORWARD;
        }
        else if (bIsBackward)
        {
            DodgeLookup += "BR_Seq.csv";
            DodgeAction = character::ActionType::DODGE_BACKWARD;
        }
        else if (bIsRight && !bIsForward && !bIsBackward)
        {
            DodgeLookup += "R_Seq.csv";
            DodgeAction = character::ActionType::DODGE_RIGHT;
        }
        else if (bIsLeft && !bIsForward && !bIsBackward)
        {
            DodgeLookup += "L_Seq.csv";
            DodgeAction = character::ActionType::DODGE_LEFT;
        }

        auto World = CharacterEntity.world();
        auto AnimPrefab = AnimQuery.find([&](animations::AnimData &Anim)
        {
            return Anim.ActionType == DodgeAction && Anim.Path == DodgeLookup;
        });

        if (!AnimPrefab)
        {
            Log->Warn("PreUpdate: HandleDodge: failed to find animation prefab data for dodge");
            return;
        }

        auto AnimData = AnimPrefab.get<animations::AnimData>();
        float NewRunTime = 0.f;
        CharacterEntity.is_a(AnimPrefab)
            .set<animations::RunTime>({NewRunTime})
            .set<animations::MovementRotationDirection>({MovementDirection, InRotation})
            .add(AnimData->ActionType);

        Log->Info("PreUpdate: HandleDodge: Created dodge! Ent: {}, Anim: {} Has Anim: {}", CharacterEntity.id(), AnimData->Path, CharacterEntity.has<animations::AnimData>());
        
        CharacterController->ApplyRootMotion(CharacterEntity, MovementDirection, InRotation, AnimData->AnimCurve, NewRunTime);
    }

    void PMOCharacter::RemoveActiveAnimation(logging::Logger *Log, flecs::entity &CurrentAnimPrefab) const
    {
        if (!CurrentAnimPrefab.is_valid())
        {
            return;
        }
        
        CharacterEntity
            .remove(flecs::IsA, CurrentAnimPrefab)
            .remove<animations::RunTime>()
            .remove<animations::MovementRotationDirection>()
            .add(character::ActionType::NOOP);
        
        Log->Info("PreUpdate: RemoveActiveAnimation: Removed CurrentAnimPrefab");
    }

    bool PMOCharacter::HandleActionAnimations(logging::Logger *Log, const JPH::Vec3 &DirectionVector, const JPH::Quat &InRotation, character::ActionType &OutAnimType, flecs::entity &CurrentAnimPrefab) const
    {
        bool bCharacterInAnimation = CharacterEntity.has<animations::AnimData>();
        if (!bCharacterInAnimation)
        {
            return true;
        }
        JPH::Vec3 Direction = DirectionVector;
        JPH::Quat Rotation = InRotation;

        auto Action = CharacterEntity.get<character::ActionType>();
        bool bCharacterIsAttacking = IsAttacking(*Action);
        bool bCharacterIsDodging = IsDodging(*Action);

        // If we are in an animation that has a direction (dodge/roll) we need to extract that and override our DirectionVector
        if (CharacterEntity.has<animations::MovementRotationDirection>())
        {
            auto MoveRot = CharacterEntity.get<animations::MovementRotationDirection>();
            Direction = MoveRot->Direction;
            Rotation = MoveRot->Rotation;
        }

        bool bCanTransitionNextAction = false;
        auto AnimData = CharacterEntity.get<animations::AnimData>();
        // Get ref to prefab so we can remove the relationship, or tell the caller which anim to remove (you can't remove AnimData directly)
        CurrentAnimPrefab = CharacterEntity.world().lookup(AnimData->Path.c_str());
        auto AnimRunTime = CharacterEntity.get_mut<animations::RunTime>();
        Log->Info("PreUpdate: HandleActionInput: Got AnimRunTime {}", (AnimRunTime != nullptr));
        float CurrentAnimRunTime = AnimRunTime->Time;
        OutAnimType = AnimData->ActionType; // so we increment the value properly

        Log->Info("PreUpdate: HandleActionInput: bCharacterInAnimation combat attack! AnimRunTime: {}, Anim: {} AnimTotalRunTime: {}",
            AnimRunTime->Time, AnimData->Path, AnimData->AnimCurve.RunTime());
        
        if (AnimRunTime->Time >= AnimData->AnimCurve.RunTime())
        {
            Log->Info("PreUpdate: HandleActionInput: Removing AnimData/RunTime"); 
            bCanTransitionNextAction = true;
            
            RemoveActiveAnimation(Log, CurrentAnimPrefab);
            
            OutAnimType = character::ActionType::SWORDSH_ATTACK_1;
        }
        else if (AnimRunTime->Time >= AnimData->TransitionAtSeconds)
        {
            Log->Info("PreUpdate: HandleActionInput: Can Transition time reached: {} {}",AnimRunTime->Time, AnimData->TransitionAtSeconds);
            if (AnimData->CanTransitionTo.size() > 0 && bCharacterIsAttacking)
            {
                // TODO handle if player wants to transition to secondary attack
                OutAnimType = static_cast<character::ActionType>(static_cast<uint8_t>(OutAnimType)+1);
                bCanTransitionNextAction = true;
                Log->Info("PreUpdate: HandleActionInput: OutAnimType: {}", static_cast<uint8_t>(OutAnimType));   
            }
            else if (bCharacterIsAttacking)
            {
                // TODO: figure out how to cycle back to the first attack sequence
                OutAnimType = character::ActionType::SWORDSH_ATTACK_1;
                bCanTransitionNextAction = false;
            }
            else if (bCharacterIsDodging)
            {
                // allow some fudge time to go from dodge to block, 
                // no need to play the whole anim as the last .10s are unnecessary
                bCanTransitionNextAction = true;
            }
            CharacterController->ApplyRootMotion(CharacterEntity, Direction, Rotation, AnimData->AnimCurve, CurrentAnimRunTime);
            AnimRunTime->Time += DeltaTime;
        }
        else
        {
            if (AnimData->CanTransitionTo.size() > 0 && bCharacterIsAttacking)
            {
                // allow player to change direction until transition time is up
                CharacterEntity.set<animations::MovementRotationDirection>({DirectionVector, InRotation});
                Direction = DirectionVector;
                Rotation = InRotation;
            }
            
            bCanTransitionNextAction = false;
            CharacterController->ApplyRootMotion(CharacterEntity, Direction, Rotation, AnimData->AnimCurve, CurrentAnimRunTime);
            AnimRunTime->Time += DeltaTime;
        }

        return bCanTransitionNextAction;
    }

    bool PMOCharacter::IsAbleToDoAction(const bool bIsInterruptable) const
    {
        if (!CharacterEntity.is_valid() || !CharacterEntity.is_alive())
        {
            return false;
        }
        
        if (CharacterController->IsJumping())
        {
            return false;
        }

        if (CharacterEntity.has<character::ActionType>())
        {
            auto ActType = CharacterEntity.get<character::ActionType>();
            // they definitely can't do this if they are incapicated or dodging
            if ((*ActType > character::ActionType::START_INCAPACITATED && *ActType < character::ActionType::END_INCAPACITATED) ||
                (IsDodging(*ActType) && !bIsInterruptable))
            {
                return false;
            }

            // allow override (except for incapacitated/dodging/rolling if added)
            if (bIsInterruptable)
            {
                return true;
            }

            return *ActType == character::ActionType::NOOP;
        }

        return true;
    }
    
    void PMOCharacter::SetLocation(const units::Vector3 &NewLocation)
    {
        auto Loc = convert::Vector3ToJPHVec3(NewLocation);
        CharacterController->SetPosition(Loc);
    }

    void PMOCharacter::SetRotation(const units::Quat &NewRotation)
    {
        auto Rot = convert::QuatToJPHQuat(NewRotation);
        CharacterController->SetRotation(Rot);
    }

    void PMOCharacter::SetVelocity(const units::Velocity &NewVelocity)
    {
        auto Vel = convert::VelocityToJPHVec3(NewVelocity);
        CharacterController->SetVelocity(Vel);
    }

    bool PMOCharacter::EquipItem(uint8_t Slot, flecs::id ItemId, uint8_t ContainerId)
    {
        // Can't equip stuff if we are stunned etc
        if (!IsAbleToDoAction(false))
        {
            return false;
        }
        
        flecs::entity Inventory;
        if (ContainerId == 0) // 0 is player inventory
        {
            Inventory = items::GetContainer(CharacterEntity);
        }

        // TODO: Implement looking up from bags
        if (!Inventory.is_valid())
        {
            return false;
        }

        flecs::entity ItemToEquip = items::FindItemById(Inventory, ItemId);
        if (!ItemToEquip.is_valid())
        {
            return false;
        }

        return items::EquipItem(CharacterEntity, ItemToEquip, static_cast<items::EquipmentSlot>(Slot), Inventory);
    }

    float PMOCharacter::GetCapsuleRadius() const
    {
        return CharacterRadiusStanding;
    }

    units::Vector3 PMOCharacter::GetPosition() const
    {
        auto PlayerPos = CharacterController->PhysicsCharacter->GetPosition();
        return units::Vector3{PlayerPos.GetX(), PlayerPos.GetY(), PlayerPos.GetZ()};
    }

    units::Quat PMOCharacter::GetRotation() const
    {
        auto PlayerRot = CharacterController->PhysicsCharacter->GetRotation();
        return units::Quat{PlayerRot.GetX(), PlayerRot.GetY(), PlayerRot.GetZ(), PlayerRot.GetW()};
    }

    units::Vector3 PMOCharacter::GetForwardVector() const
    {
        // https://github.com/jrouwe/JoltPhysics/discussions/868#discussioncomment-8951378
        auto FwdVector = CharacterController->PhysicsCharacter->GetRotation().RotateAxisX();
        // Negate the Z axis since UE uses the left handed coordinate system and jolt uses left handed
        return units::Vector3{FwdVector.GetX(), FwdVector.GetY(), FwdVector.GetZ()*-1.f};
    }

    units::Vector3 PMOCharacter::GetForwardRightVector() const
    {
        float X = CharacterController->PhysicsCharacter->GetRotation().RotateAxisX().GetX();
        float Z = CharacterController->PhysicsCharacter->GetRotation().RotateAxisZ().GetX();
        return units::Vector3{X, 0.f, Z*-1.f};
    }

    JPH::Vec3 PMOCharacter::GetJoltForwardVector() const
    {
        return CharacterController->PhysicsCharacter->GetRotation().RotateAxisX();
    }

    JPH::Vec3 PMOCharacter::GetJoltRightVector() const
    {
        return CharacterController->PhysicsCharacter->GetRotation().RotateAxisZ();
    }

    JPH::Vec3 PMOCharacter::GetJoltForwardRightVector() const
    {
        float X = CharacterController->PhysicsCharacter->GetRotation().RotateAxisX().GetX();
        float Z = CharacterController->PhysicsCharacter->GetRotation().RotateAxisZ().GetX();
        return JPH::Vec3{X, 0.f, Z};
    }

    units::Velocity PMOCharacter::GetVelocity() const 
    {
        auto PlayerVelocity = CharacterController->PhysicsCharacter->GetLinearVelocity();
        return units::Velocity{PlayerVelocity.GetX(), PlayerVelocity.GetY(), PlayerVelocity.GetZ()};
    }

    units::Velocity PMOCharacter::GetGroundVelocity() const 
    {
        auto PlayerVelocity = CharacterController->PhysicsCharacter->GetGroundVelocity();
        return units::Velocity{PlayerVelocity.GetX(), PlayerVelocity.GetY(), PlayerVelocity.GetZ()};
    }

    float PMOCharacter::GetCosMaxSlopeAngle() const 
    {
        return CharacterController->PhysicsCharacter->GetCosMaxSlopeAngle();
    }
    
    /**
     * @brief Get the Ground State of the character (InAir, OnGround etc)
     * 
     * @return GroundState 
     */
    GroundState PMOCharacter::GetGroundState() const 
    {
        return static_cast<GroundState>(CharacterController->PhysicsCharacter->GetGroundState());
    }

    void PMOCharacter::PrePhysicsUpdate(JPH::TempAllocatorImpl *Alloc) const
    { 
        CharacterController->PrePhysicsUpdate(Alloc);
        if (!CharacterController->IsJumping() && CharacterEntity.has<character::ActionType>() && 
            *CharacterEntity.get<character::ActionType>()==character::JUMPING)
        {
            CharacterEntity.add(character::NOOP);
        }
    }

    void PMOCharacter::DebugDraw(debug::JoltDebugRenderer *DebugRenderer) const
    {
        JPH::RMat44 CenterOfMass = CharacterController->PhysicsCharacter->GetCenterOfMassTransform();
        auto CharShape = CharacterController->PhysicsCharacter->GetShape();
        CharShape->Draw(DebugRenderer, CenterOfMass, JPH::Vec3::sReplicate(1.0f), JPH::Color::sGreen, true, true);
    }
    
    bool PMOCharacter::IsBody(const JPH::BodyID &Body) const { return CharacterController->CharacterCapsule->GetID() == Body; }
}