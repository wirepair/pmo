#pragma once

#include "ecs/physics/physics.h"

namespace character
{
    /**
     * @brief Wrapped from Jolt so we can hide implementation details
     * 
     */
    enum class GroundState
    {
      OnGround,       ///< Character is on the ground and can move freely.
      OnSteepGround,  ///< Character is on a slope that is too steep and can't climb up any further. The caller should start applying downward velocity if sliding from the slope is desired.
      NotSupported,   ///< Character is touching an object, but is not supported by it and should fall. The GetGroundXXX functions will return information about the touched object.
      InAir,          ///< Character is in the air and is not touching anything.
    };

    static constexpr float DeltaTime = 0.03333333f;
    /** Physics Properties */
    static constexpr float JumpSpeed = 4.0f;
    static constexpr bool sControlMovementDuringJump = true; ///< If false the character cannot change movement direction in mid air

    static constexpr float BaseCharacterSpeed = 8.0f; // 8.0 m/s

    static constexpr float CharacterHeightStanding = 1.2f;
    static constexpr float CharacterRadiusStanding = 0.3f;
    static constexpr float CharacterHeightCrouching = 0.8f;
    static constexpr float CharacterRadiusCrouching = 0.3f;
    static constexpr float CharacterHalfCapsuleStandingSize = 0.5f * CharacterHeightStanding + CharacterRadiusStanding;

    static constexpr EBackFaceMode sBackFaceMode = EBackFaceMode::CollideWithBackFaces;
    static constexpr float sUpRotationX = 0;
    static constexpr float sUpRotationZ = 0;
    static constexpr float sMaxSlopeAngle = DegreesToRadians(45.0f);
    static constexpr float sMaxStrength = 100.0f;
    static constexpr float sCharacterPadding = 0.02f;
    static constexpr float sPenetrationRecoverySpeed = 1.0f;
    static constexpr float sPredictiveContactDistance = 0.1f;
    static constexpr bool  sEnableWalkStairs = true;
    static constexpr bool  sEnableStickToFloor = true;

    static constexpr float sRotationRate = 5.0f;
}