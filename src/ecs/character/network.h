#pragma once

#include "constants.h"
#include "ecs/units.h"
#include <Jolt/Math/Math.h>
#include <Jolt/Physics/Collision/Shape/BoxShape.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>

#include <Jolt/Physics/Collision/Shape/RotatedTranslatedShape.h>
#include <Jolt/Core/TempAllocator.h>

namespace character
{
    struct NetworkCharacter
    {
        // make this class non-copyable
        NetworkCharacter() = default;
        NetworkCharacter(const NetworkCharacter &) = delete;
        void operator =(const NetworkCharacter &) = delete;

        NetworkCharacter(JPH::PhysicsSystem *System, units::Vector3 &SpawnPosition, units::Quat &Rotation, uint64_t EntityId);

        void UpdatePositionAndRotation(units::Vector3 &Position, units::Quat &Rotation)
        {
            if (!Character)
            {
                return;
            }

            auto Pos = JPH::Vec3(Position.vec3[0], Position.vec3[1], Position.vec3[2]);
            auto Rot = JPH::Quat(Rotation.vec4[0], Rotation.vec4[1], Rotation.vec4[2], Rotation.vec4[3]);
            Character->SetPositionAndRotation(Pos, Rot);
            PhysicsSystem->GetBodyInterface().SetPosition(CharacterCapsule->GetID(), Pos, JPH::EActivation::Activate);
        }

        void UpdateVelocity(units::Velocity &Velocity)
        {
            if (!Character)
            {
                return;
            }
            auto JoltVelocity = JPH::Vec3(Velocity.vec3[0], Velocity.vec3[1], Velocity.vec3[2]);
            Character->SetLinearVelocity(JoltVelocity);
            PhysicsSystem->GetBodyInterface().SetLinearVelocity(CharacterCapsule->GetID(), JoltVelocity);
        }

        units::Vector3 GetPosition() const;

        units::Quat GetRotation() const;

        units::Velocity GetVelocity() const;

        units::Velocity GetGroundVelocity() const;

        float GetCosMaxSlopeAngle() const 
        {
            return Character->GetCosMaxSlopeAngle();
        }
        
        /**
         * @brief Checks if the passed in Body matches our CharacterCapsule Jolt Body
         * 
         * @param Body 
         * @return true 
         * @return false 
         */
        bool IsBody(const JPH::BodyID &Body) const;

        /**
         * @brief Get the Ground State of the character (InAir, OnGround etc)
         * 
         * @return GroundState 
         */
        GroundState GetGroundState() const 
        {
            return static_cast<GroundState>(Character->GetGroundState());
        }

        // move assign
        NetworkCharacter& operator=(NetworkCharacter&& other)
        {
            assert(this != &other);
           
            PhysicsSystem = std::move(other.PhysicsSystem);
            Character = std::move(other.Character);
            CharacterCapsule = std::move(other.CharacterCapsule);
            return *this;
        }
        
        // move constructor
        NetworkCharacter(NetworkCharacter&& other) noexcept
        {
            PhysicsSystem = std::move(other.PhysicsSystem);
            Character = std::move(other.Character);
            CharacterCapsule = std::move(other.CharacterCapsule);
        }

        uint64_t EntityId{};
        JPH::PhysicsSystem *PhysicsSystem;
        JPH::Character *Character;
        JPH::Body *CharacterCapsule = nullptr; // so we can do predictions locally (OnHit etc)
    };
}