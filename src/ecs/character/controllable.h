#pragma once 

#include <memory>

#include "ecs/convert.h"
#include "ecs/units.h"
#include "ecs/physics/physics.h"
#include "ecs/network/network.h"
#include "math/curves/curve.h"
#include "logger/logger.h"

#include <Jolt/Physics/Character/CharacterVirtual.h>
#include <Jolt/Physics/Character/Character.h>
#include <Jolt/Physics/Collision/Shape/RotatedTranslatedShape.h>
#include <Jolt/Core/TempAllocator.h>

namespace character
{
    /**
     * @brief Stores Movement state for replay purposes.
     * 
     */
    struct MovementFlags
    {
        bool bPlayerControlsHorizontalVelocity{};
        bool bIsMovingBackwards{};
        bool bWasJumpingPressed{};
        bool bShouldDampenMovement{};
    };

    // Character that is actually controlled by a player
    class ControllableCharacter : public JPH::CharacterContactListener
    {
    public:
        // make this class non-copyable
        ControllableCharacter() = default;
        ControllableCharacter(const ControllableCharacter &) = delete;
        void operator =(const ControllableCharacter &) = delete;

        ControllableCharacter(logging::Logger *Log, physics::Physics *System, units::Vector3 &SpawnPosition);

        /**
         * @brief Move assignment
         * 
         * @param other 
         * @return ControllableCharacter& 
         */
        ControllableCharacter& operator=(ControllableCharacter&& other)
        {
            assert(this != &other);
            other.Log->Info("Controllable Character move assign called");
            Log = std::move(other.Log);
            PhysicsSystem = other.PhysicsSystem;
            
            Settings = std::move(other.Settings);
            PhysicsCharacter = std::move(other.PhysicsCharacter);
            PhysicsCharacter->SetListener(this);
            StandingShape = std::move(other.StandingShape);
            CharacterCapsule = std::move(other.CharacterCapsule);
            bIsJumping = other.bIsJumping;
            sEnableCharacterInertia = other.sEnableCharacterInertia;
            AllowSliding = other.AllowSliding;
            return *this;
        }

        /**
         * @brief Construct a new Controllable Character object (move constructor)
         * 
         * @param other 
         */
        ControllableCharacter(ControllableCharacter&& other) noexcept
        {
            other.Log->Info("Controllable Character move constructor called");
            Log = std::move(other.Log);
            PhysicsSystem = other.PhysicsSystem;

            Settings = std::move(other.Settings);
            PhysicsCharacter = std::move(other.PhysicsCharacter);
            PhysicsCharacter->SetListener(this);
            StandingShape = std::move(other.StandingShape);
            CharacterCapsule = std::move(other.CharacterCapsule);
            bIsJumping = other.bIsJumping;
            sEnableCharacterInertia = other.sEnableCharacterInertia;
            AllowSliding = other.AllowSliding;
        }

        /**
         * @brief Set the Velocity of the character and it's capsule
         * 
         * @param NewVelocity 
         */
        void SetVelocity(JPH::Vec3Arg &NewVelocity);

        /**
         * @brief Get the Ground Velocity of the character
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetGroundVelocity() const;

        /**
         * @brief Get the Velocity of the character
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetVelocity() const;

        /**
         * 
         * @brief Set the Position of the PhysicsCharacter and Capsule.
         * 
         * @param NewLocation 
         */
        void SetPosition(JPH::Vec3Arg &NewLocation);

        /**
         * @brief Get the Position of the character
         * 
         * @return JPH::Vec3 
         */
        JPH::Vec3 GetPosition() const;

        /**
         * 
         * @brief Set the Rotation of the PhysicsCharacter and Capsule.
         * 
         * @param NewRotation 
         */
        void SetRotation(const JPH::Quat &NewRotation);
        
        /**
         * @brief Get the Rotation of the character
         * 
         * @return JPH::Quat 
         */
        JPH::Quat GetRotation() const;

        /**
         * @brief Returns jumping state (if we are still in air after a jump)
         * 
         * @return true 
         * @return false 
         */
        bool IsJumping() const;
        
        /**
         * @brief Handles player movement (both server & clients)
         * 
         * @param Character 
         * @param InMovementDirection 
         * @param InRotation 
         * @param Flags 
         */
        void HandleMovementInput(flecs::entity Character, const JPH::Vec3Arg InMovementDirection, const JPH::Quat InRotation, character::MovementFlags &Flags);

        /**
         * @brief Apply Root Motion from an AnimCurve, calls into ApplyMotion with forward/right vector of character with velocity calculated from animation distance calculated
         * 
         * @param Character 
         * @param InMovementDirection 
         * @param InRotation 
         * @param AnimCurve 
         * @param RunTime 
         */
        void ApplyRootMotion(flecs::entity Character, const JPH::Vec3Arg InMovementDirection, const JPH::Quat InRotation, const curves::Curve &AnimCurve, const float RunTime);
        
        /**
         * @brief ApplyMotion to the character
         * 
         * @param InRotation 
         * @param DesiredVelocity 
         * @param Flags 
         */
        void ApplyMotion(const JPH::Quat InRotation, const JPH::Vec3Arg DesiredVelocity, const character::MovementFlags &Flags);
        
        /**
         * @brief Updates the character must be called prior to PhysicsSystem->Update(...);
         * 
         * @param Alloc 
         */
        void PrePhysicsUpdate(JPH::TempAllocatorImpl *Alloc);

        logging::Logger *Log = nullptr;

        JPH::PhysicsSystem *PhysicsSystem = nullptr;

        JPH::Ref<JPH::CharacterVirtualSettings> Settings;

        JPH::Ref<JPH::CharacterVirtual> PhysicsCharacter = nullptr;
                
        JPH::RefConst<JPH::Shape> StandingShape = nullptr;

        JPH::Body *CharacterCapsule = nullptr;

        // Mutable Physics Properties
        JPH::Vec3 DesiredVelocity{};
        
        bool bIsJumping = false;

        bool sEnableCharacterInertia = true;

        bool AllowSliding = false;
    };

}