#include "controllable.h"
#include "constants.h"
#include "input.h"

namespace character
{

    /**
     * @brief Construct a new Controllable Character:: calls to new are wrapped in Jolts Ref types which are ref counted and deleted.
     * 
     * @param Logger 
     * @param System 
     * @param Position 
     */
    ControllableCharacter::ControllableCharacter(logging::Logger *Logger, physics::Physics *Physics, units::Vector3 &SpawnPosition) 
    {
        PhysicsSystem = Physics->System;
        Log = Logger;
        StandingShape = JPH::RotatedTranslatedShapeSettings(JPH::Vec3(0, CharacterHalfCapsuleStandingSize, 0), JPH::Quat::sIdentity(), new JPH::CapsuleShape(.5f * CharacterHeightStanding, CharacterRadiusStanding)).Create().Get();
        Settings = new JPH::CharacterVirtualSettings();
        Settings->mMaxSlopeAngle = JPH::DegreesToRadians(45.0f);
        Settings->mMaxSlopeAngle = sMaxSlopeAngle;
        Settings->mMaxStrength = sMaxStrength;
        Settings->mShape = StandingShape;
        Settings->mBackFaceMode = sBackFaceMode;
        Settings->mCharacterPadding = sCharacterPadding;
        Settings->mPenetrationRecoverySpeed = sPenetrationRecoverySpeed;
        Settings->mPredictiveContactDistance = sPredictiveContactDistance;
        Settings->mSupportingVolume = Plane(JPH::Vec3::sAxisY(), -CharacterRadiusStanding); // Accept contacts that touch the lower sphere of the capsule 
        
        auto PosVec = JPH::RVec3(SpawnPosition.vec3[0], SpawnPosition.vec3[1], SpawnPosition.vec3[2]);
        PhysicsCharacter = new JPH::CharacterVirtual(Settings, PosVec, JPH::Quat::sIdentity(), 0, PhysicsSystem);
        PhysicsCharacter->SetListener(this);

        // Pair a Capsule with the Character for ray casting and sensor hits
        // see: https://github.com/jrouwe/JoltPhysics/discussions/856 
        // and  https://github.com/jrouwe/JoltPhysics/discussions/239 for more details 
        auto ShrinkCapsuleSize = .02f;

        JPH::BodyCreationSettings CapsuleSettings(new JPH::CapsuleShape(.5f * CharacterHeightStanding-ShrinkCapsuleSize, CharacterRadiusStanding-ShrinkCapsuleSize), PosVec, JPH::Quat::sIdentity(), JPH::EMotionType::Kinematic, physics::Layers::CHARACTER);
        CapsuleSettings.mGravityFactor = 0.f;
        CapsuleSettings.mAllowSleeping = false;
        CharacterCapsule = PhysicsSystem->GetBodyInterface().CreateBody(CapsuleSettings);

        Logger->Info("CharCapsuleID: {}", CharacterCapsule->GetID().GetIndexAndSequenceNumber());
        PhysicsSystem->GetBodyInterface().AddBody(CharacterCapsule->GetID(), JPH::EActivation::Activate);
    }

    void ControllableCharacter::PrePhysicsUpdate(JPH::TempAllocatorImpl *Allocator)
    {
        JPH::CharacterVirtual::ExtendedUpdateSettings UpdateSettings;
        RVec3 OldPosition = PhysicsCharacter->GetPosition();

        if (!sEnableStickToFloor)
        {
            UpdateSettings.mStickToFloorStepDown = JPH::Vec3::sZero();
        }
        else
        {
            UpdateSettings.mStickToFloorStepDown = -PhysicsCharacter->GetUp() * UpdateSettings.mStickToFloorStepDown.Length();
        }

        if (!sEnableWalkStairs)
        {
            UpdateSettings.mWalkStairsStepUp = JPH::Vec3::sZero();
        }
        else
        {
            UpdateSettings.mWalkStairsStepUp = PhysicsCharacter->GetUp() * UpdateSettings.mWalkStairsStepUp.Length();
        }

        PhysicsCharacter->ExtendedUpdate(DeltaTime,
            -PhysicsCharacter->GetUp() * PhysicsSystem->GetGravity().Length(), 
            UpdateSettings,
            PhysicsSystem->GetDefaultBroadPhaseLayerFilter(physics::Layers::MOVING),
            PhysicsSystem->GetDefaultLayerFilter(physics::Layers::MOVING),
            {},
            {},
            *Allocator
        );
        // Update Character Capsule location as well
        JPH::RVec3 NewPosition = PhysicsCharacter->GetPosition();
 
        JPH::Vec3 Velocity = JPH::Vec3(NewPosition - OldPosition) / DeltaTime;
        PhysicsSystem->GetBodyInterface().SetLinearVelocity(CharacterCapsule->GetID(), Velocity);

        // clear jumping state
        if (bIsJumping && (PhysicsCharacter->GetGroundState() != JPH::CharacterBase::EGroundState::InAir))
        {
            bIsJumping = false;
        }
    }

    bool ControllableCharacter::IsJumping() const
    {
        return bIsJumping;
    }

    void ControllableCharacter::SetVelocity(JPH::Vec3Arg &NewVelocity)
    {
        PhysicsCharacter->SetLinearVelocity(NewVelocity);
        PhysicsSystem->GetBodyInterface().SetLinearVelocity(CharacterCapsule->GetID(), NewVelocity);
    }

    JPH::Vec3 ControllableCharacter::GetGroundVelocity() const
    {
        return PhysicsCharacter->GetGroundVelocity();
    }

    JPH::Vec3 ControllableCharacter::GetVelocity() const
    {
        return PhysicsCharacter->GetLinearVelocity();
    }

    void ControllableCharacter::SetPosition(const JPH::Vec3Arg &NewLocation)
    {
        PhysicsCharacter->SetPosition(NewLocation);
        PhysicsSystem->GetBodyInterface().SetPosition(CharacterCapsule->GetID(), NewLocation, JPH::EActivation::Activate);
    }

    JPH::Vec3 ControllableCharacter::GetPosition() const
    {
        return PhysicsCharacter->GetPosition();
    }

    void ControllableCharacter::SetRotation(const JPH::Quat &NewRotation)
    {
        PhysicsCharacter->SetRotation(NewRotation);
        PhysicsSystem->GetBodyInterface().SetRotation(CharacterCapsule->GetID(), NewRotation, JPH::EActivation::Activate);
    }

    JPH::Quat ControllableCharacter::GetRotation() const
    {
        return PhysicsCharacter->GetRotation();
    }

    void ControllableCharacter::HandleMovementInput(flecs::entity Character, const JPH::Vec3Arg InMovementDirection, const JPH::Quat InRotation, character::MovementFlags &Flags)
    {
        JPH::Vec3 MovementDirection = InMovementDirection;
        Flags.bPlayerControlsHorizontalVelocity = sControlMovementDuringJump || PhysicsCharacter->IsSupported();
        float Speed{};

        if (Flags.bPlayerControlsHorizontalVelocity)
        {
            Speed = (Flags.bIsMovingBackwards) ? BaseCharacterSpeed/2.f : BaseCharacterSpeed;
            DesiredVelocity = sEnableCharacterInertia ? 0.25f * MovementDirection * Speed + 0.75f * DesiredVelocity : MovementDirection * Speed;
        }
        // Log->Info("PreUpdate: HandleMovementInput: Delta {} Inertia: {} Horiz: {} Base: {} Speed: {} Direction: {} {} {} Desired Velocity: {} {} {}", 
        //     DeltaTime, sEnableCharacterInertia, bPlayerControlsHorizontalVelocity, BaseCharacterSpeed, Speed, 
        //     MovementDirection.GetX(), MovementDirection.GetY(), MovementDirection.GetZ(),
        //     DesiredVelocity.GetX(), DesiredVelocity.GetY(), DesiredVelocity.GetZ()
        // );

        ApplyMotion(InRotation, DesiredVelocity, Flags);
    }

    void ControllableCharacter::ApplyRootMotion(flecs::entity Character, const JPH::Vec3Arg InMovementDirection, const JPH::Quat InRotation, const curves::Curve &AnimCurve, const float RunTime)
    {
        JPH::Vec3 MovementDirection = InMovementDirection;

        bool PlayerControlsHorizontalVelocity = PhysicsCharacter->IsSupported();
        if (PlayerControlsHorizontalVelocity)
        {
            auto Value = AnimCurve.Eval(RunTime);
            auto PreviousDelta = JPH::Clamp<float>(RunTime-DeltaTime, 0, 999);
            auto PreviousValue = AnimCurve.Eval(PreviousDelta);   
            auto Distance = Value.Length() - PreviousValue.Length();
            auto Velocity = (Distance / DeltaTime) / 100.f; // Jolt uses m/s
            DesiredVelocity = MovementDirection * Velocity;
            Log->Info("Velocity ({} m/s Distance: {} Value: {} PreviousValue: {}, DeltaTime {}, RunTime: {}", Velocity, Distance, Value.Length(), PreviousValue.Length(), DeltaTime, RunTime);
        }
        // Apply the motion using our newly set DesiredVelocity
        const character::MovementFlags Flags{PlayerControlsHorizontalVelocity, false, false, false};
        ApplyMotion(InRotation, DesiredVelocity, Flags);
    }

    void ControllableCharacter::ApplyMotion(const JPH::Quat InRotation, const JPH::Vec3Arg DesiredVelocity, const character::MovementFlags &Flags)
    {
        PhysicsCharacter->SetUp(InRotation.RotateAxisY());
        //auto CurrentRot = PhysicsCharacter->GetRotation();
        auto TargetRot = InRotation; //CurrentRot.SLERP(InRotation, DeltaTime*sRotationRate);
        PhysicsCharacter->SetRotation(TargetRot);

        // A cheaper way to update the character's ground velocity,
        // the platforms that the character is standing on may have changed velocity
        PhysicsCharacter->UpdateGroundVelocity();

        // Determine new basic velocity
        JPH::Vec3 CurrentVelocity = PhysicsCharacter->GetLinearVelocity().Dot(PhysicsCharacter->GetUp()) * PhysicsCharacter->GetUp();
        JPH::Vec3 GroundVelocity = PhysicsCharacter->GetGroundVelocity();
        JPH::Vec3 NewVelocity{};
        bool MovingToGround = (CurrentVelocity.GetY() - GroundVelocity.GetY()) < 0.1f;

        // Log->Info("PreUpdate: ApplyMotion: Delta {} CurrentVelocity: {} {} {} GroundVelocity: {} {} {}", 
        //     DeltaTime,
        //     CurrentVelocity.GetX(), CurrentVelocity.GetY(), CurrentVelocity.GetZ(),
        //     GroundVelocity.GetX(), GroundVelocity.GetY(), GroundVelocity.GetZ()
        // );

        bool ShouldUseGroundVelocity = sEnableCharacterInertia ? MovingToGround : !PhysicsCharacter->IsSlopeTooSteep(PhysicsCharacter->GetGroundNormal());

        if (PhysicsCharacter->GetGroundState() == JPH::CharacterVirtual::EGroundState::OnGround && ShouldUseGroundVelocity)
        {
            // Assume velocity of ground when on ground
            NewVelocity = GroundVelocity;
            
            // Jumping
            if (Flags.bWasJumpingPressed)
            {
                NewVelocity += JumpSpeed * PhysicsCharacter->GetUp();
                bIsJumping = true;
            }
        }
        else 
        {
            NewVelocity = CurrentVelocity;
        }
        // Log->Info("PreUpdate: ApplyMotion: Delta {} NewVelocity: {} {} {}", 
        //     DeltaTime,
        //     NewVelocity.GetX(), NewVelocity.GetY(), NewVelocity.GetZ()
        // );
        // Update the character rotation and its up vector to match the up vector set by the user settings
        JPH::Quat UpRotation = JPH::Quat::sEulerAngles(JPH::Vec3(0, 0, 0));
        // Apply Gravity
        NewVelocity += (UpRotation * PhysicsSystem->GetGravity()) * DeltaTime;

        if (Flags.bPlayerControlsHorizontalVelocity)
        {
            // Player input
            NewVelocity += UpRotation * DesiredVelocity;
            //Log->Info("(Moving To Ground: {}) Applying player input of Desired Velocity: {} {} {}", MovingToGround, NewVelocity.GetX(), NewVelocity.GetY(), NewVelocity.GetZ());
        }
        else
        {
            // Preserve horizontal velocity
            JPH::Vec3 CurrentHorizontalVelocity = PhysicsCharacter->GetLinearVelocity() - CurrentVelocity;
            NewVelocity += CurrentHorizontalVelocity;
            //Log->Info("(Moving To Ground: {}) NOT Applying player input to Velocity: {} {} {}", MovingToGround, NewVelocity.GetX(), NewVelocity.GetY(), NewVelocity.GetZ());
        }

        // dampen movement if we are blocking, parrying or doing some other action that should slow our velocity
        if (Flags.bShouldDampenMovement)
        {
            NewVelocity.SetX(NewVelocity.GetX() * 0.5f);
            NewVelocity.SetZ(NewVelocity.GetZ() * 0.5f);
        }
        PhysicsCharacter->SetLinearVelocity(NewVelocity);
        // Log->Info("PreUpdate: ApplyMotion: (After SetLinearVelocity) Delta {} NewVelocity: {} {} {}", 
        //     DeltaTime,
        //     NewVelocity.GetX(), NewVelocity.GetY(), NewVelocity.GetZ()
        // );
    }
}