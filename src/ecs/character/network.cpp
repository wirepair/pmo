#include "network.h"

namespace character
{
    NetworkCharacter::NetworkCharacter(JPH::PhysicsSystem *System, units::Vector3 &SpawnPosition, units::Quat &Rotation, uint64_t NetEntityId)
    {
        PhysicsSystem = System;
        EntityId = NetEntityId;
        
        JPH::Ref<JPH::CharacterSettings> Settings = new JPH::CharacterSettings();
        Settings->mMaxSlopeAngle = JPH::DegreesToRadians(45.0f);
        Settings->mLayer = physics::Layers::CHARACTER;
        Settings->mShape = JPH::RotatedTranslatedShapeSettings(JPH::Vec3(0, CharacterHalfCapsuleStandingSize, 0), JPH::Quat::sIdentity(), new JPH::CapsuleShape(.5f * CharacterHeightStanding, CharacterRadiusStanding)).Create().Get();
        Settings->mFriction = 0.5f;
        Settings->mSupportingVolume = JPH::Plane(JPH::Vec3::sAxisY(), -CharacterRadiusStanding); // Accept contacts that touch the lower sphere of the capsule
        auto Pos = JPH::Vec3(SpawnPosition.vec3[0], SpawnPosition.vec3[1], SpawnPosition.vec3[2]);
        auto Rot = JPH::Quat(Rotation.vec4[0], Rotation.vec4[1], Rotation.vec4[2], Rotation.vec4[3]);
        Character = new JPH::Character(Settings, Pos, Rot, EntityId, PhysicsSystem);
        Character->AddToPhysicsSystem(EActivation::Activate);

        // Pair a Capsule with the Character for ray casting and sensor hits
        // see: https://github.com/jrouwe/JoltPhysics/discussions/856 
        // and  https://github.com/jrouwe/JoltPhysics/discussions/239 for more details 
        auto ShrinkCapsuleSize = .02f;

        JPH::BodyCreationSettings CapsuleSettings(new JPH::CapsuleShape(.5f * CharacterHeightStanding-ShrinkCapsuleSize, CharacterRadiusStanding-ShrinkCapsuleSize), Pos, JPH::Quat::sIdentity(), JPH::EMotionType::Kinematic, physics::Layers::CHARACTER);
        CapsuleSettings.mGravityFactor = 0.f;
        CapsuleSettings.mAllowSleeping = false;
        CharacterCapsule = System->GetBodyInterface().CreateBody(CapsuleSettings);

        System->GetBodyInterface().AddBody(CharacterCapsule->GetID(), JPH::EActivation::Activate);
    }

    units::Vector3 NetworkCharacter::GetPosition() const
    {
        if (!Character)
        {
            return units::Vector3{};
        }
        auto PlayerPos = Character->GetPosition();
        return units::Vector3{PlayerPos.GetX(), PlayerPos.GetY(), PlayerPos.GetZ()};
    } 

    units::Quat NetworkCharacter::GetRotation() const
    {
        if (!Character)
        {
            return units::Quat{};
        }
        auto PlayerRot = Character->GetRotation();
        return units::Quat{PlayerRot.GetX(), PlayerRot.GetY(), PlayerRot.GetZ(), PlayerRot.GetW()};
    }

    units::Velocity NetworkCharacter::GetVelocity() const
    {
        auto PlayerVelocity = Character->GetLinearVelocity();
        return units::Velocity{PlayerVelocity.GetX(), PlayerVelocity.GetY(), PlayerVelocity.GetZ()};
    }

    units::Velocity NetworkCharacter::GetGroundVelocity() const 
    {
        auto PlayerVelocity = Character->GetGroundVelocity();
        return units::Velocity{PlayerVelocity.GetX(), PlayerVelocity.GetY(), PlayerVelocity.GetZ()};
    }

    bool NetworkCharacter::IsBody(const JPH::BodyID &Body) const
    {
        return CharacterCapsule->GetID() == Body;
    }
}