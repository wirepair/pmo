#pragma once

#include <memory>

#include "ecs/convert.h"
#include "ecs/units.h"
#include "logger/logger.h"
#include "input/input.h"
#include "schemas/server_generated.h"
#include <unordered_set>
#include <unordered_map>

namespace character
{
    // flecs only allows 32bit enums, so we have to make a copy here because it's a waste to transmit over network :(
    // assert: cptr->size == ECS_SIZEOF(int32_t) enum component must have 32bit size (UNSUPPORTED)
    enum ActionType 
    {
        NOOP,
        JUMPING,
        BLOCKING,
        PARRYING,
        
        START_DODGE,
        DODGE_FORWARD,
        DODGE_BACKWARD,
        DODGE_LEFT,
        DODGE_RIGHT,
        END_DODGE,

        START_ATTACK,
        START_SWORDSH_ATTACK,
        SWORDSH_ATTACK_1,
        SWORDSH_ATTACK_2,
        SWORDSH_ATTACK_3,
        SWORDSH_SEC_ATTACK_1,
        SWORDSH_SEC_ATTACK_2,
        SWORDSH_SEC_ATTACK_3,
        END_SWORDSH_ATTACK,

        TWOHAND_ATTACK,
        SPEARSH_ATTACK,
        BOW_ATTACK,
        CAST_ATTACK,
        END_ATTACK,

        START_INCAPACITATED,
        STUNNED,
        SLEEPING,
        KNOCKED_DOWN,
        DEAD,
        END_INCAPACITATED,
    };

    static const std::unordered_map<std::string, ActionType> action_map = 
    {
        {"NOOP", NOOP},
        {"JUMPING", JUMPING},
        
        {"START_DODGE", START_DODGE},
        {"DODGE_FORWARD", DODGE_FORWARD},
        {"DODGE_BACKWARD", DODGE_BACKWARD},
        {"DODGE_LEFT", DODGE_LEFT},
        {"DODGE_RIGHT", DODGE_RIGHT},
        {"END_DODGE", END_DODGE},

        {"BLOCKING", BLOCKING},
        {"PARRYING", PARRYING},
        
        {"START_ATTACK", START_ATTACK},
        
        {"START_SWORDSH_ATTACK", START_SWORDSH_ATTACK},
        {"SWORDSH_ATTACK_1", SWORDSH_ATTACK_1},
        {"SWORDSH_ATTACK_2", SWORDSH_ATTACK_2},
        {"SWORDSH_ATTACK_3", SWORDSH_ATTACK_3},
        {"SWORDSH_SEC_ATTACK_1", SWORDSH_SEC_ATTACK_1},
        {"SWORDSH_SEC_ATTACK_2", SWORDSH_SEC_ATTACK_2},
        {"SWORDSH_SEC_ATTACK_3", SWORDSH_SEC_ATTACK_3},
        {"END_SWORDSH_ATTACK", END_SWORDSH_ATTACK},
        
        {"TWOHAND_ATTACK", TWOHAND_ATTACK},
        {"SPEARSH_ATTACK", SPEARSH_ATTACK},
        {"BOW_ATTACK", BOW_ATTACK},
        {"CAST_ATTACK", CAST_ATTACK},
        {"END_ATTACK", END_ATTACK},
        
        {"START_INCAPACITATED", START_INCAPACITATED},
        {"STUNNED", STUNNED},
        {"SLEEPING", SLEEPING},
        {"KNOCKED_DOWN", KNOCKED_DOWN},
        {"DEAD", DEAD},
        {"END_INCAPACITATED", END_INCAPACITATED}
    };

    struct PhysicsHistoryBuffer
    {

    };
    
    /**
     * @brief Ensures the character::ActionType is added as a Union type so we can
     * use it as a state for state machine style tests
     * 
     * @param GameWorld 
     */
    inline void InitCharacterStates(flecs::world &GameWorld)
    {
        GameWorld.component<character::ActionType>().add(flecs::Union);
    }

    /**
     * @brief Inits character components / relationships / prefabs
     * 
     * @param GameWorld 
     */
    inline void InitCharacter(flecs::world &GameWorld)
    {
        InitCharacterStates(GameWorld);
    }

    /**
     * @brief Signal that they are legit dead and should be respawned when requested
     * 
     */
    struct IsDead
    {
        double DiedAt{};
        double DeadFor{};
    };

    /**
     * @brief Component of where this character is bound to. (Used when spawning/respawning)
     * 
     */
    struct BindLocation
    {
        units::Vector3 Location{0.f, 0.f, 0.f};
    };

    /**
     * @brief Component to signal character to be respawned
     * 
     */
    struct Respawn {};

    /**
     * @brief Can this character duel weild
     * 
     */
    struct DualWeild {};

    struct Visibility
    {
        std::unordered_set<flecs::entity, units::FlecsEntityHash> Neighbors;
        std::unordered_set<flecs::entity, units::FlecsEntityHash> DamageEvents;
        
        /**
         * @brief Clears out old frame state
         * 
         */
        void Clear()
        {
            Neighbors.clear();
            DamageEvents.clear();
        }
    };
}