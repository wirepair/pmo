#ifdef IS_SERVER
#include "init_server.h"
#include <memory>
#include <filesystem>

#include "schemas/messages.h"
#include "crypto/cryptor.h"

#include "ecs/units.h"
#include "ecs/level.h"
#include "ecs/world.h"
#include "ecs/animations/animation.h"
#include "ecs/character/components.h"
#include "ecs/character/input.h"
#include "ecs/physics/physics.h"
#include "ecs/combat/combat.h"
#include "ecs/items/items.h"
#include "ecs/items/inventory.h"
#include "ecs/network/network.h"


#include "net/server.h"
#include "ecs/network/network_server.h"


bool InitPMOServer(flecs::world &GameWorld, logging::Logger &Log, config::Config &Config)
{
    // Server will create entities 5k and up
    GameWorld.set_entity_range(5000, 0);
    // Build our server
    auto InQueue = std::make_shared<net::PacketQueue>();
    auto OutQueue = std::make_shared<net::PacketQueue>();
    auto Crypto = std::make_shared<crypto::Cryptor>(Log);
    auto ServerSocket = std::make_shared<net::Socket>(Config.GetServerAddr(), Config.GetServerPort(), Log);
    auto ServerThread = std::make_shared<net::Server>(InQueue, OutQueue, Crypto, ServerSocket, Log);
    Log.Info("Server thread starting... {} {}", InQueue == nullptr, OutQueue == nullptr);
    ServerThread->Start();

    // modules
    GameWorld.import<units::Units>();
    GameWorld.import<network::Network>();

    GameWorld.import<network::NetworkServer>();
    GameWorld.get_mut<network::NetworkServer>()->Init(&Log, ServerThread, InQueue, OutQueue);

    // Load levels from disk
    Log.Info("Loading map {} from {}", Config.GetMapName(), (std::filesystem::current_path() / Config.GetMapPath()).string());
    
    GameWorld.import<physics::Physics>();

    auto PhysicsSystem = GameWorld.get_mut<physics::Physics>();
    
    // setup level
    GameWorld.import<level::Level>();
    GameWorld.get_mut<level::Level>()->Init(&Log, &Config, GameWorld, PhysicsSystem, PMOCallerType::Server);

    // Load items and equipment
    items::LoadEquipment(GameWorld, Config, Log);
    items::LoadItems(GameWorld, Config, Log);

    // load anim data
    GameWorld.import<animations::Animation>();
    GameWorld.get_mut<animations::Animation>()->Init(&Log, Config);

    // setup combat system
    GameWorld.import<combat::Combat>();
    GameWorld.get_mut<combat::Combat>()->Init(&Log, GameWorld, PhysicsSystem);

    character::InitCharacter(GameWorld);
    
    physics::Landscape Landscape{PhysicsSystem->System, Config, Log};

    if (!PhysicsSystem->InitializeWorld(&Log, Config, GameWorld, Landscape))
    {
        Log.Error("Failed to initialize world!");
        return false;
    }
    else
    {
        Log.Info("World has been initialized");
    }

    character::RegisterInputSystem(GameWorld, &Log, &Config);

    // Set our tick rate
    GameWorld.set_target_fps(30);
    return true;
}

void DeinitPMO(flecs::world &GameWorld)
{
    if (GameWorld.has<network::NetworkServer>())
    {
        auto Server = GameWorld.get_mut<network::NetworkServer>();
        if (Server->Thread)
        {
            Server->Thread->Stop();
        }
    }
    auto Physics = GameWorld.get_mut<physics::Physics>();
    GameWorld.reset();
    Physics->ShutdownWorld();
}
#endif