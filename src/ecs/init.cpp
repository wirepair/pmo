#ifndef IS_SERVER
#include "init.h"

#include <memory>
#include <filesystem>

#include "schemas/messages.h"
#include "crypto/cryptor.h"

#include "ecs/units.h"
#include "ecs/level.h"
#include "ecs/world.h"
#include "ecs/animations/animation.h"
#include "ecs/character/components.h"
#include "ecs/character/input.h"
#include "ecs/physics/physics.h"
#include "ecs/physics/state.h"
#include "ecs/combat/combat.h"
#include "ecs/items/items.h"
#include "ecs/items/inventory.h"
#include "ecs/network/network.h"
#include "ecs/network/network_client.h"

#include "net/socket.h"
#include "containers/lockfreequeue.h"

// TODO get data from DB
flecs::entity_t InitPMOClient(flecs::world &GameWorld, logging::Logger &Log, config::Config &Config, uint32_t UserId)
{
    // Only allow entity ids between 1000 and 4000, everything else will come from server and be assigned using make_alive(server_id)
    GameWorld.set_entity_range(1000, 4000);
    // Build our client
    auto InQueue = std::make_shared<net::PacketQueue>();
    auto OutQueue = std::make_shared<net::PacketQueue>();
    auto Crypto = std::make_shared<crypto::Cryptor>(Log);
    auto ServerSocket = std::make_shared<net::Socket>(Config.GetServerAddr(), Config.GetServerPort(), Log);
    auto ClientThread = std::make_shared<net::Client>(InQueue, OutQueue, Crypto, ServerSocket, Log);
    ClientThread->SetUserId(UserId);
    Log.Info("Client thread starting...");
    ClientThread->Start();
    Log.Info("Client thread started");
    GameWorld.import<units::Units>();
    Log.Info("Units");
    GameWorld.import<network::Network>();
    Log.Info("Network");
    GameWorld.import<network::NetworkClient>();
    Log.Info("Loading NetworkClient");
    GameWorld.get_mut<network::NetworkClient>()->Init(&Log, ClientThread, InQueue, OutQueue);
    
    // Load levels from disk
    Log.Info("Loading map {} from {}", Config.GetMapName(), (std::filesystem::current_path() / Config.GetMapPath()).string());



    GameWorld.import<physics::Physics>();
    auto PhysicsSystem = GameWorld.get_mut<physics::Physics>();
    
    // setup level
    GameWorld.import<level::Level>();
    GameWorld.get_mut<level::Level>()->Init(&Log, &Config, GameWorld, PhysicsSystem, PMOCallerType::Client);
    
    // load items & inventory
    items::LoadEquipment(GameWorld, Config, Log);
    items::LoadItems(GameWorld, Config, Log);

    // load anim data
    GameWorld.import<animations::Animation>();
    GameWorld.get_mut<animations::Animation>()->Init(&Log, Config);

    // setup combat system
    GameWorld.import<combat::Combat>();
    GameWorld.get_mut<combat::Combat>()->Init(&Log, GameWorld, PhysicsSystem);

    character::InitCharacter(GameWorld);

    physics::Landscape Landscape{PhysicsSystem->System, Config, Log};
    
    if (!PhysicsSystem->InitializeWorld(&Log, Config, GameWorld, Landscape))
    {
        Log.Error("Failed to initialize world!");
        return false;
    }
    else
    {
        Log.Info("World has been initialized");
    }
    character::RegisterInputSystem(GameWorld, &Log, &Config);

    // Set our tick rate
    GameWorld.set_target_fps(30);

    
    flecs::entity NewPlayer = GameWorld.entity();
    GameWorld.import<world::PMOWorld>();
    GameWorld.get_mut<world::PMOWorld>()->Init(Config.GetMapName(), NewPlayer, &Log);
    
    // Create our new player and apply the auth component so it will kick off the authentication process
    NewPlayer.set<network::User>({UserId})
        .set<network::PMOClient>({ClientThread, OutQueue})
        .set<network::Auth>({});
    return NewPlayer;
}

void DeinitPMO(flecs::world &GameWorld)
{
    if (GameWorld.has<network::NetworkClient>())
    {
        GameWorld.get_mut<network::NetworkClient>()->Thread->Stop();
    }
    auto Physics = GameWorld.get_mut<physics::Physics>();
    GameWorld.reset();
    Physics->ShutdownWorld();
}
#endif
