#pragma once 
#include <string>
#include "math/curves/curve.h"
#include "ecs/character/components.h"

namespace animations
{
    struct AnimData
    {
        std::string Path;
        curves::Curve AnimCurve;
        float AttackAtSeconds{};
        std::vector<std::string> CanTransitionTo;
        float TransitionAtSeconds{};
        character::ActionType ActionType;
    };
}