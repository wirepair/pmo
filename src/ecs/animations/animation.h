#pragma once

#include <flecs.h>
#include <map>
#include <unordered_map>
#include "animdata.h"
#include "math/curves/curve.h"
#include "config/config.h"
#include "logger/logger.h"
#include "parsers/csv.hpp"
#include "parsers/toml.hpp"

namespace animations
{
    using namespace csv;

    enum AnimSet : uint32_t
    {
        NONE,
        SWORDSH,
        TWOHAND,
        SPEARSH,
        BOW,
        CAST,
    };

    static const std::unordered_map<std::string, AnimSet> AnimSetMap = 
    {
        {"NONE", NONE},
        {"SWORDSH", SWORDSH},
        {"TWOHAND", TWOHAND},
        {"SPEARSH", SPEARSH},
        {"BOW", BOW},
        {"CAST", CAST},
    };

    static const std::unordered_map<AnimSet, std::string> AnimSetStrMap = 
    {
        {NONE, "NONE"},
        {SWORDSH, "SWORDSH"},
        {TWOHAND, "TWOHAND"},
        {SPEARSH, "SPEARSH"},
        {BOW, "BOW"},
        {CAST, "CAST"},
    };

    struct RunTime
    {
        float Time{};
    };

    struct MovementRotationDirection
    {
        JPH::Vec3 Direction;
        JPH::Quat Rotation;
    };
    
    /**
     * @brief Loads our animation data for root motion and attack timing and creates prefabs
     * into our GameWorld.
     * 
     */
    struct Animation
    {
        Animation(flecs::world &GameWorld) : World(GameWorld)
        {
            GameWorld.module<Animation>();
            GameWorld.component<AnimData>().add(flecs::OnInstantiate, flecs::Inherit);
            GameWorld.component<AnimSet>().add(flecs::Union);
        }
        /**
         * @brief Initializes all animation data sets from our assets
         * 
         * @param Log 
         * @param Config 
         * @return true 
         * @return false 
         */
        bool Init(logging::Logger *Log, config::Config &Config);

        /**
         * @brief Loads all animations as prefabs into our game world
         * 
         * @param Config 
         * @return true 
         * @return false 
         */
        bool LoadAnimations(config::Config &Config);

        /**
         * @brief Loads all combat animations as prefabs into our game world. Note we
         * only extract the Y axis of the transform as that is what is used for root motion
         * animations.
         * 
         * @param CombatAttack 
         * @param Reader a csv reader of a file containing root motion transform data
         * @return true 
         * @return false 
         */
        bool LoadCombatAnimations(struct AnimData &Anim, std::string &&CombatAttack, csv::CSVReader &Reader);

        flecs::world& World;
        logging::Logger *Logger;
    };
}