#include "animation.h"
#include "animdata.h"
#include "ecs/units.h"
#include <vector>
#include <filesystem>


namespace animations
{
    using namespace std::string_view_literals;
    bool Animation::Init(logging::Logger *Log, config::Config &Config)
    {
        Logger = Log;

        return LoadAnimations(Config);
    }

    bool Animation::LoadAnimations(config::Config &Config)
    {
        const std::filesystem::path AnimPath{Config.GetAnimPath()};

        for (auto const &file : std::filesystem::recursive_directory_iterator{AnimPath})
        {
            // Read toml files which point to CSV files
            Logger->Debug("Parent path: {} Filename: {} Stem: {}", file.path().parent_path().string(), file.path().filename().string(),  file.path().filename().extension().string());
            if (!file.path().has_filename() || file.path().filename().extension() != ".toml")
            {
                continue;
            }

            auto AnimMetadata = toml::parse_file(file.path().c_str());
            auto AnimTable = AnimMetadata["animations"].as_array(); 
            if (!AnimTable) 
            { 
                Logger->Error("failed to load animation table: {}", file.path().filename().string());
                return false;
            }

            for (const auto& Entry : *AnimTable)
            {
                AnimData Anim;
                auto ActionType = Entry.at_path("action_type").value_or("NOOP");
                Anim.ActionType = character::action_map.at(ActionType);
                Anim.Path = Entry.at_path("path").value_or("");
                Anim.AttackAtSeconds = Entry.at_path("attack_at_sec").value_or(0.f);
                Anim.TransitionAtSeconds = Entry.at_path("transition_at_sec").value_or(0.0f);

                auto CanTransition = Entry.at_path("can_transition_to").as_array();
                if (CanTransition)
                {
                    for (const auto& Transition : *CanTransition)
                    {
                        Anim.CanTransitionTo.emplace_back(Transition.value_or(""));
                    }
                }

                auto AnimPath = std::filesystem::path(Anim.Path);
                auto CsvPath = file.path().parent_path() / AnimPath;
                auto Reader = csv::CSVReader(CsvPath.string());

                auto Result = LoadCombatAnimations(Anim, AnimPath.string(), Reader);
                if (!Result)
                {
                    Logger->Error("failed to load combat animation: {}", file.path().filename().string());
                    return false;
                }
            }
        }
        return true;
    }

    bool Animation::LoadCombatAnimations(AnimData &Anim, std::string &&AttackName, csv::CSVReader &Reader)
    {
        std::vector<float> KeyValuesX;
        std::vector<float> KeyValuesY;
        std::vector<float> KeyValuesZ;

        int FrameCount = 0;
        for (const auto& Row : Reader) 
        {
            float KeyValue = Row["Value"].get<float>();

            if (Row["Axis"].get<std::string>() == "X")
            {
                FrameCount++;
                KeyValuesX.emplace_back(KeyValue);
            } 
            else if (Row["Axis"].get<std::string>() == "Y")
            {
                KeyValuesY.emplace_back(KeyValue);
            }
            else if (Row["Axis"].get<std::string>() == "Z")
            {
                KeyValuesZ.emplace_back(KeyValue);
            }
        }

        if (!((KeyValuesX.size() == KeyValuesY.size()) == (KeyValuesX.size() == KeyValuesY.size())))
        {
            Logger->Warn("Wrong number of frames {} {} {}", KeyValuesX.size(), KeyValuesY.size(), KeyValuesZ.size());
            return false;
        }

        if (FrameCount == 0)
        {
            Logger->Warn("No frames found in combat animation data");
            return false;
        }

        float AnimRunTime = static_cast<float>(FrameCount) / 60.f;
        float TimePerFrame = AnimRunTime / static_cast<float>(FrameCount);

        uint32_t Idx = 0;
        for (const auto &FrameValX : KeyValuesX)
        {
            float FrameValY = KeyValuesY.at(Idx);
            float FrameValZ = KeyValuesZ.at(Idx);
            auto FrameTime = TimePerFrame*static_cast<float>(Idx++);
            Anim.AnimCurve.InsertOrderedKey(curves::CurveKey(FrameTime, JPH::Vec3{FrameValX, FrameValY, FrameValZ}));
        }

        World.prefab(AttackName.c_str()).set<animations::AnimData>({Anim});//Anim
        return true;
    }
}