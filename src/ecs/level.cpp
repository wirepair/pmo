#include "level.h"
#include "state.h"
#include "ecs/character/components.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/character/controllable.h"
#include "ecs/animations/animation.h"
#include "ecs/network/network.h"
#include "ecs/items/items.h"
#include "ecs/items/inventory.h"
#include <set>
#include <algorithm>
#include "ecs/network/network.h"

namespace level
{
    //const float AreaOfInterestDistance = 15000.f;

    Level::Level(flecs::world& GameWorld)
    {
        GameWorld.module<Level>();
        GameWorld.component<MapLevel>();
        GameWorld.component<MapChunk>();
        ServerAreaOfInterestQuery = GameWorld.query<MapChunk, LocalSensor, MultiSensor>();
        CharacterQuery = GameWorld.query<character::PMOCharacter>();
        CombatQuery = GameWorld.query<combat::ApplyDamage>();
    }

    void Level::Init(logging::Logger *Log, config::Config *Conf, flecs::world &GameWorld,  physics::Physics *PhysicSystem, PMOCallerType Caller)
    {
        Logger = Log;
        Config = Conf;
        
        if (PhysicSystem)
        {
            Physics = PhysicSystem;
            Physics->ContactEventDispatcher->AddListener(this);
        }
        LevelMode = Caller;

        // AoI is only applicable to servers
        if (LevelMode == PMOCallerType::Server)
        {
            InitAreaOfInterest(GameWorld);
            ServerLevelSystems(GameWorld);
        }
    }
    
    void Level::AddUserToLevel(flecs::world& GameWorld, flecs::entity &User, units::Vector3 &Position)
    {
        const auto MapName = Config->GetMapName();
        Logger->Info("Look up of map name: [{}]", MapName);
        auto Map = GameWorld.lookup(MapName);

        if (!Map.is_valid())
        {
            Logger->Info("Map {} did not exist, can't spawn player!", Config->GetMapName());
            return;
        }

        // Need to pass instead of creating a constructor for PMOCharacter
        auto AnimQuery = GameWorld.query_builder<animations::AnimData>().with(flecs::Prefab).build();

        // We'll want to save the users' state and load the data once we have a DB
        User.add(flecs::ChildOf, Map)
            .add(character::ActionType::NOOP)
            .add(animations::AnimSet::SWORDSH) // TODO Update based on item equipped
            .set<character::PMOCharacter>({User, std::make_unique<character::ControllableCharacter>(Logger, Physics, Position), AnimQuery})
            .set<units::Vector3>({Position})
            .set<units::Quat>({})
            .set<units::Velocity>({})        
            .set<character::BindLocation>({Position})
            .set<units::Attributes>({})
            .set<units::TraitAttributes>({})
            .set<character::Visibility>({})
            .is_a<items::Body>()
            .add<items::Inventory>(
                GameWorld.entity().add<items::Container>()
            );

        Logger->Info("User has been spawned into the world {}!", User.id());

    }

    void Level::OnContactAdded(const Body &InBody1, const Body &InBody2, const ContactManifold &InManifold, ContactSettings &InIOSettings)
    {
        if (LevelMode == PMOCallerType::Client)
        {
            // do client only stuff
            return;
        }

        // Validate this contact hit is from a sensor, otherwise bail out early
        if (!InIOSettings.mIsSensor)
        {
            return;
        }
        
        // Also make sure it's a Character
        if ((InBody1.GetObjectLayer() != physics::Layers::CHARACTER) && (InBody2.GetObjectLayer() != physics::Layers::CHARACTER))
        {
            return;
        }

        flecs::entity Character = CharacterQuery.find([&](character::PMOCharacter& Char) 
        {
            return Char.IsBody(InBody1.GetID()) || Char.IsBody(InBody2.GetID());
        });

        if (!Character.is_valid() || !Character.is_alive())
        {
            Logger->Debug("OnContactAdded Character not found or invalid");
            return;
        }

        Logger->Debug("Level::OnContactAdded called BodyID: {}, Layer: {}, BodyID: {}, Layer {}", 
            InBody1.GetID().GetIndexAndSequenceNumber(), static_cast<int>(InBody1.GetObjectLayer()), 
            InBody2.GetID().GetIndexAndSequenceNumber(), static_cast<int>(InBody2.GetObjectLayer())
        );

        Character.world().defer_begin();
        ServerAreaOfInterestQuery.each([&](flecs::iter& It, size_t Index, level::MapChunk &Chunk, level::LocalSensor &Local, level::MultiSensor &Multi)
        {
            if (Local.Sensor == InBody1.GetID() || Local.Sensor == InBody2.GetID())
            {
                Logger->Debug("Found Character {} Adding to Local Sensor {}", Character.id(), Local.Sensor.GetIndexAndSequenceNumber());
                Local.Characters.emplace(Character);
            }
            else if (Multi.Sensor == InBody1.GetID() || Multi.Sensor == InBody2.GetID())
            {
                Logger->Debug("Found Character {} Adding to Multi Sensor {}", Character.id(), Multi.Sensor.GetIndexAndSequenceNumber());
                Multi.Characters.emplace(Character);
            }
        });

        Character.world().defer_end();
    }

    void Level::OnContactRemoved(const SubShapeIDPair &InSubShapePair)
    {   
        if (LevelMode == PMOCallerType::Client)
        {
            // do client only stuff
            return;
        }
        
        const auto InBody1 = InSubShapePair.GetBody1ID();
        const auto InBody2 = InSubShapePair.GetBody2ID();

        // TODO: calling the Lock interface gives us a mutex count assert, maybe it's safe to just
        // use the non lock interface since we are only checking the layer?
        const auto Body1Layer = Physics->System->GetBodyInterfaceNoLock().GetObjectLayer(InBody1);
        const auto Body2Layer = Physics->System->GetBodyInterfaceNoLock().GetObjectLayer(InBody2);

        // Make sure it's a Character
        if ((Body1Layer != physics::Layers::CHARACTER) && (Body2Layer != physics::Layers::CHARACTER))
        {
            return;
        }
                
        flecs::entity Character = CharacterQuery.find([&](character::PMOCharacter& Char) 
        {
            return Char.IsBody(InBody1) || Char.IsBody(InBody2);
        });

        if (!Character.is_valid() || !Character.is_alive())
        {
            Logger->Debug("OnContactRemoved Character not found or invalid\n");
            return;
        }

        // Remove characters if they are no longer contained in our sensor volume
        Character.world().defer_begin();
        ServerAreaOfInterestQuery.each([&](flecs::iter& It, size_t Index, level::MapChunk &Chunk, level::LocalSensor &Local, level::MultiSensor &Multi)
        {
            if (Local.Sensor == InBody1 || Local.Sensor == InBody2)
            {
                Local.Characters.erase(Character);
            }
            else if (Multi.Sensor == InBody1 || Multi.Sensor == InBody2)
            {
                Multi.Characters.erase(Character);
            }
        });

        Character.world().defer_end();
    }

    void Level::ServerLevelSystems(flecs::world& GameWorld)
    {
          // Respawn player in the world 
        GameWorld.system<character::Respawn, network::User, network::Connected, character::PMOCharacter, character::BindLocation>("RespawnPlayer")
            .kind(flecs::PreStore)
            .each([&](flecs::iter& It, size_t Index, character::Respawn, network::User &User, network::Connected, character::PMOCharacter &Character, character::BindLocation &BindLocation)
            {
                auto RespawnedPlayer = It.entity(Index);
                RespawnedPlayer.set<units::Vector3>({BindLocation.Location})
                    .set<units::Quat>({})
                    .set<units::Velocity>({})
                    .add(character::ActionType::NOOP)
                    .set<units::Attributes>({});
                
                Character.SetLocation(BindLocation.Location);
                
                RespawnedPlayer.remove<character::Respawn>();
                RespawnedPlayer.remove<character::IsDead>();
            });
    }

    void Level::InitAreaOfInterest(flecs::world& GameWorld)
    {
        // Iterate over each map chunk and local sensor
        GameWorld.system<level::MapChunk, level::LocalSensor, level::MultiSensor>("UpdatePlayerVisibility")
            .kind(flecs::PreStore)
            .write<character::Visibility>()
            .each([&](flecs::iter& It, size_t Index, level::MapChunk &Chunk, level::LocalSensor &Local, level::MultiSensor &Multi)
        {

            for (auto Character : Local.Characters)
            {
                if (!Character.is_alive())
                {
                    continue;
                }
                auto Visibility = Character.get_mut<character::Visibility>();
                // remove previous visibility sets
                //Logger->Info("PreStore: Clearing previous visibility");
                Visibility->Clear();
                // Logger->Info("PreStore: Adding local neighbors for {}", Character.id());
                // for (auto C : Local.Characters)
                // {
                //     Logger->Info("    - {}", C.id());
                // }
                // TODO: Determine if this is doing unnecessary work, won't they also be in the multi sensor no matter what??
                Visibility->Neighbors.insert(Local.Characters.begin(), Local.Characters.end());
                // Can this character see this damage event? Yes if they can see the target hit and the instigator
                CombatQuery.each([&Visibility, &Character](flecs::entity DamageEntity, combat::ApplyDamage &DamageEvent)
                {
                    //Logger->Info("PreStore: Local Vis: DamageEntity {} Inst: {} Hit: {}", DamageEntity.id(), DamageEvent.Instigator.id(), DamageEvent.TargetHit.id());
                    // if we are the instigator or target, then yes we can see it
                    if (DamageEvent.Instigator == Character || DamageEvent.TargetHit == Character)
                    {
                        Visibility->DamageEvents.insert(DamageEntity);
                        return;
                    }

                    auto SeesInstigator = Visibility->Neighbors.find(DamageEvent.Instigator);
                    if (SeesInstigator == Visibility->Neighbors.end()) 
                    {
                        return;
                    }

                    auto SeesTargetHit = Visibility->Neighbors.find(DamageEvent.TargetHit);
                    if (SeesTargetHit == Visibility->Neighbors.end())
                    {
                        return;
                    }
                    Visibility->DamageEvents.insert(DamageEntity);
                    return;
                });
            }

            for (auto Character : Multi.Characters)
            {
                if (!Character.is_alive())
                {
                    continue;
                }
                auto Visibility = Character.get_mut<character::Visibility>();
                
                // Logger->Info("PreStore: Adding multi neighbors for {}", Character.id());
                // for (auto C : Multi.Characters)
                // {
                //     Logger->Info("    - {}", C.id());
                // }
                Visibility->Neighbors.insert(Multi.Characters.begin(), Multi.Characters.end());
                // Can this character see this damage event? Yes if they can see the target hit and the instigator
                CombatQuery.each([&Visibility, &Character](flecs::entity DamageEntity, combat::ApplyDamage &DamageEvent)
                {
                    //Logger->Info("PreStore: Multi Vis: DamageEntity {} Inst: {} Hit: {}", DamageEntity.id(), DamageEvent.Instigator.id(), DamageEvent.TargetHit.id());
                    // if we are the instigator or target, then yes we can see it
                    if (DamageEvent.Instigator == Character || DamageEvent.TargetHit == Character)
                    {
                        Visibility->DamageEvents.insert(DamageEntity);
                        return;
                    }

                    auto SeesInstigator = Visibility->Neighbors.find(DamageEvent.Instigator);
                    if (SeesInstigator == Visibility->Neighbors.end()) 
                    {
                        return;
                    }

                    auto SeesTargetHit = Visibility->Neighbors.find(DamageEvent.TargetHit);
                    if (SeesTargetHit == Visibility->Neighbors.end())
                    {
                        return;
                    }
                    Visibility->DamageEvents.insert(DamageEntity);
                    return;
                });
            }
        });
    }
}