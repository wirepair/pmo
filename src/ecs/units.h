#pragma once
#include <flecs.h>

namespace units
{
    struct Vector3 
    {
        bool operator==(Vector3 const& Other) const { return (vec3[0] == Other.vec3[0] && vec3[1] == Other.vec3[1] && vec3[2] == Other.vec3[2]); };
        Vector3& operator+=(const Vector3& Other)
        { 
            vec3[0]+=Other.vec3[0];
            vec3[1]+=Other.vec3[1];
            vec3[2]+=Other.vec3[2];
            return *this; 
        };

        float vec3[3];

        void AddX(float x) { vec3[0] += x; }
        void AddY(float y) { vec3[1] += y; }
        void AddZ(float z) { vec3[2] += z; }
        Vector3& operator *=(const float Other)
        {
            vec3[0] *= Other;
            vec3[1] *= Other;
            vec3[2] *= Other;
            return *this; 
        }
    };

    struct Velocity 
    {
        bool operator==(Velocity const& Other) const { return (vec3[0] == Other.vec3[0] && vec3[1] == Other.vec3[1] && vec3[2] == Other.vec3[2]); };
        float vec3[3];
    };

    struct Quat 
    {
        bool operator==(Quat const& Other) const { return (vec4[0] == Other.vec4[0] && vec4[1] == Other.vec4[1] && vec4[2] == Other.vec4[2] && vec4[3] == Other.vec4[3]); };
        float vec4[4];
    };

    struct Box
    {
        Vector3 Min;
        Vector3 Max;
    };

    /**
     * @brief Used for comparisons in containers (such as unordered_set).
     * 
     */
    struct FlecsEntityHash 
    {
        std::size_t operator()(const flecs::entity& Entity) const 
        {
            return 3+Entity.id()*53;
        }
    };
    
    struct TraitAttributes
    {
        uint8_t Intelligence{};
        uint8_t Wisdom{};
        uint8_t Dexterity{};
        uint8_t Strength{};
        uint8_t Constitution{};
        
        bool operator==(TraitAttributes const& Other) const 
        { 
            return Intelligence == Other.Intelligence && Wisdom == Other.Wisdom && 
                    Dexterity == Other.Dexterity && Strength == Other.Strength && 
                    Constitution == Other.Constitution; 
        };

    };

    constexpr static float MaxHealth = 1000.f;
    constexpr static float MaxShield = 500.f;
    constexpr static float MaxStamina = 250.f;
    constexpr static float MaxMana = 1000.f;

    struct Attributes
    {
        float Health = 500.0f;
        float Shield = .0f;
        float Stamina = .0f;
        float Mana = .0f;

        bool operator==(Attributes const& Other) const { return Health == Other.Health && Shield == Other.Shield && Stamina == Other.Stamina && Mana == Other.Mana; };
    };

    struct Units
    {
        Units(flecs::world &GameWorld);
    };
}