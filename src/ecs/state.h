#pragma once
#ifdef IS_SERVER
#include <array>
#include <cstdint>
#include <unordered_set>

#include "schemas/server_generated.h"
#include "ecs/items/inventory.h"
#include "ecs/units.h"
#include "ecs/character/components.h"
#include "ecs/combat/combat.h"

namespace state
{
    struct PlayerEntity
    {
        uint64_t EntityId; // flecs server entity id
        units::Attributes Attributes;
        units::TraitAttributes Traits;
        units::Vector3 Position;
        units::Velocity Velocity;
        units::Quat Rotation;
        std::array<uint32_t, 15> Items{}; // item ids starting from head, see items::EquipmentSlot
        character::ActionType Action{}; // actions the player is doing (e.g. JUMPING)
        std::vector<combat::ApplyDamage> OutgoingAttacks;

        bool operator== (const PlayerEntity &Other) const { return EntityId == Other.EntityId; };

    };

    struct ItemEntity
    {
        uint64_t EntityId; // flecs server entity id
        uint32_t ItemId; // Prefab ID
        bool operator== (const ItemEntity &Other) const { return EntityId == Other.EntityId; };
    };

    struct NPCEntity
    {
        uint64_t EntityId; // flecs server entity id
        uint16_t NpcId; // NPC class ID (shared between server/clients)
        units::Attributes Attributes;
        units::Vector3 Position;
        units::Velocity Velocity;
        units::Quat Rotation;
        character::ActionType Action{}; // actions the npc is doing (e.g. JUMPING)
        std::vector<combat::ApplyDamage> OutgoingAttacks;

        bool operator== (const NPCEntity &Other) const { return EntityId == Other.EntityId; };
    };

    struct StructureEntity
    {
        uint64_t EntityId; // flecs server entity id
        uint16_t StructureId; // Structure class ID (shared between server/clients)
        units::Attributes Attributes;
        units::Vector3 Position;
        units::Quat Rotation;

        bool operator== (const StructureEntity &Other) const { return EntityId == Other.EntityId; };
    };

    template <class T>
    struct EntityHash 
    {
        std::size_t operator()(const T &Entity) const 
        {
            return 3+Entity.EntityId*53;
        }
    };

    struct WorldState
    {
        // make this class non-copyable
        WorldState(const WorldState &) = delete;
        void operator =(const WorldState &) = delete;

        // move assign
        WorldState& operator=(WorldState&& other)
        {
            PlayerPosition = std::move(other.PlayerPosition);
            PlayerRotation = std::move(other.PlayerRotation);
            PlayerVelocity = std::move(other.PlayerVelocity);
            Attributes = std::move(other.Attributes);
            Traits = std::move(other.Traits);
            OutgoingAttacks = std::move(other.OutgoingAttacks);
            Equipment = std::move(other.Equipment);
            Inventory = std::move(other.Inventory);
            KnownPlayerEntities = std::move(other.KnownPlayerEntities);
            KnownNPCEntities = std::move(other.KnownNPCEntities);
            KnownStructureEntities = std::move(other.KnownStructureEntities);
            OutEntities = std::move(other.OutEntities);
            return *this;
        }

        // move constructor
        WorldState(WorldState&& other) noexcept
        {
            PlayerPosition = std::move(other.PlayerPosition);
            PlayerRotation = std::move(other.PlayerRotation);
            PlayerVelocity = std::move(other.PlayerVelocity);
            Attributes = std::move(other.Attributes);
            Traits = std::move(other.Traits);
            OutgoingAttacks = std::move(other.OutgoingAttacks);
            Equipment = std::move(other.Equipment);
            Inventory = std::move(other.Inventory);
            KnownPlayerEntities = std::move(other.KnownPlayerEntities);
            KnownNPCEntities = std::move(other.KnownNPCEntities);
            KnownStructureEntities = std::move(other.KnownStructureEntities);
            OutEntities = std::move(other.OutEntities);
        }

        ~WorldState()
        {
            Inventory.clear();
            KnownPlayerEntities.clear();
            KnownNPCEntities.clear();
            KnownStructureEntities.clear();
            OutgoingAttacks.clear();
        }

        WorldState()
        {
            KnownPlayerEntities.reserve(500);
            KnownNPCEntities.reserve(200);
            KnownStructureEntities.reserve(200);
            OutgoingAttacks.reserve(15); // how many outgoing damage events this character is instigating
        };
        
        units::Vector3 PlayerPosition{};
        units::Quat PlayerRotation{};
        units::Velocity PlayerVelocity{};
        units::Attributes Attributes{};
        units::TraitAttributes Traits{};
        std::vector<combat::ApplyDamage> OutgoingAttacks{};
        std::array<ItemEntity, items::EquipSlotMax> Equipment{};
        std::set<ItemEntity, EntityHash<ItemEntity>> Inventory{};
        std::unordered_set<PlayerEntity, EntityHash<PlayerEntity>> KnownPlayerEntities{};
        std::unordered_set<NPCEntity, EntityHash<NPCEntity>> KnownNPCEntities{}; // can't imagine more than 200 NPCs in a zone...
        std::unordered_set<StructureEntity, EntityHash<StructureEntity>> KnownStructureEntities{};

        std::unordered_set<uint64_t> OutEntities{}; // entities which should be removed by clients
    };

    struct PlayerWorldStates
    {
        // make this class non-copyable
        PlayerWorldStates() = default;
        PlayerWorldStates(const PlayerWorldStates &) = delete;
        void operator =(const PlayerWorldStates &) = delete;

        // move assign
        PlayerWorldStates& operator=(PlayerWorldStates&& other)
        {
            States = std::move(other.States);
            Acks = std::move(other.Acks);
            SequenceId = other.SequenceId;
            return *this;
        }

        // move constructor
        PlayerWorldStates(PlayerWorldStates&& other) noexcept
        {
            States = std::move(other.States);
            Acks = std::move(other.Acks);
            SequenceId = other.SequenceId;
        }

        /**
         * @brief Creates a diff from the last Ack'd sequence until now (current SequenceId)
         * 
         * @param Log
         * @param Builder
         */
        void Delta(logging::Logger *Log, flatbuffers::FlatBufferBuilder &Builder, const uint64_t ServerTime);

        /**
         * @brief Fills out the WorldState Diff with KnownPlayerEntity information
         * 
         * @param Log 
         * @param Builder
         * @param CurrentSequenceId 
         * @param PreviousSequenceId 
         */
        void DeltaKnownPlayers(logging::Logger *Log, flatbuffers::FlatBufferBuilder &Builder, const uint64_t ServerTime, const uint8_t CurrentSequenceId, const uint8_t PreviousSequenceId);

        /**
         * @brief Takes in a builder, outgoing attacks and a vector to store the serialized damageevents
         * 
         * @param Builder 
         * @param OutgoingAttacks 
         * @param OutEvents 
         */
        void AddDamageEvents(flatbuffers::FlatBufferBuilder &Builder, const std::vector<combat::ApplyDamage> &OutgoingAttacks, std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> &OutEvents);

        /**
         * @brief Adds the players inventory to OutInventory
         * 
         * @param Builder 
         * @param Inventory 
         * @param OutInventory 
         */
        void AddItemInventory(flatbuffers::FlatBufferBuilder &Builder, const std::set<state::ItemEntity, state::EntityHash<state::ItemEntity>> &Inventory, std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> &OutInventory);

        /**
         * @brief Adds the players currently equipped items
         * 
         * @param Builder 
         * @param Equipment 
         * @param OutEquipment 
         */
        void AddEquipment(flatbuffers::FlatBufferBuilder &Builder, const std::array<ItemEntity, items::EquipSlotMax> &Equipment, std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> &OutEquipment);
        
        /**
         * @brief Updates our (self) outgoing attacks (we are the instigator)
         * 
         * @param Current 
         * @param Visibility 
         */
        void UpdateOutgoingAttacks(logging::Logger &Log, state::WorldState &Current, character::Visibility &Visibility);

        /**
         * @brief Updates state at our current SequenceId
         * 
         * @param Visibility
         * @param Attributes
         * @param Position 
         * @param Velocity
         * @param Rotation 
         */
        void UpdateState(logging::Logger &Log, character::Visibility &Visibility, units::Attributes &Attributes, units::TraitAttributes &Traits, units::Vector3 &Position, units::Velocity &Velocity, units::Quat &Rotation);

        /**
         * @brief Applies visibility for this player once per frame (tracked by sequence id). This includes attaching any outgoing attacks
         * that each visible player is "executing" or "instigating"
         * 
         * @param PlayerVisibility Which player entities the character can see and any combat/damage events in the area
         */
        void ApplyPlayerVisibility(logging::Logger &Log, character::Visibility &PlayerVisibility);

        /**
         * @brief Finds the last sequence id that has been ack'd by the client.
         * 
         * @param Start 
         * @return true 
         * @return false 
         */
        bool FindLastAckedSequence(uint8_t &Start);

        /**
         * @brief Acknowledges the client recieved a state specified by the AckId
         * 
         * @param AckId 
         */
        void AckId(uint8_t AckId);

        /**
         * @brief Checks that a particular sequence Id was Acknowledged by the client 
         * 
         * @param CheckId 
         * @return true 
         * @return false 
         */
        bool WasAckd(uint8_t CheckId);

        static const uint8_t MaxSequenceId = 24;
        std::array<WorldState, MaxSequenceId> States{};
        std::array<bool, MaxSequenceId> Acks{};
        uint8_t SequenceId = 0;
    };
}
#endif