#pragma once

#include <flecs.h>
#include <Jolt/Jolt.h>
#include <Jolt/Physics/Body/BodyFilter.h>
#include <Jolt/Physics/Collision/Shape/Shape.h>
#include <Jolt/Physics/Collision/Shape/SphereShape.h>
#include "config/config.h"
#include "damageclass.h"
#include "ecs/physics/physics.h"
#include "ecs/character/network.h"
#include "math/curves/curve.h"

// fwd declare so we don't have a cyclical include
namespace character
{
    struct PMOCharacter;
}

namespace combat 
{
    // Weapon type tags for animation sets
    enum WeaponSet
    {
        SwordAndShield,
        SpearAndShield,
        BowAndArrow,
        MageStaff,
    };

    struct AnimSet
    {
        uint8_t ComboIdx;
        curves::Curve AnimCurve;
    };

    const float MaxHealth = 2000.f;
    const float MaxShield = 2000.f;
    /**
     * @brief Shape of our collision trace for combat
     * 
     */
    enum AttackContactShape
    {
        Sphere,
        Line,
    };

    /**
     * @brief Data shared between attacks and how we process them in OnHit
     * 
     */
    struct AttackDetails
    {
        flecs::entity Instigator;
        float BaseDamage{};
        float RadiusOrLength{};
        double DelayStartTime{};
        double RunTime{};
        double CanTransitionTime{};
        uint8_t NumTargets = 1;
        uint8_t NumHits = 0;
        AttackContactShape AttackShape = Sphere;
        DamageClass Type{};
        bool RunOnce = true; // If false, will run every frame from DelayStartTime -> RunTime 
        
        double CumulativeTime{};
        JPH::BodyID AttackBodyID{};
    };

    /**
     * @brief For deserializing over the network and executing on client side systems.
     * 
     */
    struct NetDamage
    {
        flecs::entity Instigator;
        flecs::entity TargetHit;
        DamageClass Type;
        float Amount;
    };

    /**
     * @brief A client only tag so we can run a client based system to execute vfx/sfx etc.
     * 
     */
    struct NotifyDamageEvent
    {
        flecs::entity TargetHit;
        units::Vector3 HitPosition;
        DamageClass Type;
    };

    struct ApplyDamage
    {
        flecs::entity Instigator;
        flecs::entity TargetHit;
        JPH::Vec3 HitPosition;
        float BaseDamage;
        DamageClass Type;
        
        /**
         * @brief Equality function for storing in unordered sets
         * 
         * @param Other 
         * @return true 
         * @return false 
         */
        bool operator== (const ApplyDamage &Other) const 
        {
            return Instigator.id() == Other.Instigator.id() && TargetHit.id() == Other.TargetHit.id() && 
                    HitPosition == Other.HitPosition && BaseDamage == Other.BaseDamage &&
                    Type == Other.Type;
        }
    };


    struct ApplyDamageHash
    {
        /**
         * @brief Hash function for storing in unordered sets
         * 
         * @param In 
         * @return std::size_t 
         */
        std::size_t operator()(const ApplyDamage &In) const 
        {
            return In.Instigator.id() ^ In.TargetHit.id() ^ std::hash<float>{}(In.HitPosition.GetX()) ^
                std::hash<float>{}(In.HitPosition.GetY()) ^ std::hash<float>{}(In.HitPosition.GetZ()) ^
                std::hash<float>{}(In.BaseDamage) ^ std::hash<uint8_t>{}(static_cast<uint8_t>(In.Type));
        }
    };

    struct DamageApplied {}; // Tag to know we've already applied this damage event and can filter it out

    /**
     * @brief Signal the player wants the server to respawn them
     * 
     */
    struct RequestRespawn 
    {
        double RequestedTime{}; 
    };

    struct Combat : JPH::ContactListener
    {
        // make this class non-copyable
        Combat(const Combat &) = delete;
        void operator =(const Combat &) = delete;

        /**
         * @brief Initialize our Combat system(s) that don't require physics
         * 
         * @param GameWorld 
         */
        Combat(flecs::world &GameWorld);

        /**
         * @brief Initialize our Combat system(s)
         * 
         * @param Log 
         * @param Physics 
         */
        void Init(logging::Logger *Log, flecs::world &GameWorld, physics::Physics *Physics);

        /**
         * @brief 
         * 
         * @param Details 
         * @param Physics 
         * @param Pos 
         * @param Rot 
         * @return * void 
         */
        JPH::BodyID InitializeAttack(const AttackDetails &Details, const JPH::Vec3 &Pos, const JPH::Quat &Rot) const;

        // See: JPH::ContactListener
        virtual void OnContactAdded(const JPH::Body &inBody1, const JPH::Body &inBody2, const JPH::ContactManifold &inManifold, JPH::ContactSettings &ioSettings) override;
        
        flecs::world &World;
        logging::Logger *Logger;
        physics::Physics *PhysicsSystem;
        #ifdef IS_SERVER
        flecs::query<character::PMOCharacter> OnHitTargetQuery;
        #else
        flecs::query<character::NetworkCharacter> OnHitTargetQuery;
        #endif
        flecs::query<AttackDetails> OnHitAttackQuery;
    };

    /**
     * @brief Class to draw only the provided jolt body to the debug renderer
     * 
     */
    class DrawAttackFilter : public JPH::BodyDrawFilter
    {
    public:
        DrawAttackFilter(JPH::BodyID BodyToCheck) : BodyToCheck(BodyToCheck) {};

        virtual bool ShouldDraw([[maybe_unused]] const Body& inBody) const override
        {
            return inBody.GetID() == BodyToCheck;
        };

        JPH::BodyID BodyToCheck;
    };

}
