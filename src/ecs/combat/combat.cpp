#include <algorithm>
#include "combat.h"
#include "ecs/physics/broadphase.h"
#include "ecs/convert.h"
#include "ecs/units.h"
#include "ecs/physics/physics.h"
#include "ecs/character/components.h"
#include "ecs/character/pmocharacter.h"
#include <Jolt/Physics/Constraints/ContactConstraintManager.h>
#include <Jolt/Physics/Collision/Shape/BoxShape.h>


namespace combat
{
    Combat::Combat(flecs::world &GameWorld) : World(GameWorld)
    {
        GameWorld.component<AttackDetails>();
        GameWorld.component<WeaponSet>().add(flecs::Union);
    }

    void Combat::OnContactAdded(const JPH::Body &InBody1, const JPH::Body &InBody2, const JPH::ContactManifold &InManifold, JPH::ContactSettings &IOSettings)
    {
        //Logger->Debug("Combat::OnContactAdded InBody1ID: {} InBody2ID: {}", InBody1.GetID().GetIndexAndSequenceNumber(), InBody2.GetID().GetIndexAndSequenceNumber());
        // Validate this contact hit is from a sensor, otherwise bail out early
        if (!IOSettings.mIsSensor)
        {
            //Logger->Info("Combat::OnContactAdded returning since not is Sensor");
            return;
        }
        // Also make sure this is for Character and Combat layers
        auto BodyIsCombat = InBody1.GetObjectLayer() == physics::Layers::COMBAT || InBody2.GetObjectLayer() == physics::Layers::COMBAT;
        auto BodyIsCharacter = InBody1.GetObjectLayer() == physics::Layers::CHARACTER || InBody2.GetObjectLayer() == physics::Layers::CHARACTER;
        if (!BodyIsCombat || !BodyIsCharacter)
        {
            //Logger->Info("Combat::OnContactAdded returning since not char or combat InBody1ID: {} InBody2ID: {}", InBody1.GetID().GetIndexAndSequenceNumber(), InBody2.GetID().GetIndexAndSequenceNumber());
            return;
        }
        
        Logger->Info("OnUpdate: Combat::OnContactAdded looking up target hit");
        #ifdef IS_SERVER
        flecs::entity Target = OnHitTargetQuery.find([&](character::PMOCharacter& Char) 
        {
            return Char.IsBody(InBody1.GetID()) || Char.IsBody(InBody2.GetID());
        });
        #else
        flecs::entity Target = OnHitTargetQuery.find([&](character::NetworkCharacter& Char) 
        {
            return Char.IsBody(InBody1.GetID()) || Char.IsBody(InBody2.GetID());
        });
        #endif
        if (!Target.is_valid() || !Target.is_alive())
        {
            Logger->Debug("OnContactAdded Attack Target not found or invalid");
            return;
        }

        flecs::entity Attack = OnHitAttackQuery.find([&](AttackDetails &Attack) 
        {
            Logger->Debug("OnContactAdded Attack.AttackBodyID: {} InBody1ID: {} InBody2ID: {}", Attack.AttackBodyID.GetIndexAndSequenceNumber(), InBody1.GetID().GetIndexAndSequenceNumber(), InBody2.GetID().GetIndexAndSequenceNumber());
            return (Attack.AttackBodyID == InBody1.GetID() || Attack.AttackBodyID == InBody2.GetID());
        });

        if (!Attack.is_valid() || !Attack.is_alive())
        {
            Logger->Debug("OnContactAdded Attack not found or invalid");
            return;
        }

        auto Details = Attack.get_mut<AttackDetails>();
        if (!Details || Target == Details->Instigator)
        {
            Logger->Debug("OnContactAdded Attack Entity did not have AttackDetails component");
            return;
        }

        // This should be safe to compare/increment since OnContactAdded calls are protected by a mutex in our physics::ContactDispatcher
        if (Details->NumHits >= Details->NumTargets)
        {
            return;
        }
        Details->NumHits++;
        
        // Create DamageApplied entity so our system can pick it up and 
        // do the calculations necessary to apply damage
        auto DamageApplied = Target.world().entity();
        #ifdef IS_SERVER
        DamageApplied.set<combat::ApplyDamage>({Details->Instigator, Target, InManifold.GetWorldSpaceContactPointOn1(0), Details->BaseDamage});
        #else
        // we know it's coming from us, so just include target/type and hit location
        DamageApplied.set<combat::NotifyDamageEvent>({Target, convert::JPHVec3ToVector3(InManifold.GetWorldSpaceContactPointOn1(0)), Details->Type});
        #endif
        Logger->Info("OnUpdate: DamageApplied {} entity created Instigator: {} Target: {}", Details->BaseDamage, Details->Instigator.id(), Target.id());
    }

    void Combat::Init(logging::Logger *Log, flecs::world &GameWorld, physics::Physics *Physics)
    {
        Logger = Log;
        PhysicsSystem = Physics;
        OnHitAttackQuery = GameWorld.query<AttackDetails>();
        #ifdef IS_SERVER
        OnHitTargetQuery = GameWorld.query<character::PMOCharacter>();
        #else
        OnHitTargetQuery = GameWorld.query<character::NetworkCharacter>();
        #endif
        // may be null in tests
        if (PhysicsSystem)
        {
            PhysicsSystem->ContactEventDispatcher->AddListener(this);
        }
        else 
        {
            Logger->Warn("Combat::Init PhysicsSystem was null");
        }

        GameWorld.system<AttackDetails>("PlayerAttack")
            .kind(flecs::PreUpdate)
            .write<character::ActionType>()
            .each([this](flecs::iter& It, size_t Index, AttackDetails &Attack)
            {
                if (!Attack.Instigator.is_valid() || !Attack.Instigator.is_alive())
                {
                    Logger->Warn("Attack instigator is no longer valid, attack failed");
                    return;
                }

                auto Attacker = Attack.Instigator.get<character::PMOCharacter>();

                auto ForwardVector = Attacker->GetForwardVector();
                float ArmReach = 1.0f + Attacker->GetCapsuleRadius();
                ForwardVector *= ArmReach;

                auto Position = Attacker->GetPosition();
                Position += ForwardVector;
                auto UpdatedPosition = convert::Vector3ToJPHVec3(Position);

                // only initialize attack after:
                // - Start delay has passed.
                // - The physics body isn't active
                if (Attack.CumulativeTime >= Attack.DelayStartTime && Attack.AttackBodyID.IsInvalid())
                {
                    auto UpdatedRotation = convert::QuatToJPHQuat(Attacker->GetRotation());
                    Attack.AttackBodyID = InitializeAttack(Attack, UpdatedPosition, UpdatedRotation);
                    Logger->Info("PreUpdate: Attacking with Base Dmg: {} Pos: {} {} {} Instigator: {} AttackBodyId: {}", Attack.BaseDamage, UpdatedPosition.GetX(), UpdatedPosition.GetY(), UpdatedPosition.GetZ(), Attack.Instigator.id(), Attack.AttackBodyID.GetIndexAndSequenceNumber());
                }
                // if we only run once, and our attack is past the delay start time and the body is valid then we want to remove it
                else if (Attack.RunOnce && (Attack.CumulativeTime >= Attack.DelayStartTime) && !Attack.AttackBodyID.IsInvalid())
                {
                    Logger->Info("PreUpdate: Removing attack collision body: {} RunOnce: {}, CumulativeTime: {}, DelayStartTime: {} IsInvalid: {}", 
                        It.entity(Index).id(), Attack.RunOnce, Attack.CumulativeTime, Attack.DelayStartTime, Attack.AttackBodyID.IsInvalid());

                    PhysicsSystem->System->GetBodyInterface().RemoveBody(Attack.AttackBodyID);
                    PhysicsSystem->System->GetBodyInterface().DestroyBody(Attack.AttackBodyID);
                    Attack.AttackBodyID = JPH::BodyID(JPH::BodyID::cInvalidBodyID); // immediately set it to invalid
                }

                // Handle removing or updating
                Attack.CumulativeTime += It.delta_time(); 
                auto AttackEntity = It.entity(Index);
                
                // We've already destroyed the physics body
                if (Attack.RunOnce && Attack.CumulativeTime >= Attack.RunTime)
                {
                    AttackEntity.destruct();
                }
                else if (!Attack.RunOnce && Attack.CumulativeTime >= Attack.RunTime && !Attack.AttackBodyID.IsInvalid())
                {
                    Logger->Info("PreUpdate: Removing Long Running Attack");
                    PhysicsSystem->System->GetBodyInterface().RemoveBody(Attack.AttackBodyID);
                    PhysicsSystem->System->GetBodyInterface().DestroyBody(Attack.AttackBodyID);
                    AttackEntity.destruct();
                }
                else if (!Attack.AttackBodyID.IsInvalid())
                {
                    // TODO: Here we would move the body if it's a projectile or something else
                    // Logger->Info("Updating Pos: {} {} {}", UpdatedPosition.GetX(), UpdatedPosition.GetY(), UpdatedPosition.GetZ());
                    if (PhysicsSystem->DebugRenderer)
                    {
                        JPH::BodyManager::DrawSettings BodyDrawSettings;
                        BodyDrawSettings.mDrawShapeWireframe = true;

                        auto AttackFilter = std::make_unique<DrawAttackFilter>(Attack.AttackBodyID);
                        PhysicsSystem->System->DrawBodies(BodyDrawSettings, PhysicsSystem->DebugRenderer.get(), static_cast<JPH::BodyDrawFilter *>(AttackFilter.get()));
                    }
                }
                
            });

        #ifdef IS_SERVER
        GameWorld.system<combat::ApplyDamage>("OnApplyDamage")
            .kind(flecs::PostUpdate)
            .without<combat::DamageApplied>() // so we don't apply multiple times
            .write<units::Attributes>()
            .write<character::IsDead>()
            .write<character::ActionType>()
            .each([this](flecs::iter& It, size_t Index, combat::ApplyDamage &Damage)
            {
                Logger->Info("PostUpdate: ApplyDamage called");
                if(!Damage.TargetHit.is_alive() || !Damage.TargetHit.has<units::Attributes>())
                {
                    return;
                }
                
                if (Damage.TargetHit.has<character::IsDead>())
                {
                    Logger->Info("PostUpdate: player already dying or dead.");
                    return;
                }

                auto RemainingDamage = Damage.BaseDamage;
                auto EntityAttribs = Damage.TargetHit.get_mut<units::Attributes>();
                auto RemainingShield = EntityAttribs->Shield - RemainingDamage;
                EntityAttribs->Shield = std::clamp(static_cast<float>(RemainingShield), 0.f, MaxShield);

                // Set damage to whatever negative amount of shield is left or zero
                RemainingDamage =  (RemainingShield <= 0.f) ?  abs(RemainingShield) : 0.f;
                EntityAttribs->Health = std::clamp(EntityAttribs->Health - RemainingDamage, 0.f, MaxHealth);
                if (EntityAttribs->Health == 0.f)
                {
                    Damage.TargetHit.set<character::IsDead>({.DiedAt=0.f});
                    Damage.TargetHit.add(character::ActionType::DEAD);
                    Logger->Info("PostUpdate: Target {} died!", Damage.TargetHit.id());
                }
                // Destroy the applydamage entity so we don't keep calculating damage.
                Logger->Info("PostUpdate: Doing {} damage to {} with {} health remaining!", Damage.BaseDamage, Damage.TargetHit.id(), EntityAttribs->Health);
                It.entity(Index).add<combat::DamageApplied>();
            });

        // Clears damage events after they've been captured by OnServerUpdateState but before the next world tick
        GameWorld.system<combat::ApplyDamage, combat::DamageApplied>("OnClearDamageEvents")
            .kind(flecs::PostFrame) // MUST CALL AFTER State OnServerUpdateState which is flecs::OnStore
            .each([this](flecs::iter& It, size_t Index, combat::ApplyDamage, combat::DamageApplied)
            {
                auto ApplyDamageEntity = It.entity(Index);
                Logger->Info("PostFrame: Destroying apply damage entity: {}", ApplyDamageEntity.id());
                ApplyDamageEntity.destruct();
            });
        #endif
    }

    JPH::BodyID Combat::InitializeAttack(const AttackDetails &Details, const JPH::Vec3 &Pos, const JPH::Quat &Rot) const
    {
        // Create Physics system shape & register it
        JPH::BodyCreationSettings BodySettings;
        if (Details.AttackShape == AttackContactShape::Sphere)
        {
            BodySettings = JPH::BodyCreationSettings(new JPH::SphereShape(Details.RadiusOrLength), Pos, Rot, JPH::EMotionType::Static, physics::Layers::COMBAT);
        } 
        else if (Details.AttackShape == AttackContactShape::Line)
        {
            // TODO: need more info for box size for now 1 meter by 1 meter
            BodySettings = JPH::BodyCreationSettings(new JPH::BoxShape(JPH::Vec3(Details.RadiusOrLength, 1.f, 1.f)), Pos, Rot, JPH::EMotionType::Static, physics::Layers::COMBAT);
        }
        BodySettings.mIsSensor = true;

        JPH::BodyInterface &BodyInterface = PhysicsSystem->System->GetBodyInterface();
        return BodyInterface.CreateAndAddBody(BodySettings, EActivation::Activate);
    }
}