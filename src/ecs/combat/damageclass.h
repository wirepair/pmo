#pragma once

#include <map>
#include <string_view>

namespace combat
{
    struct Resistances
    {
        float Fall{};

        float Peirce{};
        float Slash{};
        float Blunt{};

        float Fire{};
        float Ice{};
        float Water{};
        float Lightning{};
        float Poison{};
        float Holy{};
        float Corruption{};
        float Blood{};
    };

    enum class DamageClass : uint8_t
    {
        INVALID,
        FALL,
        PEIRCE,
        SLASH,
        BLUNT,
        FIRE,
        ICE,
        WATER,
        LIGHTNING,
        POISON,
        HOLY,
        CORRUPTION,
        BLOOD,
    };

    const std::map<std::string_view, DamageClass> DamageClasses =
    {
        {"Invalid", DamageClass::INVALID}, 
        {"Fall", DamageClass::FALL},
        {"Peirce", DamageClass::PEIRCE},
        {"Slash", DamageClass::SLASH},
        {"Blunt", DamageClass::BLUNT},
        {"Fire", DamageClass::FIRE},
        {"Ice", DamageClass::ICE},
        {"Water", DamageClass::WATER},
        {"Lightning", DamageClass::LIGHTNING},
        {"Poison", DamageClass::POISON},
        {"Holy", DamageClass::HOLY},
        {"Corruption", DamageClass::CORRUPTION},
        {"Blood", DamageClass::BLOOD},
    };

    inline const char * const *EnumDamageClass() 
    {
        static const char * const names[14] = 
        {
            "Invalid",
            "Fall",
            "Peirce",
            "Slash",
            "Blunt",
            "Fire",
            "Ice",
            "Water",
            "Lightning",
            "Poison",
            "Holy",
            "Corruption",
            "Blood",
            nullptr
        };
        return names;
    }
}