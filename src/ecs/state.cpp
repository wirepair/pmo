#ifdef IS_SERVER
#include "state.h"
#include <ranges>
#include <algorithm>

namespace state
{
    using FlatBufferDamageEvent = flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Game::Message::DamageEvent>>>;
    using FlatBufferInventory = flatbuffers::Offset<flatbuffers::Vector<flatbuffers::Offset<Game::Message::InventoryEntity>>>;
    using FlatBufferEquipment = FlatBufferInventory;

    void PlayerWorldStates::Delta(logging::Logger *Log, flatbuffers::FlatBufferBuilder &Builder, uint64_t ServerTime)
    {
        uint8_t LastAckdSequenceId = (SequenceId + MaxSequenceId - 1) % MaxSequenceId;
        auto FoundAck = WasAckd(LastAckdSequenceId) ? true : FindLastAckedSequence(LastAckdSequenceId);

        auto CurrentSequenceId = SequenceId;

        // Update our SequenceId here so we can exit early if first run
        SequenceId = (SequenceId + 1) % MaxSequenceId;

        //Log->Debug("Delta: foundack: {} Last Seq: {} Cur Seq: {}", FoundAck, LastAckdSequenceId, CurrentSequenceId);

        // If this is the first check or we haven't recv'd any acks yet, we need to send everything we see as-is.
        if (!FoundAck)
        {
            auto Idx = CurrentSequenceId;
            auto &Current = States.at(Idx);
            // Iterate over all KnownPlayers and create Entity->PlayerEntity details
            std::vector<flatbuffers::Offset<Game::Message::Entity>> NewEntities{};
            NewEntities.reserve(Current.KnownPlayerEntities.size()); // +States.at(Idx).KnownNPCEntities.size()+States.at(Idx).KnownStructureEntities.size());
            for (auto Player : Current.KnownPlayerEntities)
            {
                FlatBufferDamageEvent Attacks;
                std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> OutDamageEvents;
                // Add damage events this player instigates, but only if there's damage events
                AddDamageEvents(Builder, Player.OutgoingAttacks, OutDamageEvents);
                Log->Info("PostFrame: Adding player {} to NewEntities for !FoundAck has attacks: {}", Player.EntityId, Player.OutgoingAttacks.size());
                if (OutDamageEvents.size() > 0) 
                {
                    Attacks = Builder.CreateVector<Game::Message::DamageEvent>(OutDamageEvents.data(), OutDamageEvents.size());
                }

                Game::Message::PlayerEntityBuilder PlayEnt(Builder);
                
                if (OutDamageEvents.size() > 0) 
                {
                    PlayEnt.add_damage_events(Attacks.o);
                }

                PlayEnt.add_action(static_cast<uint8_t>(Player.Action));

                auto Attributes = Game::Message::Attributes(Current.Attributes.Health, Current.Attributes.Shield, Current.Attributes.Stamina,Current.Attributes.Mana);
                PlayEnt.add_attributes(&Attributes);
                
                auto Traits = Game::Message::Traits(Current.Traits.Intelligence, Current.Traits.Wisdom, Current.Traits.Dexterity, Current.Traits.Strength, Current.Traits.Constitution);
                PlayEnt.add_traits(&Traits);
                
                auto PlayerPos = Game::Message::Vec3(Player.Position.vec3[0], Player.Position.vec3[1], Player.Position.vec3[2]);
                PlayEnt.add_pos(&PlayerPos);

                auto PlayerVel = Game::Message::Vec3(Player.Velocity.vec3[0], Player.Velocity.vec3[1], Player.Velocity.vec3[2]);
                PlayEnt.add_vel(&PlayerVel);

                auto PlayerRot = Game::Message::Quat4(Player.Rotation.vec4[0], Player.Rotation.vec4[1], Player.Rotation.vec4[2], Player.Rotation.vec4[3]);
                PlayEnt.add_rot(&PlayerRot);
                
                auto PlayerEntFinished = PlayEnt.Finish();

                Game::Message::EntityBuilder Ent(Builder);
                Ent.add_id(Player.EntityId);
                Ent.add_type(Game::Message::EntityType_Player);
                Ent.add_player(PlayerEntFinished.o);
                
                auto EntityFinished = Ent.Finish();
                NewEntities.emplace_back(EntityFinished);
                Log->Info("PostFrame: Adding player {} to NewEntities for !FoundAck", Player.EntityId);
            }


            // Add our (self) characters attacks if we have any
            Log->Info("PostFrame: This Player has attacks: {}", Current.OutgoingAttacks.size());
            FlatBufferDamageEvent OurAttacks;
            if (Current.OutgoingAttacks.size() > 0) 
            {
                std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> OutDamageEvents;
                AddDamageEvents(Builder, Current.OutgoingAttacks, OutDamageEvents);
                OurAttacks = Builder.CreateVector<Game::Message::DamageEvent>(OutDamageEvents.data(), OutDamageEvents.size());
            }

            FlatBufferInventory OurInventory;
            if (Current.Inventory.size() > 0) 
            {
                std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> OutInventory;
                AddItemInventory(Builder, Current.Inventory, OutInventory);
                OurInventory = Builder.CreateVector<Game::Message::InventoryEntity>(OutInventory.data(), OutInventory.size());
            }

            std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> OutEquipment;
            AddEquipment(Builder, Current.Equipment, OutEquipment);
            FlatBufferEquipment OurEquipment = Builder.CreateVector<Game::Message::InventoryEntity>(OutEquipment.data(), OutEquipment.size());
    
            auto AllNewEntities = Builder.CreateVector<Game::Message::Entity>(NewEntities.data(), NewEntities.size());

            Game::Message::StateBuilder Diff(Builder);

            Diff.add_equipment(OurEquipment.o);

            if (Current.OutgoingAttacks.size() > 0) 
            {
                Diff.add_damage_events(OurAttacks.o);
            }

            if (Current.Inventory.size() > 0)
            {
                Diff.add_inventory(OurInventory.o);
            }

            Diff.add_in_entities(AllNewEntities.o);

            auto Pos = Current.PlayerPosition;
            auto PlayerPos = Game::Message::Vec3(Pos.vec3[0], Pos.vec3[1], Pos.vec3[2]);
            Diff.add_pos(&PlayerPos);

            auto Vel = Current.PlayerVelocity;
            auto PlayerVel = Game::Message::Vec3(Vel.vec3[0], Vel.vec3[1], Vel.vec3[2]);
            Diff.add_vel(&PlayerVel);
            
            // TODO: Switch to using angles
            auto Rot = Current.PlayerRotation;
            auto PlayerRot = Game::Message::Quat4(Rot.vec4[0], Rot.vec4[1], Rot.vec4[2], Rot.vec4[3]);
            Diff.add_rot(&PlayerRot);

            auto Attributes = Game::Message::Attributes(Current.Attributes.Health, Current.Attributes.Shield, Current.Attributes.Stamina, Current.Attributes.Mana);
            Diff.add_attributes(&Attributes);

            auto Traits = Game::Message::Traits(Current.Traits.Intelligence, Current.Traits.Wisdom, Current.Traits.Dexterity, Current.Traits.Strength, Current.Traits.Constitution);
            Diff.add_traits(&Traits);


            auto DiffResult = Diff.Finish();
            Game::Message::ServerCommandBuilder ServerCommand(Builder);
            ServerCommand.add_opcode(Game::Message::ServerOpCode_Update);
            ServerCommand.add_sequence_id(CurrentSequenceId);
            ServerCommand.add_server_time(ServerTime);
            ServerCommand.add_state(DiffResult);
            //Log->Debug("Sending non-delta update to client");
            Builder.Finish(ServerCommand.Finish());
            return;
        }
        
        //Log->Info("DeltaKnownPlayers Current: {} LastAckd: {}", CurrentSequenceId, LastAckdSequenceId);
        DeltaKnownPlayers(Log, Builder, ServerTime, CurrentSequenceId, LastAckdSequenceId);
    }

    bool PlayerWorldStates::FindLastAckedSequence(uint8_t &Start)
    {
        auto PrevSeq = Start;

        for (int i = MaxSequenceId; i > 0; i--)
        {
            PrevSeq = (PrevSeq + MaxSequenceId - 1) % MaxSequenceId;
            if (Acks.at(PrevSeq))
            {
                return true;
            }
        }
        return false;
    }

    void PlayerWorldStates::AckId(uint8_t AckId)
    {
        Acks.at(AckId) = true;
    }

    void PlayerWorldStates::DeltaKnownPlayers(logging::Logger *Log, flatbuffers::FlatBufferBuilder &Builder, const uint64_t ServerTime, const uint8_t CurrentSequenceId, const uint8_t PreviousSequenceId)
    {
        auto &Current = States.at(CurrentSequenceId);
        auto &Previous = States.at(PreviousSequenceId);

        std::vector<flatbuffers::Offset<Game::Message::Entity>> UpdatedEntities{};
        std::vector<flatbuffers::Offset<Game::Message::Entity>> NewEntities{};
        std::vector<uint64_t> OutEntities{};

        // Just assume we won't have more than whatever the largest is here.
        auto Size = (Current.KnownPlayerEntities.size() > Previous.KnownPlayerEntities.size()) ? Current.KnownPlayerEntities.size() : Previous.KnownPlayerEntities.size();
        UpdatedEntities.reserve(Size);
        NewEntities.reserve(Size);
        OutEntities.reserve(Size);

        // Handle Updated Players
        for (auto PreviousPlayer : Previous.KnownPlayerEntities)
        {
            auto PlayerToUpdate = Current.KnownPlayerEntities.find(PreviousPlayer);

            // We no longer see the player, remove them
            if (PlayerToUpdate == Current.KnownPlayerEntities.end())
            {
                // TODO: We want to actually cache outentites and only send it if it's "true" after a certain period of time
                // for performance reasons.
                OutEntities.emplace_back(PreviousPlayer.EntityId);
                Log->Info("PostFrame: Adding player {} to OutEntities", PreviousPlayer.EntityId);
                continue;
            }

            Log->Info("PostFrame: Adding player {} to UpdatedEntities with attacks: {} ", PreviousPlayer.EntityId, PlayerToUpdate->OutgoingAttacks.size());
            // We still see the other player, do updates
            FlatBufferDamageEvent PlayerAttacks;
            if (PlayerToUpdate->OutgoingAttacks.size() > 0) 
            {
                std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> OutDamageEvents;
                // Add damage events this player instigates
                AddDamageEvents(Builder, PlayerToUpdate->OutgoingAttacks, OutDamageEvents);
                PlayerAttacks = Builder.CreateVector<Game::Message::DamageEvent>(OutDamageEvents.data(), OutDamageEvents.size());
            }

            Game::Message::PlayerEntityBuilder PlayEnt(Builder);

            if (PlayerToUpdate->OutgoingAttacks.size() > 0) 
            {
                PlayEnt.add_damage_events(PlayerAttacks.o);
            }

            // Always set action otherwise it'll end up being set to NOOP (it's only one byte anyways)
            PlayEnt.add_action(static_cast<uint8_t>(PlayerToUpdate->Action));
                        
            //Log->Info("State Update: Character {} state: {}", PlayerToUpdate->EntityId, static_cast<uint8_t>(PlayerToUpdate->Action));

            if (PlayerToUpdate->Position != PreviousPlayer.Position)
            {
                auto PlayerPos = Game::Message::Vec3(PlayerToUpdate->Position.vec3[0], PlayerToUpdate->Position.vec3[1], PlayerToUpdate->Position.vec3[2]);
                PlayEnt.add_pos(&PlayerPos);
            }

            if (PlayerToUpdate->Velocity != PreviousPlayer.Velocity)
            {
                auto PlayerVel = Game::Message::Vec3(PlayerToUpdate->Velocity.vec3[0], PlayerToUpdate->Velocity.vec3[1], PlayerToUpdate->Velocity.vec3[2]);
                PlayEnt.add_vel(&PlayerVel);
            }

            if (PlayerToUpdate->Rotation != PreviousPlayer.Rotation)
            {
                auto PlayerRot = Game::Message::Quat4(PlayerToUpdate->Rotation.vec4[0], PlayerToUpdate->Rotation.vec4[1], PlayerToUpdate->Rotation.vec4[2], PlayerToUpdate->Rotation.vec4[3]);
                PlayEnt.add_rot(&PlayerRot);
            }

            if (PlayerToUpdate->Attributes != PreviousPlayer.Attributes)
            {
                auto Attributes = Game::Message::Attributes(PlayerToUpdate->Attributes.Health, PlayerToUpdate->Attributes.Shield, PlayerToUpdate->Attributes.Stamina, PlayerToUpdate->Attributes.Mana);
                PlayEnt.add_attributes(&Attributes);
            }

            if (PlayerToUpdate->Traits != PreviousPlayer.Traits)
            {
                auto Traits = Game::Message::Traits(PlayerToUpdate->Traits.Intelligence, PlayerToUpdate->Traits.Wisdom, PlayerToUpdate->Traits.Dexterity, PlayerToUpdate->Traits.Strength, PlayerToUpdate->Traits.Constitution);
                PlayEnt.add_traits(&Traits);
            }
            
            auto PlayerEntFinished = PlayEnt.Finish();

            Game::Message::EntityBuilder Ent(Builder);
            Ent.add_player(PlayerEntFinished.o);
            Ent.add_id(PlayerToUpdate->EntityId);
            Ent.add_type(Game::Message::EntityType_Player);
            
            auto EntityFinished = Ent.Finish();
            UpdatedEntities.emplace_back(EntityFinished);
            //Log->Debug("Adding player {} to UpdatedEntities", Player->EntityId);
        }

        // Handle New Players
        // Loop again for new players that don't exist in our previous known player list
        for (auto NewlySeenPlayer : Current.KnownPlayerEntities)
        {
            auto PreviousPlayer = Previous.KnownPlayerEntities.find(NewlySeenPlayer);

            if (PreviousPlayer != Previous.KnownPlayerEntities.end())
            {
                // we've already seen this player and updates were handled above
                continue;
            }
            Log->Info("PostFrame: Adding player {} to NewEntites without going attacks: {}", NewlySeenPlayer.EntityId, NewlySeenPlayer.OutgoingAttacks.size());
            // we have a new player, need to add them
            FlatBufferDamageEvent NewPlayerAttacks;
            if (NewlySeenPlayer.OutgoingAttacks.size() > 0)
            {
                std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> OutDamageEvents;
                // Add damage events this previously known player instigates
                AddDamageEvents(Builder, NewlySeenPlayer.OutgoingAttacks, OutDamageEvents);
                NewPlayerAttacks = Builder.CreateVector<Game::Message::DamageEvent>(OutDamageEvents.data(), OutDamageEvents.size());
            }

            Game::Message::PlayerEntityBuilder PlayEnt(Builder);
            if (NewlySeenPlayer.OutgoingAttacks.size() > 0)
            {
                PlayEnt.add_damage_events(NewPlayerAttacks.o);
            }

            PlayEnt.add_action(static_cast<uint8_t>(NewlySeenPlayer.Action));

            auto Attributes = Game::Message::Attributes(NewlySeenPlayer.Attributes.Health, NewlySeenPlayer.Attributes.Shield, NewlySeenPlayer.Attributes.Stamina, NewlySeenPlayer.Attributes.Mana);
            PlayEnt.add_attributes(&Attributes);

            auto Traits = Game::Message::Traits(NewlySeenPlayer.Traits.Intelligence, NewlySeenPlayer.Traits.Wisdom, NewlySeenPlayer.Traits.Dexterity, NewlySeenPlayer.Traits.Strength, NewlySeenPlayer.Traits.Constitution);
            PlayEnt.add_traits(&Traits);

            auto PlayerPos = Game::Message::Vec3(NewlySeenPlayer.Position.vec3[0], NewlySeenPlayer.Position.vec3[1], NewlySeenPlayer.Position.vec3[2]);
            PlayEnt.add_pos(&PlayerPos);

            auto PlayerVel = Game::Message::Vec3(NewlySeenPlayer.Velocity.vec3[0], NewlySeenPlayer.Velocity.vec3[1], NewlySeenPlayer.Velocity.vec3[2]);
            PlayEnt.add_vel(&PlayerVel);

            auto PlayerRot = Game::Message::Quat4(NewlySeenPlayer.Rotation.vec4[0], NewlySeenPlayer.Rotation.vec4[1], NewlySeenPlayer.Rotation.vec4[2], NewlySeenPlayer.Rotation.vec4[3]);
            PlayEnt.add_rot(&PlayerRot);
            
            auto PlayerEntFinished = PlayEnt.Finish();

            Game::Message::EntityBuilder Ent(Builder);
            Ent.add_player(PlayerEntFinished.o);
            Ent.add_id(NewlySeenPlayer.EntityId);
            Ent.add_type(Game::Message::EntityType_Player);
            
            auto EntityFinished = Ent.Finish();
            NewEntities.emplace_back(EntityFinished);
            //Log->Debug("Adding player {} to NewEntities", CurrentPlayer.EntityId);
        }

        // annoying we have to create vectores before we can init our StateBuilder other wise we get !nested asserts
        auto AllNewEntities = Builder.CreateVector<Game::Message::Entity>(NewEntities.data(), NewEntities.size());
        auto AllUpdatedEntities = Builder.CreateVector<Game::Message::Entity>(UpdatedEntities.data(), UpdatedEntities.size());
        auto AllOutEntities = Builder.CreateVector<uint64_t>(OutEntities.data(), OutEntities.size());

        Log->Info("PostFrame: Adding ThisPlayersAttacks without going attacks: {}", Current.OutgoingAttacks.size());            

        FlatBufferDamageEvent ThisPlayersAttacks;
        if (Current.OutgoingAttacks.size() > 0)
        {
            // Now do damage events
            std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> OutDamageEvents;
            // Add damage events this player instigates
            AddDamageEvents(Builder, Current.OutgoingAttacks, OutDamageEvents);
            ThisPlayersAttacks = Builder.CreateVector<Game::Message::DamageEvent>(OutDamageEvents.data(), OutDamageEvents.size());
        }

        // If equipped items changed, we have to resend the whole array
        FlatBufferEquipment ThisPlayersEquipment;
        bool bEquipmentChanged = Current.Equipment != Previous.Equipment;
        if (bEquipmentChanged)
        {
            std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> OutEquipment;
            AddEquipment(Builder, Current.Equipment, OutEquipment);
            ThisPlayersEquipment = Builder.CreateVector<Game::Message::InventoryEntity>(OutEquipment.data(), OutEquipment.size());
        }

        FlatBufferInventory ThisPlayerInventory;
        bool bInventoryChanged = Current.Inventory != Previous.Inventory;
        if (Current.Inventory.size() > 0 && bInventoryChanged)
        {
            std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> OutInventory;
            AddItemInventory(Builder, Current.Inventory, OutInventory);
            ThisPlayerInventory = Builder.CreateVector<Game::Message::InventoryEntity>(OutInventory.data(), OutInventory.size());
        }

        Game::Message::StateBuilder Diff(Builder);

        if (bEquipmentChanged)
        {
            Diff.add_equipment(ThisPlayersEquipment.o);
        
        }
        if (Current.OutgoingAttacks.size() > 0)
        {
            Diff.add_damage_events(ThisPlayersAttacks.o);
        }
        
        // only send if inventory changed
        if (Current.Inventory.size() > 0 && bInventoryChanged)
        {
            Diff.add_inventory(ThisPlayerInventory.o);
        }

        // Conditionally add if we have anything to add to keep our buffer sizes smaller
        if (NewEntities.size() > 0)
        {
            Diff.add_in_entities(AllNewEntities.o);
        }

        if (UpdatedEntities.size() > 0)
        {
            Diff.add_updated_entities(AllUpdatedEntities.o);
        }

        if (OutEntities.size() > 0)
        {
            Diff.add_out_entities(AllOutEntities.o);
        }

        if (Current.PlayerPosition != Previous.PlayerPosition)
        {
            auto PlayerPos = Game::Message::Vec3(Current.PlayerPosition.vec3[0], Current.PlayerPosition.vec3[1], Current.PlayerPosition.vec3[2]);
            Diff.add_pos(&PlayerPos);
        }

        if (Current.PlayerVelocity != Previous.PlayerVelocity)
        {
            Log->Info("PostFrame: ThisPlayer Velocity: {} {} {}", Current.PlayerVelocity.vec3[0], Current.PlayerVelocity.vec3[1], Current.PlayerVelocity.vec3[2]);  
            auto PlayerVel = Game::Message::Vec3(Current.PlayerVelocity.vec3[0], Current.PlayerVelocity.vec3[1], Current.PlayerVelocity.vec3[2]);
            Diff.add_vel(&PlayerVel);
        }

        if (Current.PlayerRotation != Previous.PlayerRotation) 
        {
            auto PlayerRot = Game::Message::Quat4(Current.PlayerRotation.vec4[0], Current.PlayerRotation.vec4[1], Current.PlayerRotation.vec4[2], Current.PlayerRotation.vec4[3]);
            Diff.add_rot(&PlayerRot);
        }

        if (Current.Attributes != Previous.Attributes)
        {
            auto Attributes = Game::Message::Attributes(Current.Attributes.Health, Current.Attributes.Shield, Current.Attributes.Stamina, Current.Attributes.Mana);
            Diff.add_attributes(&Attributes);
        }

        if (Current.Traits != Previous.Traits)
        {
            auto Traits = Game::Message::Traits(Current.Traits.Intelligence, Current.Traits.Wisdom, Current.Traits.Dexterity, Current.Traits.Strength, Current.Traits.Constitution);
            Diff.add_traits(&Traits);
        }

        auto DiffResult = Diff.Finish();

        Game::Message::ServerCommandBuilder ServerCommand(Builder);
        ServerCommand.add_opcode(Game::Message::ServerOpCode_Update);
        ServerCommand.add_sequence_id(CurrentSequenceId);
        ServerCommand.add_server_time(ServerTime);
        ServerCommand.add_state(DiffResult);

        Builder.Finish(ServerCommand.Finish());
    }

    void PlayerWorldStates::AddDamageEvents(flatbuffers::FlatBufferBuilder &Builder, const std::vector<combat::ApplyDamage> &OutgoingAttacks, std::vector<flatbuffers::Offset<Game::Message::DamageEvent>> &OutEvents)
    {
        for (auto DamageEvent : OutgoingAttacks)
        {
            Game::Message::DamageClass AttackClass;
            AttackClass = static_cast<Game::Message::DamageClass>(DamageEvent.Type);
            
            Game::Message::DamageEventBuilder Event(Builder);
            Event.add_amount(DamageEvent.BaseDamage);
            Event.add_damage_class(AttackClass);
            Event.add_target(DamageEvent.TargetHit.id());

            OutEvents.emplace_back(Event.Finish());
        }
    }

    void PlayerWorldStates::AddItemInventory(flatbuffers::FlatBufferBuilder &Builder, const std::set<state::ItemEntity, state::EntityHash<state::ItemEntity>> &Inventory, std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> &OutInventory)
    {
        for (auto Item : Inventory)
        {            
            Game::Message::InventoryEntityBuilder ItemInInventory(Builder);
            ItemInInventory.add_id(Item.EntityId);
            ItemInInventory.add_item_id(Item.ItemId);

            OutInventory.emplace_back(ItemInInventory.Finish());
        }
    }

    void PlayerWorldStates::AddEquipment(flatbuffers::FlatBufferBuilder &Builder, const std::array<ItemEntity, items::EquipSlotMax> &Equipment, std::vector<flatbuffers::Offset<Game::Message::InventoryEntity>> &OutEquipment)
    {
        for (auto Equipped : Equipment)
        {
            Game::Message::InventoryEntityBuilder EquippedItem(Builder);
            EquippedItem.add_id(Equipped.EntityId);
            EquippedItem.add_item_id(Equipped.ItemId);

            OutEquipment.emplace_back(EquippedItem.Finish());
        }
    }

    void PlayerWorldStates::UpdateOutgoingAttacks(logging::Logger &Log, state::WorldState &Current, character::Visibility &Visibility)
    {
        // Clear out old attacks first
        Current.OutgoingAttacks.clear();
        Log.Info("OnStore: Cleared outgoing attacks, adding {} (seq id: {})", Visibility.DamageEvents.size(), SequenceId);
        std::vector<combat::ApplyDamage> OutgoingAttacks;
        for (auto EventEntity : Visibility.DamageEvents)
        {
            if (!EventEntity.is_valid() || !EventEntity.is_alive())
            {
                Log.Info("OnStore: UpdateOutgoingAttacks: EventEntity was not valid!!");
                continue;
            }
            // Since we are applying our own outgoing attacks, no need to compare ids
            auto DamageEvent = EventEntity.get<combat::ApplyDamage>();
            Current.OutgoingAttacks.emplace_back(*DamageEvent);
        }
    }

    void PlayerWorldStates::UpdateState(logging::Logger &Log, character::Visibility &Visibility, units::Attributes &Attributes, units::TraitAttributes &Traits, units::Vector3 &Position, units::Velocity &Velocity, units::Quat &Rotation)
    {
        auto &Current = States.at(SequenceId);
        Current.PlayerPosition = Position;
        Current.PlayerRotation = Rotation;
        Current.PlayerVelocity = Velocity;
        Current.Attributes = Attributes;
        Current.Traits = Traits;
        UpdateOutgoingAttacks(Log, Current, Visibility);
        ApplyPlayerVisibility(Log, Visibility);
    }

    void PlayerWorldStates::ApplyPlayerVisibility(logging::Logger &Log, character::Visibility &PlayerVisibility)
    {
        // clear out previous data
        States.at(SequenceId).KnownPlayerEntities.clear();
        Log.Info("OnStore: Cleared KnownPlayerEntities attacks seq id: {}", SequenceId);
        // check if this character can even see anything first
        if (PlayerVisibility.Neighbors.size() <= 0)
        {
            return;
        }

        for (auto Neighbor : PlayerVisibility.Neighbors)
        {
            if (!Neighbor.is_valid() || !Neighbor.is_alive())
            {
                continue;
            }

            auto Pos = Neighbor.get_ref<units::Vector3>();
            auto Vel = Neighbor.get_ref<units::Velocity>();
            auto Rot = Neighbor.get_ref<units::Quat>();
            auto Attributes = Neighbor.get_ref<units::Attributes>();
            auto Traits = Neighbor.get_ref<units::TraitAttributes>();
            // TODO: Actually build these ECS components Look these up
            auto Items = std::array<uint32_t, 15>{};
            auto NeighborVisibility = Neighbor.get<character::Visibility>();
            
            auto Action = Neighbor.get<character::ActionType>();
            // probably want to cap this and reduce it's area to just in the vicinity for visualizations?
            std::vector<combat::ApplyDamage> OutgoingAttacks;
            Log.Info("OnStore: ({}) NeighborVisibility->DamageEvents: {}", Neighbor.id(), NeighborVisibility->DamageEvents.size());
            for (auto EventEntity : NeighborVisibility->DamageEvents)
            {
                if (!EventEntity.is_valid() || !EventEntity.is_alive())
                {
                    Log.Info("OnStore: NeighborVisibility->DamageEvents: EventEntity was not valid!!");
                    continue;
                }

                auto DamageEvent = EventEntity.get<combat::ApplyDamage>();
                if (DamageEvent->Instigator.id() == Neighbor.id())
                {
                    OutgoingAttacks.emplace_back(*DamageEvent); // copy into the DamageEvents state
                    Log.Info("OnStore: ({}) OutgoingAttacks: {} emplaced", Neighbor.id(), DamageEvent->Instigator.id());
                }
            }

            States.at(SequenceId).KnownPlayerEntities.insert(PlayerEntity{
                .EntityId=Neighbor.id(), 
                .Attributes=*Attributes.get(),
                .Traits=*Traits.get(), 
                .Position=*Pos.get(), 
                .Velocity=*Vel.get(), 
                .Rotation=*Rot.get(), 
                .Items=Items,
                .Action=*Action, 
                .OutgoingAttacks=OutgoingAttacks}
            );
        }
       
    }

    bool PlayerWorldStates::WasAckd(uint8_t CheckId)
    {
        return Acks.at(CheckId % 24);
    }
}
#endif