#pragma once

#include <Jolt/Jolt.h>
#include "units.h"

namespace convert 
{
    inline JPH::Vec3 Vector3ToJPHVec3(const units::Vector3 &Vec)
    {
        return JPH::Vec3(Vec.vec3[0], Vec.vec3[1], Vec.vec3[2]);
    }

    inline JPH::Vec3 VelocityToJPHVec3(const units::Velocity &Vec)
    {
        return JPH::Vec3(Vec.vec3[0], Vec.vec3[1], Vec.vec3[2]);
    }

    inline units::Vector3 JPHVec3ToVector3(const JPH::Vec3 &Vec)
    {
        return units::Vector3{Vec.GetX(), Vec.GetY(), Vec.GetZ()};
    }

    inline units::Velocity JPHVec3ToVelocity(const JPH::Vec3 &Vec)
    {
        return units::Velocity{Vec.GetX(), Vec.GetY(), Vec.GetZ()};
    }

    inline JPH::Quat QuatToJPHQuat(const units::Quat &Rot)
    {
        return JPH::Quat(Rot.vec4[0],Rot.vec4[1],Rot.vec4[2],Rot.vec4[3]);
    }

    inline units::Quat JPHQuatToQuat(const JPH::Quat &Rot)
    {
        return units::Quat{Rot.GetX(),Rot.GetY(), Rot.GetZ(), Rot.GetW()};
    }
}