#include "items.h"


namespace items
{
    bool LoadEquipment(flecs::world &GameWorld, config::Config &Config, logging::Logger &Log)
    {
        GameWorld.component<Weapon>().add(flecs::OnInstantiate, flecs::Inherit).is_a<Item>();
        GameWorld.component<Shield>().add(flecs::OnInstantiate, flecs::Inherit).is_a<Item>();

        const std::filesystem::path EquipmentPath{Config.GetEquipmentPath()};

        for (auto &file : std::filesystem::directory_iterator{EquipmentPath})
        {

            auto Reader = csv::CSVReader(file.path().string());

            if (file.path().filename() == "melee_weapons.csv")
            {
                if (!LoadWeapons(GameWorld, Log, Reader))
                {
                    return false;
                }
            } 
            else if (file.path().filename() == "shields.csv")
            {
                if (!LoadShields(GameWorld, Log, Reader))
                {
                    return false;
                }
            }
            else
            {
                Log.Warn("Unknown file: {}", file.path().filename().string());
            }
        }

        return true;
    }

    bool LoadWeapons(flecs::world &GameWorld, logging::Logger &Log, csv::CSVReader &Reader)
    {
        Log.Info("Parsing melee weapons");

        for (auto& Row : Reader) 
        {
            int Id = std::stol(Row["Id"].get<std::string>(), nullptr, 16);

            combat::DamageClass DamageType = combat::DamageClass::INVALID;
            if (combat::DamageClasses.contains(Row["DamageType"].get<std::string_view>()))
            {
                DamageType = combat::DamageClasses.at(Row["DamageType"].get<std::string_view>());
            }

            if (DamageType == combat::DamageClass::INVALID)
            {
                return false;
            }

            flecs::entity WeaponPrefab = GameWorld.prefab(Row["Name"].get<std::string>().c_str());
            WeaponPrefab.is_a<Item>()
                .set<items::Weapon>({
                    .Id = static_cast<uint16_t>(Id),
                    .Hand = Row["Hand"].get<uint8_t>(),
                    .Name = Row["Name"].get<std::string>(),
                    .DamageType = DamageType,
                    .BaseDamage = Row["BaseDamage"].get<float>(),
                    .WeightKg = Row["WeightKg"].get<float>(),
                    .Price = Row["Price"].get<float>(),
                    .HitStartTimeSec = Row["HitStartTimeSec"].get<float>(),
                    .AttackTimeSec = Row["AttackTimeSec"].get<float>()
            });
        }

        return true;
    }

    bool LoadShields(flecs::world &GameWorld, logging::Logger &Log, csv::CSVReader &Reader)
    {
        Log.Info("Parsing shields");

        for (auto& Row : Reader) 
        {
            int Id = std::stol(Row["Id"].get<std::string>(), nullptr, 16);

            flecs::entity ShieldPrefab = GameWorld.prefab(Row["Name"].get<std::string>().c_str());
            ShieldPrefab.is_a<Item>()
                .set<items::Shield>({
                    .Id = static_cast<uint16_t>(Id),
                    .Hand = Row["Hand"].get<uint8_t>(),
                    .Name = Row["Name"].get<std::string>(),
                    .BaseArmor = Row["BaseArmor"].get<float>(),
                    .WeightKg = Row["WeightKg"].get<float>(),
                    .Price = Row["Price"].get<float>()
            });
        }
        
        return true;
    }
}