#include "inventory.h"
#include "ecs/character/components.h"

namespace items
{

    void LoadItems(flecs::world &GameWorld, config::Config &Config, logging::Logger &Log)
    {
        GameWorld.component<ContainedBy>()
            .add(flecs::Exclusive); // Item can only be contained by one container

        GameWorld.prefab<Body>();
            GameWorld.prefab<Body::Head>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Neck>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Ears>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Rings>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Chest>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Arms>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Hands>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::LeftHand>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::RightHand>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Legs>().slot_of<Body>().add<Container>();
            GameWorld.prefab<Body::Feet>().slot_of<Body>().add<Container>();
    }

    flecs::entity FindItemKind(flecs::entity ItemToFind) 
    {
        flecs::world GameWorld = ItemToFind.world();
        flecs::entity Result;

        ItemToFind.each([&](flecs::id Id) 
        {
            if (Id.is_entity()) 
            { 
                // If id is a plain entity (component), check if component inherits
                // from Item
                if (Id.entity().has(flecs::IsA, GameWorld.id<Item>())) 
                {
                    Result = Id.entity();
                }
            }
            else if (Id.is_pair()) 
            {
                // If item has a base entity, check if the base has an attribute
                // that is an Item.
                if (Id.first() == flecs::IsA) 
                {
                    flecs::entity base_kind = FindItemKind(Id.second());
                    if (base_kind) 
                    {
                        Result = base_kind;
                    }
                }
            }
        });

        return Result;
    }

    const char* GetItemName(flecs::entity ItemToGet) 
    {
        flecs::world GameWorld = ItemToGet.world();
        const char *Result = nullptr;

        ItemToGet.each([&](flecs::id id) 
        {
            if (id.is_entity())
            { 
                if (id.entity().has(flecs::IsA, GameWorld.id<Item>())) 
                {
                    Result = id.entity().name();
                }
            }
            else if (id.is_pair()) 
            {
                if (id.first() == flecs::IsA) 
                {
                    flecs::entity base_kind = FindItemKind(id.second());
                    if (base_kind) 
                    {
                        Result = id.second().name();
                    }
                }
            }
        });

        return Result;
    }

    flecs::entity GetContainer(flecs::entity Target) 
    {
        if (Target.has<Container>()) 
        {
            return Target;
        }
        return Target.target<Inventory>();
    }

    flecs::entity FindItemById(flecs::entity ContainerToSearch, flecs::id ItemId)
    {
        flecs::entity Result;

        ContainerToSearch = GetContainer(ContainerToSearch);

        ForEachItem(ContainerToSearch, [&](flecs::entity ContainerItem) 
        {
            if (ContainerItem.raw_id() == ItemId) 
            {
                Result = ContainerItem;
            }
        });

        return Result;
    }

    flecs::entity FindItemWithKind(flecs::entity ContainerToSearch, flecs::entity KindOfItem)
    {
        flecs::entity Result{};

        ContainerToSearch = GetContainer(ContainerToSearch);

        ForEachItem(ContainerToSearch, [&](flecs::entity ContainerItem) 
        {
            flecs::entity FoundItem = FindItemKind(ContainerItem);
            if (FoundItem == KindOfItem) 
            {
                Result = ContainerItem;
            }
        });

        return Result;
    }

    void TransferItem(flecs::entity DestContainer, flecs::entity ItemToTransfer) 
    {
        const Amount *AmountOfItem = ItemToTransfer.get<Amount>();
        if (AmountOfItem) 
        {
            // If item has amount we need to check if the container already has an
            // item of this kind, and increase the value.
            flecs::entity FoundItem = FindItemKind(ItemToTransfer);
            flecs::entity DestItem = FindItemWithKind(DestContainer, FoundItem);
            if (DestItem) 
            {
                // If a matching item was found, increase its amount
                Amount& DestAmount = DestItem.ensure<Amount>();
                DestAmount.Value += AmountOfItem->Value;
                ItemToTransfer.destruct(); // Remove the src item
                return;
            } else {
                // If no matching item was found, fallthrough which will move the
                // item from the src container to the dst container
            }
        }

        // Move item to target container (replaces previous ContainedBy, if any)
        ItemToTransfer.add<ContainedBy>(DestContainer);
    }

    /**
     * @brief Move items from one container to another
     * 
     * @param Dest 
     * @param Src 
     */
    void TransferItems(flecs::entity Dest, flecs::entity Src) 
    {
        // Defer, because we're adding/removing components while we're iterating
        Dest.world().defer([&] 
        {
            Dest = GetContainer(Dest); // Make sure to replace players with container
            Src = GetContainer(Src);

            ForEachItem(Src, [&](flecs::entity item) 
            {
                TransferItem(Dest, item);
            });
        });
    }

    bool EquipItem(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity Inventory)
    {
        auto GameWorld = Player.world();

        if (!Inventory.is_valid())
        {
            return false;
        }


        flecs::entity PreviouslyEquipped;
        flecs::entity TargetSlot;
        switch (Slot)
        {
            // Armor
            case EquipmentSlot::HEAD:
            {
                TargetSlot = Player.target<items::Body::Head>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::CHEST:
            {
                TargetSlot = Player.target<items::Body::Chest>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::ARMS:
            {
                TargetSlot = Player.target<items::Body::Arms>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::HANDS:
            {
                TargetSlot = Player.target<items::Body::Hands>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::LEGS:
            {
                TargetSlot = Player.target<items::Body::Legs>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::FEET:
            {
                TargetSlot = Player.target<items::Body::Feet>();
                return EquipArmor(Player, Item, Slot, TargetSlot, Inventory);
            }
            // Jewelery
            case EquipmentSlot::NECK:
            {
                TargetSlot = Player.target<items::Body::Neck>();
                return EquipJewelery(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::EARS:
            {
                TargetSlot = Player.target<items::Body::Ears>();
                return EquipJewelery(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::RINGS:
            {
                TargetSlot = Player.target<items::Body::Rings>();
                return EquipJewelery(Player, Item, Slot, TargetSlot, Inventory);
            }
            // Finally WeaponOrShields
            case EquipmentSlot::LEFT_HAND:
            {
                TargetSlot = Player.target<items::Body::LeftHand>();
                return EquipShieldOrWeapon(Player, Item, Slot, TargetSlot, Inventory);
            }
            case EquipmentSlot::RIGHT_HAND:
            {
                TargetSlot = Player.target<items::Body::RightHand>();
                return EquipShieldOrWeapon(Player, Item, Slot, TargetSlot, Inventory);
            }
            default:
            {
                return false;
            }
        }
        return false;
    }

    bool EquipJewelery(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory)
    {
        auto GameWorld = Player.world();

        // Get the Jewelery struct details, will return nullptr if it's not Jewelery
        auto ItemToEquip = Item.get<items::Jewelery>();
        if (!ItemToEquip)
        {
            return false;
        }

        // Make sure the slot is correct
        if (ItemToEquip->Slot != Slot)
        {
            return false;
        }
        // TODO: THis is more complicated because we will have multiple peices of jewelery in the slot, as of now,
        // this code will not work for more than one peice per slot
        auto PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Jewelery>());

        if (PreviouslyEquipped.is_valid())
        {
            TransferItem(Inventory, PreviouslyEquipped);
        }

        TransferItem(TargetSlot, Item);

        return true;
    }

    bool EquipShieldOrWeapon(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory)
    {
        auto GameWorld = Player.world();

        
        auto PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Weapon>());
        if (!PreviouslyEquipped.is_valid())
        {
            PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Shield>());
        }
        
        // If weapon...
        auto WeaponToEquip = Item.get<items::Weapon>();
        if (WeaponToEquip)
        {
            auto PlayerCanDualWeild = Player.has<character::DualWeild>();
            if (WeaponToEquip->Hand == 1)
            {
                // Can't equip to left hand if we don't have the ability to dualweild
                if (Slot == items::EquipmentSlot::LEFT_HAND && !PlayerCanDualWeild)
                {
                    return false;
                }
                // We should be safe to remove previously equipped item (if any) and move that to inventory
                // and move the item into the left or right hand
                if (PreviouslyEquipped.is_valid())
                {
                    // TODO: Handle overflow automatically by applying an Overflow component if Inventory > max slots which UE5 will read as 
                    // use the cursor to hold the item so they can destroy it or whatever
                    TransferItem(Inventory, PreviouslyEquipped);
                }
                TransferItem(TargetSlot, Item);
            }
            else if (WeaponToEquip->Hand == 2)
            {
                flecs::entity SecondarySlot;
                flecs::entity PreviouslyEquippedLeft = PreviouslyEquipped;
                flecs::entity PreviouslyEquippedRight = PreviouslyEquipped;
                // now we have to handle checking both slots
                if (Slot == EquipmentSlot::RIGHT_HAND)
                {
                    SecondarySlot = Player.target<items::Body::LeftHand>();
                    PreviouslyEquippedLeft = GetPreviouslyEquipped(SecondarySlot);
                }
                else 
                {
                    SecondarySlot = Player.target<items::Body::RightHand>();
                    PreviouslyEquippedRight = GetPreviouslyEquipped(SecondarySlot);
                    TargetSlot = SecondarySlot; // Always make Target slot the right hand for a 2H weapon/Shield
                }

                if (PreviouslyEquippedLeft.is_valid())
                {
                    TransferItem(Inventory, PreviouslyEquippedLeft);
                }

                if (PreviouslyEquippedRight.is_valid())
                {
                    TransferItem(Inventory, PreviouslyEquippedRight);
                }
                TransferItem(TargetSlot, Item);
            }

            return true;
        }

        auto ShieldToEquip = Item.get<items::Shield>();
        if (ShieldToEquip)
        {
            if (WeaponToEquip->Hand == 1)
            {
                // Shields always go to left hand, fail otherwise.
                if (Slot != items::EquipmentSlot::LEFT_HAND)
                {
                    return false;
                }
                // We should be safe to remove previously equipped item (if any) and move that to inventory
                // and move the item into the left or right hand
                if (PreviouslyEquipped.is_valid())
                {
                    // TODO: Handle overflow automatically by applying an Overflow component if Inventory > max slots which UE5 will read as 
                    // use the cursor to hold the item so they can destroy it or whatever
                    TransferItem(Inventory, PreviouslyEquipped);
                }
                TransferItem(TargetSlot, Item);
            }
            else if (ShieldToEquip->Hand == 2)
            {
                flecs::entity SecondarySlot;
                flecs::entity PreviouslyEquippedLeft = PreviouslyEquipped;
                flecs::entity PreviouslyEquippedRight = PreviouslyEquipped;
                // now we have to handle checking both slots
                if (Slot == EquipmentSlot::RIGHT_HAND)
                {
                    SecondarySlot = Player.target<items::Body::LeftHand>();
                    PreviouslyEquippedLeft = GetPreviouslyEquipped(SecondarySlot);
                }
                else 
                {
                    SecondarySlot = Player.target<items::Body::RightHand>();
                    PreviouslyEquippedRight = GetPreviouslyEquipped(SecondarySlot);
                    TargetSlot = SecondarySlot; // Always make Target slot the right hand for a 2H weapon/Shield
                }

                if (PreviouslyEquippedLeft.is_valid())
                {
                    TransferItem(Inventory, PreviouslyEquippedLeft);
                }

                if (PreviouslyEquippedRight.is_valid())
                {
                    TransferItem(Inventory, PreviouslyEquippedRight);
                }
                // Actually Equip the shield
                TransferItem(TargetSlot, Item);
            }
        }
        return true;
    }

    // Gets either the weapon or shield for the slot
    flecs::entity GetPreviouslyEquipped(flecs::entity TargetSlot)
    {
        auto GameWorld = TargetSlot.world();

        auto PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Weapon>());
        if (!PreviouslyEquipped.is_valid())
        {
            PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Shield>());
        }
        return PreviouslyEquipped;
    }

    bool EquipArmor(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory)
    {
        auto GameWorld = Player.world();

        // Get the Armor struct details, will return nullptr if it's not an Armor peice
        auto ItemToEquip = Item.get<items::Armor>();
        if (!ItemToEquip)
        {
            return false;
        }

        // Make sure the slot is correct
        if (ItemToEquip->Slot != Slot)
        {
            return false;
        }
        auto PreviouslyEquipped = items::FindItemWithKind(TargetSlot, GameWorld.entity<items::Armor>());

        if (PreviouslyEquipped.is_valid())
        {
            TransferItem(Inventory, PreviouslyEquipped);
        }

        TransferItem(TargetSlot, Item);

        return true;
    }
}