#pragma once

#include <flecs.h>
#include "items.h"

/**
 * @brief Basic inventory copied from:
 *  https://github.com/SanderMertens/flecs/blob/488bc24fab97597c7edfebacb5b28f25bc9312b6/examples/cpp/game_mechanics/inventory_system/src/main.cpp
 *  Will be enhanced later.
 */
namespace items 
{
    struct Body
    {
        struct Head {};
        struct Neck {};
        struct Chest {};
        struct Ears {}; // can hold multiple
        struct Rings {}; // can hold multiple
        struct Arms {};
        struct Hands {}; // armor
        struct LeftHand {}; // weilding left hand
        struct RightHand {}; // weilding right hand
        struct Legs {};
        struct Feet {};
    };

    /**
     * @brief Loads and initializes inventory and containers
     * 
     * @param GameWorld 
     * @param Config 
     * @param Log 
     */
    void LoadItems(flecs::world &GameWorld, config::Config &Config, logging::Logger &Log);

    /**
     * @brief Iterate all items in an inventory
     * 
     * @tparam Func 
     * @param ContainerToIter 
     * @param IterFunc 
     */
    template <typename Func>
    void ForEachItem(flecs::entity ContainerToIter, const Func& IterFunc) 
    {
        ContainerToIter.world().query_builder()
            .with<ContainedBy>(ContainerToIter)
            .each(IterFunc);
    }

    /**
     * @brief Find item given it's ItemId provided it's in the container
     * 
     * @param ContainerToSearch 
     * @param ItemId 
     * @return flecs::entity 
     */
    flecs::entity FindItemById(flecs::entity ContainerToSearch, flecs::id ItemId);

    /**
     * @brief Find Item kind of entity
     * 
     * @param ItemToFind 
     * @return flecs::entity 
     */
    flecs::entity FindItemKind(flecs::entity ItemToFind);

    /**
     * @brief Almost the same as item_kind, but return name of prefab vs item kind. This
     * returns a more user-friendly name, like "WoodenSword" vs. just "Sword"
     * 
     * @param ItemToGet 
     * @return const char* 
     */
    const char* GetItemName(flecs::entity ItemToGet);
    /**
     * @brief If entity is not a container, get its inventory
     * 
     * @param Target 
     * @return flecs::entity 
     */
    flecs::entity GetContainer(flecs::entity Target);
    /**
     * @brief Find item in inventory of specified kind
     * 
     * @param ContainerToSearch 
     * @param KindOfItem 
     * @return flecs::entity 
     */
    flecs::entity FindItemWithKind(flecs::entity ContainerToSearch, flecs::entity KindOfItem);

    /**
     * @brief Transfer item to container, takes care to check if it has an amount
     * component for tracking "stacks" of the item
     * 
     * @param DestContainer 
     * @param ItemToTransfer 
     */
    void TransferItem(flecs::entity DestContainer, flecs::entity ItemToTransfer);

    /**
     * @brief Move items from one container to another
     * 
     * @param Dest 
     * @param Src 
     */
    void TransferItems(flecs::entity Dest, flecs::entity Src);

    /**
     * @brief Get the Previously Equipped shield or weapon for left/right hands ONLY
     * 
     * @param TargetSlot 
     * @return flecs::entity 
     */
    flecs::entity GetPreviouslyEquipped(flecs::entity TargetSlot);

    /**
     * @brief Equips an item, removing currently slot'd item and putting it 
     * somewhere in the supplied Inventory
     * 
     * @param Item 
     * @param Slot 
     */
    bool EquipItem(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity Inventory);

    /**
     * @brief 
     * 
     * @param Player 
     * @param Item 
     * @param Slot 
     * @param TargetSlot 
     * @param Inventory 
     * @return true 
     * @return false 
     */
    bool EquipJewelery(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory);

    /**
     * @brief 
     * 
     * @param Player 
     * @param Item 
     * @param Slot 
     * @param TargetSlot 
     * @param Inventory 
     * @return true 
     * @return false 
     */
    bool EquipShieldOrWeapon(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory);

    /**
     * @brief 
     * 
     * @param Player 
     * @param Item 
     * @param Slot 
     * @param TargetSlot 
     * @param Inventory 
     * @return true 
     * @return false 
     */
    bool EquipArmor(flecs::entity Player, flecs::entity Item, EquipmentSlot Slot, flecs::entity TargetSlot, flecs::entity Inventory);
}