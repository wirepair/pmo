#pragma once

#include <flecs.h>

#include "ecs/combat/damageclass.h"
#include "config/config.h"
#include "logger/logger.h"

#include "parsers/csv.hpp"

namespace items
{
    using namespace csv;

    enum class EquipmentSlot : uint8_t
    {
        INVALID,
        HEAD,
        NECK,
        EARS,
        RINGS,
        SHOULDERS,
        CHEST,
        ARMS,
        HANDS,
        LEFT_HAND,
        RIGHT_HAND,
        LEGS,
        FEET,
        MAX
    };

    constexpr uint8_t EquipSlotMax = static_cast<uint8_t>(items::EquipmentSlot::MAX)-1;

    struct Amount 
    {
        int Value;          // Number of items the instance represents
    };

    // Inventory tags/relationships
    struct Item { };        // Base item type
    struct Container { };   // Container tag
    struct Inventory 
    {
        uint8_t BaseSlots = 20;
    };

    struct ContainedBy { }; // ContainedBy relationship

    struct Active {}; // Is the item active

    struct Prefix {}; // TODO: Implement enchantments like Shadowbane
    struct Suffix {}; // TODO: Implement enchantments like Shadowbane

    // Weapon prefab
    struct Weapon : Item
    {
        uint16_t Id{};
        uint8_t Hand{}; // 1 for 1h (Defaults to RightHand), 2 for 2h
        std::string Name;
        combat::DamageClass DamageType{};
        float BaseDamage{};
        float WeightKg{};
        float Price{};
        float HitStartTimeSec{};
        float AttackTimeSec{};
    };

    // Shield prefab
    struct Shield : Item
    {
        uint16_t Id{};
        uint8_t Hand{}; // 1 for 1h, (defaults to LeftHand) 2 for 2h (Palisade)
        std::string Name;
        float BaseArmor{};
        float WeightKg{};
        float Price{};
    };

    // Armor prefab
    struct Armor : Item 
    {
        EquipmentSlot Slot;
    };

    // Jewlery prefab
    struct Jewelery : Item
    {
        EquipmentSlot Slot;
    };

    /**
     * @brief Loads equipment from our assets/equipment directory, parses each individual csv file
     * to create prefabs
     * 
     * @param GameWorld 
     * @param Config 
     * @param Log 
     * @return true 
     * @return false 
     */
    bool LoadEquipment(flecs::world &GameWorld, config::Config &Config, logging::Logger &Log);

    /**
     * @brief Load weapon prefab data from csv
     * 
     * @param GameWorld 
     * @param Log 
     * @param Reader 
     * @return true 
     * @return false 
     */
    bool LoadWeapons(flecs::world &GameWorld, logging::Logger &Log, csv::CSVReader &Reader);

    /**
     * @brief Load shield prefab data from csv
     * 
     * @param GameWorld 
     * @param Log 
     * @param Reader 
     * @return true 
     * @return false 
     */
    bool LoadShields(flecs::world &GameWorld, logging::Logger &Log, csv::CSVReader &Reader);
}