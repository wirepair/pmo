#pragma once
#ifndef IS_SERVER

#include <flecs.h>
#include "caller.h"

#include "net/listener.h"
#include "logger/logger.h"
#include "config/config.h"

/**
 * @brief InitPMOClient returning our player as flecs::entity_t (uint64_t)
 * 
 * @param GameWorld 
 * @param Log 
 * @param Config 
 * @param UserId 
 * @return flecs::entity_t 
 */
flecs::entity_t InitPMOClient(flecs::world &GameWorld, logging::Logger &Log, config::Config &Config, uint32_t UserId);

/**
 * @brief DeinitPMO shuts down our world (threads, physics and flecs)
 * 
 * @param GameWorld 
 */
void DeinitPMO(flecs::world &GameWorld);
#endif
