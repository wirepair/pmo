#include "dispatcher.h"

namespace physics
{
        JPH::ValidateResult ContactDispatcher::OnContactValidate(const JPH::Body &InBody1, const JPH::Body &InBody2, JPH::RVec3Arg InBaseOffset, const JPH::CollideShapeResult &InCollisionResult) 
        {
            // Check ordering contract between body 1 and body 2
            bool contract = InBody1.GetMotionType() >= InBody2.GetMotionType()
                || (InBody1.GetMotionType() == InBody2.GetMotionType() && InBody1.GetID() < InBody2.GetID());
            if (!contract)
                JPH_BREAKPOINT;

            JPH::ValidateResult result;
            if (Listeners.size() > 0)
            {
                for (auto Listener : Listeners)
                {
                    // lock before we start messing with the world/vectors
                    std::lock_guard<std::mutex> guard(FlecsMutex);
                    result = Listener->OnContactValidate(InBody1, InBody2, InBaseOffset, InCollisionResult);
                    if (result == JPH::ValidateResult::RejectAllContactsForThisBodyPair || result == JPH::ValidateResult::RejectContact)
                    {
                        return result;
                    }
                }
            }
            else
            {
                result = ContactListener::OnContactValidate(InBody1, InBody2, InBaseOffset, InCollisionResult);
            }
            return result;
        }

        void ContactDispatcher::OnContactAdded(const JPH::Body &InBody1, const JPH::Body &InBody2, const JPH::ContactManifold &InManifold, JPH::ContactSettings &IOSettings) 
        {
            for (auto Listener : Listeners)
            {
                // lock before we start messing with the world/vectors
                std::lock_guard<std::mutex> guard(FlecsMutex);
                Listener->OnContactAdded(InBody1, InBody2, InManifold, IOSettings);
            }
        }
        
        void ContactDispatcher::OnContactPersisted(const JPH::Body &InBody1, const JPH::Body &InBody2, const JPH::ContactManifold &InManifold, JPH::ContactSettings &IOSettings) 
        {
            for (auto Listener : Listeners)
            {
                // lock before we start messing with the world/vectors
                std::lock_guard<std::mutex> guard(FlecsMutex);
                Listener->OnContactPersisted(InBody1, InBody2, InManifold, IOSettings);
            }
        }
        
        void ContactDispatcher::OnContactRemoved(const JPH::SubShapeIDPair &inSubShapePair) 
        {
            for (auto Listener : Listeners)
            {
                // lock before we start messing with the world/vectors
                std::lock_guard<std::mutex> guard(FlecsMutex);
                Listener->OnContactRemoved(inSubShapePair);
            }
        }
}