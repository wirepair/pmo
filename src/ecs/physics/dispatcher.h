#pragma once

#include <vector>
#include <mutex>
#include <Jolt/Jolt.h>
#include <Jolt/Physics/Body/Body.h>
#include <Jolt/Physics/Collision/ContactListener.h>


namespace physics
{
    // Tests the contact listener callbacks
    class ContactDispatcher : public JPH::ContactListener
    {
    public:
        ContactDispatcher()
        {
            Listeners.reserve(100);
        }
        // See: ContactListener
        virtual JPH::ValidateResult OnContactValidate(const JPH::Body &InBody1, const JPH::Body &InBody2, JPH::RVec3Arg InBaseOffset, const JPH::CollideShapeResult &InCollisionResult) override;
        virtual void OnContactAdded(const JPH::Body &InBody1, const JPH::Body &InBody2, const JPH::ContactManifold &InManifold, JPH::ContactSettings &IOSettings) override;
        virtual void OnContactPersisted(const JPH::Body &InBody1, const JPH::Body &InBody2, const JPH::ContactManifold &InManifold, JPH::ContactSettings &IOSettings) override;
        virtual void OnContactRemoved(const JPH::SubShapeIDPair &InSubShapePair) override;

        /**
         * @brief Adds a listener, not thread safe
         * 
         * @param Listener 
         */
        void AddListener(JPH::ContactListener *Listener) 
        {
            Listeners.emplace_back(Listener);
        }

        /**
         * @brief Removes a listener, not thread safe
         * 
         * @param Listener 
         */
        void RemoveListener(JPH::ContactListener *Listener)
        {
            std::erase(Listeners, Listener);
        }

        virtual ~ContactDispatcher() = default;

    private:
         std::vector<JPH::ContactListener *> Listeners;
         std::mutex FlecsMutex; // protects our flecs world from invalid multi-thread access
    };

}
