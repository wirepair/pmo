#include <sstream>
#include "landscape.h"

#include "loaders/ufbx.h"
#include "broadphase.h"



namespace physics
{
    Landscape::Landscape(JPH::PhysicsSystem *System, config::Config &Config, logging::Logger &Logger) : Log(Logger)
    {
        MapName = std::string(Config.GetMapName());

        auto MapPath = fs::path(Config.GetMapPath());
        auto BinPath = MapPath / MapName;
        BinPath += ".obin";
        
        if (fs::exists(BinPath))
        {
            LoadBodyData(BinPath);
            return;
        }
        
        Log.Info("Map has not been serialized, attempting to loading fbx file {}", MapName);     

        if (!SerializeFBX(System, MapPath))
        {
            Log.Error("Failed to serialize FBX data");
            return;
        }

        LoadBodyData(BinPath);
    }

    bool Landscape::NextLandscapeChunk(JPH::Ref<JPH::Shape> &OutShape)
    {
        if (StreamIn.IsEOF())
        {
            Log.Info("No more Landscape chunks to load");
            return false;
        }

        if (StreamIn.IsFailed())
        {
            Log.Info("Failed to read from stream");
            return false;
        }

        JPH::Shape::ShapeResult Result = JPH::Shape::sRestoreWithChildren(StreamIn, IdToShape, IdToMaterial);
        if (!Result.IsValid())
        {
            // Oddly it will attempt to read, and if it fails (!Result.IsValid()), then we should check if IsEOF to make sure we are done?
            // This doesn't seem right.
            if (StreamIn.IsEOF())
            {
                Log.Info("No more Landscape chunks to load");
                return false;
            }
            
            Log.Error("Failed to load map from serialized data, file corrupt?");
            return false;
        }
       
        OutShape = Result.Get();
        return true;
    }

    void Landscape::LoadBodyData(fs::path BinPath)
    {
        std::ifstream InFile{BinPath.string(), std::ifstream::in | std::ifstream::binary};
        if (InFile.is_open())
        {
            Data << InFile.rdbuf();
            InFile.close();
        }
        else
        {
            Log.Error("Failed to open {} for reading!", BinPath.string());
        }
    }

    // SERIALIZE, 
    bool Landscape::SerializeFBX(JPH::PhysicsSystem *System, fs::path MapPath)
    {
        Log.Info("SerializeFBX called");
        auto OutPath = MapPath / MapName;
        OutPath += ".obin";
        auto FullPath = MapPath / MapName;
        FullPath +=  + ".fbx";

        if (!fs::exists(FullPath))
        {
            Log.Info("Unable to find FBX map file for level {}", MapName);
            return false;
        }
        ufbx_load_opts opts = { 0 }; // Optional, pass NULL for defaults
        ufbx_error error; // Optional, pass NULL if you don't care about errors
        
        Log.Info("Loading FBX file");

        ufbx_scene *scene = ufbx_load_file(FullPath.string().c_str(), &opts, &error);
        if (!scene) 
        {
            Log.Error("Failed to load: {}", error.description.data);
            return false;
        }
        else
        {
            Log.Info("Loaded FBX");
        }

        JPH::VertexList VertexList;
        JPH::IndexedTriangleList IdxTriangleList;
        JPH::TriangleList Triangles;

        // For serializing to disk
        std::stringstream Data;
        JPH::StreamOutWrapper OutStream(Data);
        // Docs say to re-use these across saves if our shapes are the same.
        JPH::Shape::ShapeToIDMap ShapeToId;
        JPH::Shape::MaterialToIDMap MaterialToId;

        // We need to store ref's to them while we are serializing to avoid: 
        // https://github.com/jrouwe/JoltPhysics/issues/1088
        std::vector<std::unique_ptr<BodyCreationSettings>> Bodies;
        std::vector<Ref<MeshShapeSettings>> Meshes;
        // iterate over all meshes
        for (size_t MeshIdx = 0; MeshIdx < scene->nodes.count; MeshIdx++) 
        {
            ufbx_node *node = scene->nodes.data[MeshIdx];
            if (node->is_root) continue;

            auto Mesh = node->mesh;
            if (!Mesh) 
            {
                continue;
            }

            Log.Info("Processing Mesh [{}]", MeshIdx);

            JPH::VertexList VertList;
            JPH::IndexedTriangleList TriList;
            for (auto& Vertex : Mesh->vertices) 
            {
                auto N2W = ufbx_transform_position(&node->node_to_world, Vertex);
                auto LocalScale = node->local_transform.scale;

                float X = N2W.x / LocalScale.x;
                float Y = N2W.y / LocalScale.y;
                float Z = N2W.z / LocalScale.z;
                JPH::Vec3 Pos{X, Y, Z};
                // Y/Z are rotated differently between UE export and Jolt
                auto Rot = JPH::Quat::sRotation(Vec3::sAxisX(), .5f * JPH_PI); 
                // Rotate the point instead of the BodyCreationSettings as it seems to cause problems 
                // with bounding boxes if you don't?
                auto Rotated = Rot.InverseRotate(Pos);
                auto Scaled = JPH::Float3(Rotated.GetX(), Rotated.GetY(), Rotated.GetZ());
                
                VertList.emplace_back(Scaled);
            }

            for (size_t i = 0; i < Mesh->num_indices; i+=3)
            {
                auto idx0 = Mesh->vertex_indices[i];
                auto idx1 = Mesh->vertex_indices[i + 1];
                auto idx2 = Mesh->vertex_indices[i + 2];

                JPH::IndexedTriangle triangle(idx0, idx1, idx2);

                TriList.emplace_back(triangle);
            }

            JPH::Ref<JPH::MeshShapeSettings> MeshSettings = new JPH::MeshShapeSettings(std::move(VertList), std::move(TriList));
           
            auto LandscapeSettings = std::make_unique<JPH::BodyCreationSettings>(MeshSettings, JPH::Vec3::sZero(), JPH::Quat::sIdentity(), JPH::EMotionType::Static, Layers::NON_MOVING);
            Meshes.emplace_back(MeshSettings);

            Body &Floor = *System->GetBodyInterface().CreateBody(*LandscapeSettings);
            Floor.GetShape()->SaveWithChildren(OutStream, ShapeToId, MaterialToId);
            Bodies.emplace_back(std::move(LandscapeSettings));
        }
        
        // release scene resources
        ufbx_free_scene(scene);

        Log.Info("Finished processing Meshes... Saving to disk");
        std::ofstream OutFile{OutPath.string(), std::ofstream::out | std::ofstream::trunc | std::ofstream::binary};
        OutFile << Data.rdbuf();
        OutFile.close();
        Log.Info("Saved collision mesh data to disk");
        // clear out our references
        Meshes.clear();
        Bodies.clear();
        return true;
    }
}