#pragma once

#include "config/config.h"
#include "logger/logger.h"

#include <Jolt/Jolt.h>
#include <Jolt/Physics/PhysicsSystem.h>
#include <Jolt/Physics/Collision/Shape/MeshShape.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>
#include <Jolt/Core/StreamWrapper.h>

#include <filesystem>
namespace fs = std::filesystem;

namespace physics
{
    /**
     * @brief Loads the landscape meshes or optimized collision data for importing into
     * Jolt
     * 
     */
    class Landscape
    {
    public:
        /**
         * @brief Construct a new Landscape for this server
         * Loads the landscape mesh or binary data to create a JoltPhysics Body.
         * 
         * @param System
         * @param Config 
         * @param Logger 
         */
        Landscape(JPH::PhysicsSystem *System, config::Config &Config, logging::Logger &Logger);

        /**
         * @brief Loads the next landscape chunk, returning true if loaded 
         * 
         * @param OutShape 
         * @return true chunk has been read into Ref<JPH::Shape>
         * @return false no more chunks or error occurred.
         */
        bool NextLandscapeChunk(JPH::Ref<JPH::Shape> &OutShape);

        /**
         * @brief Serializes 
         * 
         * @param System Physics System necessary for creating bodies
         * @param MapPath Path to where the map file is
         * @param MapName Name of the map (part of the map without file extension)
         * @return true if loaded successfully.
         * @return false 
         */
        bool SerializeFBX(JPH::PhysicsSystem *System, fs::path MapPath);

    private:
        /**
         * @brief Load our body data and prepare the streams
         * 
         * @param BinPath 
         * @param OutBodySettings 
         */
        void LoadBodyData(fs::path BinPath);

        std::string MapName;
        logging::Logger &Log;
        
        /** Holds our data from disk */
        std::stringstream Data;
        /** Stream manager of landscape data */
        JPH::StreamInWrapper StreamIn{Data};

        // Necessary for tracking multiple loads of the landscape
        JPH::Shape::IDToShapeMap IdToShape;
        JPH::Shape::IDToMaterialMap IdToMaterial;
    };
}
