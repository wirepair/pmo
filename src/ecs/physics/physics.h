#pragma once

#include <flecs.h>
#include <vector>

#include "broadphase.h"
#include "landscape.h"
#include "dispatcher.h"
#include "state.h"
#include "config/config.h"
#include "logger/logger.h"
#include "debugrender/debugrender.h"
#include "debugrender/joltdebugrender.h"
#include "ecs/units.h"

// Jolt includes
#include <Jolt/RegisterTypes.h>
#include <Jolt/Core/Factory.h>
#include <Jolt/Core/TempAllocator.h>
#include <Jolt/Core/JobSystemThreadPool.h>
#include <Jolt/Physics/PhysicsSettings.h>
#include <Jolt/Physics/PhysicsSystem.h>
#include <Jolt/Physics/Collision/Shape/BoxShape.h>
#include <Jolt/Physics/Collision/Shape/CapsuleShape.h>
#include <Jolt/Physics/Collision/Shape/RotatedTranslatedShape.h>
#include <Jolt/Physics/Collision/ContactListener.h>
#include <Jolt/Physics/Body/BodyCreationSettings.h>
#include <Jolt/Physics/Body/BodyActivationListener.h>
#include <Jolt/Physics/Character/Character.h>

namespace physics
{
    struct PhysicsBody
    {
        JPH::BodyID ID;
    };

    struct CollisionCapsule
    {
        JPH::BodyID ID;
        float Height;
        float Radius;
    };

    class Physics
    {
    public:
        Physics(flecs::world &GameWorld);

        ~Physics()
        {
            delete System;
            delete JobThreadPool;
            delete Allocator;
        }
        // move assign
        Physics& operator=(Physics&& other)
        {
            Allocator = std::move(other.Allocator);
            JobThreadPool = std::move(other.JobThreadPool);
            System = std::move(other.System);
            Settings = other.Settings;
            StateFilter = other.StateFilter;
            BroadPhaseLayer = std::move(other.BroadPhaseLayer);
            ObjectVsBroadPhaseLayerFilter = std::move(other.ObjectVsBroadPhaseLayerFilter);
            ObjectLayerPairFilter = std::move(other.ObjectLayerPairFilter);
            DebugRenderer = std::move(other.DebugRenderer);
            ContactEventDispatcher = std::move(other.ContactEventDispatcher);
            Log = std::move(other.Log);
            return *this;
        }

        // move constructor
        Physics(Physics&& other) noexcept
        {
            Allocator = std::move(other.Allocator);
            JobThreadPool = std::move(other.JobThreadPool);
            System = std::move(other.System);
            Settings = other.Settings;
            StateFilter = other.StateFilter;
            BroadPhaseLayer = std::move(other.BroadPhaseLayer);
            ObjectVsBroadPhaseLayerFilter = std::move(other.ObjectVsBroadPhaseLayerFilter);
            ObjectLayerPairFilter = std::move(other.ObjectLayerPairFilter);
            DebugRenderer = std::move(other.DebugRenderer);
            ContactEventDispatcher = std::move(other.ContactEventDispatcher);
            Log = std::move(other.Log);
        }
        
        /**
         * @brief Initialize our Physics world
         * 
         * @param Log 
         * @param GameWorld 
         * @param Config 
         * @param LandScape 
         * @return true 
         * @return false 
         */
        bool InitializeWorld(logging::Logger *Log, config::Config &Config, flecs::world &GameWorld, Landscape &LandScape);

        /**
         * @brief Shuts down our world
         * 
         */
        void ShutdownWorld();
        
        /**
         * @brief Adds a collision capsule to the world and sets the entities physics::CollisionCapsule properties.
         * 
         */
        void AddCapsuleBody(flecs::entity &BodyReciever, float Height, float Radius, units::Vector3 &Position);

        /**
         * @brief Initialize the Physics ECS systems which runs our collision sims.
         * 
         * @param GameWorld 
         */
        void PhysicsSystems(flecs::world &GameWorld);

        static constexpr float DeltaTime = 0.03333333f;

        JPH::TempAllocatorImpl *Allocator = nullptr;

        JPH::JobSystemThreadPool *JobThreadPool = nullptr;

        JPH::PhysicsSystem *System = nullptr;

        JPH::PhysicsSettings Settings;

        StateSaverFilter StateFilter;

        std::unique_ptr<BPLayerInterfaceImpl> BroadPhaseLayer;

        std::unique_ptr<ObjectVsBroadPhaseLayerFilterImpl> ObjectVsBroadPhaseLayerFilter;

        std::unique_ptr<ObjectLayerPairFilterImpl> ObjectLayerPairFilter;

        logging::Logger *Log = nullptr;

        std::unique_ptr<debug::JoltDebugRenderer> DebugRenderer = nullptr;

        std::unique_ptr<ContactDispatcher> ContactEventDispatcher = nullptr;
               
        int FrameCount = 0;
    };
}
