#pragma once 
#include <array>

#include <Jolt/Jolt.h>
#include <Jolt/Physics/Body/Body.h>
#include <Jolt/Physics/StateRecorderImpl.h>

namespace physics
{
    struct State
    {
        std::array<JPH::StateRecorderImpl, 24> Physics{};
    };

    /**
     * @brief Determines which physics bodies to record state for
     * 
     */
    class StateSaverFilter : public JPH::StateRecorderFilter
    {
    public:
        virtual bool ShouldSaveBody(const JPH::Body &inBody) const override;
    };
}