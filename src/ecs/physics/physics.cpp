#include "physics.h"

#include <chrono>
#include "ecs/state.h"
#include "ecs/units.h"
#include "ecs/level.h"
#include "ecs/character/input.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/character/components.h"
#include "ecs/character/network.h"
#include "ecs/network/network.h"
#include <thread>
#include <fstream>

namespace physics
{
    Physics::Physics(flecs::world &GameWorld) 
    {
        GameWorld.module<Physics>();

        assert(JPH::VerifyJoltVersionID());
        
        // This is the max amount of rigid bodies that you can add to the physics system. If you try to add more you'll get an error.
        // Note: This value is low because this is a simple test. For a real project use something in the order of 65536.
        const uint cMaxBodies = 65536;

        // This determines how many mutexes to allocate to protect rigid bodies from concurrent access. Set it to 0 for the default settings.
        const uint cNumBodyMutexes = 0;

        // This is the max amount of body pairs that can be queued at any time (the broad phase will detect overlapping
        // body pairs based on their bounding boxes and will insert them into a queue for the narrowphase). If you make this buffer
        // too small the queue will fill up and the broad phase jobs will start to do narrow phase work. This is slightly less efficient.
        // Note: This value is low because this is a simple test. For a real project use something in the order of 65536.
        const uint cMaxBodyPairs = 65536;

        // This is the maximum size of the contact constraint buffer. If more contacts (collisions between bodies) are detected than this
        // number then these contacts will be ignored and bodies will start interpenetrating / fall through the world.
        // Note: This value is low because this is a simple test. For a real project use something in the order of 10240.
        const uint cMaxContactConstraints = 10240;

        JPH::RegisterDefaultAllocator();
        // Register allocation hook. In this example we'll just let Jolt use malloc / free but you can override these if you want (see Memory.h).
        // This needs to be done before any other Jolt function is called.
        Allocator =  new JPH::TempAllocatorImpl(10*1024*1024);
        JobThreadPool = new JPH::JobSystemThreadPool(JPH::cMaxPhysicsJobs, JPH::cMaxPhysicsBarriers, thread::hardware_concurrency() - 1);
        System = new JPH::PhysicsSystem();

        JPH::Factory::sInstance = new JPH::Factory(); // this has to be used with JPH's "new" override

        // Register all physics types with the factory and install their collision handlers with the CollisionDispatch class.
        // If you have your own custom shape types you probably need to register their handlers with the CollisionDispatch before calling this function.
        // If you implement your own default material (PhysicsMaterial::sDefault) make sure to initialize it before this function or else this function will create one for you.
        JPH::RegisterTypes();

        BroadPhaseLayer = std::make_unique<BPLayerInterfaceImpl>();
        ObjectVsBroadPhaseLayerFilter = std::make_unique<ObjectVsBroadPhaseLayerFilterImpl>();
        ObjectLayerPairFilter = std::make_unique<ObjectLayerPairFilterImpl>();
        ContactEventDispatcher = std::make_unique<ContactDispatcher>();

        System->Init(cMaxBodies, cNumBodyMutexes, cMaxBodyPairs, cMaxContactConstraints, *BroadPhaseLayer.get(), *ObjectVsBroadPhaseLayerFilter.get(), *ObjectLayerPairFilter.get());
        System->SetPhysicsSettings(Settings);
        System->SetContactListener(static_cast<JPH::ContactListener *>(ContactEventDispatcher.get()));
        
        System->SetGravity(JPH::Vec3(0.f, -9.81f, 0.f));
    }
   
    bool Physics::InitializeWorld(logging::Logger *Logger, config::Config &Config, flecs::world &GameWorld, Landscape &Landscape)
    {
        Log = Logger;

        Ref<JPH::Shape> LandShape;

        const auto MapName = Config.GetMapName();
        auto MapEntity = GameWorld.entity(MapName)
            .set<level::MapLevel>({Config.GetMapName()});
        
        int ChunkCount = 0;
        
        JPH::BodyInterface &BodyInterface = System->GetBodyInterface();
        
        while(Landscape.NextLandscapeChunk(LandShape))
        {
            JPH::BodyCreationSettings LandSettings(LandShape, RVec3::sZero(), JPH::Quat::sIdentity(), EMotionType::Static, physics::Layers::NON_MOVING);
            auto LandscapeBody = BodyInterface.CreateBody(LandSettings);
            auto BodyID = LandscapeBody->GetID();
            BodyInterface.AddBody(BodyID, EActivation::DontActivate);
            
            auto LandscapeCenter = LandscapeBody->GetShape()->GetLocalBounds().GetCenter();
            auto LandscapeSize = LandscapeBody->GetShape()->GetLocalBounds().GetExtent();

            // Y might be a plane, so too small for the convex radius (.05)
            if (LandscapeSize.GetY() < 20.f)
            {
                LandscapeSize.SetY(20.f);
            }
            
            BodyCreationSettings LocalSensorSettings(new BoxShape(LandscapeSize), LandscapeCenter, Quat::sIdentity(), EMotionType::Static, Layers::SENSOR);
            LocalSensorSettings.mIsSensor = true;
            auto LocalSensor = BodyInterface.CreateAndAddBody(LocalSensorSettings, EActivation::DontActivate);

            BodyCreationSettings MultiSensorSettings(new BoxShape(LandscapeSize*3), LandscapeCenter, Quat::sIdentity(), EMotionType::Static, Layers::SENSOR);
            MultiSensorSettings.mIsSensor = true;
            auto MultiSensor = BodyInterface.CreateAndAddBody(MultiSensorSettings, EActivation::DontActivate);

            // register the landscape chunk and it's AoI sensors with ECS Physics Systems
            auto Chunk = GameWorld.entity()
                .set<level::MapChunk>({LandscapeBody->GetID()})
                .set<level::LocalSensor>({LocalSensor})
                .set<level::MultiSensor>({MultiSensor});
            
            Chunk.child_of(MapEntity);
            ChunkCount++;
        }

        System->OptimizeBroadPhase();
        Log->Info("Created MapLevel [{}] consisting of {} chunks", Config.GetMapName(), ChunkCount);

        // Create a JoltDebugRenderer from our input renderer (UE5).
        if (Config.GetDebugRenderer())
        {
            DebugRenderer = std::make_unique<debug::JoltDebugRenderer>(Config.GetDebugRenderer());
        }

        PhysicsSystems(GameWorld);

        return ChunkCount > 0;
    }

    void Physics::PhysicsSystems(flecs::world &GameWorld) 
    {
        GameWorld.component<physics::CollisionCapsule>();

        #ifndef IS_SERVER
        /**
         * @brief This system needs to run ASAP after our main world.ProcessServerUpdate has occurred.
         * We need to get the state at which we got desync'd
         * We need to teleport the player back to where the server see's us
         * We need to start resimulating from desynch frame +1, extract the client player input
         * Apply the new motion information
         * Call PrePhysicsUpdater on our char
         * Call 
         * Call physics->Update to simulate with the adjust positions
         * 
         * Is this going to fuck up network players positions? Do i need to store that state too?? I don't think so because we'll get the latest
         * network character positions from world.ProcessServerUpdate, which will update this frame with that latest position.
         */
        GameWorld.system<network::PhysicsDesynced, physics::State, character::PMOCharacter, network::ServerStateBuffer, network::ClientStateBuffer>("OnPhysicsDesynched")
            .kind(flecs::OnLoad)
            .each([&](flecs::iter& It, size_t Index, network::PhysicsDesynced &Desynch, physics::State &PhysicsState, character::PMOCharacter &Character, network::ServerStateBuffer &ServerState, network::ClientStateBuffer &ClientState)
            {
                auto Start = std::chrono::system_clock::now();
                auto Player = It.entity(Index);
                auto &State = PhysicsState.Physics.at(Desynch.SequenceId);
                State.Rewind();
                auto OriginalPos = Character.GetPosition();
                auto OriginalRot = Character.GetRotation();
                if (System->RestoreState(State))
                {
                    // Teleport the player back to where the server see's us so we can restart simulation from that frame +1
                    auto ServerSeqId = ServerState.State.SequenceId;
                    auto ServerPosition = ServerState.State.PositionAt(ServerSeqId);
                    auto ServerRotation = ServerState.State.RotationAt(ServerSeqId);
                    auto ServerVelocity = ServerState.State.VelocityAt(ServerSeqId);
                    
                    Character.SetLocation(ServerPosition);
                    Character.SetRotation(ServerRotation);
                    Character.SetVelocity(ServerVelocity);
                    // update our old/incorrect state with the one from the server and reset our sequenceId
                    ClientState.State.SequenceId = Desynch.SequenceId;
                    ClientState.State.AddStates(ServerPosition, ServerVelocity, ServerRotation);
                    Log->Info("flecs::OnLoad Rewinding player state to {} Server Seq: {} Client Seq: {} Pos: {} {} {}", Desynch.SequenceId, ServerSeqId, ClientState.State.SequenceId, ServerPosition.vec3[0], ServerPosition.vec3[1], ServerPosition.vec3[2]);
                    
                    ClientState.State.Inc();
                    const int CollisionSteps = 1;

                    for (size_t i = 1; i < ServerState.SequenceDistance; i++)
                    {
                        auto ReplaySeqId = (Desynch.SequenceId+i) % 24;
                        auto Inputs = ClientState.InputAt(ReplaySeqId);
                        Character.CharacterController->DesiredVelocity = ClientState.DesiredVelocityAt(ReplaySeqId);

                        Log->Info("flecs::OnLoad Re-simulating player state to {}, Replaying input: {} yaw: {}", ReplaySeqId, Inputs.KeyPresses.KeyPresses, Inputs.YawAngle);
                        
                        Character.PrePhysicsUpdate(Allocator);
                        // restore the previous desired velocity

                        character::ProcessUserInput(Log, Character, Inputs);
                        
                        System->Update(DeltaTime, CollisionSteps, Allocator, JobThreadPool);
                        
                        auto UpdatedPos = convert::JPHVec3ToVector3(Character.CharacterController->GetPosition());
                        auto UpdatedVel = convert::JPHVec3ToVelocity(Character.CharacterController->GetVelocity());
                        auto UpdatedRot = convert::JPHQuatToQuat(Character.CharacterController->GetRotation());
                        Log->Info("flecs::OnLoad player now at Pos {} {} {} Rot: {} {} {} {} was OldPos: {} {} {} OldRot: {} {} {} {} (Seq Id: {})", 
                            UpdatedPos.vec3[0], UpdatedPos.vec3[1], UpdatedPos.vec3[2],
                            UpdatedRot.vec4[0], UpdatedRot.vec4[1], UpdatedRot.vec4[2], UpdatedRot.vec4[3],
                            OriginalPos.vec3[0], OriginalPos.vec3[1], OriginalPos.vec3[2],
                            OriginalRot.vec4[0], OriginalRot.vec4[1], OriginalRot.vec4[2], OriginalRot.vec4[3],
                            ClientState.State.SequenceId
                        );
                        ClientState.State.AddStates(UpdatedPos, UpdatedVel, UpdatedRot);
                        ClientState.State.Inc();
                    }
                    auto Runtime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - Start);
                    Log->Info("---------------- desynched client after simulation (took {}usec/{}ms) ----------------", Runtime.count(), std::chrono::duration_cast<std::chrono::milliseconds>(Runtime).count());
                    ClientState.State.Print(*Log);
                    // remove now that we've replayed
                    Player.remove<network::PhysicsDesynced>();
                }
                else
                {
                    Log->Warn("failed to restore PhysicsState!");
                }

            });

            // Call this (PostLoad) BEFORE handling input (PreUpdate)
        GameWorld.system<physics::State, character::PMOCharacter, network::ClientStateBuffer>("OnSavePhysicsState")
            .kind(flecs::PostLoad)
            .each([&](flecs::iter& It, size_t Index, physics::State &PhysicsState, character::PMOCharacter &Character, network::ClientStateBuffer &ClientState)
            {
                //Log->Info("PostLoad: OnSavePhysicsState");
                auto SeqId = ClientState.State.SequenceId;
                PhysicsState.Physics.at(SeqId).Clear();
                auto Start = std::chrono::system_clock::now();
                System->SaveState(PhysicsState.Physics.at(SeqId), JPH::EStateRecorderState::All, &StateFilter);
                auto Runtime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - Start);
                auto RuntimeMilli = std::chrono::duration_cast<std::chrono::milliseconds>(Runtime);
                Log->Info("PostLoad: OnSavePhysicsState took: {}usec/{}ms", Runtime.count(), RuntimeMilli.count());
            });
        #endif

        GameWorld.system<character::PMOCharacter>("PrePhysicsUpdate")
            .kind(flecs::PostLoad)
            .immediate(true)
            .each([&](flecs::iter& It, size_t Index, character::PMOCharacter &Character)
            {
                //Log->Info("PostLoad: PrePhysicsUpdate");
                auto Start = std::chrono::system_clock::now();
                Character.PrePhysicsUpdate(Allocator);
                auto Runtime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - Start);
                auto RuntimeMilli = std::chrono::duration_cast<std::chrono::milliseconds>(Runtime);
                Log->Info("PostLoad: PrePhysicsUpdate state took: {}usec/{}ms", Runtime.count(), RuntimeMilli.count());
            });

        GameWorld.system("PhysicsUpdate")
            .kind(flecs::OnUpdate)
            .write<combat::ApplyDamage, combat::AttackDetails>()
            .immediate(true)
            .run([&](flecs::iter& It)
            {
                //Log->Info("OnUpdate: PhysicsUpdate");
                // Walk over the iterator to empty it, otherwise we'll get a leak assert
                // (this only happens with .run functions)
                while (It.next()) {}
                const int CollisionSteps = 1;
                // System->Update(It.delta_time(), CollisionSteps, Allocator, JobThreadPool);
                auto Start = std::chrono::system_clock::now();
                System->Update(DeltaTime, CollisionSteps, Allocator, JobThreadPool);
                auto Runtime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - Start);
                auto RuntimeMilli = std::chrono::duration_cast<std::chrono::milliseconds>(Runtime);
                Log->Info("OnUpdate: PhysicsUpdate state took: {}usec/{}ms", Runtime.count(), RuntimeMilli.count());
            });


        #ifndef IS_SERVER
        
        // take values from server and set pre/post simulation for all network actors
        GameWorld.system<character::NetworkCharacter, units::Vector3, units::Velocity, units::Quat>("UpdateNetworkPlayersPhysics")
            .kind(flecs::PostUpdate)
            .each([&](flecs::iter& It, size_t Index, character::NetworkCharacter &Character, units::Vector3 &Pos, units::Velocity &Vel, units::Quat &Rot)
            {
                const float cCollisionTolerance = 0.05f;
                // Pos/Vel/Rot come from world.cpp ProcessServerCommand
                Character.Character->SetLinearVelocity(JPH::Vec3{Vel.vec3[0], Vel.vec3[1], Vel.vec3[2]});
                Character.Character->PostSimulation(cCollisionTolerance);
                Character.UpdatePositionAndRotation(Pos, Rot);
            });


        GameWorld.system<character::PMOCharacter, network::ClientStateBuffer, units::Vector3, units::Velocity, units::Quat>("PostPhysicSimulation")
            .kind(flecs::PreStore)
            .each([&](flecs::iter& It, size_t Index, character::PMOCharacter &Character, network::ClientStateBuffer &ClientState, units::Vector3 &Pos, units::Velocity &Vel, units::Quat &Rot)
            {
                Pos = Character.GetPosition();
                Rot = Character.GetRotation();
                Vel = Character.GetVelocity();
                
                Log->Info("PreStore: PostPhysicSimulation: Client seq: {} Pos: {} {} {} Rot {} {} {} {} Vel {} {} {}",
                    ClientState.State.SequenceId,
                    Pos.vec3[0], Pos.vec3[1], Pos.vec3[2],
                    Rot.vec4[0], Rot.vec4[1], Rot.vec4[2], Rot.vec4[3],
                    Vel.vec3[0], Vel.vec3[1], Vel.vec3[2]
                );
                
                ClientState.State.AddPosition(Pos);
                ClientState.State.AddRotation(Rot);
                ClientState.State.AddVelocity(Vel);
                
            });
        #endif
        
        // Server Only
        #ifdef IS_SERVER
        GameWorld.system<character::PMOCharacter, units::Vector3, units::Velocity, units::Quat>("PostPhysicSimulation")
            .kind(flecs::PreStore)
            .each([&](flecs::iter& It, size_t Index, character::PMOCharacter &Character, units::Vector3 &Pos, units::Velocity &Vel, units::Quat &Rot)
            {
                Pos = Character.GetPosition();
                Rot = Character.GetRotation();
                Vel = Character.GetVelocity();
                Log->Info("PreStore: PostPhysicSimulation: Pos: {} {} {} Rot {} {} {} {} Vel {} {} {}",
                    Pos.vec3[0], Pos.vec3[1], Pos.vec3[2],
                    Rot.vec4[0], Rot.vec4[1], Rot.vec4[2], Rot.vec4[3],
                    Vel.vec3[0], Vel.vec3[1], Vel.vec3[2]
                );
            });

        GameWorld.system<state::PlayerWorldStates, character::Visibility, units::Attributes, units::TraitAttributes, units::Vector3, units::Velocity, units::Quat>("OnServerUpdateState")
            .kind(flecs::OnStore)
            .each([&](flecs::iter& It, size_t Index, state::PlayerWorldStates &States, character::Visibility &Visibility, units::Attributes &Attributes, units::TraitAttributes &Traits, units::Vector3 &Pos, units::Velocity &Vel, units::Quat &Rot) 
            {
                auto ThisCharacter = It.entity(Index);
                Visibility.Neighbors.erase(ThisCharacter); // Remove ourselves from our visibility list.
                Log->Info("OnStore: ServerUpdateState: EntityId: {} SeqId: {} Pos Now: {} {} {}", static_cast<uint64_t>(It.entity(Index).id()), States.SequenceId, Pos.vec3[0], Pos.vec3[1], Pos.vec3[2]);
                States.UpdateState(*Log, Visibility, Attributes, Traits, Pos, Vel, Rot);
            });
        #endif
    }

    void Physics::ShutdownWorld()
    {
        // Unregisters all types with the factory and cleans up the default material
        JPH::UnregisterTypes();

        // Destroy the factory
        delete JPH::Factory::sInstance;
        JPH::Factory::sInstance = nullptr;
    }
}

