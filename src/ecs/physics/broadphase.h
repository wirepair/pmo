#pragma once

#include <Jolt/Jolt.h>

#include <Jolt/Physics/PhysicsSystem.h>

using namespace JPH;

// If you want your code to compile using single or double precision write 0.0_r to get a Real value that compiles to double or float depending if JPH_DOUBLE_PRECISION is set or not.
using namespace JPH::literals;

namespace physics 
{
    // Layer that objects can be in, determines which other objects it can collide with
    // Typically you at least want to have 1 layer for moving bodies and 1 layer for static bodies, but you can have more
    // layers if you want. E.g. you could have a layer for high detail collision (which is not used by the physics simulation
    // but only if you do collision testing).
    namespace Layers
    {
        static constexpr ObjectLayer NON_MOVING = 0;
        static constexpr ObjectLayer MOVING = 1;
        static constexpr ObjectLayer SENSOR = 2;
        static constexpr ObjectLayer CHARACTER = 3;
        static constexpr ObjectLayer COMBAT = 4;
        static constexpr ObjectLayer NUM_LAYERS = 5;
    }

    // Each broadphase layer results in a separate bounding volume tree in the broad phase. You at least want to have
    // a layer for non-moving and moving objects to avoid having to update a tree full of static objects every frame.
    // You can have a 1-on-1 mapping between object layers and broadphase layers (like in this case) but if you have
    // many object layers you'll be creating many broad phase trees, which is not efficient. If you want to fine tune
    // your broadphase layers define JPH_TRACK_BROADPHASE_STATS and look at the stats reported on the TTY.
    namespace BroadPhaseLayers
    {
        static constexpr BroadPhaseLayer NON_MOVING(0);
        static constexpr BroadPhaseLayer MOVING(1);
        static constexpr BroadPhaseLayer SENSOR(2);
        static constexpr BroadPhaseLayer CHARACTER(3);
        static constexpr BroadPhaseLayer COMBAT(4);
        static constexpr uint NUM_LAYERS(5);
    }


    // BroadPhaseLayerInterface implementation
    // This defines a mapping between object and broadphase layers.
    class BPLayerInterfaceImpl final : public BroadPhaseLayerInterface
    {
    public:
        BPLayerInterfaceImpl()
        {
            // Create a mapping table from object to broad phase layer
            ObjectToBroadPhase[Layers::NON_MOVING] = BroadPhaseLayers::NON_MOVING;
            ObjectToBroadPhase[Layers::MOVING] = BroadPhaseLayers::MOVING;
            ObjectToBroadPhase[Layers::SENSOR] = BroadPhaseLayers::SENSOR;
            ObjectToBroadPhase[Layers::CHARACTER] = BroadPhaseLayers::CHARACTER;
            ObjectToBroadPhase[Layers::COMBAT] = BroadPhaseLayers::COMBAT;
        }

        virtual uint GetNumBroadPhaseLayers() const override
        {
            return BroadPhaseLayers::NUM_LAYERS;
        }

        virtual BroadPhaseLayer	GetBroadPhaseLayer(ObjectLayer InLayer) const override
        {
            JPH_ASSERT(InLayer < Layers::NUM_LAYERS);
            return ObjectToBroadPhase[InLayer];
        }

    #if defined(JPH_EXTERNAL_PROFILE) || defined(JPH_PROFILE_ENABLED)
        virtual const char* GetBroadPhaseLayerName(BroadPhaseLayer InLayer) const override
        {
            switch ((BroadPhaseLayer::Type)InLayer)
            {
            case (BroadPhaseLayer::Type)BroadPhaseLayers::NON_MOVING:	return "NON_MOVING";
            case (BroadPhaseLayer::Type)BroadPhaseLayers::MOVING:		return "MOVING";
            case (BroadPhaseLayer::Type)BroadPhaseLayers::SENSOR:		return "SENSOR";
            case (BroadPhaseLayer::Type)BroadPhaseLayers::CHARACTER:	return "CHARACTER";
            case (BroadPhaseLayer::Type)BroadPhaseLayers::COMBAT:	    return "COMBAT";
            default:													JPH_ASSERT(false); return "INVALID";
            }
        }
    #endif // JPH_EXTERNAL_PROFILE || JPH_PROFILE_ENABLED

    private:
        BroadPhaseLayer	ObjectToBroadPhase[Layers::NUM_LAYERS];
    };

    /// Class that determines if two object layers can collide
    class ObjectLayerPairFilterImpl : public ObjectLayerPairFilter
    {
    public:
        virtual bool ShouldCollide(ObjectLayer InObject1, ObjectLayer InObject2) const override
        {
            switch (InObject1)
            {
            case Layers::NON_MOVING:
                return InObject2 == Layers::MOVING; // Non moving only collides with moving
            case Layers::MOVING:
                return InObject2 == Layers::NON_MOVING || InObject2 == Layers::MOVING || InObject2 == Layers::SENSOR;; // Moving collides with everything
            case Layers::CHARACTER:
                // TODO: We may only want characters to collide when PvP'ing/fighting which will greatly complicate this class
                return InObject2 == Layers::CHARACTER || InObject2 == Layers::NON_MOVING || InObject2 == Layers::SENSOR || InObject2 == Layers::COMBAT;
            // case Layers::COMBAT:
            //     return InObject2 == Layers::CHARACTER;
            default:
                JPH_ASSERT(false);
                return false;
            }
        }
    };

    /// Class that determines if an object layer can collide with a broadphase layer
    class ObjectVsBroadPhaseLayerFilterImpl : public ObjectVsBroadPhaseLayerFilter
    {
    public:
        virtual bool ShouldCollide(ObjectLayer InLayer1, BroadPhaseLayer InLayer2) const override
        {
            switch (InLayer1)
            {
            case Layers::NON_MOVING:
                return InLayer2 == BroadPhaseLayers::MOVING;
            case Layers::MOVING:
                return InLayer2 == BroadPhaseLayers::NON_MOVING || InLayer2 == BroadPhaseLayers::MOVING || InLayer2 == BroadPhaseLayers::SENSOR;
            case Layers::CHARACTER:
			    return InLayer2 == BroadPhaseLayers::CHARACTER || InLayer2 == BroadPhaseLayers::NON_MOVING || InLayer2 == BroadPhaseLayers::SENSOR || InLayer2 == BroadPhaseLayers::COMBAT;
            // case Layers::COMBAT:
            //     return InLayer2 == BroadPhaseLayers::CHARACTER;
            default:
                JPH_ASSERT(false);
                return false;
            }
        }
    };
}
