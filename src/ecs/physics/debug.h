#pragma once

#include "physics.h"
#include "logger/logger.h"

namespace physics
{
    void DebugPrintBodies(logging::Logger &Logger, physics::Physics &PhysicsSystem)
    {
        JPH::BodyIDVector Bodies;
        PhysicsSystem.System->GetBodies(Bodies);
        for (auto Body : Bodies)
        {
            auto Loc = PhysicsSystem.System->GetBodyInterface().GetWorldTransform(Body).GetTranslation();
            
            Logger.Info("BodyID: {} isActive: {} Loc: {} {} {}", 
                Body.GetIndexAndSequenceNumber(), 
                PhysicsSystem.System->GetBodyInterface().IsActive(Body),
                Loc.GetX(), Loc.GetY(), Loc.GetZ()
            );
        }
    }

}