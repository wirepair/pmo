#include "state.h"
#include <algorithm>

namespace physics
{
    bool StateSaverFilter::ShouldSaveBody(const JPH::Body &InBody) const 
    {
        if (!InBody.IsActive() || InBody.IsStatic())
        {
            return false;
        }
        return true;
    }
}