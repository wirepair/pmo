#pragma once
#ifdef IS_SERVER

#include <flecs.h>
#include "network.h"

namespace network
{
    struct NetworkServer
    {
        NetworkServer(flecs::world &GameWorld);
        
        /**
         * @brief Configures our NetworkServer ECS components
         * 
         * @param GameWorld 
         */
        void Components(flecs::world &GameWorld);

        /**
         * @brief Configures our NetworkServer ECS systems
         * 
         * @param GameWorld 
         */
        void Systems(flecs::world &GameWorld);

        /**
         * @brief Initializes class variables after the module is created
         * 
         * @param Log 
         * @param ListenerThread 
         * @param In 
         * @param Out 
         * @return * void 
         */
        inline void Init(logging::Logger *Log, std::shared_ptr<net::Listener> ListenerThread, net::PacketQueueSharedPtr In, net::PacketQueueSharedPtr Out) 
        { 
            Logger = Log;
            Thread = ListenerThread;
            InQueue = In;
            OutQueue = Out;
        };

        /**
         * @brief Executes various queries given a socket message for a server
         * 
         * @param GameWorld 
         * @param UserID
         * @param Command 
         */
        void ExecQueries(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command);

        /**
         * @brief Processes incoming client commands
         * 
         * @param GameWorld 
         * @param UserId 
         * @param SequenceId
         * @param Command 
         */
        void ProcessClientCommand(flecs::world &GameWorld, const uint32_t UserId, const uint16_t SequenceId, const Game::Message::ClientCommand *Command);

        /**
         * @brief 
         * 
         * @param UserId 
         * @param SequenceId 
         * @param bWasEquipped 
         */
        void SendEquipResponse(const uint32_t UserId, const uint16_t SequenceId, bool bWasEquipped);

        /**
         * @brief Processes the client's RequestRespawn by setting character::Respawn tag and responding to the client
         * 
         * @param PlayerEntity 
         * @param UserId 
         */
        void ProcessClientRequestRespawn(flecs::entity &PlayerEntity, const uint32_t UserId, const uint16_t SequenceId);

        /**
         * @brief Processes client's inputs
         * 
         * @param PlayerEntity 
         * @param UserId 
         * @param SequenceId 
         * @param Command 
         */
        void ProcessClientInput(flecs::entity &PlayerEntity, const uint32_t UserId, const uint16_t SequenceId, const Game::Message::ClientCommand *Command);
 
        inline void SetHeartbeatTimeout(std::chrono::milliseconds Duration) { HeartbeatTimeout = Duration; };

        inline void SetDisconnectedTimeout(std::chrono::milliseconds Duration) { ConnectionTimeout = Duration; };

        public:
            net::PacketQueueSharedPtr InQueue; 
            net::PacketQueueSharedPtr OutQueue;
            std::shared_ptr<net::Listener> Thread;

        private:
            /**
             * @brief This query matches connected players and updates various details, also makes 
             * sure the client is still connected.
             * 
             * @param GameWorld 
             * @param Message 
             */
            void ExecHeartbeatQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command);

            /**
             * @brief This query matches all players who had an AuthComponent but now sent us their first ClientCommand
             * Meaning we can remove the AuthComponent and set them to Connected.
             * 
             * @param GameWorld 
             * @param UserId
             * @param Command 
             */
            void ExecAuthQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command);
            
            logging::Logger *Logger;
            
            std::chrono::milliseconds HeartbeatTimeout = std::chrono::milliseconds(1000);
            std::chrono::milliseconds ConnectionTimeout = std::chrono::milliseconds(4000);

            flecs::query<network::User> PlayerQuery;
            flecs::query<network::User, network::PMOServer, network::Auth> AuthQuery;
            flecs::query<network::User, network::PMOServer, network::Connected> HeartbeatQuery;
    };
}
#endif