#pragma once
#ifndef IS_SERVER

#include <flecs.h>
#include "network.h"
#include "ecs/world.h"

namespace network
{
    struct NetworkClient
    {
        NetworkClient(flecs::world &GameWorld);

        inline void Init(logging::Logger *Log, std::shared_ptr<net::Listener> ListenerThread, net::PacketQueueSharedPtr In, net::PacketQueueSharedPtr Out) 
        { 
            Logger = Log;
            Thread = ListenerThread;
            InQueue = In;
            OutQueue = Out;
        };

        /**
         * @brief Configures our NetworkServer ECS components
         * 
         * @param GameWorld 
         */
        void Components(flecs::world &GameWorld);

        /**
         * @brief Configures our NetworkServer ECS systems
         * 
         * @param GameWorld 
         */
        void Systems(flecs::world &GameWorld);

        /**
         * @brief Determines what to do based off of opcode
         * 
         * @param GameWorld 
         * @param UserId 
         * @param SequenceId 
         * @param Command 
         */
        void ProcessServerCommand(flecs::world &GameWorld, world::PMOWorld &PlayerWorld, const Game::Message::ServerCommand *Command);

        /**
         * @brief Process a failed message
         * 
         * @param GameWorld 
         * @param PlayerWorld 
         * @param Message 
         */
        void ProcessMessageFailed(flecs::world &GameWorld, world::PMOWorld &PlayerWorld, const Game::Message::GameMessage &Message);

        /**
         * @brief Executes various queries given a socket message for a client
         * 
         * @param GameWorld
         * @param UserID
         * @param Message 
         */
        void ExecQueries(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ServerCommand *Command);

    public:
        net::PacketQueueSharedPtr InQueue; 
        net::PacketQueueSharedPtr OutQueue;
        std::shared_ptr<net::Listener> Thread;

    private:

        void ExecHeartbeatQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ServerCommand *Command);

        logging::Logger *Logger;
    
        flecs::query<network::User, network::PMOClient, network::Connected> HeartbeatQuery;
    };
}
#endif