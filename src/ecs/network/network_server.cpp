#ifdef IS_SERVER

#include <span>

#include "network_server.h"
#include "ecs/units.h"
#include "ecs/level.h"
#include "ecs/state.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/items/inventory.h"


namespace network
{
    using namespace Game::Message;

    NetworkServer::NetworkServer(flecs::world &GameWorld) 
    {
        GameWorld.module<NetworkServer>();
    
        Components(GameWorld);
        Systems(GameWorld);
    }
    
    void NetworkServer::Components(flecs::world &GameWorld)
    {
        GameWorld.component<PMOServer>();
        AuthQuery = GameWorld.query<network::User, network::PMOServer, network::Auth>();
        HeartbeatQuery = GameWorld.query<network::User, network::PMOServer, network::Connected>();
        PlayerQuery = GameWorld.query<network::User>();
    }

    void NetworkServer::ProcessClientCommand(flecs::world &GameWorld, const uint32_t UserId, const uint16_t SequenceId, const Game::Message::ClientCommand *Command)
    {
        auto PlayerEntity = PlayerQuery.find([&UserId](network::User& p) {
            return p.UserId == UserId;
        });

        if (!PlayerEntity.is_valid())
        {
            Logger->Warn("Unable to find player entity UserId:{}, not processing enc client command", UserId);
            return;
        }

        switch (Command->opcode())
        {
            case Game::Message::ClientOpCode::ClientOpCode_Invalid:
            {
                Logger->Error("User {} sent ClientOpCode_Invalid", UserId);
                break;
            }
            case Game::Message::ClientOpCode::ClientOpCode_Input:
            {
                Logger->Warn("User {} sent opcode: {} ClientOpCode_Input Simulated Pos: {} {} {} Simulated Rot: {} {} {} {}", 
                    UserId, static_cast<uint16_t>(Command->opcode()),
                    Command->sim_loc()->x(),Command->sim_loc()->y(),Command->sim_loc()->z(),
                    Command->sim_rot()->x(),Command->sim_rot()->y(),Command->sim_rot()->z(),Command->sim_rot()->w()
                );
                ProcessClientInput(PlayerEntity, UserId, SequenceId, Command);
                break;
            }
            case Game::Message::ClientOpCode::ClientOpCode_RequestRespawn:
            {
                Logger->Warn("User {} sent opcode: {} ClientOpCode_RequestRespawn", UserId, static_cast<uint16_t>(Command->opcode()));
                ProcessClientRequestRespawn(PlayerEntity, UserId, SequenceId);
                break;
            }
            case Game::Message::ClientOpCode::ClientOpCode_RequestEquip:
            {
                Logger->Warn("User {} sent opcode: {} ClientOpCode_RequestEquip", UserId, static_cast<uint16_t>(Command->opcode()));

                auto Character = PlayerEntity.get_mut<character::PMOCharacter>();
                if (!Character)
                {
                    Logger->Error("User {} does not have character::PMOCharacter", UserId);
                    SendEquipResponse(UserId, SequenceId, false);
                    return;
                }
                
                auto EquipMsg = Command->equip();
                if (!EquipMsg)
                {
                    Logger->Error("User {} sent invalid/empty equip msg", UserId);
                    SendEquipResponse(UserId, SequenceId, false);
                    return;
                }

                flecs::id ItemId = GameWorld.id(EquipMsg->item());
                auto bWasEquipped = Character->EquipItem(EquipMsg->equipment_slot(), ItemId, EquipMsg->container());

                SendEquipResponse(UserId, SequenceId, bWasEquipped);
                break;
            }
            default:
            {
                Logger->Warn("User {} sent unknown opcode: {}", UserId, static_cast<uint16_t>(Command->opcode()));
                break;
            }
        }
    }

    void NetworkServer::SendEquipResponse(const uint32_t UserId, const uint16_t SequenceId, bool bWasEquipped)
    {
        flatbuffers::FlatBufferBuilder Builder(128);
        ServerCommandBuilder CmdBuilder(Builder);
        CmdBuilder.add_opcode(Game::Message::ServerOpCode_Equip);
        CmdBuilder.add_sequence_id(SequenceId);
        EquipResponseBuilder Equip(Builder);
        Equip.add_success(bWasEquipped);
        CmdBuilder.add_equip(Equip.Finish());
        Builder.Finish(CmdBuilder.Finish());

        auto OutputLen = Builder.GetSize();
        auto ServerData = std::make_unique<std::vector<uint8_t>>();
        ServerData->reserve(OutputLen);

        std::memcpy(ServerData->data(), Builder.GetBufferPointer(), Builder.GetSize());
        Builder.Release();

        auto EquipReply = std::make_unique<GameMessage>(UserId, std::move(ServerData), Game::Message::InternalMessage::Socket);
        
        OutQueue->Push(std::move(EquipReply));
        Thread->NotifyWrite();
    }

    void NetworkServer::ProcessClientRequestRespawn(flecs::entity &PlayerEntity, const uint32_t UserId, const uint16_t SequenceId)
    {
        if (!PlayerEntity.has<character::IsDead>())
        {
            Logger->Warn("Player {} requested respawn but isn't dead.", UserId);
            return;
        }

        // Tell the ECS system we are respawning
        PlayerEntity.add<character::Respawn>(); // See RespawnPlayer system

        auto BindLocation = PlayerEntity.get<character::BindLocation>();

        flatbuffers::FlatBufferBuilder Builder(128);
        ServerCommandBuilder CmdBuilder(Builder);
        CmdBuilder.add_sequence_id(SequenceId);
        CmdBuilder.add_opcode(Game::Message::ServerOpCode_Respawn);
        auto RespawnLocation = Game::Message::Vec3{BindLocation->Location.vec3[0], BindLocation->Location.vec3[1], BindLocation->Location.vec3[2]};
        RespawnResponseBuilder Respawn(Builder);
        Respawn.add_respawn_location(&RespawnLocation);
        CmdBuilder.add_respawn(Respawn.Finish());
        Builder.Finish(CmdBuilder.Finish());

        auto OutputLen = Builder.GetSize();
        auto ServerData = std::make_unique<std::vector<uint8_t>>();
        ServerData->reserve(OutputLen);

        std::memcpy(ServerData->data(), Builder.GetBufferPointer(), Builder.GetSize());
        Builder.Release();

        auto RespawnReply = std::make_unique<GameMessage>(UserId, std::move(ServerData), Game::Message::InternalMessage::Socket);
        
        OutQueue->Push(std::move(RespawnReply));
        Thread->NotifyWrite();
    }

    void NetworkServer::ProcessClientInput(flecs::entity &PlayerEntity, const uint32_t UserId, const uint16_t SequenceId, const Game::Message::ClientCommand *Command)
    {
        // Acknowledge player has recv'd a particular state sequence so we can send deltas.
        auto PlayerState = PlayerEntity.get_mut<state::PlayerWorldStates>();
        Logger->Info("(MainState) Got player {} ack for SeqId: {} (client time {}) cur SeqId: {}", UserId, Command->server_seq_id_ack(), Command->client_time(), PlayerState->SequenceId);
        PlayerState->AckId(Command->server_seq_id_ack());

        auto IncomingInputs = Command->inputs();

        auto KeyPresses = input::Input(IncomingInputs->inputs());
        auto IncomingYaw = IncomingInputs->yaw_angle();
        auto IncomingPitch = IncomingInputs->pitch_angle();

        PlayerEntity.set<network::Inputs>({KeyPresses, IncomingYaw, IncomingPitch, PlayerState->SequenceId});
    }

    void NetworkServer::ExecQueries(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command)
    {
        GameWorld.defer_begin();
        ExecHeartbeatQuery(GameWorld, UserId, Command);
        ExecAuthQuery(GameWorld, UserId, Command);
        GameWorld.defer_end();
    }
    
    void NetworkServer::ExecHeartbeatQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command)
    {
        HeartbeatQuery.each([&](flecs::entity Player, network::User &Network, network::PMOServer &Server, network::Connected &Connected) 
            {
                if (Network.UserId != UserId)
                {
                    return;
                }
                Player.set<network::Connected>({std::chrono::system_clock::now()});
            }
        );
    }

    void NetworkServer::ExecAuthQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ClientCommand *Command)
    {
        AuthQuery.each([&](flecs::entity Player, network::User &Network, network::PMOServer &, network::Auth &) 
            {
                //Logger->Debug("Got ClientCommand message from: {} equal {}", Network.UserId, UserId);
                if (Network.UserId != UserId)
                {
                    return;
                }

                Player.remove<network::Auth>()
                    .set<network::Connected>({std::chrono::system_clock::now()})
                    .set<network::UserTime>({std::chrono::steady_clock::now()})
                    .set<network::Inputs>({});
            }
        );
    }   

    void NetworkServer::Systems(flecs::world &GameWorld)
    {
        GameWorld.system<network::User, network::PMOServer, network::Connected>("HeartbeatCheck")
            .kind(flecs::PreFrame)
            .each([&](flecs::iter& It, size_t Index, network::User &Network, network::PMOServer &Server, network::Connected &Connected) 
            {
                auto Now = std::chrono::system_clock::now();
                auto Updated = std::chrono::duration_cast<std::chrono::milliseconds>(Now-Connected.LastUpdate);
                auto Player = It.entity(Index);
                
                if (Updated < HeartbeatTimeout)
                {
                    Player.remove<HeartbeatTimedOut>();
                    return;
                }
                else if (Updated > HeartbeatTimeout && Updated < ConnectionTimeout && !Player.has<HeartbeatTimedOut>())
                {
                    Logger->Warn("Client entity {} has not responed in {} milliseconds", Player.id(), HeartbeatTimeout.count());
                    Player.set<HeartbeatTimedOut>({Now});
                }
                else if (Updated > ConnectionTimeout)
                {
                    Logger->Warn("Client entity {} has not responded in {} milliseconds, removing", Player.id(), ConnectionTimeout.count());
                    auto RemoveUser = std::make_unique<GameMessage>(Network.UserId, InternalMessage::RemoveUser);
                    Server.OutQueue->Push(std::move(RemoveUser));
                    Thread->NotifyWrite();
                    Player.destruct();
                }               
            }
        );

        GameWorld.system("OnProcessPackets")
            .kind(flecs::PreFrame)
            .immediate(true)
            .run([&](flecs::iter& It)
        {
            while (It.next()) {}

            for (auto GameData = InQueue->Pop(); GameData != std::nullopt; GameData = InQueue->Pop())
            {
                auto PacketUserId = GameData->get()->UserId;

                // check IPC messages from socket thread first
                if (GameData->get()->InternalType != Game::Message::InternalMessage::Socket)
                {
                    switch (GameData->get()->InternalType)
                    {
                        case Game::Message::InternalMessage::NewUser:
                        {
                            auto NewPlayer = GameWorld.entity()
                                .set<network::User>({GameData->get()->UserId})
                                .set<network::PMOServer>({Thread, OutQueue})
                                .add<state::PlayerWorldStates>()
                                .set<network::Auth>({});

                            Logger->Info("New player id: {} created, entity {}", GameData->get()->UserId, NewPlayer.id());
                            break;
                        }
                        case Game::Message::InternalMessage::ReliableMsgFailed:
                        {
                            Logger->Error("Reliable message {} failed to be sent to: {}", GameData->get()->OpCode, GameData->get()->UserId);
                        }
                        default:
                        {
                            break;
                        }
                    }
                }

                // process socket messages
                switch (GameData->get()->MessageType)
                {
                    case Game::Message::MessageData_EncClientCommand:
                    {
                        auto Command = Game::Message::GetClientCommand(GameData->get()->Data->data());
                        if (!Command)
                        {
                            Logger->Warn("Failed to decode command!");
                            break;
                        }
                        Logger->Info("Got ClientCommand message from: {}, opcode: {} client seq ack: {} client time {}", GameData->get()->UserId, static_cast<uint16_t>(Command->opcode()), Command->server_seq_id_ack(), Command->client_time());

                        ExecQueries(GameWorld, PacketUserId, Command);
                        ProcessClientCommand(GameWorld, PacketUserId, GameData->get()->SequenceId, Command);
                    }
                    default:
                    {
                        break;
                    }
                }
                Game::Message::ReleaseGameMessage(std::move(GameData.value()));
            }
        });

        GameWorld.system<network::User, network::PMOServer, network::Connected>("ProcessSocketReliable")
            .kind(flecs::PreFrame)
            .each([&](flecs::iter& It, size_t Index, network::User &Network, network::PMOServer &Server, network::Connected &_) 
            {
                auto Process = std::make_unique<GameMessage>(Network.UserId, InternalMessage::ProcessReliableSocket);
                Process->Time = It.delta_time();
                Server.OutQueue->Push(std::move(Process));
                Thread->NotifyWrite();
            });


        // Sending out all client state should be the last system we run
        GameWorld.system<network::User, network::UserTime, state::PlayerWorldStates, network::Connected>("SendClientStateUpdate")
            .kind(flecs::PostFrame)
            .immediate(true)
            .each([&](flecs::iter& It, size_t Index, network::User &Network, network::UserTime &Time, state::PlayerWorldStates &States, network::Connected &Connected) 
            {
                flatbuffers::FlatBufferBuilder Builder(16384);
                // Get this clients latest state
                auto Now = std::chrono::steady_clock::now();
                auto UserTime = Now - Time.UserTime;
                const auto ServerTime = std::chrono::duration_cast<std::chrono::milliseconds>(UserTime).count();
                States.Delta(Logger, Builder, ServerTime);
                auto OutputLen = Builder.GetSize();
                auto ServerCommands = std::make_unique<std::vector<uint8_t>>(OutputLen);
                
                std::memcpy(ServerCommands->data(), Builder.GetBufferPointer(), OutputLen);
                Logger->Info("PostFrame: Sending Client {} SeqId {} Time: {} State size: {}", Network.UserId, States.SequenceId, ServerTime, static_cast<uint64_t>(OutputLen));
                
                auto CommandMessage = std::make_unique<Game::Message::GameMessage>(Network.UserId, std::move(ServerCommands), MessageData_EncServerCommand);
                OutQueue->Push(std::move(CommandMessage));
                Builder.Release();
                Thread->NotifyWrite();
            });

        GameWorld.observer<network::PMOServer, network::Connected>("OnServerConnected")
            .write<state::PlayerWorldStates>()
            .write<character::ActionType>()
            .event(flecs::OnAdd)
            .each([&](flecs::iter& It, size_t Index, network::PMOServer, network::Connected)
            {
                auto CurrentPlayer = It.entity(Index);
                Logger->Info("OnConnected called for {}", CurrentPlayer.id());
                units::Vector3 Position{0.f, 1.f, 0.f};
                GameWorld.get_mut<level::Level>()->AddUserToLevel(GameWorld, CurrentPlayer, Position);
            });
    }
}
#endif