#ifndef IS_SERVER
#include "network_client.h"
#include "ecs/units.h"
#include "ecs/level.h"
#include "ecs/character/pmocharacter.h"
#include "ecs/character/components.h"
#include "ecs/network/network.h"

using namespace Game::Message;

namespace network
{
    NetworkClient::NetworkClient(flecs::world &GameWorld) 
    {
        GameWorld.module<NetworkClient>();
    
        Components(GameWorld);
        Systems(GameWorld);
    }
    
    void NetworkClient::Components(flecs::world &GameWorld)
    {
        // Client components
        GameWorld.component<PMOClient>();
        GameWorld.component<Inputs>();

        HeartbeatQuery = GameWorld.query<network::User, network::PMOClient, network::Connected>();
    }
    
    void NetworkClient::Systems(flecs::world &GameWorld)
    {
        // Events & Observers //
        GameWorld.observer<network::PMOClient, network::Connected>("OnClientConnected")
            .event(flecs::OnAdd)
            .each([&](flecs::iter& It, size_t Index, network::PMOClient, network::Connected)
            {
                // This Position data needs to come from server
                units::Vector3 Position{0.f, 1.f, 0.f};
                auto ConnectedPlayer = It.entity(Index);
                Logger->Info("OnConnected called for {}", ConnectedPlayer.id());
                GameWorld.get_mut<level::Level>()->AddUserToLevel(GameWorld, ConnectedPlayer, Position);
            });
        
        // systems
        GameWorld.system<network::User, network::PMOClient, network::Auth>("Authenticate")
            .kind(flecs::PreFrame)
            .interval(.150)
            .each([&](flecs::iter& It, size_t Index, network::User &Network, network::PMOClient &Client, network::Auth &Auth) 
            {
                if (Auth.RequestSent > 50)
                {
                    // TODO handle properly create a new internal message type
                    Logger->Warn("Auth failed too many times");
                    GameWorld.quit();
                    return;
                }       
                auto Now = std::chrono::system_clock::now();
                Logger->Info("Entity requires auth");
                Client.Client->Authenticate();
                Auth.RequestSent++;
                Auth.TimeSent = Now;
            });
       
        GameWorld.system<network::PMOClient>("ProcessNetworkQueue")
            //.immediate(true)
            .write<network::PhysicsDesynced>()
            .kind(flecs::PreFrame)
            .each([&](flecs::iter& It, size_t Index, network::PMOClient) 
            {
                auto PMOWorld = GameWorld.get_mut<world::PMOWorld>();
                auto Player = It.entity(Index);
                for (auto GameData = InQueue->Pop(); GameData != std::nullopt; GameData = InQueue->Pop())
                {
                    if (GameData->get()->InternalType != Game::Message::InternalMessage::Socket)
                    {
                        switch (GameData->get()->InternalType)
                        {
                            case Game::Message::InternalMessage::NewUser:
                            {
                                if (Player.has<network::Auth>())
                                {
                                    Player.add<network::Connected>()
                                        .add<network::ClientStateBuffer>()
                                        .add<network::ServerStateBuffer>()
                                        .add<physics::State>()
                                        .set<network::ServerStateSequenceId>({ 0 })
                                        .set<network::UserTime>({std::chrono::steady_clock::now()})
                                        .remove<network::Auth>();
                                    Logger->Info("PreFrame: We are now connected.");
                                }
                                break;
                            }
                            case Game::Message::InternalMessage::ReliableMsgFailed:
                            {
                                ProcessMessageFailed(GameWorld, *PMOWorld, *GameData->get());
                                break;
                            }
                            default:
                            {
                                Logger->Warn("PreFrame: Unknown message type returned");
                                break;
                            }
                        }
                    }
            
                    switch (GameData->get()->MessageType)
                    {
                        case Game::Message::MessageData_EncServerCommand:
                        {
                            auto Command = Game::Message::GetServerCommand(GameData->get()->Data->data());
                            if (!Command)
                            {
                                Logger->Warn("PreFrame: Failed to decode command from server!");
                                break;
                            }
            
                            Logger->Info("PreFrame: Got MessageData_EncServerCommand SeqId: {} ServerTime {}", Command->sequence_id(), Command->server_time());        
                            ProcessServerCommand(GameWorld, *PMOWorld, Command);
                            // only process one update per tick
                            if (Command->opcode() == Game::Message::ServerOpCode_Update)
                            {
                                Game::Message::ReleaseGameMessage(std::move(GameData.value()));
                                return;
                            }
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    Game::Message::ReleaseGameMessage(std::move(GameData.value()));
                }
                
            });
        
        // Only send respawn request if the user is dead and requested respawn
        // Remove combat::RequestRespawn
        GameWorld.system<network::User, network::PMOClient, network::Connected, character::IsDead, combat::RequestRespawn>("RequestRespawn")
        .kind(flecs::PostLoad)
        .each([&](flecs::iter& It, size_t Index, network::User &Network, network::PMOClient &Client, network::Connected, character::IsDead, combat::RequestRespawn &Request) 
        {
            // Already requested a respawn, exit early
            if (Request.RequestedTime > 0.f)
            {
                Request.RequestedTime += It.delta_time();
                return;
            }

            flatbuffers::FlatBufferBuilder Builder(128);
            ClientCommandBuilder CmdBuilder(Builder);
            CmdBuilder.add_opcode(Game::Message::ClientOpCode_RequestRespawn);
            CmdBuilder.add_server_seq_id_ack(0);
            Builder.Finish(CmdBuilder.Finish());

            auto OutputLen = Builder.GetSize();
            auto ClientCommands = std::make_unique<std::vector<uint8_t>>();
            ClientCommands->reserve(OutputLen);

            std::memcpy(ClientCommands->data(), Builder.GetBufferPointer(), Builder.GetSize());
            Builder.Release();

            auto RespawnRequest = std::make_unique<GameMessage>(Network.UserId, std::move(ClientCommands), Game::Message::InternalMessage::Socket);
            RespawnRequest->MessageReliability = Game::Message::Reliability::Reliable;
            RespawnRequest->OpCode = Game::Message::ClientOpCode_RequestRespawn;
            Client.OutQueue->Push(std::move(RespawnRequest));
            Logger->Info("RequestRespawn notifying write");
            Client.Client->NotifyWrite();
            Request.RequestedTime = 0.1f; // initialize it
        });

        // Send Player Outputs 
        GameWorld.system<network::User, network::UserTime, character::PMOCharacter, network::PMOClient, network::ClientStateBuffer, network::ServerStateBuffer, network::Connected>("SendPlayerInputs")
            .kind(flecs::PreUpdate)
            .immediate(true)
            .each([&](flecs::iter& It, size_t Index, network::User &Network, network::UserTime &Time, character::PMOCharacter &Character, network::PMOClient &Client, network::ClientStateBuffer &ClientState, network::ServerStateBuffer &ServerState, network::Connected) {

                flatbuffers::FlatBufferBuilder Builder(512);
                auto Inputs = ClientState.InputAt(ClientState.State.SequenceId);
                auto SendInputBuffer = Game::Message::CreateInputs(Builder, Inputs.KeyPresses.KeyPresses, Inputs.PitchAngle, Inputs.YawAngle);
                Logger->Info("OnUpdate: Sending to server Input: SeqId: {} Yaw: {} Pitch: {} KeyPress: {}",  ClientState.State.SequenceId, Inputs.YawAngle, Inputs.PitchAngle, Inputs.KeyPresses.KeyPresses);
                auto Now = std::chrono::steady_clock::now();
                auto UserTime = Now - Time.UserTime;
                auto ClientTime = std::chrono::duration_cast<std::chrono::milliseconds>(UserTime).count();

                ClientCommandBuilder CmdBuilder(Builder);
                Logger->Info("OnUpdate: Client sending ack for: {} Client ack: {} time {} Pos: {} {} {}", ServerState.State.SequenceId, ClientState.State.SequenceId, ClientTime, Character.GetPosition().vec3[0], Character.GetPosition().vec3[1], Character.GetPosition().vec3[2]);

                CmdBuilder.add_server_seq_id_ack(ServerState.State.SequenceId);
                CmdBuilder.add_client_time(ClientTime);
                CmdBuilder.add_inputs(SendInputBuffer.o);
                CmdBuilder.add_opcode(Game::Message::ClientOpCode_Input);
                auto SimulationLoc = Game::Message::Vec3(Character.GetPosition().vec3[0], Character.GetPosition().vec3[1], Character.GetPosition().vec3[2]);
                auto SimulationRot = Game::Message::Quat4(Character.GetRotation().vec4[0], Character.GetRotation().vec4[1], Character.GetRotation().vec4[2], Character.GetRotation().vec4[3]);
                CmdBuilder.add_sim_loc(&SimulationLoc);
                CmdBuilder.add_sim_rot(&SimulationRot);

                Builder.Finish(CmdBuilder.Finish());

                auto OutputLen = Builder.GetSize();
                auto ClientCommands = std::make_unique<std::vector<uint8_t>>();
                ClientCommands->resize(OutputLen);

                std::memcpy(ClientCommands->data(), Builder.GetBufferPointer(), Builder.GetSize());
                Builder.Release();
                
                auto CommandMessage = std::make_unique<Game::Message::GameMessage>(Network.UserId, std::move(ClientCommands), MessageData_EncClientCommand);
                Client.OutQueue->Push(std::move(CommandMessage));
            });
 
        // Tell Socket thread to process reliable packets
        GameWorld.system<network::User, network::PMOClient, network::Connected>("ProcessSocketReliable")
        .kind(flecs::PreUpdate)
        .each([&](flecs::iter& It, size_t Index, network::User &Network, network::PMOClient &Client, network::Connected &_) 
        {
            auto Process = std::make_unique<GameMessage>(Network.UserId, InternalMessage::ProcessReliableSocket);
            Process->Time = It.delta_time();
            Client.OutQueue->Push(std::move(Process));
        });

        // Notify network thread to send packets and increment our state to finalize this frame.
        GameWorld.system<network::ClientStateBuffer, network::PMOClient, network::Connected>("IncrementState")
            .kind(flecs::PostFrame)
            .each([&](flecs::iter& It, size_t Index, network::ClientStateBuffer &ClientState, network::PMOClient &Client, network::Connected)
            {
                Client.Client->NotifyWrite();
                ClientState.State.Inc();
            });
    }
    
    void NetworkClient::ExecQueries(flecs::world &GameWorld, uint32_t UserId, const Game::Message::ServerCommand *Command)
    {
        GameWorld.defer_begin();
        ExecHeartbeatQuery(GameWorld, UserId, Command);
        GameWorld.defer_end();
    }

    void NetworkClient::ProcessServerCommand(flecs::world &GameWorld, world::PMOWorld &PlayerWorld, const Game::Message::ServerCommand *Command)
    {
        switch (Command->opcode())
        {
            case Game::Message::ServerOpCode_Invalid:
            {
                Logger->Error("PreFrame: Server sent ServerOpCode_Invalid");
                break;
            }
            case Game::Message::ServerOpCode::ServerOpCode_Update:
            {
                Logger->Info("PreFrame: Server sent ServerOpCode_Update {} ServerTime: {}", Command->sequence_id(), Command->server_time());
                PlayerWorld.ProcessServerUpdate(Command);
                break;
            }
            case Game::Message::ServerOpCode::ServerOpCode_Respawn:
            {

            }
            case Game::Message::ServerOpCode::ServerOpCode_Equip:
            {
                
            }
            default:
            {
                Logger->Error("PreFrame: Unknown opcode: {}", static_cast<uint16_t>(Command->opcode()));
                break;
            }
        }
    }

    void NetworkClient::ProcessMessageFailed(flecs::world &GameWorld, world::PMOWorld &PlayerWorld, const Game::Message::GameMessage &Message)
    {
        if (Message.OpCode == Game::Message::ClientOpCode_RequestRespawn)
        {
            PlayerWorld.ProcessRespawnFailed(static_cast<Game::Message::ClientOpCode>(Message.OpCode));
        }
    }

    void NetworkClient::ExecHeartbeatQuery(flecs::world &GameWorld, const uint32_t UserId, const Game::Message::ServerCommand *Command)
    {
        HeartbeatQuery.each([&](flecs::entity Player, network::User &Network, network::PMOClient &Client, network::Connected &Connected) 
            {
                if (Network.UserId != UserId)
                {
                    return;
                }
                Player.set<network::Connected>({std::chrono::system_clock::now()});
            }
        );
    }
}        
#endif