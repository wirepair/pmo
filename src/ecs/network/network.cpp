#include "network.h"

namespace network
{
    Network::Network(flecs::world &GameWorld) 
    {
        GameWorld.module<Network>();
        // Shared components
        GameWorld.component<User>();
        GameWorld.component<Auth>();
        GameWorld.component<Inputs>();
        GameWorld.component<Connected>();

    }
    
    void ServerStateBuffer::CalculateSequenceDistance(uint8_t ClientSequence)
    {
        SequenceDistance = std::abs(State.SequenceId - ClientSequence - 24) % 24;
    }
}
