#pragma once

#include <flecs.h>
#include <Jolt/Jolt.h>
#include <chrono>

#include <array>

#if defined(_WIN32)
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <WinSock2.h>
#include <ws2def.h>
#include <Windows.h>
#else 
#include <netinet/in.h>
#endif

#include "ecs/units.h"
#include "input/input.h"
#include "net/listener.h"
#ifdef IS_SERVER
#include "net/server.h"
#else
#include "net/client.h"
#endif
#include "crypto/cryptor.h"
#include "logger/logger.h"
#include "schemas/client_generated.h"
#include "containers/historybuffer.h"
#include "schemas/messages.h"

using namespace std::chrono_literals;

namespace network
{
    
    struct HeartbeatTimedOut 
    {
        std::chrono::time_point<std::chrono::system_clock> At;
    };

    struct User 
    {
        uint32_t UserId;
    };

    struct UserTime 
    {
        std::chrono::time_point<std::chrono::steady_clock> UserTime;
    };

    struct Inputs
    {
        input::Input KeyPresses{};
        float YawAngle{};
        float PitchAngle{};
        //uint16_t YawAngle{};
        //uint16_t PitchAngle{};
        uint16_t SequenceId = 0;

        void Clear()
        {
            KeyPresses.Clear();
            // YawAngle = 0.f;
            // PitchAngle = 0.f;
        }
    };
    /**
     * @brief Where we desynced and need to start replaying from
     * 
     */
    struct PhysicsDesynced 
    {
        uint8_t SequenceId;
    };

    struct StateBuffer
    {
        // make this class non-copyable
        StateBuffer() = default;
        StateBuffer(const StateBuffer &) = delete;
        void operator =(const StateBuffer &) = delete;
        
        /**
         * @brief Move assignment
         * 
         * @param other flatc
         * @return StateBuffer& 
         */
        StateBuffer& operator=(StateBuffer&& other)
        {
            assert(this != &other);
            SequenceId = other.SequenceId;
            std::copy(std::begin(other.Position), std::end(other.Position), std::begin(Position));
            std::copy(std::begin(other.Velocity), std::end(other.Velocity), std::begin(Velocity));
            std::copy(std::begin(other.Rotation), std::end(other.Rotation), std::begin(Rotation));
            return *this;
        }
        
        /**
         * @brief Move constructor
         * 
         * @param other 
         * @return StateBuffer& 
         */
        StateBuffer(StateBuffer&& other) noexcept
        {
            assert(this != &other);
            SequenceId = other.SequenceId;
            std::copy(std::begin(other.Position), std::end(other.Position), std::begin(Position));
            std::copy(std::begin(other.Velocity), std::end(other.Velocity), std::begin(Velocity));
            std::copy(std::begin(other.Rotation), std::end(other.Rotation), std::begin(Rotation));
        }

        /**
         * @brief Increment our sequence id, rolling over every 24
         * 
         */
        inline void Inc()
        {
            SequenceId = (SequenceId + 1) % 24;
        }

        inline uint8_t LastSequenceId() const { return (SequenceId + 24 - 1) % 24; }
        
        void AddPosition(const units::Vector3 NewPosition) { Position.at(SequenceId % 24) = NewPosition; };
        void AddVelocity(const units::Velocity NewVelocity) { Velocity.at(SequenceId % 24) = NewVelocity; };
        void AddRotation(const units::Quat NewRotation) { Rotation.at(SequenceId % 24) = NewRotation; };
        void AddStates(const units::Vector3 NewPosition, const units::Velocity NewVelocity, const units::Quat NewRotation)
        {
            Position.at(SequenceId % 24) = NewPosition;
            Velocity.at(SequenceId % 24) = NewVelocity;
            Rotation.at(SequenceId % 24) = NewRotation;
        }

        void AddLastPosition() { Position.at(SequenceId % 24) = Position.at(LastSequenceId()); };
        void AddLastVelocity() { Velocity.at(SequenceId % 24) = Velocity.at(LastSequenceId()); };
        void AddLastRotation() { Rotation.at(SequenceId % 24) = Rotation.at(LastSequenceId()); };
        
        units::Vector3 PositionAt(const size_t Index) const { return Position.at(Index % 24);}
        units::Velocity VelocityAt(const size_t Index) const { return Velocity.at(Index % 24);}
        units::Quat RotationAt(const size_t Index) const { return Rotation.at(Index % 24);}

        void Print(logging::Logger &Log) const
        {
            for (size_t i = 0; i < 24; i++)
            {
                Log.Info("{}seq {}: Pos: {} {} {}\tRot {} {} {} {}\tVel: {} {} {}", (i == SequenceId) ? "*" : "", i, 
                    PositionAt(i).vec3[0], PositionAt(i).vec3[1], PositionAt(i).vec3[2],
                    RotationAt(i).vec4[0], RotationAt(i).vec4[1], RotationAt(i).vec4[2], RotationAt(i).vec4[3],
                    VelocityAt(i).vec3[0], VelocityAt(i).vec3[1], VelocityAt(i).vec3[2]
                );
            }
        }

        uint8_t SequenceId{};
        std::array<units::Vector3, 24> Position{};
        std::array<units::Velocity, 24> Velocity{};
        std::array<units::Quat, 24> Rotation{};
    };



    struct ClientStateBuffer
    {
        // This state is for validating against server state
        StateBuffer State{};
        
        // This state data is for save & replay
        std::array<network::Inputs, 24> Inputs{};
        std::array<JPH::Vec3, 24> DesiredVelocities{};

        void AddInput(const network::Inputs &Input)
        {
            auto Pos = State.SequenceId;
            Inputs.at(Pos) = Input;
        }

        network::Inputs InputAt(const size_t Index) const { return Inputs.at(Index % 24);}

        void AddDesiredVelocity(const JPH::Vec3 DesiredVelocity)
        {
            auto Pos = State.SequenceId;
            DesiredVelocities.at(Pos) = DesiredVelocity;
        }

        JPH::Vec3 DesiredVelocityAt(const size_t Index) const { return DesiredVelocities.at(Index % 24);}
    };

    /**
     * @brief This is what the server sees for the player as we need to possibly rollback physics
     * 
     */
    struct ServerStateBuffer
    {
        uint8_t SequenceDistance{}; // number of sequences the server is "off" by
        StateBuffer State{};

        void CalculateSequenceDistance(uint8_t ClientSequence);
    };
    
    struct ServerPosition
    {
        units::Vector3 Position;
    };

    /**
     * @brief This is what the server sees for the player as we need to possibly rollback physics
     * 
     */
    struct ServerRotation
    {
        units::Quat Rotation;
    };

    /**
     * @brief This is what the server sees for the player as we need to possibly rollback physics
     * 
     */
    struct ServerVelocity
    {
        units::Velocity Velocity;
    };

    /**
     * @brief Last state sequence ID seen from server
     * 
     */
    struct ServerStateSequenceId
    {
        uint8_t SequenceId;
    };

    // Tracks auth attempts for both client/server players
    struct Auth 
    {
        Auth() : AuthMessage(nullptr), 
                          RequestSent(0), 
                          TimeSent(std::chrono::time_point<std::chrono::system_clock>()) {};

        Auth(std::unique_ptr<std::vector<uint8_t>> Message) : AuthMessage(std::move(Message)), 
                                                              RequestSent(0), 
                                                              TimeSent(std::chrono::time_point<std::chrono::system_clock>()) {};

        std::unique_ptr<std::vector<uint8_t>> AuthMessage;
        uint16_t RequestSent = 0;
        std::chrono::time_point<std::chrono::system_clock> TimeSent;
    };

    // Signals we are connected
    struct Connected
    {
        std::chrono::time_point<std::chrono::system_clock> LastUpdate;
    };
    
    #ifndef IS_SERVER
    struct PMOClient 
    {
        std::shared_ptr<net::Client> Client;
        net::PacketQueueSharedPtr OutQueue;
    };
    #else
    struct PMOServer 
    {
        std::shared_ptr<net::Listener> Server;
        net::PacketQueueSharedPtr OutQueue;
    };
    #endif    
    /**
     * @brief Our Network module, registers common components
     * 
     */
    struct Network 
    {
        Network(flecs::world &GameWorld);
    };
}