#include "units.h"

namespace units
{
    Units::Units(flecs::world &GameWorld)
    {
        GameWorld.module<Units>();
        GameWorld.component<Vector3>();
        GameWorld.component<Velocity>();
        GameWorld.component<Quat>();
        GameWorld.component<Box>();
        GameWorld.component<TraitAttributes>();
        GameWorld.component<Attributes>();
    }
}