#pragma once

#include <flecs.h>
#include <string>
#include <cstdint>
#include <map>
#include <unordered_set>

#include "units.h"
#include "caller.h"
#include "combat/combat.h"
#include "physics/physics.h"
#include "logger/logger.h"

namespace level
{
    struct MapLevel 
    {
        const char *Name;
    };

    struct MapChunk
    {
        JPH::BodyID ChunkID;
    };

    struct LocalSensor
    {
        JPH::BodyID Sensor;
        std::unordered_set<flecs::entity, units::FlecsEntityHash> Characters;
    };

    struct MultiSensor
    {
        JPH::BodyID Sensor;
        std::unordered_set<flecs::entity, units::FlecsEntityHash> Characters;
    };
    
    struct Level : public JPH::ContactListener 
    {
        Level(flecs::world &GameWorld);

        // move assign
        Level& operator=(Level&& other)
        {
            assert(this != &other);

            Physics = std::move(other.Physics);
            
            ServerAreaOfInterestQuery = std::move(other.ServerAreaOfInterestQuery);
            CharacterQuery = std::move(other.CharacterQuery);
            CombatQuery = std::move(other.CombatQuery);
            Logger = std::move(other.Logger);
            Config = std::move(other.Config);
            LevelMode = other.LevelMode;
            return *this;
        }

        // move constructor
        Level(Level&& other) noexcept
        {
            Physics = std::move(other.Physics);
            
            ServerAreaOfInterestQuery = std::move(other.ServerAreaOfInterestQuery);
            CharacterQuery = std::move(other.CharacterQuery);
            CombatQuery = std::move(other.CombatQuery);
            Logger = std::move(other.Logger);
            Config = std::move(other.Config);
            LevelMode = other.LevelMode;
        }

        /**
         * @brief Init our level in the Physics world. Janky as we don't want to store a ref to GameWorld, but need to init area of interest with it
         * 
         * @param Log 
         * @param Conf 
         * @param Physics 
         */
        void Init(logging::Logger *Log, config::Config *Conf, flecs::world &GameWorld, physics::Physics *Physics, PMOCallerType Caller);
        
        /**
         * @brief Adds the user to our loaded level
         * 
         * @param GameWorld 
         * @param User 
         */
        void AddUserToLevel(flecs::world& GameWorld, flecs::entity &User, units::Vector3 &Position);

        /**
         * @brief Initialize the AoI system for servers (only)
         * 
         * @param GameWorld 
         */
        void InitAreaOfInterest(flecs::world& GameWorld);

        /**
         * @brief Systems applicable to server only
         * 
         * @param GameWorld 
         */
        void ServerLevelSystems(flecs::world& GameWorld);

        /**
         * @brief Called by the physics system by multiple threads
         * 
         * @param InBody1 
         * @param InBody2 
         * @param InManifold 
         * @param InIOSettings 
         */
        virtual void OnContactAdded(const Body &InBody1, const Body &InBody2, const ContactManifold &InManifold, ContactSettings &InIOSettings) override;

        /**
         * @brief Called by the physics system by multiple threads
         * 
         * @param InSubShapePair 
         */
	    virtual void OnContactRemoved(const SubShapeIDPair &InSubShapePair) override;

    private:
        flecs::query<MapChunk, LocalSensor, MultiSensor> ServerAreaOfInterestQuery;
        flecs::query<character::PMOCharacter> CharacterQuery;
        flecs::query<combat::ApplyDamage> CombatQuery;

        physics::Physics *Physics = nullptr;
        logging::Logger *Logger = nullptr;
        config::Config *Config = nullptr;
        PMOCallerType LevelMode;
    };
}