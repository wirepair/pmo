#pragma once

#include <flecs.h>
#include <Jolt/Jolt.h>
#include <Jolt/Physics/PhysicsSystem.h>

#include "ecs/physics/physics.h"
#include "ecs/network/network.h"
#include "ecs/character/network.h"
#include "config/config.h"
#include "logger/logger.h"
#include "schemas/server_generated.h"

namespace world
{
    /**
     * @brief Our world according to the client, manages server commands to update what the world looks like (entities, player and other characters)
     * 
     */
    struct PMOWorld
    {
        PMOWorld(flecs::world &GameWorld) : World(GameWorld)
        {
            GameWorld.module<PMOWorld>();
        }
    
        // move assign
        PMOWorld& operator=(PMOWorld&& other)
        {
            assert(this != &other);

            Player = std::move(other.Player);
            Log = std::move(other.Log);
            PhysicsSystem = std::move(other.PhysicsSystem);
            Map = std::move(other.Map);
            return *this;
        }

        // move constructor
        PMOWorld(PMOWorld&& other) noexcept : World(other.World)
        {
            Player = std::move(other.Player);
            Log = std::move(other.Log);
            PhysicsSystem = std::move(other.PhysicsSystem);
            Map = std::move(other.Map);
        }
        
        /**
         * @brief Initialize the PMOWorld
         * 
         * @param GameWorld
         * @param MapName 
         * @param ClientPlayer 
         * @param Logger 
         */
        void Init(const char *MapName, flecs::entity ClientPlayer, logging::Logger *Logger);
    
        /**
         * @brief Process World state updates
         * 
         * @param Command 
         */
        void ProcessServerUpdate(const Game::Message::ServerCommand *Command);

        /**
         * @brief Process a failed respawn message.
         * 
         * @param OpCode 
         */
        void ProcessRespawnFailed(const Game::Message::ClientOpCode OpCode);

        /**
         * @brief Adds a network player that has not been seen/is in our state
         * 
         * @param Entity 
         */
        void AddNetworkPlayer(const Game::Message::Entity *Entity);

        /**
         * @brief Updates a known network player
         * 
         * @param Entity 
         */
        void UpdateNetworkPlayer(const Game::Message::Entity *Entity);

        /**
         * @brief Updates the current (client) players data with the latest state
         * 
         * @param Entity 
         * @param SequenceId
         */
        void UpdateCurrentPlayer(const Game::Message::State *State, const uint8_t SequenceId, const uint64_t ServerTime);

        flecs::world &World;
        flecs::entity Player;
        logging::Logger *Log;
        JPH::PhysicsSystem *PhysicsSystem;
        flecs::entity Map;
    };
}