#pragma once

#ifdef IS_SERVER
#include <flecs.h>
#include "caller.h"

#include "net/listener.h"
#include "logger/logger.h"
#include "config/config.h"

/**
 * @brief InitPMOServer initializes the server
 * 
 * @param GameWorld 
 * @param Log 
 * @param Config 
 * @return true 
 * @return false 
 */
bool InitPMOServer(flecs::world &GameWorld, logging::Logger &Log, config::Config &Config);

/**
 * @brief DeinitPMO shuts down our world (threads, physics and flecs)
 * 
 * @param GameWorld 
 */
void DeinitPMO(flecs::world &GameWorld);
#endif
