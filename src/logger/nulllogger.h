#pragma once 

#include "logger.h"

namespace logging
{
    class NullLogger : public Logger
    {
    public:
        NullLogger() {};

    private:
        virtual void LogDebug(const std::string &Msg) override {}; 
        
        virtual void LogInfo(const std::string &Msg) override {};
        
        virtual void LogWarn(const std::string &Msg) override {};

        virtual void LogError(const std::string &Msg) override {};
    };
}
