#pragma once

#include "logger.h"
#include "spdlog/spdlog.h"


namespace logging
{
    class SpdLogger : public Logger
    {
    public:
        SpdLogger(const char *LogName, const char *LogFilePath);

    private:
        virtual void LogDebug(const std::string &Msg) override;
        
        virtual void LogInfo(const std::string &Msg) override;
        
        virtual void LogWarn(const std::string &Msg) override;

        virtual void LogError(const std::string &Msg) override;

        std::shared_ptr<spdlog::logger> Logger;
    };
}