#include "spdlogger.h"
#include "spdlog/sinks/basic_file_sink.h"
namespace logging
{
    SpdLogger::SpdLogger(const char *LogName, const char *LogFilePath)
    {
        Logger = spdlog::basic_logger_mt(LogName, LogFilePath, true);
        spdlog::set_level(spdlog::level::debug);
        spdlog::flush_on(spdlog::level::info);
        spdlog::set_default_logger(Logger);
    }

    void SpdLogger::LogDebug(const std::string &Msg)
    {
        Logger->debug(Msg);
    }

    void SpdLogger::LogInfo(const std::string &Msg)
    {
        Logger->info(Msg);
    }

    void SpdLogger::LogWarn(const std::string &Msg) 
    {
        Logger->warn(Msg);
    }

    void SpdLogger::LogError(const std::string &Msg)
    {
        Logger->error(Msg);
    }
}
