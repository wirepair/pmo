#pragma once

#include <format>
#include <string>

namespace logging
{
    class Logger
    {
    public:
        template<typename ...Args>
        void Debug(const char* Format, Args && ...AArgs)
        {
            LogDebug(LogFormat(Format, std::forward<Args>(AArgs)...));
        }

        template<typename ...Args>
        void Info(const char* Format, Args && ...AArgs)
        {
            LogInfo(LogFormat(Format, std::forward<Args>(AArgs)...));
        }

        template<typename ...Args>
        void Warn(const char* Format, Args && ...AArgs)
        {
            LogWarn(LogFormat(Format, std::forward<Args>(AArgs)...));
        }

        template<typename ...Args>
        void Error(const char* Format, Args && ...AArgs)
        {
            LogError(LogFormat(Format, std::forward<Args>(AArgs)...));
        }

    private:
        template<typename ...Args>
        inline std::string LogFormat(const char *Format, Args && ...AArgs)
        {
            return std::vformat(Format, std::make_format_args(AArgs...));
        }

        virtual void LogDebug(const std::string &) = 0;
        
        virtual void LogInfo(const std::string &) = 0;
        
        virtual void LogWarn(const std::string &) = 0;

        virtual void LogError(const std::string &) = 0;
    };
}