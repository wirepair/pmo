#pragma once

#ifndef JPH_DEBUG_RENDERER
#define JPH_DEBUG_RENDERER 
#endif

#include "debugrender.h"

#include <Jolt/Jolt.h>
#include <Jolt/Renderer/DebugRenderer.h>
#include <Jolt/Renderer/DebugRendererSimple.h>

namespace debug 
{
    class JoltDebugRenderer : public JPH::DebugRendererSimple
    {

    public:
        JoltDebugRenderer(debug::DebugRenderer *InRenderer) : Renderer(InRenderer)
        {
            this->Initialize();
        }

        virtual void DrawLine(JPH::RVec3Arg InFrom, JPH::RVec3Arg InTo, JPH::ColorArg InColor) override
        {
            // Implement
            debug::DebugVec3 From{InFrom.GetX(), InFrom.GetY(), InFrom.GetZ()};
            debug::DebugVec3 To{InTo.GetX(), InTo.GetY(), InTo.GetZ()};
            Renderer->DrawLine(From, To, InColor.GetUInt32());
        }

        virtual void DrawTriangle(JPH::RVec3Arg InVec1, JPH::RVec3Arg InVec2, JPH::RVec3Arg InVec3, JPH::ColorArg InColor, ECastShadow inCastShadow) override
        {
            debug::DebugVec3 Vec1{InVec1.GetX(), InVec1.GetY(), InVec1.GetZ()};
            debug::DebugVec3 Vec2{InVec2.GetX(), InVec2.GetY(), InVec2.GetZ()};
            debug::DebugVec3 Vec3{InVec3.GetX(), InVec3.GetY(), InVec3.GetZ()};
            bool CastShadow = inCastShadow == ECastShadow::On ? true : false;

            Renderer->DrawTriangle(Vec1, Vec2, Vec3, InColor.GetUInt32(), CastShadow);
        }

        virtual void DrawText3D(JPH::RVec3Arg,const std::string_view &,JPH::ColorArg,float) override { }

        private:
            debug::DebugRenderer *Renderer;
    };
}