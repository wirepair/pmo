#pragma once

#include <cstdint>

namespace debug 
{
    struct DebugVec3
    {
        DebugVec3(float InX, float InY, float InZ) : X(InX), Y(InY), Z(InZ) {};
        float X = 0;
        float Y = 0;
        float Z = 0;
    };

    class DebugRenderer
    {
    public:
        virtual ~DebugRenderer() = default;
        
        virtual void DrawLine(DebugVec3 &From, DebugVec3 &To, uint32_t InColor) = 0;

        virtual void DrawTriangle(DebugVec3 &Vec1, DebugVec3 &Vec2, DebugVec3 &Vec3, uint32_t InColor, bool CastShadow) = 0;
    };
}
