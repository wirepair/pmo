#pragma once

#if defined(_WIN32)
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <WinSock2.h>
#else 
#include <netinet/in.h>
#endif

#include <cstdint>
#include <iostream>
#include <vector>
#include <memory>
#include <cstring>

#include "messages_generated.h"

namespace Game
{
    namespace Message
    {
        enum InternalMessage : uint16_t 
        {
            Socket,
            ProcessReliableSocket,
            NewUser,
            RemoveUser,
            ReliableMsgFailed,
        };

        enum Reliability : uint8_t 
        {
            Unreliable,
            Reliable,
        };
        /**
         * @brief Used for passing messages between socket thread/main thread. Assumes MessageData is serialized flatbuffer data
         * that is not yet encrypted.
         * 
         */
        struct GameMessage
        {
            GameMessage() {};
            
            /**
             * @brief Construct a new Game Message object, defaults to InternalType of Socket
             * 
             * @param UserId 
             * @param Message 
             * @param MessageType 
             */
            GameMessage(const uint32_t UserId, const uint32_t SequenceId, std::unique_ptr<std::vector<uint8_t>> Message, const MessageData MessageType) : 
                UserId(UserId), SequenceId(SequenceId), Data(std::move(Message)), MessageType(MessageType), InternalType(Socket) {};

            /**
             * @brief Construct a new Game Message object without sequenceId, defaults to InternalType of Socket
             * 
             * @param UserId 
             * @param Message 
             * @param MessageType 
             */
            GameMessage(const uint32_t UserId, std::unique_ptr<std::vector<uint8_t>> Message, const MessageData MessageType) : 
                UserId(UserId), Data(std::move(Message)), MessageType(MessageType), InternalType(Socket) {};

            /**
             * @brief Construct a new Game Message object with data, requires InternalMessage Type
             * 
             * @param UserId 
             * @param Message 
             * @param MessageType 
             * @param Type 
             */
            GameMessage(const uint32_t UserId, std::unique_ptr<std::vector<uint8_t>> Message, const InternalMessage Type) : 
                UserId(UserId), Data(std::move(Message)), InternalType(Type) {};
            
            /**
             * @brief Construct a new Game Message object that is used for IPC between socket threads and main threads
             * 
             * @param UserId 
             * @param Type 
             */
            GameMessage(const uint32_t UserId, const InternalMessage Type) : UserId(UserId), InternalType(Type) {};

            /**
             * @brief Construct a new Game Message object that is used for IPC between socket threads and main threads with opcodes
             * 
             * @param UserId 
             * @param Type 
             */
            GameMessage(const uint32_t UserId, const InternalMessage Type, const uint16_t OpCode) : UserId(UserId), InternalType(Type), OpCode(OpCode) {};

            ~GameMessage()
            {
                Data = nullptr;
            }
            
            uint32_t UserId;
            uint16_t SequenceId;
            double Time;
            std::unique_ptr<std::vector<uint8_t>> Data = nullptr;
            MessageData MessageType;
            InternalMessage InternalType;
            uint16_t OpCode{};
            Reliability MessageReliability = Unreliable;
        };

        /**
         * @brief Necessary for releasing objects that crossed the DLL boundary.
         * If we don't release what we allocated in the pmo_library here, we get heap corruption.
         * 
         * @param Message 
         */
        static inline void ReleaseGameMessage(std::unique_ptr<GameMessage> Message)
        {
            Message = nullptr;
        }
    }
}