#include "reliable_peer.h"
#include <optional>
#include <float.h>

namespace net 
{
    void ReliablePeer::SendPacket(const Reliability ChannelId, logging::Logger &Log, crypto::Cryptor &Crypto, Game::Message::GameMessage &PacketMessage) const
    {
        Endpoints.at(ChannelId)->SendPacket(Log, Crypto, PacketMessage);
    }

    bool ReliablePeer::ReceivePacket(const Reliability ChannelId, logging::Logger &Log, crypto::Cryptor &Crypto, const Message &IncomingMessage, std::vector<unsigned char> &ReassembledData) const
    {
        return Endpoints.at(ChannelId)->ReceivePacket(Log, Crypto, IncomingMessage, ReassembledData);
    }

    int ReliablePeer::ResendReliablePackets(logging::Logger &Log, crypto::Cryptor &Crypto, net::PacketQueue &InboundQueue, const double Time) const
    {
        int Resent = 0;
        int TotalAcks = 0;
        ReliableEndpoint &Endpoint = *Endpoints.at(Reliability::Reliable).get();
        Endpoint.Update(Time);
        auto Acks = Endpoint.GetAcks(TotalAcks);
        if (TotalAcks > 0)
        {
            for (auto Ack : Acks)
            {
                if (Ack == 0)
                {
                    continue;
                }
                ProcessAck(Endpoint, Ack);
            }
        }
        
        Endpoint.ClearAcks();

        float RTT = Endpoint.GetRTT();

        // We can now re-send any un-ack'd packets
        auto Data = Endpoint.GetSentPackets()->GetEntries();
        for (auto &&Message : *Data)
        {
            // Juuuuust in case.
            if (!Message || Message->Acked)
            {
                continue;
            }

            // Don't resend if we are below the peers RTT time.
            auto TimeDiff = Time - Message->Time;
            if (TimeDiff < RTT) // may want to reduce this a bit so we more aggressively retry? Need to test later
            {
                continue;
            }
            
            // Delete this message if we are above the resend threshold.
            if (TimeDiff > DefaultConfig.ResendThreshold)
            {
                NotifyMessageFailed(*Message, InboundQueue);
                Endpoint.GetSentPackets()->RemoveWithCleanup(Message->Sequence);
               
                //InboundQueue.Push();
                continue;
            }

            if (Message->MessageLen > static_cast<size_t>(DefaultConfig.FragmentAbove))
            {
                Endpoint.SendFragmentedMessage(Log, Crypto, Message.get());
                Resent++;
            }
            else
            {
                Endpoint.SendMessage(Log, Crypto, Message.get());
                Resent++;
            }
        }

        return Resent;
    }

 
    void ReliablePeer::NotifyMessageFailed(net::ReliableMessage &Message, net::PacketQueue &InboundQueue) const
    {
        InboundQueue.Push(std::make_unique<GameMessage>(UserId, ReliableMsgFailed, Message.OpCode));
    }

    void ReliablePeer::Update(const double Time)
    {
        for (auto && Endpoint : Endpoints)
        {
            Endpoint->Update(Time);
        }
    }

    void ReliablePeer::ProcessAck(ReliableEndpoint &Endpoint, uint16_t Ack) const
    {
        auto SentPackets = Endpoint.GetSentPackets();
        auto Packet = SentPackets->Find(Ack);
        if (!Packet || !Packet->Acked)
        {
            return;
        }
        SentPackets->RemoveWithCleanup(Packet->Sequence);
    }

    ReliableEndpoint* ReliablePeer::GetEndpointChannel(const Reliability ChannelId) noexcept
    {
        if (ChannelId != Reliability::Reliable && ChannelId != Reliability::Unreliable)
        {
            return nullptr;
        }
        return Endpoints.at(ChannelId).get();
    }

    std::array<std::unique_ptr<ReliableEndpoint>, 2>* ReliablePeer::GetEndpoints() noexcept
    { 
        return &Endpoints;
    }
}