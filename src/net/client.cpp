
#include "client.h"
#if defined(_WIN32)
#include <WinSock2.h>
#else
#include <sys/epoll.h>
#endif



#include "crypto/mockauthserver.h"


using namespace Game::Message;

namespace net
{
    bool Client::Start()
    {
        auto Ret = Crypto->GenerateKeyEncryptingKey(KeyPair);
        if (!Ret)
        {
            Logger.Info("Client Start failed, Crypt failed");
            return false;
        }
        
        NetworkThread = std::thread(&Client::Init, this);
        return true;
    }

    void Client::Init()
    {
        auto Ret = ListenerSocket->Connect(); 
        if (Ret != 0)
        {
            Logger.Error("exiting due to Listen failure {}", Ret);
            return;
        }
        Logger.Info("Client Init");

        Listen();
    }

    void Client::ProcessMessage(Game::Message::GameMessage &Message)
    {
        switch(Message.InternalType)
        {
            case Game::Message::InternalMessage::Socket:
            {
                Logger.Info("Sending outbound packet");
                auto Endpoint = GetEndpoint(Message.MessageReliability);
                Endpoint->SendPacket(Logger, *Crypto, Message);
                break;
            }
            case Game::Message::InternalMessage::ProcessReliableSocket:
            {
                auto Resent = Peer->ResendReliablePackets(Logger, *Crypto, *InboundMessageQueue, Message.Time);
                if (Resent > 0)
                {
                    Logger.Info("Resent {} packets", Resent);
                }
            }
            default:
            {
                break;
            }
        }
    }

    void Client::ProcessPacket(std::array<unsigned char, PMO_BUFFER_SIZE> &PacketBuffer, const int PacketLen, struct sockaddr_in &ServerAddr)
    {
        auto Deserialized = GetMessage(PacketBuffer.data());
        if (!Deserialized)
        {
            Logger.Error("Unable to process packet len: {0}\n", PacketLen);
            return;
        }

        auto UserId = Deserialized->user_id();
        const auto MessageType = Deserialized->message_data_type();
        const auto ChannelId = Deserialized->channel_id();
        //Logger.Debug("Processing: {}", Game::Message::EnumNameMessageData(MessageType));
        switch (MessageType)
        {
            // Creates ServerEndpoints with our new encryption key
            case MessageData_GenKeyResponse:
            {
                Logger.Info("Got MessageData_GenKeyResponse packet");
                auto Message = Deserialized->message_data_as_GenKeyResponse();
                auto UserKey = std::make_shared<std::array<uint8_t, PMO_KEY_SIZE>>();

                auto Success = Crypto->DecryptKey(Deserialized->nonce()->data(), Message->encryption_key()->data(), Crypto->TestGameServerPubKey.data(), KeyPair.PrivateKey.data(), *UserKey.get());
                if (!Success)
                {
                    Logger.Error("Failed to decrypt key"); 
                    return;
                }
                Logger.Info("Got server GenKeyResponse");

                Peer = std::make_unique<ReliablePeer>(UserId, ListenerSocket, ListenerSocket->SocketAddr(), UserKey);
                // Signal we are logged in
                auto LoginMessage = std::make_unique<Game::Message::GameMessage>(UserId, Game::Message::InternalMessage::NewUser);
                InboundMessageQueue->Push(std::move(LoginMessage));
                break;
            }
            case MessageData_EncServerCommand:
            {
                auto Output = std::make_unique<std::vector<unsigned char>>(DefaultConfig.FragmentAbove);
                if (!Peer)
                {
                    Logger.Error("Saw EncServerCommand before GenKeyResponse!!");
                    return;
                }
                
                auto Endpoint = Peer->GetEndpointChannel(static_cast<Reliability>(ChannelId));
                // we are only ready if the packet was not fragmented, or we've collected all fragments at this point
                bool Ready = Endpoint->ReceivePacket(Logger, *Crypto, *Deserialized, *Output);
                
                if (Ready)
                {
                    auto EncryptedMessage = std::make_unique<Game::Message::GameMessage>(UserId, Deserialized->sequence_id(), std::move(Output), MessageType);
                    InboundMessageQueue->Push(std::move(EncryptedMessage));
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    // TODO: Move to OutboundPacketQueue so it's processed on socket thread
    void Client::Authenticate()
    {
        assert(UserId != 0);
                
        unsigned long long OutputLen{0};
    
        auto GenKeyRequest = crypto::FakeAuthServerFlat(UserId, KeyPair.PublicKey.data(), OutputLen);
        ListenerSocket->Send(GenKeyRequest->data(), OutputLen);
    }
}