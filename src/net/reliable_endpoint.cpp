#include "reliable_endpoint.h"
#include <optional>
#include <float.h>
#include <cmath>

namespace net
{
    void ReliableEndpoint::SendPacket(logging::Logger &Log, crypto::Cryptor &Crypto, Game::Message::GameMessage &PacketMessage)
    {
        const auto PacketLen = PacketMessage.Data->size();
        const auto PacketData = PacketMessage.Data->data();
        const auto MessageType = PacketMessage.MessageType;

        if (PacketLen > static_cast<size_t>(Config.MaxPacketSize))
        {
            Counters[EndpointNumPacketsTooLargeToSend]++;
            return;
        }

        auto CurrentSequence = Sequence++;
        auto Message = SentPackets.Insert(CurrentSequence);
        if (!Message)
        {
            Counters[EndpointNumPacketsInvalid]++;
            return;
        }
        
        Message->Reset();

        Message->Sequence = CurrentSequence;
        Message->MessageType = MessageType;
        Message->OpCode = PacketMessage.OpCode; // only used to notify if failed
        Message->MessageLen = PacketLen;
        Message->MessageData->resize(PacketLen);
        std::memcpy(Message->MessageData->data(), PacketData, PacketLen);
        Message->Time = Time;
        Message->Acked = false;

        ReceivedPackets.GenerateAckBits(Message->Ack, Message->AckBits);
        // Update Ack if we wrapped
        int SequenceDifference = Message->Sequence - Message->Ack;
        if ( SequenceDifference < 0 )
        {
            SequenceDifference += 65536;
        }

        if ( SequenceDifference <= 255 )
        {
            Message->SequenceWrapped = true;
            Message->Ack = (uint16_t)SequenceDifference;
        }

        // Send in fragments if too big
        if (PacketLen >= static_cast<size_t>(Config.FragmentAbove))
        {
            SendFragmentedMessage(Log, Crypto, Message);
        }
        else
        {
            SendMessage(Log, Crypto, Message);
        }

        Counters[EndpointNumPacketsSent]++;
    }
    
    // TODO: consider storing the encrypted data/nonce so we only have to encrypt once
    void ReliableEndpoint::SendMessage(logging::Logger &Log, crypto::Cryptor &Crypto, net::ReliableMessage *Message)
    {
        std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
        std::vector<unsigned char> Encrypted(Message->MessageLen+Crypto.AuthByteSize());

        auto Ret = Crypto.Encrypt(UserId, *UserKey(), Nonce, Message->MessageData->data(), Encrypted, Message->MessageLen);
        if (!Ret)
        {
            Log.Warn("[{}] failed to encrypt message {}\n", UserId, Game::Message::EnumNameMessageData(Message->MessageType));
            return;
        }
        
        flatbuffers::FlatBufferBuilder Builder(Config.FragmentAbove);
        auto EncryptedBuffer = Builder.CreateVector(Encrypted.data(), Encrypted.size());

        flatbuffers::Offset<> Data;
        BuildEncryptedField(Builder, EncryptedBuffer, Data, Message->MessageType);

        auto NonceValue = Builder.CreateVector(Nonce.data(), PMO_NONCE_BYTES);
        // Build the Reliable Message Header and apply our encrypted message type
        Game::Message::MessageBuilder GameMessage(Builder);
        GameMessage.add_user_id(UserId);
        if (Message->SequenceWrapped)
        {
            GameMessage.add_sequence_wrapped(1);
        }
        GameMessage.add_ack(Message->Ack);
        GameMessage.add_channel_id(ChannelId);
        GameMessage.add_ack_bits(Message->AckBits);
        GameMessage.add_sequence_id(Message->Sequence);
        GameMessage.add_time(Message->Time);
        GameMessage.add_message_len(Message->MessageLen);
        GameMessage.add_nonce(NonceValue);
        GameMessage.add_message_data(Data);
        GameMessage.add_message_data_type(Message->MessageType);
        
        Builder.Finish(GameMessage.Finish());

        Sock->SendTo(Addr, Builder.GetBufferPointer(), Builder.GetSize());
        Log.Info("ReliableEndpoint::SendMessage {} sent seq {} of type: {}", UserId, Message->Sequence, Game::Message::EnumNameMessageData(Message->MessageType));
    }

    void ReliableEndpoint::SendFragmentedMessage(logging::Logger &Log, crypto::Cryptor &Crypto, net::ReliableMessage *Message)
    {
        const int NumPackets = ( Message->MessageLen / Config.FragmentSize ) + ((Message->MessageLen % Config.FragmentSize) != 0 ? 1 : 0);
        int Offset = 0;
        for (int FragmentId = 0; FragmentId < NumPackets; ++FragmentId)
        {
            std::vector<unsigned char> Encrypted;
            std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};

            auto FragmentSize = Config.FragmentSize;
            // Only the last packet will be different in size
            if (FragmentId == NumPackets-1)
            {
                FragmentSize = Message->MessageLen % Config.FragmentSize;
            }
            const uint8_t *FragmentData = Message->MessageData->data()+Offset;
            
            // Encrypt the fragment
            Encrypted.resize(FragmentSize+Crypto.AuthByteSize());
            auto Ret = Crypto.Encrypt(UserId, *UserKey(), Nonce, FragmentData, Encrypted, FragmentSize);
            if (!Ret)
            {
                Log.Warn("failed to encrypt message fragment ({}/{}) {}\n", FragmentId, NumPackets, Game::Message::EnumNameMessageData(Message->MessageType));
                return;
            }

            // Update the offset
            Offset += FragmentSize;

            flatbuffers::FlatBufferBuilder Builder(Config.FragmentAbove);
            auto EncryptedBuffer = Builder.CreateVector(Encrypted.data(), Encrypted.size());
            
            flatbuffers::Offset<> Data;
            BuildEncryptedField(Builder, EncryptedBuffer, Data, Message->MessageType);

            auto NonceValue = Builder.CreateVector(Nonce.data(), PMO_NONCE_BYTES);
            // Build the Reliable Message Header and apply our encrypted message type
            Game::Message::MessageBuilder GameMessage(Builder);
            GameMessage.add_user_id(UserId);
            GameMessage.add_channel_id(ChannelId);
            GameMessage.add_ack(Message->Ack);
            GameMessage.add_ack_bits(Message->AckBits);
            GameMessage.add_sequence_id(Message->Sequence);
            if (Message->SequenceWrapped)
            {
                GameMessage.add_sequence_wrapped(1);
            }
            GameMessage.add_fragment_id(FragmentId);
            GameMessage.add_num_fragments(NumPackets-1);
            if (FragmentId == 0)
            {
                GameMessage.add_message_len(Message->MessageLen);
            }
            GameMessage.add_time(Message->Time);
            GameMessage.add_nonce(NonceValue);
            
            GameMessage.add_message_data(Data);
            GameMessage.add_message_data_type(Message->MessageType);
            
            Builder.Finish(GameMessage.Finish());

            Sock->Send(Builder.GetBufferPointer(), Builder.GetSize());
            Counters[EndpointNumPacketsFragmentsSent]++;
            Log.Info("ReliableEndpoint::SendFragmentedMessage {} sent seq {} of type: {}", UserId, Message->Sequence, Game::Message::EnumNameMessageData(Message->MessageType));
        }
    }

    void ReliableEndpoint::BuildEncryptedField(flatbuffers::FlatBufferBuilder &Builder, flatbuffers::Offset<flatbuffers::Vector<uint8_t>> &EncryptedBuffer, flatbuffers::Offset<> &Data, const Game::Message::MessageData MessageType)
    {
        switch (MessageType)
        {
            case Game::Message::MessageData_EncClientCommand:
            {
                auto EncryptedData = Game::Message::CreateEncClientCommand(Builder, EncryptedBuffer);
                Data = EncryptedData.Union();
                break;
            }
            case Game::Message::MessageData_EncServerCommand:
            {
                auto EncryptedData = Game::Message::CreateEncServerCommand(Builder, EncryptedBuffer);
                Data = EncryptedData.Union();
                break;
            }
            default:
                Data = flatbuffers::Offset<>();
        }
    }

    void ReliableEndpoint::ReadAck(const Game::Message::Message &Message, uint16_t &Ack, uint32_t &AckBits)
    {
        if (Message.sequence_wrapped() == 1)
        {
            uint8_t SequenceDifference = Message.ack(); 
            Ack = Message.sequence_id() - SequenceDifference;
        }
        else
        {
            Ack = Message.ack();
        }
    
        AckBits = Message.ack_bits();
    }

    
    bool ReliableEndpoint::ReceivePacket(logging::Logger &Log, crypto::Cryptor &Crypto, const Game::Message::Message &IncomingMessage, std::vector<unsigned char> &ReassembledData)
    {
        auto MessageType = IncomingMessage.message_data_type();
        
        const auto Enc = [&MessageType, &IncomingMessage]() -> const flatbuffers::Vector<uint8_t> * 
        {
            switch (MessageType) {
                case Game::Message::MessageData_EncClientCommand:
                    return IncomingMessage.message_data_as_EncClientCommand()->encrypted();    
                case Game::Message::MessageData_EncServerCommand:
                    return IncomingMessage.message_data_as_EncServerCommand()->encrypted();
                default:
                    return nullptr;
            }
        }();
        
        if (!Enc)
        {
            Log.Warn("[{}] Unknown message type: {}", UserId, Game::Message::EnumNameMessageData(MessageType));
            return false;
        }

        const auto Nonce = IncomingMessage.nonce();
        const auto Size = Enc->size();
        const uint16_t IncomingSequence = IncomingMessage.sequence_id();
        
        unsigned long long OutputLen{0};

        //Log.Debug("[{}] Decrypt Incoming Message Size: {}", UserId, Size);
        auto Success = Crypto.Decrypt(UserId, *UserKey(), Nonce->data(), Enc->data(), Size, ReassembledData, OutputLen);
        if (!Success)
        {
            Log.Warn("[{}] Failed to decrypt message {}", UserId, Game::Message::EnumNameMessageData(MessageType));
            return false;
        }

        if (OutputLen > static_cast<unsigned long long>(Config.MaxPacketSize))
        {
            Log.Warn("[{}] Packet too large ({}) maximum allowed is ({})", UserId, OutputLen, Config.MaxPacketSize);
            Counters[EndpointNumPacketsTooLargeToRecieve]++;
            return false;
        }

        if (IncomingMessage.num_fragments() == 0)
        {
            // regular packet
            Counters[EndpointNumPacketsReceived]++;
            uint16_t Ack = 0;
            uint32_t AckBits = 0;
            ReadAck(IncomingMessage, Ack, AckBits);
            
            if (!ReceivedPackets.TestInsert(IncomingSequence))
            {
                Log.Warn("[{}] ignoring stale packet, id: {}", UserId, IncomingSequence);
                Counters[EndpointNumPacketsStale]++;
                return false;
            }

            auto ReceivedMessage = ReceivedPackets.Insert(IncomingSequence);
            FragmentReassembly.AdvanceWithCleanup(IncomingSequence);

            ReceivedMessage->Time = Time;
            ReceivedMessage->MessageLen = OutputLen;

            for (int i = 0; i < 32; ++i)
            {
                if (AckBits & 1)
                {
                    uint16_t AckSequence = Ack - ((uint16_t)i);
                    auto SentPacketData = SentPackets.Find(AckSequence);
                    if ( SentPacketData && !SentPacketData->Acked && NumAcks < Config.AckBufferSize)
                    {
                        //Log.Info("[{}] acked packet {}", UserId, AckSequence);
                        Acks.at(NumAcks++) = AckSequence;
                        Counters.at(EndpointNumPacketsAcked)++;
                        SentPacketData->Acked = true;

                        float RoundTripTime = (float)(this->Time - SentPacketData->Time) * 1000.f;
                        if ( (this->Rtt == 0.f && RoundTripTime > 0.f) || std::fabs(this->Rtt - RoundTripTime) < 0.00001)
                        {
                            this->Rtt = RoundTripTime;
                        }
                        else
                        {
                            this->Rtt += (RoundTripTime - this->Rtt) * Config.RTTSmoothingFactor;
                        }
                    }
                }
                AckBits >>= 1;
            }
            return true;
        }
        else
        {
            // fragmented packet           
            uint16_t Ack = 0;
            uint32_t AckBits = 0;
            const uint8_t NumFragments = IncomingMessage.num_fragments();
            const auto FragmentId = IncomingMessage.fragment_id();

            if (NumFragments > Config.MaxFragments)
            {
                Log.Warn("[{}] number of fragments {} are out side of range of max {}: {}", UserId, NumFragments, Config.MaxFragments, Game::Message::EnumNameMessageData(MessageType));
                Counters.at(EndpointNumPacketsFragmentsInvalid)++;
                return false;
            }

            if (FragmentId > NumFragments)
            {
                Log.Warn("[{}] fragment id {} is out side of range of num fragments: {}", UserId, IncomingMessage.fragment_id(), NumFragments, Game::Message::EnumNameMessageData(MessageType));
                Counters.at(EndpointNumPacketsFragmentsInvalid)++;
                return false;
            }

            ReadAck(IncomingMessage, Ack, AckBits);

            auto Fragment = FragmentReassembly.Find(IncomingSequence);
            if (!Fragment)
            {
                // Create a new message that holds all fragments of this sequence packet
                Fragment = FragmentReassembly.InsertWithCleanup(IncomingSequence);
                if (!Fragment)
                {
                    Log.Warn("[{}] ignoring invalid fragment. could not insert in reassembly buffer (stale): {}", UserId, Game::Message::EnumNameMessageData(MessageType));
                    Counters.at(EndpointNumPacketsFragmentsInvalid)++;
                    return false;
                }
                ReceivedPackets.Advance(IncomingSequence);
                Fragment->Sequence = IncomingSequence;
                Fragment->Ack = 0;
                Fragment->AckBits = 0;
                Fragment->MessageLen = IncomingMessage.message_len();
                Fragment->FragmentsReceived = 0;
                Fragment->FragmentsTotal = NumFragments;
                Fragment->MessageData->resize(Config.FragmentSize * NumFragments);
                std::fill(Fragment->MessageData->begin(), Fragment->MessageData->end(), 0);
                std::fill(Fragment->FragmentReceived.begin(), Fragment->FragmentReceived.end(), 0);
            }

            if (NumFragments != Fragment->FragmentsTotal)
            {
                Log.Warn("[{}] ignoring invalid fragment. invalid fragment. fragment count mismatch. expected {}, got {}", UserId, Fragment->FragmentsTotal, NumFragments);
                Counters.at(EndpointNumPacketsFragmentsInvalid)++;
                return false;
            }

            if (Fragment->FragmentReceived[FragmentId])
            {
                 Log.Debug("[{}] ignoring invalid fragment {} of sequence {}. fragment already received", UserId, FragmentId, IncomingSequence);
                 Counters.at(EndpointNumPacketsFragmentsInvalid)++;
                 return false;
            }
            
            Fragment->FragmentsReceived++;
            Fragment->FragmentReceived.at(FragmentId) = 1;

            // copy the fragment into our message buffer at whatever offset the fragment id is
            auto StartOffset = FragmentId * Config.FragmentSize;
            auto It = Fragment->MessageData->begin() + StartOffset;
            Fragment->MessageData->insert(It, ReassembledData.begin(), ReassembledData.end());

            // Only at this point do we have all the fragments and we can safely return the reassembled data to caller
            if (Fragment->FragmentsTotal == Fragment->FragmentsReceived)
            {
                ReassembledData.resize(Fragment->MessageData->size());
                std::copy(Fragment->MessageData->begin(), Fragment->MessageData->end(), ReassembledData.begin());
                FragmentReassembly.RemoveWithCleanup(IncomingSequence);
                Counters.at(EndpointNumPacketsReceived)++;
                return true;
            }

            return false;
        }
    }

    std::vector<uint16_t>& ReliableEndpoint::GetAcks(int &TotalAcks)
    {
        TotalAcks = NumAcks;
        return Acks;
    }

    void ReliableEndpoint::ClearAcks()
    {
        NumAcks = 0;
        std::fill(Acks.begin(), Acks.end(), 0);
    }

    void ReliableEndpoint::Reset()
    {
        NumAcks = 0;
        Sequence = 0;
        std::fill(Acks.begin(), Acks.end(), 0);
        std::fill(Counters.begin(), Counters.end(), 0);
        for (uint16_t i = 0; i < Config.FragmentReassemblyBufferSize; ++i)
        {
            auto ReassemblyData = FragmentReassembly.AtIndex(i);
            if (ReassemblyData && ReassemblyData->MessageData)
            {
                ReassemblyData = nullptr;
            }
        }

        SentPackets.Reset();
        ReceivedPackets.Reset();
        FragmentReassembly.Reset();
    }

    void ReliableEndpoint::Update(double NewTime)
    {
        Time = NewTime;
        // packet loss
        uint32_t BaseSequence = ( SentPackets.GetSequence() - Config.SentPacketsBufferSize + 1) + 0xFFFF;
        int NumDropped = 0;
        int NumSamples = Config.SentPacketsBufferSize / 2;
        for (int i = 0; i < NumSamples; ++i)
        {
            uint16_t SampleSequence = (uint16_t)(BaseSequence+i);
            auto SentPacket = SentPackets.Find(SampleSequence);
            if (SentPacket && !SentPacket->Acked)
            {
                NumDropped++;
            }
            float SampledPacketLoss = ((float)NumDropped) / ((float) NumSamples) * 100.0f;
            if (std::fabs(PacketLoss - SampledPacketLoss) > 0.00001)
            {
                PacketLoss += (SampledPacketLoss - PacketLoss) * Config.PacketLossSmoothingFactor;
            }
            else
            {
                PacketLoss = SampledPacketLoss;
            }
        }

        // sent bandwidth
        CalculateBandwidth(BaseSequence, SentPackets, NumSamples, SentBandwidthKbps, false);
        // ack bandwidth
        CalculateBandwidth(BaseSequence, SentPackets, NumSamples, AckedBandwidthKbps, true);
        // recv bandwidth
        BaseSequence = (ReceivedPackets.GetSequence() - Config.ReceivedPacketsBufferSize + 1) + 0xFFFF;
        NumSamples = Config.ReceivedPacketsBufferSize / 2;
        CalculateBandwidth(BaseSequence, ReceivedPackets, NumSamples, ReceivedBandwidthKbps, false);

    }

    void ReliableEndpoint::CalculateBandwidth(const uint32_t BaseSequence, SequenceBuffer<ReliableMessage> &Buffer, const int NumSamples, float &UpdateBandwidth, bool bRequireAck)
    {
        int BytesSent = 0;
        double StartTime = FLT_MAX;
        double FinishTime = 0.0;
        for (int i = 0; i < NumSamples; ++i)
        {
            uint16_t SampleSequence = (uint16_t)(BaseSequence+i);
            auto PacketData = Buffer.Find(SampleSequence);
            if (!PacketData)
            {
                continue;
            }
            if (bRequireAck && !PacketData->Acked)
            {
                continue;
            }
            BytesSent += PacketData->MessageLen;
            if (PacketData->Time < StartTime)
            {
                StartTime = PacketData->Time;
            }

            if (PacketData->Time > FinishTime)
            {
                FinishTime = PacketData->Time;
            }
        }

        if (StartTime != FLT_MAX && FinishTime != 0.0)
        {
            float BandwidthKbps = (float) (((double)BytesSent) / (FinishTime - StartTime) * 8.0f / 1000.0f );
            if (std::fabs(UpdateBandwidth - BandwidthKbps) > 0.00001 )
            {
                UpdateBandwidth += (BandwidthKbps - UpdateBandwidth) * Config.BandwidthSmoothingFactor;
            }
            else
            {
                UpdateBandwidth = BandwidthKbps;
            }
        }
    }

    float ReliableEndpoint::GetRTT()
    {
        return Rtt;
    }

    float ReliableEndpoint::GetPacketLoss()
    {
        return PacketLoss;
    }

    void ReliableEndpoint::Bandwidth(float &SentKbps, float &RecvKbps, float &AckKbps)
    {
        SentKbps = SentBandwidthKbps;
        RecvKbps = ReceivedBandwidthKbps;
        AckKbps = AckedBandwidthKbps;
    }

    std::array<uint64_t, EndpointNumCounters>& ReliableEndpoint::GetCounters()
    {
        return Counters;
    }
}