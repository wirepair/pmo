#pragma once

#include <memory>
#include <array>
#include <thread>
#include <atomic>
#include <sstream>
#include <iomanip>
#include <chrono>

#include "reliable_endpoint.h"
#include "logger/logger.h"
#include "containers/lockfreequeue.h"
#include "crypto/cryptor.h"
#include "socket.h"
#include "schemas/messages.h"

#ifndef _WIN32
#include <sys/eventfd.h>
#include <sys/epoll.h>
#endif


#define PMO_BUFFER_SIZE 1024

namespace net
{
    using namespace std::chrono_literals;
    using PacketQueue = LockFreeQueue<std::unique_ptr<Game::Message::GameMessage>, 65536>;
    using PacketQueueSharedPtr = std::shared_ptr<PacketQueue>;

    class Listener 
    {
    public:
        explicit Listener(PacketQueueSharedPtr InQueue, PacketQueueSharedPtr OutQueue, std::shared_ptr<crypto::Cryptor> Crypto, std::shared_ptr<Socket> ServerSocket, logging::Logger &Log) : 
            InboundMessageQueue(InQueue), OutboundMessageQueue(OutQueue), Crypto(Crypto), ListenerSocket(ServerSocket), Logger(Log) 
            {
#ifdef _WIN32
                WriteEventHandle = CreateEvent(NULL, FALSE, FALSE, NULL);
#else
                // Create our epoll fd which will be used in Listen()
                EpollFileDescriptor = epoll_create(2);

                // Listen for NotifyWrite() events
                WriteEventFD = eventfd(0, EFD_NONBLOCK);
                struct epoll_event WriteEvent{};
                WriteEvent.events = EPOLLET | EPOLLIN; // make it edge triggered (e.g. only set when we call NotifyWrite())
                WriteEvent.data.fd = WriteEventFD;
                
                epoll_ctl(EpollFileDescriptor, EPOLL_CTL_ADD, WriteEventFD, &WriteEvent);
#endif

                Running.store(true);
            };
        /**
         * @brief Starts the thread, calls Init
         * 
         */
        virtual bool Start() = 0;

        /**
         * @brief Stops the thread by setting Running to false
         * 
         */
        virtual void Stop();

        /**
         * @brief Drain queues
         * 
         */
        virtual void Drain();

        /**
         * @brief Listens for incoming messages using recvfrom
         * 
         */
        virtual void Listen();

        /**
         * @brief Notify epoll we are ready to write out bound messages
         */
        virtual void NotifyWrite();

        /**
         * @brief Destroy the Listener object by setting Running to false
         * 
         */
        virtual ~Listener() 
        {
            Stop(); 
        };

        /**
         * @brief ProcessPacket, must be implemented by overridden class
         * 
         * @param PacketBuffer The packed game message buffer
         * @param PacketLen 
         * @param ClientAddr 
         */
        virtual void ProcessPacket(std::array<unsigned char, PMO_BUFFER_SIZE> &PacketBuffer, const int PacketLen, struct sockaddr_in &ClientAddr) = 0;

        /**
         * @brief Process the out going message
         * 
         * @param Message 
         */
        virtual void ProcessMessage(Game::Message::GameMessage &Message) = 0;


        /**
         * @brief For debugging, print hex of a buffer
         * 
         * @tparam Iter 
         * @param Msg 
         * @param Begin 
         * @param End 
         */
        template <typename Iter>
        void LogBuffer(std::string Msg, Iter Begin, Iter End)
        {
            std::ostringstream Output;
            Output << std::hex << std::setw(2) << std::setfill('0');
            while(Begin != End)
            {
                Output << static_cast<unsigned>(*Begin++);
            }

            Logger.Info("{}: {}", Msg, Output.str());
        }

    private:
        /**
         * @brief Called from Start(), determines if we Bind or Connect
         * 
         */
        virtual void Init() = 0;

    protected:
        std::atomic_bool Running;
        PacketQueueSharedPtr InboundMessageQueue;
        PacketQueueSharedPtr OutboundMessageQueue;
        
        std::shared_ptr<crypto::Cryptor> Crypto;
        std::shared_ptr<Socket> ListenerSocket;
        logging::Logger &Logger;
        std::thread NetworkThread;
        #ifndef _WIN32
        int WriteEventFD{};
        int EpollFileDescriptor = -1;
        #else
        HANDLE WriteEventHandle;
        #endif
        const size_t BufferSize = 1024;
        const int TimeoutMs = 1;
        static const int MaxEventSize = 10000;
    };
}