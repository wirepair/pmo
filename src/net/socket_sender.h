#pragma once

#if defined(_WIN32)
#pragma comment(lib, "ws2_32.lib")

#include <WinSock2.h>
#include <WS2tcpip.h>
#else 
#include <netinet/in.h>
#include <sys/socket.h>
#endif

/**
 * @brief Used for testing to allow reliablity tests to pass in custom sender functions
 * 
 */
class SocketSender 
{
    public:
     /**
     * @brief Assumes a client socket, connected and sending to the remote server
     * 
     * @param Buffer The buffer to send
     * @param Len Length of buffer
     * @return size_t number of bytes sent
     */
    virtual size_t Send(const void *Buffer, const int Len) const noexcept = 0;

    /**
     * @brief Sends data ToAddr
     * 
     * @param ToAddr Address to send the buffer to
     * @param Buffer The buffer to send
     * @param Len Length of buffer
     * @return size_t number of bytes sent
     */
    virtual size_t SendTo(struct sockaddr_in &ToAddr, const void *Buffer, const int Len) const noexcept = 0;
};