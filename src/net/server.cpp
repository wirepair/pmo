
#ifdef IS_SERVER
#include <iomanip>
#include "server.h"

#if defined(_WIN32)
#include <WinSock2.h>
#else
#include <sys/epoll.h>
#endif


#include <sstream>
#include "schemas/messages.h"
#include "serialization/simple.h"

using namespace Game::Message;

namespace net 
{
    bool Server::Start()
    {
        NetworkThread = std::thread(&Server::Init, this);
        return true;
    }

    void Server::Init()
    {
        auto Ret = ListenerSocket->Bind();
        if (Ret != 0)
        {
            Logger.Error("Server::Init exiting due to Listen failure: {}", Ret);
            exit(-1);
        } 
        else 
        {
            Peers.reserve(1024);
            Listen();
        }

    }

    void Server::ProcessMessage(Game::Message::GameMessage &Message)
    { 

        switch(Message.InternalType)
        {
            case Game::Message::InternalMessage::Socket:
            {
               
                // Determine if we need message reliablity or not
                auto Peer = GetPeer(Message.UserId);
                if (!Peer)
                {
                    Logger.Warn("[{}] attempting to send un-authenticated packet!", Message.UserId);
                    return;
                }
                auto Endpoint = Peer->GetEndpointChannel(Message.MessageReliability);
                Logger.Debug("Server::ProcessMessage sending Message {} of size: {} Seq: {}", Game::Message::EnumNameMessageData(Message.MessageType), Message.Data->size(), Message.SequenceId);  
                Endpoint->SendPacket(Logger, *Crypto, Message);
                break;
            }
            case Game::Message::InternalMessage::ProcessReliableSocket:
            {
                auto Peer = GetPeer(Message.UserId);
                if (!Peer)
                {
                    Logger.Warn("[{}] attempting to resend reliable packets to a user without a connection!", Message.UserId);
                    return;
                }
                Peer->ResendReliablePackets(Logger, *Crypto, *InboundMessageQueue, Message.Time);
                break;
            }
            case Game::Message::InternalMessage::RemoveUser:
            {
                if (Peers.erase(Message.UserId) == 0)
                {
                    Logger.Warn("[{}] attempted to erase non-existant user.", Message.UserId);
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void Server::ProcessPacket(std::array<unsigned char, PMO_BUFFER_SIZE> &PacketBuffer, const int PacketLen, struct sockaddr_in &ClientAddr)
    {
        auto Deserialized = GetMessage(PacketBuffer.data());
        auto UserId = Deserialized->user_id();
        if (Deserialized->channel_id() != 0 && Deserialized->channel_id() != 1)
        {
            Logger.Error("Invalid ChannelId for {}", UserId);
            return;
        }
        
        Reliability ChannelId = static_cast<Reliability>(Deserialized->channel_id());
        
        switch (Deserialized->message_data_type())
        {
            case MessageData_GenKeyRequest:
            {

                GenerateKeyResponse(UserId, Deserialized, ClientAddr);
                Logger.Info("Generated Key Response for {}", UserId);
                break;
            }
            case MessageData_GenKeyResponse:
            {
                auto User = GetPeer(UserId);
                if (!User)
                {
                    Logger.Warn("[{}] User attempting to send server response as client???", UserId);
                    return;
                }
                break;
            }
            case MessageData_EncClientCommand:
            {
                auto UserPeer = GetPeer(UserId);
                if (!UserPeer)
                {
                    return;
                }

                Logger.Debug("ProcessPacket: recv {} bytes from client {}", PacketLen, UserId);
                std::unique_ptr<std::vector<unsigned char>> Output = std::make_unique<std::vector<unsigned char>>(DefaultConfig.FragmentAbove);

                // we are only ready if the packet was not fragmented, or we've collected all fragments at this point
                bool Ready = UserPeer->ReceivePacket(ChannelId, Logger, *Crypto, *Deserialized, *Output);
                if (Ready)
                {
                    // build SocketMessage and send back to main thread
                    auto EncryptedMessage = std::make_unique<GameMessage>(UserId, Deserialized->sequence_id(), std::move(Output), Deserialized->message_data_type());
                    InboundMessageQueue->Push(std::move(EncryptedMessage));
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    void Server::GenerateKeyResponse(const uint32_t UserId, const Game::Message::Message *Deserialized, struct sockaddr_in &ClientAddr)
    {
        auto PubKeyRequest = Deserialized->message_data_as_GenKeyRequest();
        auto Signature = PubKeyRequest->signature();
        auto PublicKey = PubKeyRequest->pubkey();

        // Verify first
        if (!Crypto->VerifyUser(UserId, PubKeyRequest->time_stamp(), Signature->Data(), PublicKey->Data()))
        {
            Logger.Error("Unable to verify user {}\n", UserId);
            return;
        }

        // Generate new user key
        auto UserExists = Peers.contains(UserId);
        if (!UserExists)
        {
            auto UserKey = std::make_shared<std::array<uint8_t, PMO_KEY_SIZE>>();
            Crypto->GenerateKey(*UserKey.get());
            auto UserEndpoints = std::make_unique<ReliablePeer>(UserId, ListenerSocket, ClientAddr, UserKey); 
            Peers.insert_or_assign(UserId, std::move(UserEndpoints));
            Logger.Info("[{}] user has been created", UserId);
        }

        // Get a pointer to the user's key
        auto UserKey = Peers.at(UserId)->GetUserKey();
        // Encrypt the generated key with users pubkey and our private server key                
        std::array<unsigned char, PMO_NONCE_BYTES> Nonce{0};
        
        std::vector<unsigned char> EncryptedKey;
        EncryptedKey.resize(Crypto->EncryptKeySize());
        auto Success = Crypto->EncryptKey(Nonce, PubKeyRequest->pubkey()->data(), *UserKey, EncryptedKey);
        if (!Success)
        {
            Logger.Error("Unable to Encrypt Key for user {}\n", UserId);
            return;
        }
       
        flatbuffers::FlatBufferBuilder KeyResponseBuilder(512);
        auto KeyBuffer = KeyResponseBuilder.CreateVector(EncryptedKey.data(), EncryptedKey.size());
        auto NonceValue = KeyResponseBuilder.CreateVector(Nonce.data(), PMO_NONCE_BYTES);

        // Create our GenKeyResponse nested table
        GenKeyResponseBuilder GenKey(KeyResponseBuilder);
        GenKey.add_encryption_key(KeyBuffer);
        GenKey.add_time_stamp(serialization::TimeNow());
        auto FinishedGenKey = GenKey.Finish();

        // Build the Game Message with nested GenKeyResponse we will send to the user
        MessageBuilder Message(KeyResponseBuilder);
        Message.add_user_id(UserId);
        Message.add_channel_id(Reliability::Unreliable);
        Message.add_nonce(NonceValue);
        Message.add_message_data_type(MessageData_GenKeyResponse);
        Message.add_message_data(FinishedGenKey.Union());
        KeyResponseBuilder.Finish(Message.Finish());
        
        // Copy the serialized buffer and create a OutboundMessage
        const int MessageLen = KeyResponseBuilder.GetSize();
        auto MessageData = std::make_unique<std::vector<uint8_t>>();
        MessageData->resize(MessageLen);
        std::copy(KeyResponseBuilder.GetBufferSpan().begin(), KeyResponseBuilder.GetBufferSpan().end(), MessageData->begin());
        KeyResponseBuilder.Release();
        
        // Queue NewUser processing in the main loop, server code needs to know the client validated(TODO: ref count this so we don't send it too often)
        if (!UserExists)
        {
            auto NewUserMessage = std::make_unique<GameMessage>(UserId, nullptr, InternalMessage::NewUser);
            InboundMessageQueue->Push(std::move(NewUserMessage));
        }
        
        // Always resend key
        Logger.Warn("[{}] sending GenKeyResponse bytes {}", UserId, MessageData->size());
        ListenerSocket->SendTo(ClientAddr, MessageData->data(), MessageData->size());
    }
}
#endif