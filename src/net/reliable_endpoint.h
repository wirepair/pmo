#pragma once

#include <algorithm>
#include <cstdint>
#include <array>

#include "socket_sender.h"
#include "reliable_message.h"

#include "crypto/cryptor.h"
#include "crypto/consts.h"
#include "logger/logger.h"
#include "containers/sequencebuffer.h"
#include "schemas/messages.h"

namespace net
{
    /** Taken/Translated to C++ from https://github.com/networkprotocol/reliable/ **/
    const int EndpointNumPacketsSent = 0;
    const int EndpointNumPacketsReceived = 1;
    const int EndpointNumPacketsAcked = 2;
    const int EndpointNumPacketsStale = 3;
    const int EndpointNumPacketsInvalid = 4;
    const int EndpointNumPacketsTooLargeToSend = 5;
    const int EndpointNumPacketsTooLargeToRecieve = 6;
    const int EndpointNumPacketsFragmentsSent = 7;
    const int EndpointNumPacketsFragmentsReceived = 8;
    const int EndpointNumPacketsFragmentsInvalid = 9;
    const int EndpointNumCounters = 10;

    struct ReliableEndpointConfig
    {
        int Index = 0;
        int MaxPacketSize = 16 * 1024;
        int FragmentAbove = 1024;
        int MaxFragments = 16;
        int FragmentSize = 1024;
        int AckBufferSize = 256;
        int SentPacketsBufferSize = 256;
        int ReceivedPacketsBufferSize = 256;
        int FragmentReassemblyBufferSize = 64;
        float RTTSmoothingFactor = 0.0025f;
        float PacketLossSmoothingFactor = 0.1f;
        float BandwidthSmoothingFactor = 0.1f;
        int PacketHeaderSize = 28;        // note: UDP over IPv4 = 20 + 8 bytes, UDP over IPv6 = 40 + 8 bytes;
        double ResendThreshold = 2000.f; // stop trying to re-send after 2 seconds.
    };

    const ReliableEndpointConfig DefaultConfig;

    class ReliableEndpoint
    {
    public:
        ReliableEndpoint(const uint8_t ChannelId, const uint32_t ReliableUserId,  std::shared_ptr<SocketSender> Socket, struct sockaddr_in& EndpointAddr, std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>> UserKey) : 
            ReliableEndpoint(ChannelId, ReliableUserId, Socket, EndpointAddr, UserKey, DefaultConfig, 0) {};

        ReliableEndpoint(const uint8_t ChannelId, const uint32_t ReliableUserId, std::shared_ptr<SocketSender> Socket, struct sockaddr_in& EndpointAddr, std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>> UserKey, const ReliableEndpointConfig &EndpointConfig, double CreationTime) 
                : ChannelId(ChannelId), UserId(ReliableUserId), Sock(Socket), Key(UserKey), Config(EndpointConfig), Time(CreationTime), 
                SentPackets(SequenceBuffer<ReliableMessage>(EndpointConfig.SentPacketsBufferSize)),
                ReceivedPackets(SequenceBuffer<ReliableMessage>(EndpointConfig.ReceivedPacketsBufferSize)),
                FragmentReassembly(SequenceBuffer<ReliableMessage>(EndpointConfig.FragmentReassemblyBufferSize))
            {
                Addr.sin_addr.s_addr = EndpointAddr.sin_addr.s_addr;
                Addr.sin_family = EndpointAddr.sin_family;
                Addr.sin_port = EndpointAddr.sin_port;

                Acks.resize(EndpointConfig.AckBufferSize);
                std::fill(Acks.begin(), Acks.end(), 0);
            };
        
        ~ReliableEndpoint() 
        {
            SentPackets.Reset();
            ReceivedPackets.Reset();
            FragmentReassembly.Reset();
        }

        uint16_t NextPacketSequence() { return Sequence; };

        /**
         * @brief Sends a packet, by encrypting packetdata of packetlen for message type
         * 
         * @param Log 
         * @param Crypto 
         * @param PacketData 
         * @param PacketLen 
         * @param MessageType 
         */
        void SendPacket(logging::Logger &Log, crypto::Cryptor &Crypto, Game::Message::GameMessage &PacketMessage);
       
        /**
         * @brief 
         * 
         * @param Log 
         * @param Crypto 
         * @param Message 
         */
        void SendMessage(logging::Logger &Log, crypto::Cryptor &Crypto, net::ReliableMessage *Message);

        /**
         * @brief 
         * 
         * @param Log 
         * @param Crypto 
         * @param Message 
         */
        void SendFragmentedMessage(logging::Logger &Log, crypto::Cryptor &Crypto, net::ReliableMessage *Message);
        
        /**
         * @brief 
         * 
         * @param Builder 
         * @param EncryptedBuffer 
         * @param Data 
         * @param MessageType 
         */
        void BuildEncryptedField(flatbuffers::FlatBufferBuilder &Builder, flatbuffers::Offset<flatbuffers::Vector<uint8_t>> &EncryptedBuffer, flatbuffers::Offset<> &Data, const Game::Message::MessageData MessageType);

        /**
         * @brief 
         * 
         * @param Message 
         * @param Ack 
         * @param AckBits 
         */
        void ReadAck(const Game::Message::Message &Message, uint16_t &Ack, uint32_t &AckBits);

        /**
         * @brief 
         * 
         * @param Log 
         * @param Crypto 
         * @param IncomingMessage 
         * @param ReassembledData 
         * @return true 
         * @return false 
         */
        bool ReceivePacket(logging::Logger &Log, crypto::Cryptor &Crypto, const Game::Message::Message &IncomingMessage, std::vector<unsigned char> &ReassembledData);

        /**
         * @brief Get the Acks object
         * 
         * @param TotalAcks 
         * @return std::vector<uint16_t>& 
         */
        std::vector<uint16_t>& GetAcks(int &TotalAcks);
        
        /**
         * @brief 
         * 
         */
        void ClearAcks();

        /**
         * @brief 
         * 
         */
        void Reset();

        /**
         * @brief 
         * 
         * @param Time 
         */
        void Update(double Time);

        /**
         * @brief 
         * 
         * @return float 
         */
        float GetRTT();
        
        /**
         * @brief Get the Packet Loss object
         * 
         * @return float 
         */
        float GetPacketLoss();

        /**
         * @brief 
         * 
         * @param BaseSequence 
         * @param Buffer 
         * @param NumSamples 
         * @param UpdateBandwidth 
         * @param bRequireAck 
         */
        void CalculateBandwidth(const uint32_t BaseSequence, SequenceBuffer<ReliableMessage> &Buffer, const int NumSamples, float &UpdateBandwidth, bool bRequireAck);

        /**
         * @brief 
         * 
         * @param SentKbps 
         * @param RecvKbps 
         * @param AckKbps 
         */
        void Bandwidth(float &SentKbps, float &RecvKbps, float &AckKbps);

        /**
         * @brief Get the Counters object
         * 
         * @return std::array<uint64_t, EndpointNumCounters>& 
         */
        std::array<uint64_t, EndpointNumCounters>& GetCounters();
        
        /**
         * @brief 
         * 
         * @return std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>* 
         */
        std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>* UserKey() { return Key.get(); };

        /**
         * @brief Get the Addr object
         * 
         * @return sockaddr_in& 
         */
        inline sockaddr_in& GetAddr() { return Addr; }

        /**
         * @brief Get the Sent Packets object
         * 
         * @return SequenceBuffer<ReliableMessage>* 
         */
        inline SequenceBuffer<ReliableMessage>* GetSentPackets() { return &SentPackets; };


        uint32_t GetUserId() { return UserId; };
        /**
         * @brief 
         * 
         * @tparam Iter 
         * @param Msg 
         * @param Begin 
         * @param End 
         * @param Logger 
         */
        template <typename Iter>
        void LogBuffer(std::string Msg, Iter Begin, Iter End, logging::Logger &Logger)
        {
            std::ostringstream Output;
            Output << std::hex << std::setw(2) << std::setfill('0');
            while(Begin != End)
            {
                Output << static_cast<unsigned>(*Begin++);
            }

            Logger.Info("{}: {}", Msg, Output.str());
        }

    private:
        const uint8_t ChannelId;
        uint32_t UserId = 0;
        std::shared_ptr<SocketSender> Sock;
        struct sockaddr_in Addr;
        std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>> Key = nullptr;
        const ReliableEndpointConfig &Config;

        double Time = 0.f;
        float Rtt = 0.f;
        float PacketLoss = 0.f;
        float SentBandwidthKbps = 0.f;
        float ReceivedBandwidthKbps = 0.f;
        float AckedBandwidthKbps = 0.f;
        int NumAcks = 0;
        uint16_t Sequence = 0;

        std::vector<uint16_t> Acks;

        SequenceBuffer<ReliableMessage> SentPackets;
        SequenceBuffer<ReliableMessage> ReceivedPackets;
        SequenceBuffer<ReliableMessage> FragmentReassembly;
        std::array<uint64_t, EndpointNumCounters> Counters{0};
    };
}