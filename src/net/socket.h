#pragma once

#include <iostream>
#include <memory>
#include <cstring>

#if defined(_WIN32)
#pragma comment(lib, "ws2_32.lib")

#include <WinSock2.h>
#include <WS2tcpip.h>
#else 
#include <netinet/in.h>
#include <sys/socket.h>
#endif

#include "socket_sender.h"
#include "logger/logger.h"

namespace net 
{
    class Socket : public SocketSender {
    public:
#ifdef _WIN32
        Socket(const std::string IP, const int Port, logging::Logger &Log) : IP(IP), Port(Port), Logger(Log)
        {
            WSADATA Data;

            WORD wVersionRequested = 0x202;
            auto Ret = ::WSAStartup(wVersionRequested, &Data);
            if (Ret != 0)
            {
                return;
            }
            SocketFileDescriptor = ::WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, NULL, 0, WSA_FLAG_OVERLAPPED);
            std::memset((char *)&Addr, 0, sizeof(Addr));
            Addr.sin_family = AF_INET;
            Addr.sin_port = htons(Port);
            // todo handle errors better
            inet_pton(AF_INET, IP.c_str(), &(Addr.sin_addr));
        };
#else
        Socket(const std::string IP, const int Port, logging::Logger &Log) : IP(IP), Port(Port), Logger(Log)
        {
            SocketFileDescriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
            std::memset((char *)&Addr, 0, sizeof(Addr));
            Addr.sin_family = AF_INET;
            Addr.sin_port = htons(Port);
        };
        
        Socket(const Socket& other) : IP(other.IP), Port(other.Port), Logger(other.Logger),
            SocketFileDescriptor(other.SocketFileDescriptor), Addr(other.Addr) {};

        Socket& operator=(const Socket& other)
        {
            if (&other != this)
            {
                IP = other.IP;
                Port = other.Port;
                SocketFileDescriptor = other.SocketFileDescriptor;
                Addr = other.Addr;
            }
            return *this;
        }
#endif
        
        virtual ~Socket()
        {
#ifdef _WIN32
            closesocket(SocketFileDescriptor);
            WSACleanup();
#endif
            SocketFileDescriptor = -1;
        }



        /**
         * @brief Binds the socket for servers
         * 
         * @return int 
         */
        int Bind() noexcept;
        
        /**
         * @brief Connects the socket to the server (for clients)
         * 
         * @return int 
         */
        int Connect() noexcept;

        /**
         * @brief Assumes a client socket, connected and sending to the remote server
         * 
         * @param Buffer The buffer to send
         * @param Len Length of buffer
         * @return size_t number of bytes sent
         */
        virtual size_t Send(const void *Buffer, const int Len) const noexcept override;

        /**
         * @brief Sends data ToAddr
         * 
         * @param ToAddr Address to send the buffer to
         * @param Buffer The buffer to send
         * @param Len Length of buffer
         * @return size_t number of bytes sent
         */
        virtual size_t SendTo(struct sockaddr_in &ToAddr, const void *Buffer, const int Len) const noexcept override;
        
        /**
         * @brief 
         * 
         * @param RemoteAddr 
         * @param Buffer 
         * @param Len 
         * @return size_t 
         */
        size_t Receive(struct sockaddr_in &RemoteAddr, void *Buffer, const int &Len) const noexcept;

        /**
         * @brief Return the SocketFileDescriptor
         * 
         * @return int 
         */
        int GetFD() const;

        /**
         * @brief Returns the address we were initialized with
         * 
         * @return struct sockaddr_in& 
         */
        struct sockaddr_in& SocketAddr() { return Addr; };

    private:
        int SetSockOptions() noexcept;

        int SetNonBlocking() const;

    private:
        std::string IP = "";
        int Port = 4242;
        logging::Logger &Logger;

        int SocketFileDescriptor = -1;
        int SocketBufferSize = 2000000;
        struct sockaddr_in Addr;
    };
}
