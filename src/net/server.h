#pragma once
#ifdef IS_SERVER
#include <memory>
#include <array>
#include <thread>
#include <atomic>
#include <unordered_map>

#include "listener.h"
#include "socket.h"
#include "reliable_peer.h"
#include "reliable_endpoint.h"

namespace net
{
    class Server : public Listener
    {
        using Listener::Listener;
    public:
        virtual bool Start() override;
        
        void Init() override;

        virtual void ProcessPacket(std::array<unsigned char, PMO_BUFFER_SIZE> &PacketBuffer, const int PacketLen, struct sockaddr_in &ClientAddr) override;

        virtual void ProcessMessage(Game::Message::GameMessage &Message) override;
        
        ReliableEndpoint* GetEndpointChannel(const uint32_t UserId, const Reliability ChannelId) noexcept
        { 
            try 
            { 
                if (Peers.at(UserId) == nullptr)
                {
                    return nullptr;
                }
                return Peers.at(UserId)->GetEndpointChannel(ChannelId);
            } 
            catch(std::out_of_range const&) 
            { 
                return nullptr;
            } 
        };

        ReliablePeer* GetPeer(const uint32_t UserId) noexcept
        { 
            try 
            { 
                return Peers.at(UserId).get(); 
            } 
            catch(std::out_of_range const&) 
            { 
                return nullptr; 
            } 
        };
    protected:
        void GenerateKeyResponse(const uint32_t UserId, const Game::Message::Message *Deserialized, struct sockaddr_in &ClientAddr);

    private:
        std::unordered_map<uint32_t, std::unique_ptr<ReliablePeer>> Peers{};
    };
}
#endif