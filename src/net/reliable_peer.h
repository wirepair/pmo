#pragma once

#include <algorithm>
#include <cstdint>
#include <array>

#include "listener.h"
#include "socket_sender.h"
#include "reliable_endpoint.h"
#include "reliable_message.h"
#include "crypto/cryptor.h"
#include "crypto/consts.h"
#include "logger/logger.h"
#include "containers/lockfreequeue.h"
#include "containers/sequencebuffer.h"
#include "schemas/messages.h"

using namespace Game::Message;

namespace net
{
    class ReliablePeer
    {
    public:
        ReliablePeer(const uint32_t ReliableUserId, std::shared_ptr<SocketSender> Socket, struct sockaddr_in& EndpointAddr, std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>> UserKey) : 
            ReliablePeer(ReliableUserId, Socket, EndpointAddr, UserKey, DefaultConfig, 0) {};

        ReliablePeer(const uint32_t ReliableUserId, std::shared_ptr<SocketSender> Socket, struct sockaddr_in& EndpointAddr, std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>> UserKey, const ReliableEndpointConfig &EndpointConfig, double CreationTime)
        {
            UserId = ReliableUserId;
            Endpoints.at(0) = std::make_unique<ReliableEndpoint>(Reliability::Unreliable, ReliableUserId, Socket, EndpointAddr, UserKey);
            Endpoints.at(1) = std::make_unique<ReliableEndpoint>(Reliability::Reliable, ReliableUserId, Socket, EndpointAddr, UserKey);
        };

        /**
         * @brief Sends a packet to the endpoint either reliably or not depending on channel type
         * 
         * @param ChannelId 
         * @param Log 
         * @param Crypto 
         * @param MessagePacket
         */
        void SendPacket(const Reliability ChannelId, logging::Logger &Log, crypto::Cryptor &Crypto, Game::Message::GameMessage &PacketMessage) const;

        /**
         * @brief  ReceivePacket may or not return a packet in ReassembledData depending on if the incoming message was fragmented
         * 
         * @param ChannelId 
         * @param Log 
         * @param Crypto 
         * @param IncomingMessage 
         * @param ReassembledData 
         * @return true if we have a full packet ready to return to caller
         * @return false we do not have a packet ready (it was fragmented)
         */
        bool ReceivePacket(const Reliability ChannelId, logging::Logger &Log, crypto::Cryptor &Crypto, const Game::Message::Message &IncomingMessage, std::vector<unsigned char> &ReassembledData) const;

        /**
         * @brief Resends reliable packets that have not been Ack'd, clears out messages that _have_ been Ack'd.
         * TODO: Inefficient as it doesn't handle fragments well, we will end up resending the entire fragmented
         * message instead of individual fragments. Will fix after I get movement working!!!
         * 
         */
        int ResendReliablePackets(logging::Logger &Log, crypto::Cryptor &Crypto, net::PacketQueue &InboundQueue, const double Time) const;

        /**
         * @brief Notifies using our Queue that the message failed to be sent/acknowledged
         * 
         * @param FailedMessage 
         * @param InboundQueue 
         */
        void NotifyMessageFailed(net::ReliableMessage &FailedMessage, net::PacketQueue &InboundQueue) const;

        /**
         * @brief Processes the Acknowledgement from the peer endpoint. Clears out sequence and message
         * on successful Ack.
         * 
         * @param Endpoint the endpoint to process the Ack in
         * @param Ack the ack number to process
         */
        void ProcessAck(ReliableEndpoint &Endpoint, uint16_t Ack) const;

        /**
         * @brief Updates both endpoints time for RTT calculations
         * 
         * @param Time 
         */
        void Update(const double Time);

        /**
         * @brief Get the Endpoint Channel object
         * 
         * @param ChannelId 
         * @return ReliableEndpoint* 
         */
        ReliableEndpoint* GetEndpointChannel(const Reliability ChannelId) noexcept;

        std::array<std::unique_ptr<ReliableEndpoint>, 2>* GetEndpoints() noexcept;

        std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>* GetUserKey()
        {
            return Endpoints.at(0)->UserKey();
        }

    private:
        uint32_t UserId{};
        std::array<std::unique_ptr<ReliableEndpoint>, 2> Endpoints;
    };
}