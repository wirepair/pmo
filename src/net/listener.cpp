#include "listener.h"

#if defined(_WIN32)
#include "socket.h"
#include <mutex>
#else
#include <sys/epoll.h>
#endif

#include "serialization/simple.h"

#define UserIDLength sizeof(uint32_t)

namespace net 
{
    void Listener::Stop()
    {
        if (Running.load())
        {
            Logger.Info("Listener: Shutting down");
            Running.store(false);
            if (NetworkThread.joinable())
            {
                try 
                { 
                    Logger.Info("Listener: Detaching Thread");
                    NetworkThread.detach();
                } 
                catch (const std::system_error& e) 
                {
                    Logger.Error("Failed to join thread: {}", e.what());
                }
            }  
        }
    }

    void Listener::Drain()
    {
        // may not need this as clients don't need to initiate messages from the crypto/client code?
        for (auto Packet = OutboundMessageQueue->Pop(); Packet != std::nullopt; Packet = OutboundMessageQueue->Pop())
        {
            Game::Message::ReleaseGameMessage(*std::move(Packet));
        }

        for (auto Packet = InboundMessageQueue->Pop(); Packet != std::nullopt; Packet = InboundMessageQueue->Pop())
        {
             Game::Message::ReleaseGameMessage(*std::move(Packet));
        }
    }

#if defined(_WIN32)
    void Listener::Listen()
    {
        Logger.Info("Listen: Begin");
        auto NetEvent = WSACreateEvent();   
        if (NetEvent == nullptr)
        {
            Logger.Error("Listen: Failed to WSACreateEvent: {}\n", WSAGetLastError());
            return;
        }
        
        auto Ret = WSAEventSelect(ListenerSocket->GetFD(), NetEvent, FD_READ | FD_WRITE);
        if (Ret != 0) 
        {
            Logger.Error("Listen: Failed to WSAEventSelect: {}\n", WSAGetLastError());
            return;
        }
        Logger.Info("Listen: Network loop {}", Running.load());

        while (Running.load())
        {
            const DWORD EventTotal = 2;
            WSAEVENT EventArray[WSA_MAXIMUM_WAIT_EVENTS] = {NetEvent, WriteEventHandle};
            WSANETWORKEVENTS wsaEvents;
            // Wait for events
            auto Index = ::WaitForMultipleObjects(EventTotal, EventArray, FALSE, -1);
            if (EventArray[Index] == NetEvent)
            {
                // Reset our event handle
                auto ResetResult = WSAResetEvent(NetEvent);
                if (ResetResult == 0) 
                {
                    Logger.Error("Listen: WSAResetEvent failed with error = {}", WSAGetLastError());
                    return;
                }
    
                // Figure out what kind of event we have
                if (WSAEnumNetworkEvents(ListenerSocket->GetFD(), NetEvent, &wsaEvents) != 0)
                {
                    Logger.Error("Listen: WSAEnumNetworkEvents failed.  Error code {}", WSAGetLastError() );
                    continue;
                }
                
                // Not a read? BALEETED, or well, go back to WaitForMultipleObjects
                if (!(wsaEvents.lNetworkEvents & FD_READ))
                {
                    continue;
                }
                
                // OK we have an inbound packet, process it.
                struct sockaddr_in ClientAddr;
                std::memset((char *)&ClientAddr, 0, sizeof(ClientAddr));
    
                std::array<unsigned char, PMO_BUFFER_SIZE> PacketBuffer{0};
                
                // Finally call recvfrom on our socket
                auto PacketLen = ListenerSocket->Receive(ClientAddr, PacketBuffer.data(), BufferSize);
                if (PacketLen <= 0)
                {
                    continue;
                }
                
                // Process our packet
                ProcessPacket(PacketBuffer, PacketLen, ClientAddr);
            } 
            else if (EventArray[Index] = WriteEventHandle)
            {
                ResetEvent(WriteEventHandle);
                // Send outgoing messages that are queued up.
                for (auto Packet = OutboundMessageQueue->Pop(); Packet != std::nullopt; Packet = OutboundMessageQueue->Pop())
                {
                    ProcessMessage(*Packet->get());
                    Game::Message::ReleaseGameMessage(std::move(*Packet));
                }
            }
        }
        Logger.Info("Listen: Exiting Listener Loop");
    }

    void Listener::NotifyWrite()
    {
        auto Ret = SetEvent(WriteEventHandle);
        Logger.Info("Listen: WriteEventHandle triggered, result: {}", Ret);
    }
#else

    void Listener::Listen()
    {
        // To shut up error: `variable length arrays are a C99 feature [-Werror,-Wvla-extension]`
        // we have to define this in the same method instead of using the class property???
        //const int MaxEventSize = 10000;
        struct epoll_event Events[MaxEventSize];
        
        struct epoll_event SocketEvent{};
        SocketEvent.events = EPOLLET | EPOLLIN;
        SocketEvent.data.fd = ListenerSocket->GetFD(); 
        epoll_ctl(EpollFileDescriptor, EPOLL_CTL_ADD, ListenerSocket->GetFD(), &SocketEvent);


        while (Running.load()) 
        {    
            auto Ready = epoll_wait(EpollFileDescriptor, Events, MaxEventSize, 0);
            if (Ready < 0)
            {
                std::error_code ec(errno, std::system_category());
                Logger.Error("Server::Listen exiting {}\n", ec.message());
                return;
            }
            else
            {
                for (int i = 0; i < Ready; i++)
                {
                    if (Events[i].data.fd == ListenerSocket->GetFD())
                    {
                        struct sockaddr_in ClientAddr;
                        std::memset((char *)&ClientAddr, 0, sizeof(ClientAddr));
        
                        std::array<unsigned char, PMO_BUFFER_SIZE> PacketBuffer{0};
                        
                        auto PacketLen = ListenerSocket->Receive(ClientAddr, PacketBuffer.data(), BufferSize);
                        if (PacketLen <= 0)
                        {
                            Logger.Warn("Recv'd < 0 ({})", PacketLen);
                            continue;
                        }
                        Logger.Info("Calling ProcessPacket with {} bytes", PacketLen);
                        ProcessPacket(PacketBuffer, PacketLen, ClientAddr);
                    }
                    else if (Events[i].data.fd == WriteEventFD)
                    {
                        for (auto Packet = OutboundMessageQueue->Pop(); Packet != std::nullopt; Packet = OutboundMessageQueue->Pop())
                        {
                            ProcessMessage(*Packet->get());
                            Game::Message::ReleaseGameMessage(std::move(*Packet));
                        }
                    }

                }
            }
        }
    }

    void Listener::NotifyWrite()
    {
        eventfd_write(WriteEventFD, 1);
    }
#endif
}