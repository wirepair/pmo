#include "socket.h"
#include <fcntl.h>
#if defined(_WIN32)
#include <winsock2.h>
#else
#include <sys/epoll.h>
#include <arpa/inet.h>
#endif

namespace net 
{
#ifdef _WIN32
    int Socket::Bind() noexcept
    {
        auto ret = SetSockOptions();
        if (ret != 0)
        {
            return ret;
        }

        ret = inet_pton(AF_INET, IP.c_str(), (in_addr *)&Addr.sin_addr);
        if (ret == 0) 
        {
            Logger.Error("Bind: invalid ip address\n");
            return -1;
        }

        ret = bind(SocketFileDescriptor, (struct sockaddr *)&Addr, sizeof(Addr));
        if (ret == SOCKET_ERROR)
        {
            Logger.Error("Bind: failed to bind {}\n", WSAGetLastError());
            return ret;
        }
        
        return 0;
    }

#else
    int Socket::Bind() noexcept
    {
        auto ret = SetSockOptions();
        if (ret != 0)
        {
            Logger.Error("Bind: SetSockOptions failure: {}", ret);
            return ret;
        }

        ret = inet_pton(AF_INET, IP.c_str(), (in_addr *)&Addr.sin_addr);
        if (ret == 0) 
        {
            Logger.Error("Bind: invalid ip address: {}", ret);
            return -1;
        }

        ret = bind(SocketFileDescriptor, (struct sockaddr *)&Addr, sizeof(Addr));
        if (ret != 0)
        {
            Logger.Error("Bind: bind failure: {}", ret);
            return ret;
        }
        return 0;
    }
#endif

#ifdef _WIN32
    int Socket::Connect() noexcept
    {
        auto ret = SetSockOptions();
        if (ret != 0)
        {
            std::error_code ec(errno, std::system_category());
            Logger.Error("Connect failed to set SetSockOptions {}\n", ec.message());
            return ret;
        }

        auto Ret = WSAConnect(SocketFileDescriptor, (sockaddr*)(&Addr), sizeof(Addr), NULL, NULL, NULL, NULL);
        if (Ret == SOCKET_ERROR)
        {
            Logger.Error("Connect failed due to WSAConnect SOCKET_ERROR {}\n", WSAGetLastError());
            return -1;
        }

        return 0;
    }


#else
    int Socket::Connect() noexcept
    {
        auto ret = SetSockOptions();
        if (ret != 0)
        {
            std::error_code ec(errno, std::system_category());
            Logger.Error("Connect failed to set SetSockOptions {}\n", ec.message());
            return ret;
        }

        std::memset((char *)&Addr, 0, sizeof(Addr));
        Addr.sin_family = AF_INET;
        Addr.sin_port = htons(Port);

        ret = inet_pton(AF_INET, IP.c_str(), &Addr.sin_addr);
        if (ret == 0)
        {
            std::error_code ec(errno, std::system_category());
            Logger.Error("Connect failed to set inet address {}\n", ec.message());
            return ret;
        }

        ret = connect(SocketFileDescriptor, (struct sockaddr *)&Addr, sizeof(Addr));
        if (ret != 0)
        {
            return ret;
        }
        return 0;
    }
#endif

    int Socket::SetSockOptions() noexcept
    {
        auto ret = setsockopt(SocketFileDescriptor, SOL_SOCKET, SO_RCVBUF, (char *)&SocketBufferSize, sizeof(SocketBufferSize));
        if (ret != 0)
        {
            Logger.Error("SetSockOptions: invalid SO_RCVBUF call\n");
            return ret;
        }

        ret = setsockopt(SocketFileDescriptor, SOL_SOCKET, SO_SNDBUF, (char *)&SocketBufferSize, sizeof(SocketBufferSize));
        if (ret != 0)
        {
            Logger.Error("SetSockOptions: invalid SO_SNDBUF call\n");
            return ret;
        }
        
        return SetNonBlocking();
    }


    int Socket::GetFD() const 
    {
        return SocketFileDescriptor;
    }

#ifdef _WIN32
    int Socket::SetNonBlocking() const
    {
        unsigned long Mode = 1; // Non-blocking
        auto Ret = ioctlsocket(SocketFileDescriptor, FIONBIO, &Mode);
        if (Ret != NO_ERROR)
        {
            Logger.Error("ioctlsocket failed with error: {}\n", Ret);
            return -1;
        }
        return 0;
    }
#else
    int Socket::SetNonBlocking() const
    {
        int Flags;
        if ((Flags = fcntl(SocketFileDescriptor, F_GETFD, 0)) < 0)
        {
            Logger.Error("SetNonBlocking: failed to get file descriptor flags\n");
            return -1;
        }
        
        Flags |= O_NONBLOCK;
        if (fcntl(SocketFileDescriptor, F_SETFL, Flags) < 0)
        {
            Logger.Error("SetNonBlocking: failed to set non-blocking\n");
            return -1;
        }

        return 0;
    }
#endif

    size_t Socket::Send(const void *Buffer, const int Len) const noexcept
    {
        auto Ret = sendto(SocketFileDescriptor, (const char *)Buffer, Len, 0, (struct sockaddr *)&Addr, sizeof(Addr));
        if (Ret < 0) 
        {
            std::error_code ec(errno, std::system_category());
            Logger.Error("Send: failed to send data {}\n", ec.message());
        }
        Logger.Info("send packet: {}: {}", Len, Ret);
        return Ret;
    }

    size_t Socket::SendTo(struct sockaddr_in &ToAddr, const void *Buffer, const int Len) const noexcept
    {
        auto Ret = sendto(SocketFileDescriptor, (const char *)Buffer, Len, 0, (struct sockaddr *)&ToAddr, sizeof(ToAddr));
        if (Ret < 0) 
        {
            std::error_code ec(errno, std::system_category());
            Logger.Error("Send: failed to send data {}\n", ec.message());
        }
        Logger.Info("sendto packet: {}: {}", Len, Ret);
        return Ret;
    }

    size_t Socket::Receive(struct sockaddr_in &RemoteAddr, void *Buffer, const int &Len) const noexcept
    {
        socklen_t SocketLen = sizeof(RemoteAddr);
        auto Ret = recvfrom(SocketFileDescriptor, (char *)Buffer, Len, 0, (struct sockaddr*)&RemoteAddr, &SocketLen);
        Logger.Info("recvfrom packet: {}: {}", Len, Ret);
        return Ret;
    }

}