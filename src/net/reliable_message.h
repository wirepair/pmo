#pragma once
#include <memory>
#include <vector>
#include "schemas/messages.h"
#include "crypto/consts.h"

namespace net
{
        struct ReliableMessage
        {
            ReliableMessage() {};

            /**
             * @brief Reset our properties, leaving data as we will resize the vector 
             * and update it's contents guarunteed.
             * 
             */
            void Reset() 
            {
                MessageLen = 0;
                Sequence = 0;
                SequenceWrapped = false;
                Acked = false;
                Ack = 0;
                AckBits = 0;
                FragmentsReceived = 0;
                FragmentsTotal = 0;
                OpCode = 0;
                FragmentReceived = std::array<uint8_t, 256>{};
            };

            size_t MessageLen = 0;
            uint16_t Sequence = 0;
            bool SequenceWrapped = false;
            bool Acked = false;
            uint16_t Ack = 0;
            uint32_t AckBits = 0;
            double Time = 0.f;
            uint8_t FragmentsReceived = 0;
            uint8_t FragmentsTotal = 0;
            uint16_t OpCode = 0;
            std::array<uint8_t, 256> FragmentReceived{0};
            Game::Message::MessageData MessageType;
            std::unique_ptr<std::vector<uint8_t>> MessageData = std::make_unique<std::vector<uint8_t>>();
        };
}