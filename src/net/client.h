#pragma once

#include "listener.h"
#include "crypto/keypair.h"
#include "reliable_peer.h"
#include "reliable_endpoint.h"

namespace net 
{
    class Client : public Listener
    { 
        using Listener::Listener;
        
    public:
        
        virtual bool Start() override;
        
        void Init() override;

        void SetUserId(uint32_t ClientUserId) { this->UserId = ClientUserId; };

        inline uint32_t GetUserID() { return UserId; };

        std::array<unsigned char, PMO_KEY_SIZE> GetUserKey() 
        { 
            std::array<unsigned char, PMO_KEY_SIZE> KeyCopy = {0};
            auto UserKey = Peer->GetUserKey();
            std::copy(UserKey->begin(), UserKey->end(), KeyCopy.begin());
            return KeyCopy;
        };
        
        ReliableEndpoint * GetEndpoint(const Reliability ChannelId)
        {
            return Peer->GetEndpointChannel(ChannelId);
        }

        virtual void ProcessPacket(std::array<unsigned char, PMO_BUFFER_SIZE> &PacketBuffer, const int PacketLen, struct sockaddr_in &ServerAddr) override;

        virtual void ProcessMessage(Game::Message::GameMessage &Message) override;
        
        void Authenticate();

    private:
        crypto::KeyPair KeyPair;
        std::unique_ptr<ReliablePeer> Peer;
        uint32_t UserId = 0;
    };
}
