#pragma once

#include <vector>
#include <memory>
#include <cstring>
#include <sodium.h>

using UniqueUCharPtr = std::unique_ptr<std::vector<unsigned char>>;
using UniqueUint8ArrayPtr = std::unique_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>>;
using SharedUint8ArrayPtr = std::shared_ptr<std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES>>;
