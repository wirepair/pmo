#pragma once
#include <vector>
#include <stdint.h>

namespace input
{
    enum InputType : uint32_t
    {
        NoOp = 0,
        Forward = 1,
        Backward = 1 << 1,
        Left = 1 << 2,
        Right = 1 << 3,
        Jump = 1 << 4,
        Dodge = 1 << 5,
        Ctrl = 1 << 6,
        Alt = 1 << 7,
        StrafeLeft = 1 << 8,
        StrafeRight = 1 << 9,
        ActionOne = 1 << 10,
        ActionTwo = 1 << 11,
        ActionThree = 1 << 12,
        ActionFour = 1 << 13,
        ActionFive = 1 << 14,
        ActionSix = 1 << 15,
        ActionSeven = 1 << 16,
        ActionEight = 1 << 17,
        ActionNine = 1 << 18,
        ActionTen = 1 << 19,
        MouseLeft = 1 << 20,
        MouseRight = 1 << 21,
        MouseMiddle = 1 << 22,

        // Combinations
        YAxisMovement = Forward | Backward,
        XAxisMovement = Left | Right | StrafeLeft | StrafeRight,
    };

    struct Input
    {
        Input() {}

        explicit Input(const uint32_t KeyPresses) : KeyPresses(KeyPresses) {}
        /**
         * @brief Processes inputs and adds it to our key presses
         * 
         * @param Keys 
         * @return true if inputs are same as they were last time we processed
         * @return false at least one key press has changed
         */
        bool ProcessInputs(std::vector<InputType> Keys)
        {
            KeyPresses = 0;
            for (auto Key : Keys)
            {
                KeyPresses |= Key;
            }

            if (PreviousKeyPresses == KeyPresses)
            {
                return true;
            }

            PreviousKeyPresses = KeyPresses;
            return false;
        }

        /**
         * @brief Press the key and store it in KeyPresses
         * 
         * @param Key 
         */
        void Press(const InputType Key)
        {
            KeyPresses |= Key;
        }

        /**
         * @brief Unset the pressed key (if pressed) otherwise, do nothing
         * 
         * @param Key 
         */
        void Unpress(const InputType Key)
        {
            KeyPresses &= ~Key;
        }

        /**
         * @brief Returns true if no keys are currently pressed
         * 
         * @return true 
         * @return false 
         */
        bool IsNothingPressed() const
        {
            return KeyPresses == 0;
        }

        /**
         * @brief Test if a key is pressed
         * 
         * @param Key 
         * @return true 
         * @return false 
         */
        bool IsPressed(InputType Key) const
        {
            return (KeyPresses & Key) != 0;
        }

        /**
         * @brief Test if a key was pressed
         * 
         * @param Key 
         * @return true 
         * @return false 
         */
        bool WasPressed(InputType Key) const
        {
            return (PreviousKeyPresses & Key) != 0;
        }
        
        /**
         * @brief Clear our key press and history
         * 
         */
        void Clear()
        {
            KeyPresses = 0;
            PreviousKeyPresses = 0;
        }

        uint32_t KeyPresses = 0;
        uint32_t PreviousKeyPresses = 0;
    };
}   