#include <cstring>


#include "cryptor.h"
#include "serialization/simple.h"



namespace crypto 
{
    Cryptor::Cryptor(logging::Logger &Log) : Logger(Log)
    {
        auto Ret = sodium_init();
        Logger.Info("Sodium Initialized: {}", Ret);
    }

    /**
     * @brief Generates a xchacha20poly1305 key
     * 
     */
    void Cryptor::GenerateKey(std::array<uint8_t, PMO_KEY_SIZE> &Key) const
    {
        return crypto_aead_xchacha20poly1305_ietf_keygen(Key.data());
    }

    /**
     * @brief Generates a KeyPair of pub/private for handling key encrypting keys
     * 
     * @return true if generated, false if error
     */
    bool Cryptor::GenerateKeyEncryptingKey(KeyPair& KeyEncryptingKeys) const
    {
        return crypto_box_keypair(KeyEncryptingKeys.PublicKey.data(), KeyEncryptingKeys.PrivateKey.data()) == 0;
    }

    /**
     * @brief Helper to get xchacha20poly1305 keysize
     * 
     * @return uint32_t 
     */
    uint32_t Cryptor::KeySize() const
    {
        return PMO_KEY_SIZE;
    }

    uint32_t Cryptor::EncryptKeySize() const
    {
        return crypto_box_MACBYTES + KeySize();
    }

    uint32_t Cryptor::AuthByteSize() const
    {
        return PMO_A_BYTES;
    }


    /**
     * @brief Takes in a signed message and signed message length to verify that a user has been authorized by
     * our AuthServer.
     * 
     * @param SignedMessage 
     * @param SignedMessageLen 
     * @return true 
     * @return false 
     */
    bool Cryptor::VerifyUser(const unsigned char *SignedMessage, const unsigned long long SignedMessageLen) const
    {
        auto MessageLen = SignedMessageLen-crypto_sign_BYTES;
        if (SignedMessageLen <= 0)
        {
            return false;
        }
        
        // need to use a vector to handle variable size array decl
        std::vector<unsigned char>Message(MessageLen);
        std::memcpy(Message.data(), SignedMessage, MessageLen);

        std::vector<unsigned char>Signature(crypto_sign_BYTES);
        std::memcpy(Signature.data(), SignedMessage+MessageLen, crypto_sign_BYTES);

        return crypto_sign_verify_detached(Signature.data(), Message.data(), MessageLen, TestAuthServerPubKey.data()) == 0;
    }

    /**
     * @brief Verify a user by using the auth servers public key to verify it was a valid signed message coming from the client
     * 
     * @param UserId UserId to verify
     * @param TimeStamp When this message was signed so we can validate
     * @param Signature Signature that came from the authentication server
     * @param PublicKey Users public key
     * @return true 
     * @return false 
     */
    bool Cryptor::VerifyUser(const uint32_t UserId, const std::time_t Deadline, const unsigned char *Signature, const unsigned char *PublicKey) const
    {
        // TODO: Verify deadline is within allowed time range 
        unsigned char User[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, User);
        unsigned char TimeStamp[sizeof(std::time_t)];
        serialization::TimetToUChar(Deadline, TimeStamp);
        
        auto DataToSignSize = sizeof(uint32_t)+sizeof(std::time_t)+crypto_sign_ed25519_PUBLICKEYBYTES;
        auto ToSign = std::make_unique<std::vector<unsigned char>>(DataToSignSize);

        std::memcpy(ToSign->data(), User, sizeof(uint32_t));
        std::memcpy(ToSign->data()+sizeof(uint32_t), TimeStamp, sizeof(std::time_t));
        std::memcpy(ToSign->data()+sizeof(uint32_t)+sizeof(std::time_t), PublicKey, crypto_sign_ed25519_PUBLICKEYBYTES);

        return crypto_sign_verify_detached(Signature, ToSign->data(), DataToSignSize, TestAuthServerPubKey.data()) == 0;
    }

    /**
     * @brief Encrypts the MessageBuffer of MessageLen size returning the Nonce value and a ptr to the CipherText.
     * 
     * @param UserId UserId to lookup the encryption key to encrypt with
     * @param UserKey UserKey used for encrypting
     * @param Nonce A buffer to store the nonce bytes for the encrypted message
     * @param MessageBuffer The serialized/buffer containing what we want to encrypt
     * @param CipherText A std::vector<uchar> of MessageLen + AuthByteSize(); 
     * @param MessageLen The length of the serialized/buffer
     * @return true if successfully encrypted to CipherText
     */
    bool Cryptor::Encrypt(const uint32_t UserId, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &UserKey, std::array<unsigned char, PMO_NONCE_BYTES> &Nonce, const unsigned char *MessageBuffer, std::vector<unsigned char> &CipherText, unsigned long long MessageLen)
    {
        auto FullMessageLength = MessageLen + PMO_A_BYTES;
        if (CipherText.size() != FullMessageLength)
        {
            return false;
        }

        randombytes_buf(Nonce.data(), PMO_NONCE_BYTES);

        unsigned char AD[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, AD);
        unsigned long long OutputLen{0};

        return crypto_aead_xchacha20poly1305_ietf_encrypt(CipherText.data(), &OutputLen,
                                    MessageBuffer, MessageLen,
                                    AD, sizeof(UserId),
                                    NULL, Nonce.data(), UserKey.data()) == 0;
        
    }

    /**
     * @brief Encrypts a generated key using a public key of one user, and a secret key of another.
     * 
     * @param Nonce 
     * @param UsersPublicKey 
     * @param GeneratedKey 
     * @param CipherText A std::vector<uchar> of EncryptKeySize() length.
     * @return bool true on success, false otherwise 
     */
    bool Cryptor::EncryptKey(std::array<unsigned char, crypto_box_NONCEBYTES> &Nonce, const unsigned char *UsersPublicKey, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &GeneratedKey, std::vector<unsigned char> &CipherText) const
    {
        if (CipherText.size() != EncryptKeySize())
        {
            return false;
        }
        randombytes_buf(Nonce.data(), sizeof(Nonce));
        return crypto_box_easy(CipherText.data(), GeneratedKey.data(), KeySize(), Nonce.data(), UsersPublicKey, TestGameServerPrivateKey.data()) == 0;
    }

    /**
     * @brief Decrypts a generated key using a public key of one user, and a secret key of another.
     * 
     * @param Nonce The nonce value
     * @param Message The message to decrypt
     * @param PublicKey The public key of user
     * @param PrivateKey Secret key of server
     * @param DecryptedKey The DecryptedKey of PMO_KEY_SIZE
     * @return true on success
     * @return false otherwise
     */
    bool Cryptor::DecryptKey(const unsigned char *Nonce, const unsigned char *Message, const unsigned char *PublicKey, const unsigned char *PrivateKey, std::array<uint8_t, PMO_KEY_SIZE> &DecryptedKey)
    {
        auto MessageLen = crypto_box_MACBYTES + KeySize();
        auto Ret = crypto_box_open_easy(DecryptedKey.data(), Message, MessageLen, Nonce, PublicKey, PrivateKey);
        return (Ret == 0);
    }

    /**
     * @brief Decrypts a message intended for UserId, validates the UserId as part of the Authenticated Data (AD).
     * 
     * @param UserId used for validating the AD
     * @param UserKey used for decrypting
     * @param Nonce 
     * @param CipherText 
     * @param CipherTextLen 
     * @param MessageLen 
     * @param OutputLen 
     * @return true if successfully decrypted, false otherwise 
     */
    bool Cryptor::Decrypt(const uint32_t UserId, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &UserKey, const unsigned char *Nonce, const unsigned char *CipherText, const unsigned long long CipherTextLen, std::vector<unsigned char> &OutputBuffer, unsigned long long &OutputLen)
    {
        unsigned char AD[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, AD);
        auto ret = crypto_aead_xchacha20poly1305_ietf_decrypt(OutputBuffer.data(), &OutputLen,
                                               NULL,
                                               CipherText, CipherTextLen,
                                               AD, sizeof(UserId),
                                               Nonce, UserKey.data());
        if (ret != 0) 
        {
            /* message forged! */
            return false;
        }
        return true;
    }
}