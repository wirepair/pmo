#pragma once 

#include <vector>
#include <chrono>
#include <memory>
#include <cstring>
#include <sodium.h>

#include "serialization/simple.h"
#include "schemas/messages.h"

namespace crypto
{
    /**
     * @brief For testing, emulates an auth server
     * 
     * @param UserId 
     * @param PublicKey 
     * @param OutputLen 
     * @return std::unique_ptr<std::vector<unsigned char>> 
     */
    std::unique_ptr<std::vector<unsigned char>> FakeAuthServer(uint32_t UserId, unsigned char *PublicKey, unsigned long long &OutputLen);

    /**
     * @brief Fake authentication by signing a user id + timestamp + pubkey, returning a serialized GameMessage of type GenKeyRequest
     * 
     * @param UserId User's ID
     * @param PublicKey User's PublicKey for encrypting a symmetrical key
     * @param OutputLen Length of the serialized output GameMessage
     * 
     * @return std::unique_ptr<std::vector<unsigned char>> GameMessage of type GenKeyRequest
     */
    std::unique_ptr<std::vector<unsigned char>> FakeAuthServerFlat(uint32_t UserId, unsigned char *PublicKey, unsigned long long &OutputLen);
}