#pragma once

#include <array>
#include <memory>
#include <sodium.h>

namespace crypto
{
    struct KeyPair
    {
        KeyPair()
        {
            std::fill(PublicKey.begin(), PublicKey.end(), 0);
            std::fill(PrivateKey.begin(), PrivateKey.end(), 0);
        };

        KeyPair (const KeyPair&) = delete;
        KeyPair& operator= (const KeyPair&) = delete;

        uint32_t PublicKeySize() const { return crypto_box_PUBLICKEYBYTES; };
        uint32_t PrivateKeySize() const { return crypto_box_SECRETKEYBYTES; };

        std::array<unsigned char, crypto_box_PUBLICKEYBYTES>PublicKey;
        std::array<unsigned char, crypto_box_SECRETKEYBYTES>PrivateKey;
    };
}