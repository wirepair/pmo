#include "mockauthserver.h"

namespace crypto
{

    /**
     * @brief For testing, emulates an auth server
     * 
     * @param UserId 
     * @param PublicKey 
     * @param OutputLen 
     * @return std::unique_ptr<std::vector<unsigned char>> 
     */
    std::unique_ptr<std::vector<unsigned char>> FakeAuthServer(uint32_t UserId, unsigned char *PublicKey, unsigned long long &OutputLen)
    {
        std::array<unsigned char, crypto_sign_ed25519_SECRETKEYBYTES> TestAuthServerPrivateKey = {
            0x4d, 0x11, 0xfa, 0x7c, 0x12, 0x1b, 0x89, 0xc2, 0x5b, 0x15, 0xb8, 0xe7, 0x72, 0xa9, 0xde, 0xc3,
            0x63, 0x5c, 0xe6, 0xed, 0x31, 0xaf, 0xef, 0x63, 0x70, 0x2d, 0xb3, 0x31, 0x48, 0x95, 0xb5, 0x7d,
            0x32, 0x0d, 0x38, 0x28, 0xee, 0x6b, 0x27, 0x11, 0x24, 0xa5, 0xbf, 0xed, 0x1e, 0xc6, 0x5f, 0x7c,
            0xfc, 0x47, 0xae, 0x54, 0x2a, 0x4c, 0x68, 0x0b, 0xcd, 0xf3, 0x8d, 0xd6, 0x0f, 0x30, 0x9d, 0x08,
        };

        auto DataSize = sizeof(uint8_t)+sizeof(uint32_t)+sizeof(int64_t)+crypto_sign_ed25519_PUBLICKEYBYTES;
        auto BufferSize = DataSize+crypto_sign_BYTES;

        auto Message = std::make_unique<std::vector<unsigned char>>(BufferSize);
        std::fill(Message->begin(), Message->end(), 0);

        unsigned char User[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, User);
        auto MessageOffset = Message->data()+sizeof(uint8_t);
        std::memcpy(MessageOffset, User, sizeof(uint32_t));

        MessageOffset += sizeof(uint32_t);

        using clock = std::chrono::system_clock;
        clock::time_point Deadline = clock::now() + std::chrono::minutes(1);

        unsigned char TimeStamp[sizeof(std::time_t)];
        serialization::TimetToUChar(std::chrono::system_clock::to_time_t(Deadline), TimeStamp);

        std::memcpy(MessageOffset, TimeStamp, sizeof(std::time_t));
        MessageOffset += sizeof(std::time_t);

        std::memcpy(MessageOffset, PublicKey, crypto_sign_ed25519_PUBLICKEYBYTES);
        MessageOffset += crypto_sign_ed25519_PUBLICKEYBYTES;

        auto Signature = std::vector<unsigned char>(crypto_sign_BYTES);
        if (crypto_sign_ed25519_detached(Signature.data(), &OutputLen, Message->data(), DataSize, TestAuthServerPrivateKey.data()) != 0)
        {
            return nullptr;
        }
        OutputLen += DataSize;
        std::memcpy(MessageOffset, Signature.data(), crypto_sign_BYTES);
        return Message;
    }

    /**
     * @brief Fake authentication by signing a user id + timestamp + pubkey, returning a serialized GameMessage of type GenKeyRequest
     * 
     * @param UserId User's ID
     * @param PublicKey User's PublicKey for encrypting a symmetrical key
     * @param OutputLen Length of the serialized output GameMessage
     * 
     * @return std::unique_ptr<std::vector<unsigned char>> GameMessage of type GenKeyRequest
     */
    std::unique_ptr<std::vector<unsigned char>> FakeAuthServerFlat(uint32_t UserId, unsigned char *PublicKey, unsigned long long &OutputLen)
    {
        std::array<unsigned char, crypto_sign_ed25519_SECRETKEYBYTES> TestAuthServerPrivateKey = {
            0x4d, 0x11, 0xfa, 0x7c, 0x12, 0x1b, 0x89, 0xc2, 0x5b, 0x15, 0xb8, 0xe7, 0x72, 0xa9, 0xde, 0xc3,
            0x63, 0x5c, 0xe6, 0xed, 0x31, 0xaf, 0xef, 0x63, 0x70, 0x2d, 0xb3, 0x31, 0x48, 0x95, 0xb5, 0x7d,
            0x32, 0x0d, 0x38, 0x28, 0xee, 0x6b, 0x27, 0x11, 0x24, 0xa5, 0xbf, 0xed, 0x1e, 0xc6, 0x5f, 0x7c,
            0xfc, 0x47, 0xae, 0x54, 0x2a, 0x4c, 0x68, 0x0b, 0xcd, 0xf3, 0x8d, 0xd6, 0x0f, 0x30, 0x9d, 0x08,
        };
        
        unsigned char User[sizeof(uint32_t)];
        serialization::UInt32tToUChar(UserId, User);

        using clock = std::chrono::system_clock;
        std::time_t Deadline = std::chrono::system_clock::to_time_t(clock::now() + std::chrono::minutes(1));
        unsigned char TimeStamp[sizeof(std::time_t)];
        serialization::TimetToUChar(Deadline, TimeStamp);

        auto DataToSignSize = sizeof(uint32_t)+sizeof(std::time_t)+crypto_sign_ed25519_PUBLICKEYBYTES;
        auto ToSign = std::make_unique<std::vector<unsigned char>>(DataToSignSize);

        std::memcpy(ToSign->data(), User, sizeof(uint32_t));
        std::memcpy(ToSign->data()+sizeof(uint32_t), TimeStamp, sizeof(std::time_t));
        std::memcpy(ToSign->data()+sizeof(uint32_t)+sizeof(std::time_t), PublicKey, crypto_sign_ed25519_PUBLICKEYBYTES);

        auto Signature = std::vector<unsigned char>(crypto_sign_BYTES);
        if (crypto_sign_ed25519_detached(Signature.data(), &OutputLen, ToSign->data(), DataToSignSize, TestAuthServerPrivateKey.data()) != 0)
        {
            return nullptr;
        }

        flatbuffers::FlatBufferBuilder KeyResponseBuilder(512);

        auto PubKeyValue = KeyResponseBuilder.CreateVector(PublicKey, crypto_sign_ed25519_PUBLICKEYBYTES);
        auto SignatureValue = KeyResponseBuilder.CreateVector(Signature);
        auto GenKey = Game::Message::CreateGenKeyRequest(KeyResponseBuilder, Deadline, 32, 32, PubKeyValue, SignatureValue);


        Game::Message::MessageBuilder GameMessage(KeyResponseBuilder);
        GameMessage.add_user_id(UserId);
        GameMessage.add_channel_id(0);
        GameMessage.add_message_data_type(Game::Message::MessageData_GenKeyRequest);
        GameMessage.add_message_data(GenKey.Union());

        KeyResponseBuilder.Finish(GameMessage.Finish());

        auto Buffer = KeyResponseBuilder.GetBufferPointer();
        
        OutputLen = KeyResponseBuilder.GetSize();
        auto Output = std::make_unique<std::vector<unsigned char>>(OutputLen);
        std::memcpy(Output->data(), Buffer, OutputLen);
        KeyResponseBuilder.Release();

        return Output;
    }
}