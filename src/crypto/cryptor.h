#pragma once 

#include <optional>
#include <array>
#include <memory>
#include <sodium.h>
#include <mutex>

#include <sstream>
#include <iomanip>

#include "keypair.h"
#include "consts.h"
#include "types/pointers.h"
#include "logger/logger.h"


namespace crypto
{
    class Cryptor 
    {
    public:
        Cryptor(logging::Logger &Log);
        
        /**
         * @brief Helper to get xchacha20poly1305 keysize
         * 
         * @return uint32_t 
         */
        uint32_t KeySize() const;

        /**
         * @brief Return the size of the encryption key size (KEY+HMAC bytes)
         * 
         * @return uint32_t 
         */
        uint32_t EncryptKeySize() const;

        /**
         * @brief Size of our auth bytes
         * 
         * @return uint32_t 
         */
        uint32_t AuthByteSize() const;

        /**
         * @brief Generates a KeyPair of pub/private for handling key encrypting keys
         * 
         * @param Key 
         */
        void GenerateKey(std::array<uint8_t, PMO_KEY_SIZE> &Key) const;

        /**
         * @brief Generate a Key encrypting key.
         * 
         * @param KeyEncryptingKeys 
         * @return true 
         * @return false 
         */
        bool GenerateKeyEncryptingKey(KeyPair& KeyEncryptingKeys) const;

        /**
         * @brief Takes in a signed message and signed message length to verify that a user has been authorized by
         * our AuthServer.
         * 
         * @param SignedMessage 
         * @param SignedMessageLen 
         * @return true 
         * @return false 
         */
        bool VerifyUser(const unsigned char *SignedMessage, const unsigned long long SignedMessageLen) const;

        /**
         * @brief Verify a user by using the auth servers public key to verify it was a valid signed message coming from the client
         * 
         * @param UserId UserId to verify
         * @param TimeStamp When this message was signed so we can validate
         * @param Signature Signature that came from the authentication server
         * @param PublicKey Users public key
         * @return true 
         * @return false 
         */
        bool VerifyUser(const uint32_t UserId, const std::time_t TimeStamp, const unsigned char *Signature, const unsigned char *PublicKey) const;

        /**
         * @brief Encrypts the MessageBuffer of MessageLen size returning the Nonce value and a ptr to the CipherText.
         * 
         * @param UserId UserId to lookup the encryption key to encrypt with
         * @param UserKey UserKey used for encrypting
         * @param Nonce A buffer to store the nonce bytes for the encrypted message
         * @param MessageBuffer The serialized/buffer containing what we want to encrypt
         * @param CipherText A std::vector<uchar> of MessageLen + AuthByteSize(); 
         * @param MessageLen The length of the serialized/buffer
         * @return true if successfully encrypted to CipherText
         */
        bool Encrypt(const uint32_t UserId, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &UserKey, std::array<unsigned char, PMO_NONCE_BYTES> &Nonce, const unsigned char *MessageBuffer, std::vector<unsigned char> &CipherText, unsigned long long MessageLen);

        /**
         * @brief Decrypts a message intended for UserId, validates the UserId as part of the Authenticated Data (AD).
         * 
         * @param UserId used for validating the AD
         * @param UserKey used for decrypting
         * @param Nonce 
         * @param CipherText 
         * @param CipherTextLen 
         * @param MessageLen 
         * @param OutputLen 
         * @return true if successfully decrypted, false otherwise 
         */
        bool Decrypt(const uint32_t UserId, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &UserKey, const unsigned char *Nonce, const unsigned char *CipherText, const unsigned long long CipherTextLen, std::vector<unsigned char> &OutputBuffer, unsigned long long &OutputLen);
        
        /**
         * @brief Encrypts a generated key using a public key of one user, and a secret key of another.
         * 
         * @param Nonce 
         * @param UsersPublicKey 
         * @param GeneratedKey 
         * @param CipherText A std::vector<uchar> of EncryptKeySize() length.
         * @return bool true on success, false otherwise 
         */
        bool EncryptKey(std::array<unsigned char, crypto_box_NONCEBYTES> &Nonce, const unsigned char *UsersPublicKey, const std::array<uint8_t, crypto_aead_xchacha20poly1305_ietf_KEYBYTES> &GeneratedKey, std::vector<unsigned char> &CipherText) const;
        
        /**
         * @brief Decrypts a generated key using a public key of one user, and a secret key of another.
         * 
         * @param Nonce The nonce value
         * @param Message The message to decrypt
         * @param PublicKey The public key of user
         * @param PrivateKey Secret key of server
         * @param DecryptedKey The DecryptedKey of PMO_KEY_SIZE
         * @return true on success
         * @return false otherwise
         */
        bool DecryptKey(const unsigned char *Nonce, const unsigned char *Message, const unsigned char *PublicKey, const unsigned char *PrivateKey, std::array<uint8_t, PMO_KEY_SIZE> &DecryptedKey);

        /**
         * @brief Logs a buffer in hex for debugging purposes.
         * 
         * @tparam Iter 
         * @param Msg 
         * @param Begin 
         * @param End 
         * @param Logger 
         */
        template <typename Iter>
        void LogBuffer(std::string Msg, Iter Begin, Iter End, logging::Logger &Logger)
        {
            std::ostringstream Output;
            Output << std::hex << std::setw(2) << std::setfill('0');
            while(Begin != End)
            {
                Output << static_cast<unsigned>(*Begin++);
            }

            Logger.Info("{}: {}", Msg, Output.str());
        }
      
    public:
        std::array<unsigned char, crypto_box_PUBLICKEYBYTES> TestAuthServerPubKey = {
            0x32, 0x0d, 0x38, 0x28, 0xee, 0x6b, 0x27, 0x11, 0x24, 0xa5, 0xbf, 0xed, 0x1e, 0xc6, 0x5f, 0x7c,
            0xfc, 0x47, 0xae, 0x54, 0x2a, 0x4c, 0x68, 0x0b, 0xcd, 0xf3, 0x8d, 0xd6, 0x0f, 0x30, 0x9d, 0x08,
        };
        std::array<unsigned char, crypto_box_PUBLICKEYBYTES> TestGameServerPubKey = {
            0x61, 0xbb, 0x17, 0x68, 0x78, 0x8f, 0x2a, 0xb2, 0x79, 0x70, 0xb6, 0xa2, 0xfd, 0x49, 0x82, 0x34, 
            0xcf, 0xf6, 0xff, 0x9a, 0xc7, 0xa9, 0xb4, 0x0f, 0x90, 0x24, 0xd6, 0xa4, 0x3a, 0x78, 0x64, 0x2d,
        };
        
    private:


        std::array<unsigned char, crypto_box_SECRETKEYBYTES> TestGameServerPrivateKey = {
            0x91, 0xdd, 0x2b, 0xe7, 0xbb, 0x1a, 0x7c, 0x03, 0x77, 0x38, 0x62, 0xee, 0xde, 0xae, 0x99, 0xa1, 
            0x11, 0x14, 0x63, 0xa6, 0x47, 0xd2, 0xb2, 0x60, 0x33, 0x5b, 0xd7, 0x87, 0x25, 0xe0, 0xa2, 0xc2, 
        };

        // Disable copy / assignment constructors
        Cryptor (const Cryptor&) = delete;
        Cryptor& operator= (const Cryptor&) = delete;

        logging::Logger &Logger;
    };
}
