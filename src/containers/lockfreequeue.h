#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <atomic>
#include <optional>

template <class T, size_t N>
class LockFreeQueue 
{
    std::array<T, N> Buffer{};
    std::atomic<size_t> QueueSize{0};
    size_t ReadPos{0};
    size_t WritePos{0};
    static_assert(std::atomic<size_t>::is_always_lock_free);
    bool DoPush(T&& t)
    {
        if (QueueSize.load() == N)
        {
            return false;
        }
        Buffer[WritePos] = std::forward<decltype(t)>(t);

        WritePos = (WritePos + 1) % N;
        QueueSize.fetch_add(1);
        return true;
    }

    bool DoPushByValue(T t)
    {
        if (QueueSize.load() == N)
        {
            return false;
        }

        Buffer[WritePos] = t;
        WritePos = (WritePos + 1) % N;
        QueueSize.fetch_add(1);
        return true;
    }

public:
    ~LockFreeQueue()
    {
        for (auto Element = Pop(); Element != std::nullopt; Element = Pop()) { }
    }
    // Writer Thread
    bool Push(T&& t) { return DoPush(std::move(t)); };
    bool Push(const T& t) { return DoPush(t); };
    bool PushByValue(const T t) { return DoPushByValue(t); };

    // Reader Thread
    auto Pop() -> std::optional<T>
    {
        auto val = std::optional<T>{};
        if (QueueSize.load() > 0)
        {
            val = std::move(Buffer[ReadPos]);
            ReadPos = (ReadPos + 1) % N;
            QueueSize.fetch_sub(1);
        }
        return val;
    }
    // Both allowed to access (atomic)
    auto Size() const noexcept { return QueueSize.load(); }
};
