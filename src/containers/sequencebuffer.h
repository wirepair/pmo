#pragma once

#include <iostream>
#include <algorithm>
#include <vector>
#include <memory>

/** Taken/Translated to C++ from https://github.com/networkprotocol/reliable/ **/
template <class T>
class SequenceBuffer
{
public:
    /**
     * @brief Construct a new Sequence Buffer object
     * 
     * @param NumberOfEntries Number of entries for the buffer
     */
    SequenceBuffer(int NumberOfEntries) : NumEntries(NumberOfEntries)
    {
        Sequence = 0;
        EntrySequence.resize(NumberOfEntries);
        EntryData.resize(NumberOfEntries);
        
        std::fill(EntrySequence.begin(), EntrySequence.end(), 0xFFFFFFFF);
    };
    
    /**
     * @brief Destroy the Sequence Buffer object
     * 
     */
    ~SequenceBuffer()
    {
        EntryData.clear();
    }

    /**
     * @brief Reset's our buffer, setting our sequence ID back to 0,
     * Filling the buffer with 0xFFFFFFFF's to signal they are unused and
     * clearing our EntryData vector
     * 
     */
    void Reset()
    {
        Sequence = 0;
        std::fill(EntrySequence.begin(), EntrySequence.end(), 0xFFFFFFFF);
        EntryData.clear();
        EntryData.resize(NumEntries);
    }

    /**
     * @brief Removes entries from start to finish
     * 
     * @param StartSequenceId 
     * @param FinishSequenceId 
     * @param bCleanUp if true, delete the reference to the data between start/finish IDs
     */
    void RemoveEntries(const int StartSequenceId, int FinishSequenceId, const bool bCleanUp)
    {
        if (FinishSequenceId < StartSequenceId)
        {
            FinishSequenceId += 65536;
        }

        if (FinishSequenceId - StartSequenceId < NumEntries)
        {
            int CurrentSequenceId = 0;
            for ( CurrentSequenceId = StartSequenceId; CurrentSequenceId <= FinishSequenceId; ++CurrentSequenceId)
            {
                if (bCleanUp)
                {
                    EntryData.at(CurrentSequenceId % NumEntries) = nullptr;
                }
                EntrySequence.at(CurrentSequenceId % NumEntries) = 0xFFFFFFFF;
            }
        }
        else
        {
            for (int Index = 0; Index < NumEntries; ++Index)
            {
                if (bCleanUp)
                {
                    EntryData.at(Index) = nullptr;
                }
                EntrySequence.at(Index) = 0xFFFFFFFF;
            }
        }
    }

    /**
     * @brief Removes a specific sequence ID by setting it to 0xFF's
     * 
     * @param RemoveSequence 
     */
    void Remove(const uint16_t RemoveSequence)
    {
        EntrySequence.at(RemoveSequence % NumEntries) = 0xFFFFFFFF;
    }

    /**
     * @brief Removes a specific sequence ID and deletes it's reference to the entry
     * 
     * @param RemoveSequence 
     */
    void RemoveWithCleanup(const uint16_t RemoveSequence)
    {
        int Index = RemoveSequence % NumEntries;
        if (EntrySequence.at(Index) != 0xFFFFFFFF)
        {
            EntrySequence.at(Index) = 0xFFFFFFFF;
            EntryData.at(Index) = nullptr;
        }
    }

    /**
     * @brief Tests that we can insert at this sequence Id 
     * (e.g. it's less than our current sequence-number of entries)
     * 
     * @param InsertSequence 
     * @return int 
     */
    int TestInsert(const uint16_t InsertSequence)
    {
        auto Ret = SequenceLessThan(InsertSequence,  Sequence - ((uint16_t)NumEntries));
        if (Ret) 
        {
            return ((uint16_t)0);
        }
        return ((uint16_t)1);
    }

    /**
     * @brief Advance our sequence, removing the entry between our current sequence
     * and the new advanced sequence ID.
     * 
     * @param AdvanceSequence 
     */
    void Advance(const uint16_t AdvanceSequence)
    {
        if (SequenceGreaterThan(AdvanceSequence + 1, Sequence))
        {
            auto bCleanUp = false;
            RemoveEntries(Sequence, AdvanceSequence, bCleanUp);
            Sequence = AdvanceSequence + 1;
        }
    }

    /**
     * @brief Advance our sequence, removing the entry and data between our current sequence
     * and the new advanced sequence ID.
     * 
     * @param AdvanceSequence 
     */
    void AdvanceWithCleanup(const uint16_t AdvanceSequence)
    {
        if (SequenceGreaterThan(AdvanceSequence + 1, Sequence))
        {
            auto bCleanUp = true;
            RemoveEntries(Sequence, AdvanceSequence, bCleanUp);
            Sequence = AdvanceSequence + 1;
        }
    }

    /**
     * @brief Determines if the provided SequenceId is un-used
     * 
     * @param AvailableSequenceId 
     * @return int 
     */
    int Available(const uint16_t AvailableSequenceId)
    {
        return EntrySequence.at(AvailableSequenceId % NumEntries) == 0xFFFFFFFF;
    }

    /**
     * @brief Determines if the provided SequenceId is used.
     * 
     * @param ExistsSequenceId 
     * @return int 
     */
    int Exists(const uint16_t ExistsSequenceId)
    {
        return EntrySequence.at(ExistsSequenceId % NumEntries) == (uint32_t)ExistsSequenceId;
    }

    /**
     * @brief Inserts a new SequenceId, returning a pointer to the newly created value.
     * 
     * @param InsertSequenceId 
     * @return T*
     */
    T* Insert(const uint16_t InsertSequenceId)
    {
        if (SequenceLessThan(InsertSequenceId, Sequence - ((uint16_t)NumEntries)))
        {
            return nullptr;
        }

        if (SequenceGreaterThan(InsertSequenceId+1, Sequence))
        {
            auto bCleanUp = false;
            RemoveEntries(Sequence, InsertSequenceId, bCleanUp);
            Sequence = InsertSequenceId+1;
        }
        int Index = InsertSequenceId % NumEntries;
        EntrySequence.at(Index) = InsertSequenceId;

        // only create a new one if we've never done it before
        if (EntryData.at(Index) == nullptr)
        {
            EntryData.at(Index) = std::make_unique<T>();
        }
        return EntryData.at(Index).get();
    };

    /**
     * @brief Inserts a new SequenceId, cleans out the current data and returns the
     * new value
     * 
     * @param InsertSequenceId 
     * @return T*
     */
    T* InsertWithCleanup(const uint16_t InsertSequenceId)
    {
        if (SequenceGreaterThan(InsertSequenceId+1, Sequence))
        {
            auto bCleanUp = true;
            RemoveEntries(Sequence, InsertSequenceId, bCleanUp);
            Sequence = InsertSequenceId + 1;
        }
        else if (SequenceLessThan(InsertSequenceId, Sequence -((uint16_t)NumEntries)))
        {
            return nullptr;
        }

        int Index = InsertSequenceId % NumEntries;
        if (EntrySequence.at(Index) != 0xFFFFFFFF)
        {
            EntryData[InsertSequenceId % NumEntries] = nullptr;
        }
        EntrySequence.at(Index) = InsertSequenceId;
        EntryData.at(Index) = std::make_unique<T>();
        return EntryData.at(Index).get();
    };

    /**
     * @brief Finds and returns the data by SequenceId, or returns nullptr.
     * 
     * @param FindSequenceId 
     * @return T*
     */
    T* Find(const uint16_t FindSequenceId)
    {
        int Index = FindSequenceId % NumEntries;
    
        auto Exists = (uint32_t)EntrySequence.at(Index) == (uint32_t) FindSequenceId;
        if (Exists)
        {
            return EntryData.at(Index).get();
        }

        return nullptr;
    };

    /**
     * @brief Returns the Data at the provided ID.
     * 
     * @param AtIndexId 
     * @return T*
     */
    T* AtIndex(const uint16_t AtIndexId)
    {
        auto Exists = EntrySequence.at(AtIndexId) != 0xFFFFFFFF;
        if (Exists)
        {
            return EntryData.at(AtIndexId).get();
        }
        
        return nullptr;
    };

    /**
     * @brief Generates bits for determining acknowledgements by setting a bit
     * for each sequenceId that exists in our buffer, if it doesn't we set that
     * bit to a 0. This allows us to ack up to 32 messages from our current
     * Sequence.
     * 
     * @param Ack 
     * @param AckBits 
     */
    void GenerateAckBits(uint16_t &Ack, uint32_t &AckBits)
    {
        Ack = Sequence - 1;
        AckBits = 0;
        uint32_t Mask = 1;
        int i = 0;
        for (i = 0; i < 32; ++i)
        {
            uint16_t SequenceId = Ack - ((uint16_t)i);
            if (Exists(SequenceId))
            {
                AckBits |= Mask;
            }
            Mask <<= 1;
        }
    }

    /**
     * @brief Get the Sequence object
     * 
     * @return uint16_t 
     */
    uint16_t GetSequence() { return Sequence; };

    /**
     * @brief Returns true if Sequence1 > Sequence2
     * 
     * @param Sequence1 
     * @param Sequence2 
     * @return int 
     */
    int SequenceGreaterThan(const uint16_t Sequence1, const uint16_t Sequence2)
    {
        return (( Sequence1 > Sequence2 ) && ( Sequence1 - Sequence2 <= 32768 )) || 
               (( Sequence1 < Sequence2 ) && ( Sequence2 - Sequence1  > 32768 ));
    }

    /**
     * @brief Returns true if Sequence1 < Sequence2
     * 
     * @param Sequence1 
     * @param Sequence2 
     * @return int 
     */
    int SequenceLessThan(const uint16_t Sequence1, const uint16_t Sequence2)
    {
        return SequenceGreaterThan(Sequence2, Sequence1);
    }

    /**
     * @brief Return a pointer to our data
     * 
     */
    std::vector<std::unique_ptr<T>>* GetEntries() { return &EntryData; }

private:
    int NumEntries = 0;
    uint16_t Sequence{0};
    std::vector<uint32_t> EntrySequence;
    std::vector<std::unique_ptr<T>> EntryData;
};
