#pragma once

#include <iostream>
#include <span>
#include <memory>
#include <ranges>

template <class T, size_t N>
class HistoryBuffer 
{
public:
    HistoryBuffer() : Idx(0) {}

    /**
     * @brief Construct a new History Buffer object from compressed input
     * 
     * @param Values 
     * @param CompressionKey 
     */
    HistoryBuffer(std::span<T> &Values, const uint32_t CompressionKey) : Idx(N-1)
    {
        LoadCompressed(Values, CompressionKey);
    }

    HistoryBuffer& operator=(HistoryBuffer&& other)
    {
        return *this;
    }

    HistoryBuffer(HistoryBuffer&& other) noexcept
    {
        Buffer = other.Buffer;
        Idx = other.Idx;
    }

    /**
     * @brief Construct this object with the input Values and a CompressionKey.
     * Note this will fill the entire buffer up to N elements, meaning we expect
     * a full range of values. 
     * 
     * @param Values A vector of values, in order of the CompressionKey
     * @param CompressionKey A bitmap of unique and repeating value indexes 
     * 
     */
    template<typename Iter>
    bool LoadCompressed(Iter Start, Iter End, const uint32_t CompressionKey)
    {
        auto Pos = N-1; // start writing from end of buffer
        auto Value = Start;
        Buffer.at(Pos) = *Value; // always write first value
        Pos--;
        for (size_t i = 1; i < static_cast<size_t>(N); i++)
        {
            if ( (CompressionKey & (1<<i)) ) 
            {
                Value++;
            }

            // Protect against possible out of bounds access
            if (Value == End)
            {
                Idx = 0;
                return false;
            }

            Buffer.at(Pos) = *Value;

            Pos = (Pos == 0) ? N - 1 : Pos - 1; // wrap around if Pos == 0
        }

        Idx = N-1;
        return true;
    }

    /**
     * @brief Push a value into our history, wrapping around if full
     * and tracking size until it's full
     * 
     * @param Value 
     */
    void Push(const T Value)
    {
        Idx = (Idx+1) % N;
        Buffer.at(Idx) = Value;
    }

    /**
     * @brief Get the history starting from index and reversed so it's Last-In-First-Out (LIFO)
     * LIFO helps with replaying the values.
     * 
     * @param OutHistory An array of N elements of our entire buffer history in order.
     */
    void Get(std::array<T, N> &OutHistory)
    {
        // ensure position is correct even if idx is zero
        size_t Pos = Idx;

        for (size_t i = 0; i < N; i++)
        {
            OutHistory.at(i) = Buffer.at(Pos);
            if (Pos == 0)
            {
                Pos = N;
            }
            Pos--;
        }
    }

    /**
     * @brief Our current write index
     * 
     * @return size_t Current index we are writing at
     */
    size_t Index() { return Idx; }

    /**
     * @brief Writes unique key presses to OutHistory, where repeating key presses are mapped to 
     * the CompressionKey, where the index of the unique value is a 1 and the repeating keys are 0
     * 
     */
    void CompressBuffer(std::vector<T> &OutHistory, uint32_t &CompressionKey)
    {
        CompressionKey = 1;

        size_t Pos = Idx; 
        auto Prev = Pos == 0 ? N-1 : Pos-1;

        // always add first element
        OutHistory.emplace_back(Buffer.at(Pos));

        for (size_t i = 0; i < static_cast<size_t>(N)-1; i++)
        {
            if (Buffer.at(Pos) != Buffer.at(Prev))
            {
                CompressionKey |= (1 << (i+1)); // mark this position in our bitmap
                OutHistory.emplace_back(Buffer.at(Prev)); // add this value that was transitioned to
            }
            
            // wrap around Pos, if Pos is zero
            Pos = (Pos == 0) ? N-1 : Pos - 1;
            // wrap around Prev, if Pos was _just_ set to 0
            Prev = (Pos == 0) ? N-1 : Pos - 1; 
        }
    }

    /**
     * @brief Wraps the arrays built in iterator
     * 
     * @return std::array<T, N>::iterator 
     */
    typename std::array<T, N>::iterator begin()
    {
        return Buffer.begin();
    } 
    
    /**
     * @brief Wraps the arrays built in iterator
     * 
     * @return std::array<T, N>::iterator 
     */
    typename std::array<T, N>::iterator end()
    {
        return Buffer.end();
    } 


private:
    std::array<T, N> Buffer{};
    size_t Idx = 0;
};