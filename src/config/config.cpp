#include "config.h"

namespace config
{
    Config::Config(const char *InLogFile, const char *InAssetPath, const char *InMapName, const char *InServerAddr, int InServerPort, debug::DebugRenderer *InRenderer)
    {
        init(InLogFile, InAssetPath, InMapName, InServerAddr, InServerPort, InRenderer);
    }

    Config::Config(toml::v3::ex::parse_result &Parsed)
    {
        init(
            Parsed["log"]["name"].value_or("pmo.log"),
            Parsed["assets"]["asset_path"].value_or(""),
            Parsed["assets"]["map_name"].value_or(""),
            Parsed["server"]["addr"].value_or("0.0.0.0"),
            Parsed["server"]["port"].value_or(4040),
            nullptr
        );
    }

    void Config::init(const char *InLogFile, const char *InAssetPath, const char *InMapName, const char *InServerAddr, int InServerPort, debug::DebugRenderer *InRenderer)
    {
        LogFile = InLogFile;
        AssetPath = InAssetPath;
        MapName = InMapName;
        ServerAddr = InServerAddr;

        ServerPort = InServerPort;
        Renderer = InRenderer;
    }

    fs::path Config::GetMapPath()
    {
        auto Asset = fs::path(AssetPath) / fs::path("maps");
        return Asset;
    }

    const char *Config::GetAssetPath()
    {
        return AssetPath;
    }

    fs::path Config::GetEquipmentPath()
    {
        auto Asset = fs::path(AssetPath) / fs::path("equipment");
        return Asset;
    }

    fs::path Config::GetAnimPath()
    {
        auto Asset = fs::path(AssetPath) / fs::path("animations");
        return Asset;
    }

    const char * Config::GetMapName()
    {
        return MapName;
    }

    const char * Config::GetLogFile()
    {
        return LogFile;
    }

    const char * Config::GetServerAddr()
    {
        return ServerAddr;
    }

    int Config::GetServerPort()
    {
        return ServerPort;
    }

    void Config::SetDebugRenderer(debug::DebugRenderer *DebugRenderer)
    {
        Renderer = DebugRenderer;
    }

    debug::DebugRenderer *Config::GetDebugRenderer()
    {
        return Renderer;
    }
}