#pragma once

#include <filesystem>
#include <iostream>
#include <string>

#include "parsers/toml.hpp"
#include "debugrender/debugrender.h"

namespace fs = std::filesystem;

namespace config
{
    struct ConfigData
    {
        const char *LogFile;
        const char *AssetPath;
        const char *MapName;
        const char *ServerAddr;
        int ServerPort;
        
        debug::DebugRenderer *Renderer;

    };

    class Config
    {
    public:
        Config() {};
        
        /**
         * @brief Construct a new Config object. Does not make copies, caller is responsible for ensuring the pointers stay alive.
         * 
         * @param LogFile 
         * @param AssetPath 
         * @param MapName 
         * @param ServerAddr 
         * @param ServerPort 
         * @param Renderer 
         */
        Config(const char *LogFile, const char *AssetPath, const char *MapName, const char *ServerAddr, int ServerPort, debug::DebugRenderer *Renderer);
        
        Config(toml::v3::ex::parse_result &Parsed);

        const char * GetLogFile();

        /**
         * @brief Path to the (single) map we are going to load. If the optimized
         * physics shape binary exists, we will load it directly. If only an fbx
         * is detected, we'll translate it to the optimized physics shape
         * 
         * @return fs::path 
         */
        fs::path GetMapPath();

        /**
         * @brief Path to our equipment
         * 
         * @return fs::path
         */
        fs::path GetEquipmentPath();

        /**
         * @brief Path to our animation data
         * 
         * @return fs::path
         */
        fs::path GetAnimPath();

        /**
         * @brief Return path to assets
         * 
         * @return const char* 
         */
        const char * GetAssetPath();

        /**
         * @brief Name of the map (must match the fbx file and serialized name minus extension)
         * 
         * @return const char *
         */
        const char * GetMapName();

        const char * GetServerAddr();

        int GetServerPort();

        void SetDebugRenderer(debug::DebugRenderer *Renderer);

        debug::DebugRenderer *GetDebugRenderer();
      
    private:
        void init(const char *LogFile, const char *AssetPath, const char *MapName, const char *ServerAddr, int ServerPort, debug::DebugRenderer *Renderer);
        
        const char * LogFile;
        const char * AssetPath;
        const char * MapName;
        const char * ServerAddr;
        int ServerPort;
        
        debug::DebugRenderer *Renderer;
    };

}