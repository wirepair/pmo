#pragma once

#include <memory>
#include <cmath>

namespace serialization
{
    /**
     * @brief Clips a value between min/max
     * 
     * @tparam T 
     * @param N 
     * @param Lower 
     * @param Upper 
     * @return T 
     */
    template <typename T>
    T Clip(const T& N, const T& Lower, const T& Upper) 
    {
        return std::max(Lower, std::min(N, Upper));
    }
    
    /**
     * @brief Takes in a float value of x, scale of s, and z (zero), along with min/max values
     *  to quantize it down to a uint8_t.
     * @param X 
     * @param S 
     * @param Z 
     * @param Min 
     * @param Max 
     * @return uint8_t 
     */
    inline static uint8_t Quantization(float X, float S, float Z, float Min, float Max)
    {
        float X_Q = round(1 / S * X + Z);
        return Clip(X_Q, Min, Max);
    }
    
    /**
     * @brief Takes in a quantized uint8_t and scales it back to a float.
     * 
     * @param X_Q 
     * @param S 
     * @param Z 
     * @return float 
     */
    inline static float Dequantization(uint8_t X_Q, float S, float Z)
    {
        return S * (X_Q - Z);
    }

    inline static uint64_t TimeNow()
    {
        using clock = std::chrono::system_clock;
        clock::time_point Deadline = clock::now() + std::chrono::minutes(1);

        return std::chrono::system_clock::to_time_t(Deadline);
    }

    /**
     * @brief Converts an unsigned int64 to a character array
     * 
     * @param Input 
     * @param Out
     */
    inline static void UInt64tToUChar(const uint64_t Input, unsigned char *Out)
    {
        Out[7] = (Input >> 56); 
        Out[6] = (Input >> 48);
        Out[5] = (Input >> 40);
        Out[4] = (Input >> 32);
        Out[3] = (Input >> 24);
        Out[2] = (Input >> 16);
        Out[1] = (Input >> 8);
        Out[0] = (Input);
    }

        /**
     * @brief Converts an std::time_t to a character array
     * 
     * @param Input the std:time_t Value
     * @param Out The UChar output
     */
    inline static void TimetToUChar(const std::time_t Input, unsigned char *Out)
    {
        Out[7] = (Input >> 56); 
        Out[6] = (Input >> 48);
        Out[5] = (Input >> 40);
        Out[4] = (Input >> 32);
        Out[3] = (Input >> 24);
        Out[2] = (Input >> 16);
        Out[1] = (Input >> 8);
        Out[0] = (Input);
    }

    /**
     * @brief Converts our character array to our input, assumes caller knows the input is 8 bytes
     * 
     * @param Input an 8 byte character array
     * @return uint64_t 
     */
    inline static uint64_t UCharToUInt64t(const unsigned char *Input)
    {
        uint64_t Out = 0;
        Out = (((uint64_t)Input[7] << 56) & 0xFF00000000000000) |
              (((uint64_t)Input[6] << 48) & 0x00FF000000000000) |
              (((uint64_t)Input[5] << 40) & 0x0000FF0000000000) |
              (((uint64_t)Input[4] << 32) & 0x000000FF00000000) |
              (((uint64_t)Input[3] << 24) & 0x00000000FF000000) |
              (((uint64_t)Input[2] << 16) & 0x0000000000FF0000) |
              (((uint64_t)Input[1] <<  8) & 0x000000000000FF00) |
              ((uint64_t)Input[0]         & 0x00000000000000FF);
        return Out;
    }

     /**
     * @brief Converts our character array to std::time_t, assumes caller knows the input is 8 bytes
     * 
     * @param Input an 8 byte character array
     * @return std::time_t 
     */
    inline static std::time_t UCharToTimeT(const unsigned char *Input)
    {
        std::time_t Out = 0;
        Out = (((std::time_t)Input[7] << 56) & 0xFF00000000000000) |
              (((std::time_t)Input[6] << 48) & 0x00FF000000000000) |
              (((std::time_t)Input[5] << 40) & 0x0000FF0000000000) |
              (((std::time_t)Input[4] << 32) & 0x000000FF00000000) |
              (((std::time_t)Input[3] << 24) & 0x00000000FF000000) |
              (((std::time_t)Input[2] << 16) & 0x0000000000FF0000) |
              (((std::time_t)Input[1] <<  8) & 0x000000000000FF00) |
              ((std::time_t)Input[0]         & 0x00000000000000FF);
        return Out;
    }

    
     /**
     * @brief Converts our character array to uint32_t, assumes caller knows the input is 4 bytes
     * 
     * @param Input an 8 byte character array
     * @return uint32_t 
     */
    inline static uint32_t UCharToUint32t(const unsigned char *Input)
    {
        uint32_t Out = 0;
        Out = (((uint32_t)Input[3] << 24) & 0xFF000000) |
              (((uint32_t)Input[2] << 16) & 0x00FF0000) |
              (((uint32_t)Input[1] <<  8) & 0x0000FF00) |
              ((uint32_t)Input[0]         & 0x000000FF);
        return Out;
    }

    /**
     * @brief Converts an unsigned int32 to a character array 
     * 
     * @param Input The UInt32
     * @param Out The UChar
     */
    inline static void UInt32tToUChar(const uint32_t Input, unsigned char *Out)
    {
        Out[3] = (Input >> 24);
        Out[2] = (Input >> 16);
        Out[1] = (Input >> 8);
        Out[0] = (Input);
    }
}